
<!doctype html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.io.*,java.util.*" %>
<%@ page import="javax.servlet.*, javax.servlet.http.*" %>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
        <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/libs/css/style.css">
        <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="assets/vendor/charts/chartist-bundle/chartist.css">
        <link rel="stylesheet" href="assets/vendor/charts/morris-bundle/morris.css">
        <link rel="stylesheet" href="assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="assets/vendor/charts/c3charts/c3.css">
        <link rel="stylesheet" href="assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <!------ Include the above in your HEAD tag ---------->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
        <title>Concept - Bootstrap 4 Admin Dashboard Template</title>

        <style>
            .row {
                margin-bottom: 15px; /* ?i?u ch?nh margin d??i c?a div row */
            }
            .centered-button {
                display: flex;
                justify-content: center;
                margin-top: 20px; /* C� th? ?i?u ch?nh kho?ng c�ch gi?a n�t v� h�ng tr�n */
            }

            .update-button {
                background-color: #007bff; /* M�u n?n */
                color: #ffffff; /* M�u ch? */
                border: none; /* Lo?i b? ???ng vi?n */
                padding: 10px 20px; /* K�ch th??c l? */
                border-radius: 5px; /* Bo g�c */
                cursor: pointer; /* ??i con tr? th�nh con tr? tay khi di chu?t v�o */
                transition: background-color 0.3s ease; /* Hi?u ?ng chuy?n ??i m�u n?n */
            }

            .update-button:hover {
                background-color: #0056b3; /* M�u n?n khi di chu?t v�o */
            }
            .col-sm-9 {
                margin-top: 10px; /* ?i?u ch?nh margin tr�n c?a div col-sm-9 */
            }
            .increase-font {
                font-size: 18px; /* ??t k�ch th??c font ch? mong mu?n */
            }
            .active-icon {
                width: 25px;
                filter: drop-shadow(0 0 5px #007bff); /* �p d?ng hi?u ?ng drop shadow m�u xanh */
            }
            body {
                background-color: #f8f9fa;
            }
            .breadcrumb {
                background-color: #f8f9fa !important;
            }
            .card {
                border: 1px solid #d1d3e2;
                border-radius: 10px;
            }
            .increase-font {
                font-size: 18px;
            }
            .row hr {
                border-top: 1px solid #d1d3e2;
            }
            .font-weight-bold {
                font-weight: bold;
            }
            .input-like-p {
                background: transparent;
                border: none;
                outline: none;
                padding: 0;
                font-family: inherit;
                font-size: inherit;
                cursor: text;
            }
        </style>

    </style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg bg-white fixed-top">
                <a class="navbar-brand" href="index.html">Concept</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

            </nav>
        </div>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">
                                Menu
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="admindashboard" >
                                    <i class="fa fa-fw fa-user-circle"></i>
                                    Dashboard
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="usermanager" >
                                    <i class="fas fa-fw fa-chart-pie"></i>
                                    User Manager
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link active" href="settinglist">
                                    <i class="fas fa-fw fa-chart-pie"></i>
                                    Setting List
                                </a>
                            </li>
                    </div>
                </nav>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content">
                    <fieldset>
                        <!-- Form Name -->
                        <legend>Setting Information</legend>
                        <section>
                            <div class="container py-5">
                                <div class="row">
                                    <div class="col">
                                         <nav aria-label="breadcrumb" class="bg-light rounded-3 p-3 mb-4">
                                                <ol class="breadcrumb mb-0">
                                                    <li class="breadcrumb-item"><a href="settinglist">Setting List</a></li>
                                                    <li class="breadcrumb-item active" aria-current="page">Edit Setting</li>
                                                </ol>
                                            </nav>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-12">
                                        <div class="card mb-4">
                                            <form action="editsetting" method="post">                                               
                                                <div class="card-body increase-font">
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <p class="mb-0">Setting Type</p>
                                                        </div>
                                                        <div class="col-sm-9 input-like-p">
                                                            <select name="type" required="" class="input-like-p">
                                                                <option value="" disabled selected hidden>Type</option>
                                                                <c:forEach items="${listsc}" var="sc">
                                                                    <option value="${sc.sc_id}" ${sc.name == s.type_name ? "selected" : ""}>${sc.name}</option>                                                          
                                                                </c:forEach>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class="row">
                                                         <input type="hidden" name="id" value="${s.id}" >
                                                        <div class="col-sm-3">
                                                            <p class="mb-0">Setting Value</p>
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <input class="input-like-p" type="text" value="${s.value}" name="value" required=""/>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                <p class="mb-0">Setting Order</p>
                                                            </div>
                                                            <div class="col-sm-9">
                                                                <input class="input-like-p" type="text" value="${s.order}" name="order" required=""/>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                <p class="mb-0">Setting Status</p>
                                                            </div>
                                                            <div class="col-sm-9 flex">
                                                                <select name="status" class="input-like-p">
                                                                    <option value="Active" ${s.status == Active ? "selected" : ""}>Active</option>
                                                                    <option value="Inactive" ${s.status == Inactive ? "selected" : ""}>Inactive</option>                                                           
                                                                </select>

                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row centered-button">
                                                            <div class="col-sm-9">
                                                                <input type="submit" class="update-button" value="Update">
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                        </section>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>  

    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    Copyright � 2018 Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="text-md-right footer-links d-none d-sm-block">
                        <a href="javascript: void(0);">About</a>
                        <a href="javascript: void(0);">Support</a>
                        <a href="javascript: void(0);">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
<!-- bootstap bundle js -->
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
<!-- slimscroll js -->
<script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
<!-- main js -->
<script src="assets/libs/js/main-js.js"></script>
<!-- chart chartist js -->
<script src="assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
<!-- sparkline js -->
<script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
<!-- morris js -->
<script src="assets/vendor/charts/morris-bundle/raphael.min.js"></script>
<script src="assets/vendor/charts/morris-bundle/morris.js"></script>
<!-- chart c3 js -->
<script src="assets/vendor/charts/c3charts/c3.min.js"></script>
<script src="assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
<script src="assets/vendor/charts/c3charts/C3chartjs.js"></script>
<script src="assets/libs/js/dashboard-ecommerce.js"></script>



</body>

</html>