
<!doctype html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
        <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/libs/css/style.css">
        <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="assets/vendor/charts/chartist-bundle/chartist.css">
        <link rel="stylesheet" href="assets/vendor/charts/morris-bundle/morris.css">
        <link rel="stylesheet" href="assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="assets/vendor/charts/c3charts/c3.css">
        <link rel="stylesheet" href="assets/vendor/fonts/flag-icon-css/flag-icon.min.css">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
        <title>Concept - Bootstrap 4 Admin Dashboard Template</title>

        <style>
            .error-message {
                text-align: center; /* c?n gi?a n?i dung */
                font-weight: bold; /* l�m n?i b?t */
                color: red; /* m�u s?c */
                font-size: larger;
            }

            .filter {
                display: flex;
                align-items: center;
                margin-bottom: 10px;
            }
            .filter1 {
                display: flex;
                align-items: center;
            }

            .texta {
                margin-right: 10px;
                font-weight: bold;
                font-size: 18px;

            }

            select {
                padding: 8px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            /* T�y ch?nh c�c select n?m trong .filter */
            .filter select {
                margin-right: 10px;
            }

            /* T�y ch?nh m�u n?n khi hover tr�n select */
            .filter select:hover {
                background-color: #f5f5f5;
            }
            select:hover {
                background-color: #f5f5f5;
            }

            input[type="submit"] {
                padding: 10px 15px;
                background-color: #4CAF50;
                color: white;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            /* Hi?u ?ng hover cho n�t Filter */
            input[type="submit"]:hover {
                background-color: #45a049;
            }

            .search-p{
                justify-content: right;
            }

        </style>
    </head>

    <body>
        <!-- ============================================================== -->
        <!-- main wrapper -->
        <!-- ============================================================== -->
        <div class="dashboard-main-wrapper">
            <!-- ============================================================== -->
            <!-- navbar -->
            <!-- ============================================================== -->
            <div class="dashboard-header">
                <nav class="navbar navbar-expand-lg bg-white fixed-top">
                    <a class="navbar-brand" href="home">Concept</a>
                </nav>
            </div>
            <!-- ============================================================== -->
            <!-- end navbar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- left sidebar -->
            <!-- ============================================================== -->
            <div class="nav-left-sidebar sidebar-dark">
                <div class="menu-list">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                          <ul class="navbar-nav flex-column">
                                <li class="nav-divider">
                                    Menu
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="dashboardmkt" onclick="setActive(this)">
                                        <i class="fa fa-fw fa-user-circle"></i>
                                        Dashboard
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="productmanager" onclick="setActive(this)">
                                        <i class="fas fa-fw fa-chart-pie"></i>
                                        Product Manager
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="customerlist" onclick="setActive(this)">
                                        <i class="fas fa-fw fa-chart-pie"></i>
                                        Customers List
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="blogmanager" onclick="setActive(this)">
                                        <i class="fas fa-fw fa-chart-pie"></i>
                                        Blog Manager
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="sliderlist" onclick="setActive(this)">
                                        <i class="fas fa-fw fa-chart-pie"></i>
                                        Slider List
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link  " href="feedbacklist" onclick="setActive(this)">
                                        <i class="fas fa-fw fa-chart-pie"></i>
                                        Feedback List
                                    </a>
                                </li>
                            </ul>

                        </div>
                    </nav>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end left sidebar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- wrapper  -->
            <!-- ============================================================== -->
            <div class="dashboard-wrapper">
                <div class="dashboard-ecommerce">
                    <div class="container-fluid dashboard-content ">
                        <!-- ============================================================== -->
                        <!-- pageheader  -->
                        <!-- ============================================================== -->
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="page-header">
                                    <h2 class="pageheader-title">Blog List</h2>
                                    <div class="page-breadcrumb">
                                        <nav aria-label="breadcrumb">
                                            <ol class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="dashboardmkt" class="breadcrumb-link">Dashboard</a></li>
                                                <li class="breadcrumb-item active" aria-current="page">Blog List</li>
                                            </ol>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end pageheader  -->
                        <!-- ============================================================== -->
                        <div class="ecommerce-widget">

                            <div class="row">
                                <!-- ============================================================== -->

                                <!-- ============================================================== -->

                                <!-- recent orders  -->
                                <!-- ============================================================== -->
                                <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                    <div class="filter1">
                                        <form action="filterblogmanager">
                                            <div class="filter">
                                                <a class="texta" style=" color: #454444f7;" >Filter:</a>
                                                <div>
                                                    <select name="blog_categories">
                                                        <option>Categories</option>
                                                        <c:forEach items="${listBC}" var="bc">
                                                            <option value="${bc.bc_id}" ${(tagc==bc.bc_id)?"selected":""}>${bc.bc_name}</option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                                <div>
                                                    <select name="author">
                                                        <option>Author</option>
                                                        <c:forEach items="${listA}" var="a">
                                                            <option value="${a}" ${(author==a)?"selected":""}>${a}</option>
                                                        </c:forEach>
                                                    </select>   
                                                </div>
                                                <div>
                                                    <select name="status">
                                                        <option>Status</option>
                                                        <c:forEach items="${listS}" var="s">
                                                            <option value="${s.sid}" ${(tags==s.sid)?"selected":""}>${s.status}</option>                  
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                                <input type="submit" value="Filter"/>                                              
                                            </div>
                                        </form>
                                        <div style="margin-left: 15px;">
                                            <a href="addblog"><button type="button" class="btn btn-danger">Add Blog</button> </a>
                                        </div>
                                        <div class="input-group rounded search-p">
                                            <form action="searchblogmanager">
                                                <input type="search" name="search" class="form-control rounded" placeholder="Search" value="${search}" aria-label="Search" aria-describedby="search-addon" style="max-width: 95%" />

                                            </form>
                                            <span class="input-group-text border-0" id="search-addon">
                                                <i class="fas fa-search"></i>
                                            </span>
                                        </div> 
                                    </div>

                                    <div class="card">
                                        <div class="card-body p-0">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead class="bg-light">
                                                        <tr class="border-0">
                                                            <th class="border-0">Id</th>
                                                            <th class="border-0">Thumbnail</th>
                                                            <th class="border-0">Title</th>    
                                                            <th class="border-0">Author</th>
                                                            <th class="border-0">Category</th>                                                         
                                                            <th class="border-0">Status</th>
                                                            <th class="border-0">Feature</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <c:forEach items="${listB}" var="b" varStatus="loop">
                                                            <tr>
                                                                <td>${b.blog_id} </td>
                                                                <td>
                                                                    <div class="m-r-10"><img src="${b.thumbnail}" alt="user" class="rounded" width="100px"></div>
                                                                </td>
                                                                <td style="width: 40%">${b.title} </td>
                                                                <td>${b.author}</td>
                                                                <td>${b.blog_categories_name}</td>

                                                                <td>
                                                                    <c:choose>
                                                                        <c:when test="${b.status_name eq 'Available'}">
                                                                            <span style="background-color: #1ae91a;" class="badge-dot badge-brand mr-1"></span>${b.status_name}
                                                                        </c:when>
                                                                        <c:otherwise>  
                                                                            <span style="background-color: #ff0000;" class="badge-dot badge-brand mr-1"></span>${b.status_name}
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </td>

                                                                <td><div class="dropdown">
                                                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                            More
                                                                        </button>
                                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">                                                                                                                                                                  
                                                                            <a class="dropdown-item"  href="editblog?blog_id=${b.blog_id}">Edit</a>
                                                                            <a class="dropdown-item"  href="deleteblog?blog_id=${b.blog_id}">Delete</a>
                                                                        </div>
                                                                    </div></td>

                                                            </tr> 
                                                        </c:forEach>                                      
                                                        <tr>
                                                            <c:if test="${not empty error}">
                                                            <tr>
                                                                <td colspan="9" class="error-message" style="color: red">${error}</td>
                                                            </tr>
                                                        </c:if>
                                                        </tr>
                                                    </tbody>

                                                </table>

                                                <%-- Ph�n trang cho blog--%>
                                                <nav aria-label="Page navigation example">
                                                    <ul class="pagination justify-content-end">

                                                        <c:forEach begin="1" end="${endp}" var="i">
                                                            <li class="page-item ${i == currentPage ? "active" : ''}"><a class="page-link" href="blogmanager?index=${i}">${i}</a></li>
                                                            </c:forEach>

                                                    </ul>                                                   
                                                </nav>
                                                <%-- Ph�n trang cho blog availible--%>
                                                <nav aria-label="Page navigation example">
                                                    <ul class="pagination justify-content-end">
                                                        <c:forEach begin="1" end="${endps}" var="s">
                                                            <li class="page-item ${s == currentPage1 ? "active" : ''}"><a class="page-link" href="filterblogmanager?blog_categories=${bc_id}&&status=${tags}&&author=${author}&&indexS=${s}">${s}</a></li>
                                                            </c:forEach>    
                                                    </ul>  
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ============================================================== -->
                                <!-- end recent orders  -->


                                <!-- ============================================================== -->
                                <!-- ============================================================== -->
                                <!-- customer acquistion  -->
                                <!-- ============================================================== -->
                                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">

                                </div>
                                <!-- ============================================================== -->
                                <!-- end customer acquistion  -->
                                <!-- ============================================================== -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            Copyright � 2018 Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="text-md-right footer-links d-none d-sm-block">
                                <a href="javascript: void(0);">About</a>
                                <a href="javascript: void(0);">Support</a>
                                <a href="javascript: void(0);">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="assets/libs/js/dashboard-ecommerce.js"></script>






</body>

</html>