<!doctype html>
<html lang="en">
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ page import="java.util.List" %>
    <%@ page import="model.Order" %>
    <%@ page import="model.SaleTotal" %>
       <%@page contentType="text/html" pageEncoding="UTF-8"%>
    <head>c
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
        <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/libs/css/style.css">
        <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="assets/vendor/vector-map/jqvmap.css">
        <link rel="stylesheet" href="assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css">
        <link rel="stylesheet" href="assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
        <title>Concept - Bootstrap 4 Admin Dashboard Template</title>
        <style>
            h4 {
                display: flex;
                align-items: center;
                justify-content: right;
            }

            input {
                padding: 8px;
                border: 1px solid #ccc;
                border-radius: 3px;
                outline: none;

            }

            a {
                color: #fff;
                text-decoration: none;
                margin: 0 10px;
            }
        </style>
    </head>

    <body>
        <!-- ============================================================== -->
        <!-- main wrapper -->
        <!-- ============================================================== -->
        <div class="dashboard-main-wrapper" id="dataContainer"  >
            <!-- ============================================================== -->
            <!-- navbar -->
            <!-- ============================================================== -->
            <div class="dashboard-header">
                <nav class="navbar navbar-expand-lg bg-white fixed-top">
                    <a class="navbar-brand" href="home">GenzFashion</a>

                </nav>
            </div>
            <!-- ============================================================== -->
            <!-- end navbar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- left sidebar -->
            <!-- ============================================================== -->
            <div class="nav-left-sidebar sidebar-dark">
                <div class="menu-list">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav flex-column">
                                <li class="nav-divider">
                                    Dashboard-Sales-Manager
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="#" onclick="setActive(this)">
                                        <i class="fa fa-fw fa-user-circle"></i>
                                        Dashboard
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="ordermanagersm" onclick="setActive(this)">
                                        <i class="fas fa-fw fa-chart-pie"></i>
                                        Order Manager
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end left sidebar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- wrapper  -->
            <!-- ============================================================== -->
            <div class="dashboard-wrapper">
                <div class="container-fluid  dashboard-content">
                    <!-- ============================================================== -->
                    <!-- pagehader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h3 class="mb-2">Sales Manager Dashboard </h3>


                                <div class="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Sales Dashboard Template</li>

                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- pagehader  -->
                    <!-- ============================================================== -->
                   

                    <div class="row">
                        <!-- metric -->

                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="text-muted">Sales</h5>
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-1 text-primary">${sales}</h1>
                                    </div>

                                </div>
                                <div id="sparkline"></div>
                            </div>
                        </div>
                        <!-- /. metric -->
                        <!-- metric -->
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="text-muted">Orders</h5>
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-1 text-primary">${orders} </h1>
                                    </div>

                                </div>
                                <div id="sparkline"></div>
                            </div>
                        </div>
                        <!-- /. metric -->
                        <!-- metric -->
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="text-muted">Total Amount</h5>
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-1 text-primary original-price">${total}đ</h1>
                                    </div>

                                </div>
                                <div id="sparkline">
                                </div>
                            </div>
                        </div>
                        <!-- /. metric -->

                    </div>
                    <!-- ============================================================== -->
                    <!-- revenue  -->
                    <!-- ============================================================== -->
                    <div class="row" >
                       
                       
                        <div class="col-xl-6 col-lg-12 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Total Amount Sale</h5>
                                <div class="card-body">
                                    <canvas id="myPieChart" width="220" height="155"></canvas>
                                    <div class="chart-widget-list" style="text-align: center;">
                                        <h5 style="display: block;">Sales Total Amount</h5>    
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-12 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Total Amount Sale</h5>
                                <div class="card-body">
                                    <canvas id="myPieChart2" width="220" height="155"></canvas>
                                    <div class="chart-widget-list" style="text-align: center;">
                                        <h5 style="display: block;">Order Status</h5>    
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                    <div class="row">
                         <div class="col-xl-12 col-lg-12 col-md-8 col-sm-12 col-12">
                            <div class="card">

                                <h5 class="card-header">Revenue<a>
                                        <select id="yearSelect" onchange="loadData()" style="float:right;" name="year"></select>
                                    </a>
                                </h5>  


                                <div class="card-body" >
                                    <canvas id="myChart" width="400" height="150"></canvas>
                                </div>
                                <div class="card-body border-top">
                                    <div class="row">
                                        <div class="offset-xl-1 col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12 p-3">
                                            <h4> Today's Earning:<a class="original-price"> ${erning}đ</a></h4>

                                            </p>
                                        </div>
                                        <div class="offset-xl-1 col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 p-3">
                                            <h2 class="font-weight-normal mb-3"><span class="original-price">${high.totalAmount}đ</span>                                                    </h2>
                                            <div class="mb-0 mt-3 legend-item">

                                                <span class="legend-text"> Highest Month</span></div>
                                        </div>
                                        <div class="offset-xl-1 col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 p-3">
                                            <h2 class="font-weight-normal mb-3">

                                                <span class="original-price">${low.totalAmount}đ</span>
                                            </h2>
                                            <div class="text-muted mb-0 mt-3 legend-item"><span class="legend-text">Lowest Month</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- ============================================================== -->
                        <!-- top selling products  -->
                        <!-- ============================================================== -->
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Top Selling Products</h5>
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead class="bg-light">
                                                <tr class="border-0">
                                                    <th class="border-0">#</th>
                                                    <th class="border-0">Image</th>
                                                    <th class="border-0">Product Name</th>
                                                    <th class="border-0">Product Id</th>
                                                    <th class="border-0">Price</th>
                                                    <th class="border-0">Quantity Sold</th>
                                                    <th class="border-0">Price Sold</th>


                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${listod}" var="o">
                                                    <tr>
                                                        <td>1</td>
                                                        <td>
                                                            <div class="m-r-10"><img src="${o.img}" alt="user" class="rounded" width="45"></div>
                                                        </td>
                                                        <td>${o.pname}</td>
                                                        <td>${o.productId}</td>
                                                        <td class="original-price">${o.productPrice}đ</td>
                                                        <td>${o.quantity}</td>
                                                        <td class="original-price">${o.price}đ</td>


                                                    </tr>

                                                </c:forEach>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- ============================================================== -->
                        <!-- end revenue locations  -->
                        <!-- ============================================================== -->
                    </div>

                </div>
                
                
            </div>
            <!-- ============================================================== -->
            <!-- end wrapper  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end main wrapper  -->
        <!-- ============================================================== -->
        <!-- Optional JavaScript -->
        <!-- jquery 3.3.1 js-->
        <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
        <!-- bootstrap bundle js-->
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
        <!-- slimscroll js-->
        <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
        <!-- chartjs js-->
        <script src="assets/vendor/charts/charts-bundle/Chart.bundle.js"></script>
        <script src="assets/vendor/charts/charts-bundle/chartjs.js"></script>

        <!-- main js-->
        <script src="assets/libs/js/main-js.js"></script>
        <!-- jvactormap js-->
        <script src="assets/vendor/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
        <script src="assets/vendor/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- sparkline js-->
        <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
        <script src="assets/vendor/charts/sparkline/spark-js.js"></script>
        <!-- dashboard sales js-->
        <script src="assets/libs/js/dashboard-sales.js"></script>


        <script>
            <%
List<Order> listbt = (List<Order>) request.getAttribute("listr");
            %>


                                            var xValues = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                                            var yValues = [
            <%
                         for (Order order : listbt) {
                            out.println(order.getTotalAmount() + ",");
                        }
            %>
                                            ];
                                            var barColors = ["#9c9ab5", "#9c9ab5", "#9c9ab5", "#9c9ab5", "#9c9ab5", "#9c9ab5", "#9c9ab5", "#9c9ab5", "#9c9ab5", "#9c9ab5", "#9c9ab5", "#9c9ab5"];
                                            new Chart("myChart", {
                                                type: "bar",
                                                data: {
                                                    labels: xValues,
                                                    datasets: [{
                                                            backgroundColor: barColors,
                                                            data: yValues
                                                        }]
                                                },
                                                options: {
                                                    legend: {display: false},
                                                    title: {
                                                        display: true,
                                                        text: "Revenue"
                                                    }
                                                }
                                            });



        </script>
        <script>
           
            function generateYearOptions() {
                var currentYear = new Date().getFullYear();
                var selectElement = $("#yearSelect");

                for (var i = 0; i <= 10; i++) {
                    var year = currentYear - i;
                    var option = $("<option>").val(year).text(year);
                    selectElement.append(option);
                }
            }

            // G?i hàm khi trang ???c t?i
            $(document).ready(function () {
                generateYearOptions();
            });
        </script>
        <script>
            <%
               List<SaleTotal> list = (List<SaleTotal>) request.getAttribute("list");
            %>
            var xValues = [
            <%
                for (SaleTotal s : list) {
                 out.println("\"" + s.getName() + "\"" + ",");
                 }
            %>
            ];


            var yValues = [
            <%
                for (SaleTotal s : list) {
                    out.println(s.getTotal() + ",");
                 }
            %>
            ];
            var barColors = [
                "#b91d47",
                "#1e7145",
                "#00aba9",
                "#2b5797",
                "#e8c3b9",
            ];

            new Chart("myPieChart", {
                type: "pie",
                data: {
                    labels: xValues,
                    datasets: [{
                            backgroundColor: barColors,
                            data: yValues
                        }]
                },
                options: {
                    title: {
                        display: false,

                    }
                }
            });
        </script>
        <script>
            
        
            var barColors = [
                "#FFFFCC",
                "#CCFFFF",
                "#FFFF33",
                "#00FF66",
                "#FF0000"
            ];

            new Chart("myPieChart2", {
                type: "pie",
                data: {
                    labels:  ["Waiting", "Confirm", "Shipping", "Success","Cancel"],
                    datasets: [{
                            backgroundColor: barColors,
                            data: [${s1},${s2},${s3},${s4},${s5}]
                        }]
                },
                options: {
                    title: {
                        display: false,

                    }
                }
            });
        </script>
   <script>
            document.addEventListener("DOMContentLoaded", function () {
                var originalPrices = document.querySelectorAll('.original-price');
                originalPrices.forEach(function (originalPrice) {
                    originalPrice.textContent = formatPrice(originalPrice.textContent.trim());
                });

                function formatPrice(price) {
                    return (parseFloat(price)).toLocaleString('vi-VN', {style: 'currency', currency: 'VND'}).replace('VND', '');
                }
            });
        </script>
    </body>

</html>