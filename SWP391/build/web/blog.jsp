<!DOCTYPE html>
<html lang="zxx">
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Male_Fashion Template">
        <meta name="keywords" content="Male_Fashion, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Male-Fashion | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap"
              rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
    </head>

    <body>
        <div id="preloder">
            <div class="loader"></div>
        </div>

        <!-- Offcanvas Menu Begin -->
        <div class="offcanvas-menu-overlay"></div>
        <div class="offcanvas-menu-wrapper">
            <div class="offcanvas__option">
                <div class="offcanvas__links">
                    <a href="#">Sign in</a>
                </div>

            </div>
            <div class="offcanvas__nav__option">
                <a href="#" class="search-switch"><img src="img/icon/search.png" alt=""></a>
                <a href="#"><img src="img/icon/cart.png" alt=""> <span>0</span></a>
                <div class="price">$0.00</div>
            </div>
            <div id="mobile-menu-wrap"></div>
            <div class="offcanvas__text">
                <p>Free shipping, 30-day return or refund guarantee.</p>
            </div>
        </div>
        <!-- Offcanvas Menu End -->

        <!-- Header Section Begin -->
        <header class="header">
            <div class="header__top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-7">
                            <div class="header__top__left">
                                <c:if test="${sessionScope.message != null}">
                                    <div id="message" class="notification" style="color: white ">
                                        ${sessionScope.message}
                                    </div>
                                    <script>
                                        // Hien thi thong bao 5s
                                        setTimeout(function () {
                                            var messageDiv = document.getElementById("message");
                                            if (messageDiv) {
                                                messageDiv.style.display = "none";

                                        <%
                                                    session.removeAttribute("message");
                                        %>
                                            }
                                        }, 5000);
                                    </script>
                                </c:if>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-5">
                            <div class="header__top__right">
                                <div class="header__top__links">
                                    <c:if test="${sessionScope.acc==null}">
                                        <a href="login">Sign in</a> 
                                    </c:if>


                                </div>
                                <c:if test="${sessionScope.acc!=null}">
                                    <div class="header__top__hover">

                                        <span>${sessionScope.acc.name} <i class="arrow_carrot-down"></i></span>
                                        <ul>
                                            <li><a href="userprofile?uid=${sessionScope.acc.email}">Profile</a> </li>
                                                <c:choose>
                                                    <c:when test="${sessionScope.acc.roleId eq 1}">
                                                    <li><a href="admindashboard">Dashboard</a></li>
                                                    </c:when>
                                                    <c:when test="${sessionScope.acc.roleId eq 2}">
                                                    <li><a href="salemanagerdashboard">Dashboard</a></li>
                                                    </c:when>
                                                    <c:when test="${sessionScope.acc.roleId eq 3}">
                                                    <li><a href="ordermanager">Dashboard</a></li>
                                                    </c:when>
                                                    <c:when test="${sessionScope.acc.roleId eq 4}">
                                                    <li><a href="dashboardmkt">Dashboard</a></li>
                                                    </c:when>
                                                    <c:when test="${sessionScope.acc.roleId eq 5}">
                                                    <li><a href="myorder?acid=${sessionScope.acc.user_id}">My Order</a> </li>
                                                    </c:when>
                                                </c:choose>
                                            <li><a href="logout">Log out</a></li>
                                        </ul>


                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div class="header__logo">
                            <a href="./home"><img src="img/logo.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li><a href="./home">Home</a></li>
                                <li ><a href="./shop">Shop</a></li>

                                <li  class="active"><a href="./blog">Blog</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <div class="header__nav__option">
                            <a href="#" class="search-switch"><img src="img/icon/search.png" alt=""></a>
                                <c:if test="${sessionScope.acc.roleId eq 5}">
                                <a href="viewcart"><img src="img/icon/cart.png" alt=""> <span>${size}</span></a>
                                <div class="price original-price">${total}?</div>
                            </c:if>
                        </div>
                    </div>
                </div>
                <div class="canvas__open"><i class="fa fa-bars"></i></div>
            </div>
        </header>
        <!-- Header Section End -->

        <!-- Breadcrumb Section Begin -->
        <section class="breadcrumb-blog set-bg" data-setbg="img/breadcrumb-bg.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Our Blog</h2>
                    </div>
                </div>
            </div>
        </section>
        <!-- Breadcrumb Section End -->

        <!-- Blog Section Begin -->
        <section class="blog spad">
            <div class="container">
                <div class="row">


                    <div class="col-lg-3">
                        <div class="shop__sidebar">
                            <div class="shop__sidebar__search">
                                <form action="searchblog">
                                    <input type="text" placeholder="Search" name = "search" value="${search}">
                                    <button type="submit"><span class="icon_search"></span></button>
                                </form>
                            </div>
                            <div class="shop__sidebar__accordion">
                                <div class="accordion" id="accordionExample">
                                    <div class="card">
                                        <div class="card-heading">
                                            <a data-toggle="collapse" data-target="#collapseOne">Categories</a>
                                        </div>
                                        <div id="collapseOne" class="collapse show" data-parent="#accordionExample">
                                            <div class="card-body">
                                                <div class="shop__sidebar__categories">
                                                    <ul class="nice-scroll">
                                                        <c:forEach items="${listBC}" var="c">
                                                            <li><a href="blogcatecon?bc_id=${c.bc_id}" style="${tag == c.bc_id ? 'color: black;' : ''}">
                                                                    ${c.bc_name}
                                                                </a>
                                                            </li> 
                                                        </c:forEach>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-9">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="shop__product__option__left">
                                    <p>Showing 1-12 of 126 results</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="shop__product__option__right">
                                    <p>Sort by Updated Date :</p>
                                    <form>
                                        <select name="sort" onchange="form.submit()">
                                            <option value="" ${empty param.sort ? 'selected' : ''}>Default</option>
                                            <option value="oldest" ${param.sort == 'oldest' ? 'selected' : ''}>Oldest to Latest</option>
                                            <option value="latest" ${param.sort == 'latest' ? 'selected' : ''}>Latest to Oldest</option>
                                        </select>
                                        <% if(request.getParameter("page") != null) { %>
                                        <input type="hidden" name="page" value="<%=request.getParameter("page")%>"/>
                                        <% } %>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <c:forEach items="${listB}" var="c">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="blog__item">
                                        <div class="blog__item__pic set-bg" data-setbg="${c.thumbnail}"></div>
                                        <div class="blog__item__text">
                                            <span><img src="img/icon/calendar.png" alt="">${c.date}</span>
                                            <h5 style=" display: -webkit-box; -webkit-box-orient: vertical; overflow: hidden; -webkit-line-clamp: 2;">${c.title}</h5>
                                            <a href="blogdetail?blog_id=${c.blog_id}">Read More</a>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="product__pagination ">
                                    <!-- Hi?n th? ph�n trang -->
                                    <ul class="pagination">
                                        <c:forEach begin="1" end="${totalPages}" var="page">
                                            <li class="page-item ${page == currentPage ? 'active' : ''}">
                                                <c:url var="pageUrl" value="blog">
                                                    <c:param name="page" value="${page}" />
                                                    <%-- Ki?m tra n?u c� tham s? sort, th�m v�o URL --%>
                                                    <c:if test="${not empty param.sort}">
                                                        <c:param name="sort" value="${param.sort}" />
                                                    </c:if>
                                                </c:url>
                                                <a class="page-link" href="${pageUrl}">${page}</a>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </section>
        <!-- Blog Section End -->

        <!-- Footer Section Begin -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="footer__about">
                            <div class="footer__logo">
                                <a href="#"><img src="img/footer-logo.png" alt=""></a>
                            </div>
                            <p>The customer is at the heart of our unique business model, which includes design.</p>
                            <a href="#"><img src="img/payment.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-2 offset-lg-1 col-md-3 col-sm-6">
                        <div class="footer__widget">
                            <h6>Shopping</h6>
                            <ul>
                                <li><a href="#">Clothing Store</a></li>
                                <li><a href="#">Trending Shoes</a></li>
                                <li><a href="#">Accessories</a></li>
                                <li><a href="#">Sale</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-6">
                        <div class="footer__widget">
                            <h6>Shopping</h6>
                            <ul>
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">Payment Methods</a></li>
                                <li><a href="#">Delivary</a></li>
                                <li><a href="#">Return & Exchanges</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 offset-lg-1 col-md-6 col-sm-6">
                        <div class="footer__widget">
                            <h6>NewLetter</h6>
                            <div class="footer__newslatter">
                                <p>Be the first to know about new arrivals, look books, sales & promos!</p>
                                <form action="#">
                                    <input type="text" placeholder="Your email">
                                    <button type="submit"><span class="icon_mail_alt"></span></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="footer__copyright__text">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            <p>Copyright �
                                <script>
                                    document.write(new Date().getFullYear());
                                </script>2020
                                All rights reserved | This template is made with <i class="fa fa-heart-o"
                                                                                    aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                            </p>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Section End -->

        <!-- Search Begin -->
        <div class="search-model">
            <div class="h-100 d-flex align-items-center justify-content-center">
                <div class="search-close-switch">+</div>
                <form class="search-model-form">
                    <input type="text" id="search-input" placeholder="Search here.....">
                </form>
            </div>
        </div>
        <!-- Search End -->

        <!-- Js Plugins -->
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery.nicescroll.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/jquery.countdown.min.js"></script>
        <script src="js/jquery.slicknav.js"></script>
        <script src="js/mixitup.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/main.js"></script>
        <script>      document.addEventListener("DOMContentLoaded", function () {
                                        var originalPrices = document.querySelectorAll('.original-price');
                                        originalPrices.forEach(function (originalPrice) {
                                            originalPrice.textContent = formatPrice(originalPrice.textContent.trim());
                                        });

                                        function formatPrice(price) {
                                            return (parseFloat(price)).toLocaleString('vi-VN', {style: 'currency', currency: 'VND'}).replace('VND', '');
                                        }
                                    });</script>
    </body>

</html>