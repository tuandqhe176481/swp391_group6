
<!doctype html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
        <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/libs/css/style.css">
        <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="assets/vendor/charts/chartist-bundle/chartist.css">
        <link rel="stylesheet" href="assets/vendor/charts/morris-bundle/morris.css">
        <link rel="stylesheet" href="assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="assets/vendor/charts/c3charts/c3.css">
        <link rel="stylesheet" href="assets/vendor/fonts/flag-icon-css/flag-icon.min.css">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
        <title>Concept - Bootstrap 4 Admin Dashboard Template</title>

        <style>
            .error-message {
                text-align: center; /* c?n gi?a n?i dung */
                font-weight: bold; /* l�m n?i b?t */
                color: blue; /* m�u s?c */
                font-size: larger;
            }
            .filter {
                display: flex;
                align-items: center;
                margin-bottom: 10px;
            }
            .filter1 {
                display: flex;
                align-items: center;
            }

            .texta {
                margin-right: 10px;
                font-weight: bold;
                font-size: 18px;

            }

            select {
                padding: 8px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            /* T�y ch?nh c�c select n?m trong .filter */
            .filter select {
                margin-right: 10px;
            }

            /* T�y ch?nh m�u n?n khi hover tr�n select */
            .filter select:hover {
                background-color: #f5f5f5;
            }
            select:hover {
                background-color: #f5f5f5;
            }

            input[type="submit"] {
                padding: 10px 15px;
                background-color: #4CAF50;
                color: white;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            /* Hi?u ?ng hover cho n�t Filter */
            input[type="submit"]:hover {
                background-color: #45a049;
            }

            .search-p{
                justify-content: right;
            }
        </style>
    </head>

    <body>
        <!-- ============================================================== -->
        <!-- main wrapper -->
        <!-- ============================================================== -->
        <div class="dashboard-main-wrapper">
            <!-- ============================================================== -->
            <!-- navbar -->
            <!-- ============================================================== -->
            <div class="dashboard-header">
                <nav class="navbar navbar-expand-lg bg-white fixed-top">
                    <a class="navbar-brand" href="home">Concept</a>
                </nav>
            </div>
            <!-- ============================================================== -->
            <!-- end navbar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- left sidebar -->
            <!-- ============================================================== -->
            <div class="nav-left-sidebar sidebar-dark">
                <div class="menu-list">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                          <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav flex-column">
                                <li class="nav-divider">
                                    Menu
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="admindashboard" >
                                        <i class="fa fa-fw fa-user-circle"></i>
                                        Dashboard
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="usermanager" >
                                        <i class="fas fa-fw fa-chart-pie"></i>
                                        User Manager
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link active" href="settinglist">
                                        <i class="fas fa-fw fa-chart-pie"></i>
                                        Setting List
                                    </a>
                                </li>


                        </div>
                    </nav>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end left sidebar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- wrapper  -->
            <!-- ============================================================== -->
            <div class="dashboard-wrapper">
                <div class="dashboard-ecommerce">
                    <div class="container-fluid dashboard-content ">
                        <!-- ============================================================== -->
                        <!-- pageheader  -->
                        <!-- ============================================================== -->
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="page-header">
                                    <h2 class="pageheader-title">Setting List</h2>
                                    <div class="page-breadcrumb">
                                        <nav aria-label="breadcrumb">
                                            <ol class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="admindashboard" class="breadcrumb-link">Dashboard</a></li>
                                                <li class="breadcrumb-item active" aria-current="page">Setting List</li>
                                            </ol>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end pageheader  -->
                        <!-- ============================================================== -->
                        <div class="ecommerce-widget">

                            <div class="row">
                                <!-- ============================================================== -->

                                <!-- ============================================================== -->

                                <!-- recent orders  -->
                                <!-- ============================================================== -->
                                <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                    <div class="filter1">
                                        <form action="filtersetting">
                                            <div class="filter">
                                                <a class="texta" style=" color: #454444f7;" >Filter:</a>
                                                <div>
                                                    <select name="categories">
                                                        <option value="0">Categories</option>
                                                        <c:forEach items="${listsc}" var="sc">
                                                            <option value="${sc.sc_id}" ${sc.sc_id == categories ? "selected" : ""}>${sc.name}</option>
                                                        </c:forEach>
                                                    </select>
                                                </div>

                                                <div>
                                                    <select name="status">
                                                        <option>Status</option>

                                                        <option value="Active" ${status == 'Active' ? "selected" : ""}>Active</option>  
                                                        <option value="InActive" ${status == 'InActive' ? "selected" : ""}>InActive</option>             

                                                    </select>
                                                </div>
                                                <input type="submit" value="Filter"/>                                              
                                            </div>
                                        </form>
                                        <div style="margin-left: 15px;">
                                            <a href="addsetting"><button type="button" class="btn btn-danger">Add Value</button> </a>
                                        </div>
                                        <div class="input-group rounded search-p">
                                            <form action="searchsetting">
                                                <input type="search" name="search" class="form-control rounded" placeholder="Search" value="${search}" aria-label="Search" aria-describedby="search-addon" style="max-width: 95%" />
                                            </form>
                                            <span class="input-group-text border-0" id="search-addon">
                                                <i class="fas fa-search"></i>
                                            </span>
                                        </div> 
                                    </div>

                                    <div class="card">
                                        <div class="card-body p-0">
                                            <div class="table-responsive">
                                                <table class="table" id = "settingTable">
                                                    <thead class="bg-light">
                                                        <tr class="border-0">
                                                            <th class="border-0" onclick="sortTable(0)">Id</th>
                                                            <th class="border-0" onclick="sortTable(1)">Type</th>
                                                            <th class="border-0" onclick="sortTable(2)">Value</th>    
                                                            <th class="border-0" onclick="sortTable(3)">Order</th>
                                                            <th class="border-0" onclick="sortTable(4)">Status</th>
                                                            <th class="border-0">Feature</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <c:forEach items="${list}" var="s">
                                                            <tr>
                                                                <td>${s.id} </td>
                                                                <td>${s.type_name}</td>
                                                                <td>${s.value}</td>
                                                                <td>${s.order}</td>
                                                                <td>${s.status}</td> 
                                                                <td><c:choose>
                                                                        <c:when test="${s.status eq 'Inactive'}">  
                                                                            <a href="changeSet?id=${s.id}&status=Active"  onclick="return confirmAction()" class="btn btn-xs btn-success active"> <span class="ion-checkmark">Active</span></a>
                                                                        </c:when>
                                                                        <c:when test="${s.status eq 'Active'}">              
                                                                            <a href="changeSet?id=${s.id}&status=Inactive"  onclick="return confirmAction()" class="btn btn-xs btn-danger inactive"> <span class="ion-android-close">InActive</span></a>
                                                                        </c:when>
                                                                    </c:choose>
                                                                </td>
                                                                <td>  <a class="fas fa-edit edit-icon" href="editsetting?id=${s.id}"></a> <a class="fas fa-eye view-icon" href="viewsetting?id=${s.id}"></a></td>

                                                            </tr> 
                                                        </c:forEach>
                                                        <c:if test="${not empty error}">
                                                            <tr>
                                                                <td colspan="6" class="error-message" style="color: red">${error}</td>
                                                            </tr>
                                                        </c:if>
                                                    </tbody>

                                                </table>


                                                <nav aria-label="Page navigation example">
                                                    <ul class="pagination justify-content-end">
                                                        <c:forEach begin="1" end="${end}" var="i">
                                                            <li class="page-item ${i == index ? 'active' : ''}">
                                                                <a class="page-link" href="settinglist?index=${i}">${i}</a>
                                                            </li>
                                                        </c:forEach>
                                                        <c:choose>
                                                            <c:when test="${status eq status}">      
                                                                <nav aria-label="Page navigation example">
                                                                    <ul class="pagination justify-content-end">
                                                                        <c:forEach begin="1" end="${endstatus}" var="i">
                                                                            <li class="page-item ${i == indexs ? 'active' : ''}">
                                                                                <a class="page-link" href="filtersetting?categories=0&status=${status}&indexs=${i}">${i}</a>
                                                                            </li>
                                                                        </c:forEach>
                                                                    </ul>
                                                                </nav> 
                                                            </c:when>
                                                        </c:choose>
                                                        <c:choose>
                                                            <c:when test="${categories eq categories}">
                                                                <c:forEach begin="1" end="${endcate}" var="i">
                                                                    <li class="page-item ${i == indexc ? 'active' : ''}">
                                                                        <a class="page-link" href="filtersetting?categories=${categories}&status=Status&indexc=${i}">${i}</a>
                                                                    </li>
                                                                </c:forEach>
                                                            </c:when>
                                                        </c:choose>
                                                        <c:choose>
                                                            <c:when test="${categories eq categories && status eq status}">

                                                                <c:forEach begin="1" end="${endstacate}" var="i">
                                                                    <li class="page-item ${i == indexsc ? 'active' : ''}">
                                                                        <a class="page-link" href="filtersetting?categories=${categories}&status=${status}&indexsc=${i}">${i}</a>
                                                                    </li>
                                                                </c:forEach>

                                                            </c:when>
                                                        </c:choose>
                                                        <c:choose>
                                                            <c:when test="${search eq search}">
                                                                <c:forEach begin="1" end="${endsearch}" var="i">
                                                                    <li class="page-item ${i == indexse ? 'active' : ''}">
                                                                        <a class="page-link" href="searchsetting?search=${search}&indexse=${i}">${i}</a>
                                                                    </li>
                                                                </c:forEach>
                                                            </c:when>
                                                        </c:choose>
                                                    </ul>
                                                </nav>



                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ============================================================== -->
                                <!-- end recent orders  -->


                                <!-- ============================================================== -->
                                <!-- ============================================================== -->
                                <!-- customer acquistion  -->
                                <!-- ============================================================== -->
                                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">

                                </div>
                                <!-- ============================================================== -->
                                <!-- end customer acquistion  -->
                                <!-- ============================================================== -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            Copyright � 2018 Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="text-md-right footer-links d-none d-sm-block">
                                <a href="javascript: void(0);">About</a>
                                <a href="javascript: void(0);">Support</a>
                                <a href="javascript: void(0);">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="assets/libs/js/dashboard-ecommerce.js"></script>
    <script>
                                                                                function sortTable(columnIndex) {
                                                                                    var table, rows, switching, i, x, y, shouldSwitch;
                                                                                    table = document.getElementById("settingTable");
                                                                                    switching = true;
                                                                                    while (switching) {
                                                                                        switching = false;
                                                                                        rows = table.rows;
                                                                                        for (i = 1; i < (rows.length - 1); i++) {
                                                                                            shouldSwitch = false;
                                                                                            x = rows[i].getElementsByTagName("td")[columnIndex];
                                                                                            y = rows[i + 1].getElementsByTagName("td")[columnIndex];
                                                                                            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                                                                                                shouldSwitch = true;
                                                                                                break;
                                                                                            }
                                                                                        }
                                                                                        if (shouldSwitch) {
                                                                                            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                                                                                            switching = true;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                function confirmAction() {
                                                                                    return confirm("Are you sure you want to proceed with this action?");
                                                                                }
    </script>






</body>

</html>