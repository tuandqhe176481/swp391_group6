<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="zxx">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Male_Fashion Template">
        <meta name="keywords" content="Male_Fashion, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Male-Fashion | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap"
              rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <style>
            .product__cart__item__pic {
                width: 90px;
                height: 90px;
                overflow: hidden;
            }

            .product__cart__item__pic img {
                width: 100%;
                height: 100%;
                object-fit: cover;
            }

            .shopping__cart__table table tbody tr td.quantity__item {
                width: 270px;
            }
            .shopping__cart__table table tbody tr td.quantity__price {
                width: 270px;
            }
            .shopping__cart__table table tbody tr td.product__cart__item {
                width: 450px;
            }
            .confirmation-message {
                text-align: center;
            }

            .big-text {
                font-size: 32px; /* Chữ to */
                font-weight: bold; /* Chữ đậm */
                color: #3498db; /* Màu sắc, bạn có thể thay đổi mã màu theo ý muốn */
            }
            .signature {
                font-style: italic; /* Nghiêng nghiêng */
                color: #e74c3c; /* Màu sắc, bạn có thể thay đổi mã màu theo ý muốn */
            }

            .heart {
                color: #e74c3c; /* Màu sắc trái tim */
            }
            .total {
                font-size: 24px; /* Chữ to */
                font-weight: bold; /* Chữ đậm */
                text-align: right;
            }
        </style>
    </head>

    <body>
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>

        <!-- Offcanvas Menu Begin -->
        <div class="offcanvas-menu-overlay"></div>
        <div class="offcanvas-menu-wrapper">
            <div class="offcanvas__option">
                <div class="offcanvas__links">
                    <a href="#">Sign in</a>
                    <a href="#">FAQs</a>
                </div>
                <div class="offcanvas__top__hover">
                    <span>Usd <i class="arrow_carrot-down"></i></span>
                    <ul>
                        <li>USD</li>
                        <li>EUR</li>
                        <li>USD</li>
                    </ul>
                </div>
            </div>
            <div class="offcanvas__nav__option">
                <a href="#" class="search-switch"><img src="img/icon/search.png" alt=""></a>
                <a href="#"><img src="img/icon/heart.png" alt=""></a>
                <a href="#"><img src="img/icon/cart.png" alt=""> <span>0</span></a>
                <div class="price">$0.00</div>
            </div>
            <div id="mobile-menu-wrap"></div>
            <div class="offcanvas__text">
                <p>Free shipping, 30-day return or refund guarantee.</p>
            </div>
        </div>
        <!-- Offcanvas Menu End -->

        <!-- Header Section Begin -->
        <header class="header">
            <div class="header__top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-7">
                            <div class="header__top__left">
                                <p>Free shipping, 30-day return or refund guarantee.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-5">
                            <div class="header__top__right">
                                <div class="header__top__links">
                                    <c:if test="${sessionScope.acc==null}">
                                        <a href="login">Sign in</a> 
                                    </c:if>


                                </div>
                                <c:if test="${sessionScope.acc!=null}">
                                    <div class="header__top__hover">

                                        <span>${sessionScope.acc.name} <i class="arrow_carrot-down"></i></span>
                                        <ul>
                                            <li><a href="userprofile?uid=${sessionScope.acc.email}">Profile</a> </li>
                                            <li><a href="#">My Order</a> </li>
                                            <li><a href="logout">Log out</a></li>
                                        </ul>


                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
        </header>
        <!-- Header Section End -->

        <!-- Breadcrumb Section Begin -->
        <section class="breadcrumb-option">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="breadcrumb__text">
                            <h4>Shopping Cart</h4>
                            <div class="breadcrumb__links">
                                <a href="./home">Home</a>
                                <a href="./shop">Shop</a>
                                <a href="./viewcart">Shopping Cart</a>
                                <a href="./checkout">Check Out</a>                                
                                <span>Order Submitted</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Breadcrumb Section End -->

        <!-- Shopping Cart Section Begin -->
        <section class="shopping-cart spad">
            <div class="container">
                <div class="confirmation-message">
                    <p class="big-text">You have placed your order successfully</p>
                    <p>Thank you for choosing our products.</p>
                    <p>The product will reach you as soon as possible.</p>
                    <p class="signature">from Genzfashion1756 with love <span class="heart">&#10084;</span></p>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="shopping__cart__table">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Price</th>                                        
                                        <th>Quantity</th>
                                        <th>Total</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    <c:forEach  items="${listi}" var="i">
                                        <tr>
                                            <td class="product__cart__item">
                                                <div class="product__cart__item__pic">
                                                    <img src="${i.product.img1}" alt="">
                                                </div>
                                                <div class="product__cart__item__text">
                                                    <h6>${i.product.pname}</h6>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="product__cart__item__price">
                                                    <h6 class="original-price">${i.price}đ</h6>
                                                </div>
                                            </td>
                                            <td class="quantity__item">
                                                <div class="quantity">
                                                    <div>
                                                        &emsp;<span class="qty">${i.quantity}</span>&emsp;
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="original-price">${i.price*i.quantity}đ</td>

                                        </tr>
                                    </c:forEach>




                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="continue__btn">
                                    <a href="home">Continue Shopping</a>
                                </div>
                            </div>
                             <c:set value="${total}" var="total"/>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="total">
                                    Total <span class="original-price">${total}đ</span>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- Shopping Cart Section End -->

        <!-- Footer Section Begin -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="footer__about">
                            <div class="footer__logo">
                                <a href="#"><img src="img/footer-logo.png" alt=""></a>
                            </div>
                            <p>The customer is at the heart of our unique business model, which includes design.</p>
                            <a href="#"><img src="img/payment.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-2 offset-lg-1 col-md-3 col-sm-6">
                        <div class="footer__widget">
                            <h6>Shopping</h6>
                            <ul>
                                <li><a href="#">Clothing Store</a></li>
                                <li><a href="#">Trending Shoes</a></li>
                                <li><a href="#">Accessories</a></li>
                                <li><a href="#">Sale</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-6">
                        <div class="footer__widget">
                            <h6>Shopping</h6>
                            <ul>
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">Payment Methods</a></li>
                                <li><a href="#">Delivary</a></li>
                                <li><a href="#">Return & Exchanges</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 offset-lg-1 col-md-6 col-sm-6">
                        <div class="footer__widget">
                            <h6>NewLetter</h6>
                            <div class="footer__newslatter">
                                <p>Be the first to know about new arrivals, look books, sales & promos!</p>
                                <form action="#">
                                    <input type="text" placeholder="Your email">
                                    <button type="submit"><span class="icon_mail_alt"></span></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Section End -->

        <!-- Search Begin -->
        <div class="search-model">
            <div class="h-100 d-flex align-items-center justify-content-center">
                <div class="search-close-switch">+</div>
                <form class="search-model-form">
                    <input type="text" id="search-input" placeholder="Search here.....">
                </form>
            </div>
        </div>
        <!-- Search End -->

        <!-- Js Plugins -->
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery.nicescroll.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/jquery.countdown.min.js"></script>
        <script src="js/jquery.slicknav.js"></script>
        <script src="js/mixitup.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/main.js"></script>
        <script>
             document.addEventListener("DOMContentLoaded", function () {
                                    var originalPrices = document.querySelectorAll('.original-price');
                                    originalPrices.forEach(function (originalPrice) {
                                        originalPrice.textContent = formatPrice(originalPrice.textContent.trim());
                                    });

                                    function formatPrice(price) {
                                        return (parseFloat(price)).toLocaleString('vi-VN', {style: 'currency', currency: 'VND'}).replace('VND', '');
                                    }
                                });
        </script>
    </body>

</html>