<!doctype html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">


    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
        <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/libs/css/style.css">
        <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="assets/vendor/vector-map/jqvmap.css">
        <link rel="stylesheet" href="assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css">
        <link rel="stylesheet" href="assets/vendor/fonts/flag-icon-css/flag-icon.min.css">

        <style>
            .error-message {
                text-align: center; /* c?n gi?a n?i dung */
                font-weight: bold; /* l�m n?i b?t */
              
                font-size: larger;
            }
            .filter {
                display: flex;
                align-items: center;
                margin-bottom: 10px;
            }
            .filter1 {
                display: flex;
                align-items: center;
            }

            .texta {
                margin-right: 10px;
                font-weight: bold;
                font-size: 18px;

            }

            select {
                padding: 8px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            /* T�y ch?nh c�c select n?m trong .filter */
            .filter select {
                margin-right: 10px;
            }

            /* T�y ch?nh m�u n?n khi hover tr�n select */
            .filter select:hover {
                background-color: #f5f5f5;
            }
            select:hover {
                background-color: #f5f5f5;
            }

            input[type="submit"] {
                padding: 10px 15px;
                background-color: #4CAF50;
                color: white;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            /* Hi?u ?ng hover cho n�t Filter */
            input[type="submit"]:hover {
                background-color: #45a049;
            }

            .search-p{
                justify-content: right;
            }
        </style>
        <title>Concept - Bootstrap 4 Admin Dashboard Template</title>
    </head>

    <body>
        <!-- ============================================================== -->
        <!-- main wrapper -->
        <!-- ============================================================== -->
        <div class="dashboard-main-wrapper">
            <!-- ============================================================== -->
            <!-- navbar -->
            <!-- ============================================================== -->
            <div class="dashboard-header">
                <nav class="navbar navbar-expand-lg bg-white fixed-top">
                    <a class="navbar-brand" href="home">Concept</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                
                </nav>
            </div>
            <!-- ============================================================== -->
            <!-- end navbar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- left sidebar -->
            <!-- ============================================================== -->
            <div class="nav-left-sidebar sidebar-dark">
                <div class="menu-list">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav flex-column">
                                <li class="nav-divider">
                                    Menu
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="admindashboard" >
                                        <i class="fa fa-fw fa-user-circle"></i>
                                        Dashboard
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link active" href="usermanager" >
                                        <i class="fas fa-fw fa-chart-pie"></i>
                                        User Manager
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="settinglist">
                                        <i class="fas fa-fw fa-chart-pie"></i>
                                        Setting List
                                    </a>
                                </li>


                        </div>
                        </li>
                        </ul>
                </div>
                </nav>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- top selling products  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="filter1">
                            <form action="filteruser">
                                <div class="filter">
                                    <a class="texta" style=" color: #454444f7;" >Filter:</a>

                                    <select name="gender">
                                        <option>Gender</option>
                                        <option value="Female" ${'Female' == gender ? "selected":""}>Female</option>        
                                        <option value="Male" ${'Male' == gender ? "selected":""}>Male</option>   
                                    </select>
                                    <div>
                                        <select name="role">
                                            <option>Role</option>
                                            <c:forEach items="${listr}" var="r">
                                                <option value="${r.roleId}" ${r.roleId == role ? "selected" : ""}>${r.role_name}</option>                  
                                            </c:forEach>
                                        </select>
                                    </div>                                   
                                    <div>
                                        <select name="status">
                                            <option>Status</option>
                                            <c:forEach items="${listas}" var="s">
                                                <option value="${s.statusId}" ${s.statusId == status ? "selected" : ""}>${s.status_name}</option>                  
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <input type="submit" value="Filter"/>                                              
                                </div>
                            </form>
                            <div style="margin-left: 15px;">
                                <a href="adduser"><button type="button" class="btn btn-danger">Add User</button> </a>
                            </div>
                            <div class="input-group rounded search-p">
                                <form action="searchuser">
                                    <input type="search" name="search" class="form-control rounded" placeholder="Search" value="${search}" aria-label="Search" aria-describedby="search-addon" style="max-width: 95%" />
                                </form>
                                <span class="input-group-text border-0" id="search-addon">
                                    <i class="fas fa-search"></i>
                                </span>
                            </div> 
                        </div>
                        <div class="card">
                            <h5 class="card-header">List User</h5>
                            <div class="card-body p-0">
                                <div class="table-responsive">

                                    <table class="table" id="userTable">
                                        <thead class="bg-light">
                                            <tr class="border-0">
                                                <th class="border-0" onclick="sortTable(0)">ID</th>
                                                <th class="border-0" onclick="sortTable(1)">Full Name</th>
                                                <th class="border-0" onclick="sortTable(2)">Gender</th>
                                                <th class="border-0" onclick="sortTable(3)">Email</th>
                                                <th class="border-0" onclick="sortTable(4)">Phone Number</th>
                                                <th class="border-0" onclick="sortTable(5)">Role</th>
                                                <th class="border-0" onclick="sortTable(6)">Status</th>
                                                <th class="border-0" >Feature</th>
                                                <th class="border-0" ">Feature</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${listacc}" var="a">
                                                <tr>                                                  
                                                    <td>${a.user_id}</td>
                                                    <td>
                                                        ${a.name}
                                                    </td>
                                                    <td>${a.gender eq "Male" ? "Male" : "Female"}</td>
                                                    <td>${a.email}</td>
                                                    <td>${a.phone_number}</td>
                                                    <td>${a.role_name}</td>
                                                    <td>${a.status_name}</td>
                                                    <td>
                                                        <c:choose>
                                                            <c:when test="${a.status_name eq 'Inactive'}">  
                                                                <a href="change?id=${a.user_id}&status=1"  onclick="return confirmAction()" class="btn btn-xs btn-success active"> <span class="ion-checkmark">Active</span></a></td>
                                                            </c:when>
                                                            <c:when test="${a.status_name eq 'Active'}">              
                                                    <a href="change?id=${a.user_id}&status=2"  onclick="return confirmAction()" class="btn btn-xs btn-danger inactive"> <span class="ion-android-close">InActive</span></a></td>
                                                </c:when>
                                            </c:choose>


                                            <td>  <a class="fas fa-edit edit-icon" href="edituser?id=${a.user_id}"></a> <a class="fas fa-eye view-icon" href="viewuser?id=${a.user_id}"></a></td>
                                            </tr> 
                                        </c:forEach>
                                        <c:if test="${not empty error}">
                                            <tr>
                                                <td colspan="9" class="error-message" style="color: red;">${error}</td>
                                            </tr>
                                        </c:if>
                                        </tbody>
                                    </table>    
                                    <nav aria-label="Page navigation example">
                                        <ul class="pagination justify-content-end">
                                            <c:forEach begin="1" end="${endp}" var="i">
                                                <li class="page-item ${i == currentPage ? "active" : ''}"><a class="page-link" href="usermanager?index=${i}">${i}</a></li>
                                                </c:forEach>


                                            <c:choose>
                                                <c:when test="${gender == 'Female'}">

                                                    <c:forEach begin="1" end="${endF}" var="i">
                                                        <li class="page-item ${i == indexf ? 'active' : ''}">
                                                            <a class="page-link" href="filteruser?gender=Female&status=Status&role=Role&indexf=${i}">${i}</a>
                                                        </li>
                                                    </c:forEach>

                                                </c:when>
                                                <c:when test="${gender == 'Male'}">

                                                    <c:forEach begin="1" end="${endM}" var="i">
                                                        <li class="page-item ${i == indexm ? 'active' : ''}">
                                                            <a class="page-link" href="filteruser?gender=Male&status=Status&role=Role&indexm=${i}">${i}</a>
                                                        </li>
                                                    </c:forEach>

                                                </c:when>
                                            </c:choose>

                                            <c:forEach begin="1" end="${endC}" var="i">
                                                <li class="page-item ${i == indexc ? 'active' : ''}">
                                                    <a class="page-link" href="filteruser?gender=Gender&status=Status&role=${role}&indexc=${i}">${i}</a>
                                                </li>
                                            </c:forEach>

                                            <c:choose>
                                                <c:when test="${status eq '1'}">

                                                    <c:forEach begin="1" end="${endA}" var="i">
                                                        <li class="page-item ${i == indexa ? 'active' : ''}">
                                                            <a class="page-link" href="filteruser?gender=Gender&status=1&role=Role&indexa=${i}">${i}</a>
                                                        </li>
                                                    </c:forEach>
                                                </c:when>
                                                <c:otherwise>

                                                    <c:forEach begin="1" end="${endI}" var="i">
                                                        <li class="page-item ${i == indexi ? 'active' : ''}">
                                                            <a class="page-link" href="filteruser?gender=Gender&status=2&role=Role&indexi=${i}">${i}</a>
                                                        </li>
                                                    </c:forEach>

                                                </c:otherwise>
                                            </c:choose>


                                            <c:forEach begin="1" end="${endGR}" var="i">
                                                <li class="page-item ${i == indexgr ? 'active' : ''}">
                                                    <a class="page-link" href="filteruser?gender=${gender}&status=Status&role=${role}&indexgr=${i}">${i}</a>
                                                </li>
                                            </c:forEach>


                                            <c:forEach begin="1" end="${endGS}" var="i">
                                                <li class="page-item ${i == indexgs ? 'active' : ''}">
                                                    <a class="page-link" href="filteruser?gender=${gender}&status=${status}&role=Role&indexgs=${i}">${i}</a>
                                                </li>
                                            </c:forEach>


                                            <c:forEach begin="1" end="${endRS}" var="i">
                                                <li class="page-item ${i == indexrs ? 'active' : ''}">
                                                    <a class="page-link" href="filteruser?gender=Gender&status=${status}&role=${role}&indexrs=${i}">${i}</a>
                                                </li>
                                            </c:forEach>


                                            <c:forEach begin="1" end="${end}" var="i">
                                                <li class="page-item ${i == index ? 'active' : ''}">
                                                    <a class="page-link" href="filteruser?gender=${gender}&status=${status}&role=${role}&index=${i}">${i}</a>
                                                </li>
                                            </c:forEach>


                                            <c:forEach begin="1" end="${endSearch}" var="i">
                                                <li class="page-item ${i == indexSearch ? 'active' : ''}">
                                                    <a class="page-link" href="searchuser?search=${search}&indexSearch=${i}">${i}</a>
                                                </li>
                                            </c:forEach>
                                        </ul>                                                   
                                    </nav>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <!-- ============================================================== -->
                <!-- footer -->
                <!-- ============================================================== -->
                <div class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                Copyright � 2018 Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="text-md-right footer-links d-none d-sm-block">
                                    <a href="javascript: void(0);">About</a>
                                    <a href="javascript: void(0);">Support</a>
                                    <a href="javascript: void(0);">Contact Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end footer -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- end wrapper  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end main wrapper  -->
        <!-- ============================================================== -->
        <!-- Optional JavaScript -->
        <!-- jquery 3.3.1 js-->
        <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
        <!-- bootstrap bundle js-->
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
        <!-- slimscroll js-->
        <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
        <!-- chartjs js-->
        <script src="assets/vendor/charts/charts-bundle/Chart.bundle.js"></script>
        <script src="assets/vendor/charts/charts-bundle/chartjs.js"></script>

        <!-- main js-->
        <script src="assets/libs/js/main-js.js"></script>
        <!-- jvactormap js-->
        <script src="assets/vendor/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
        <script src="assets/vendor/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- sparkline js-->
        <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
        <script src="assets/vendor/charts/sparkline/spark-js.js"></script>
        <!-- dashboard sales js-->
        <script src="assets/libs/js/dashboard-sales.js"></script>
        <script>
                                                        function sortTable(colIndex) {
                                                            var table = document.getElementById('userTable');
                                                            var rows = table.rows;
                                                            var switching = true;
                                                            var shouldSwitch = false;
                                                            var dir = 'asc';

                                                            while (switching) {
                                                                switching = false;
                                                                for (var i = 1; i < rows.length - 1; i++) {
                                                                    shouldSwitch = false;
                                                                    var x = rows[i].getElementsByTagName('td')[colIndex];
                                                                    var y = rows[i + 1].getElementsByTagName('td')[colIndex];
                                                                    if (colIndex === 0) { // If sorting by id column
                                                                        if (dir == 'asc') {
                                                                            if (parseInt(x.innerHTML) > parseInt(y.innerHTML)) {
                                                                                shouldSwitch = true;
                                                                                break;
                                                                            }
                                                                        } else if (dir == 'desc') {
                                                                            if (parseInt(x.innerHTML) < parseInt(y.innerHTML)) {
                                                                                shouldSwitch = true;
                                                                                break;
                                                                            }
                                                                        }
                                                                    } else {
                                                                        if (dir == 'asc') {
                                                                            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                                                                                shouldSwitch = true;
                                                                                break;
                                                                            }
                                                                        } else if (dir == 'desc') {
                                                                            if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                                                                                shouldSwitch = true;
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                if (shouldSwitch) {
                                                                    rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                                                                    switching = true;
                                                                } else {
                                                                    if (dir == 'asc') {
                                                                        dir = 'desc';
                                                                    } else {
                                                                        dir = 'asc';
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        function confirmAction() {
                                                            return confirm("Are you sure you want to proceed with this action?");
                                                        }
// JavaScript


        </script>
    </body>

</html>