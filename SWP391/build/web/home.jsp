
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.Product, java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>



<!DOCTYPE jsp>

<html lang="zxx">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Male_Fashion Template">
        <meta name="keywords" content="Male_Fashion, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Male-Fashion | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap"
              rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <style>
            .product__item__text h6 {
                min-height: 40px; /* Set a minimum height for the product name container */
                display: flex;
                align-items: center; /* Vertically center the content */
            }

            .product__item__text h6:hover {
                white-space: normal; /* Display full text on hover */
            }

        </style>

    </head>

    <body>
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>



        <!-- Header Section Begin -->
        <header class="header">
            <div class="header__top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-7">
                            <div class="header__top__left">
                                <c:if test="${sessionScope.message != null}">
                                    <div id="message" class="notification" style="color: white ">
                                        ${sessionScope.message}
                                    </div>
                                    <script>
                                        // Hien thi thong bao 5s
                                        setTimeout(function () {
                                            var messageDiv = document.getElementById("message");
                                            if (messageDiv) {
                                                messageDiv.style.display = "none";

                                        <%
                                                    session.removeAttribute("message");
                                        %>
                                            }
                                        }, 5000);
                                    </script>
                                </c:if>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-5">
                            <div class="header__top__right">
                                <div class="header__top__links">
                                    <c:if test="${sessionScope.acc==null}">
                                        <a href="login">Sign in</a> 
                                    </c:if>


                                </div>
                                <c:if test="${sessionScope.acc!=null}">
                                    <div class="header__top__hover">

                                        <span>${sessionScope.acc.name} <i class="arrow_carrot-down"></i></span>
                                        <ul>
                                            <li><a href="userprofile?uid=${sessionScope.acc.email}">Profile</a> </li>
                                                <c:choose>
                                                    <c:when test="${sessionScope.acc.roleId eq 1}">
                                                    <li><a href="admindashboard">Dashboard</a></li>
                                                    </c:when>
                                                    <c:when test="${sessionScope.acc.roleId eq 2}">
                                                    <li><a href="salemanagerdashboard">Dashboard</a></li>
                                                    </c:when>
                                                    <c:when test="${sessionScope.acc.roleId eq 3}">
                                                    <li><a href="ordermanager">Dashboard</a></li>
                                                    </c:when>
                                                    <c:when test="${sessionScope.acc.roleId eq 4}">
                                                    <li><a href="dashboardmkt">Dashboard</a></li>
                                                    </c:when>
                                                    <c:when test="${sessionScope.acc.roleId eq 5}">
                                                    <li><a href="myorder?acid=${sessionScope.acc.user_id}">My Order</a> </li>
                                                    </c:when>
                                                </c:choose>
                                            <li><a href="logout">Log out</a></li>
                                        </ul>


                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div class="header__logo">
                            <a href="./home"><img src="img/logo.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li class="active"><a href="./home">Home</a></li>
                                <li><a href="./shop">Shop</a></li>

                                <li><a href="./blog">Blog</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <div class="header__nav__option">
                            <a href="#" class="search-switch"><img src="img/icon/search.png" alt=""></a>
                                <c:if test="${sessionScope.acc.roleId eq 5}">
                                <a href="viewcart"><img src="img/icon/cart.png" alt=""> <span>${size}</span></a>
                                <div class="price original-price">${total}đ</div>
                            </c:if>
                        </div>
                    </div>
                </div>
                <div class="canvas__open"><i class="fa fa-bars"></i></div>
            </div>
        </header>
        <!-- Header Section End -->

        <!-- Hero Section Begin -->
        <section class="hero">
            <div class="hero__slider owl-carousel">
                <c:forEach items="${listBA}" var="c">
                    <div class="hero__items set-bg" data-setbg="${c.banImg}">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-5 col-lg-7 col-md-8">
                                    <div class="hero__text">
                                        <!--                                    <h6>Summer Collection</h6>
                                                                            <h2>Fall - Winter Collections 2024</h2>
                                                                            <p>A specialist label creating luxury essentials. Ethically crafted with an unwavering
                                                                                commitment to exceptional quality.</p>
                                        -->
                                        <br><br><br><br><br><br><br><br><br><br><br>
                                        <a style="right: 20px;" href="filter?bid=${c.bid}&cid=&price1=&price2=" class="primary-btn">Shop now <span class="arrow_right"></span></a>
                                        <!--                                    <div class="hero__social">
                                                                                <a href="#"><i class="fa fa-facebook"></i></a>
                                                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                                                <a href="#"><i class="fa fa-pinterest"></i></a>
                                                                                <a href="#"><i class="fa fa-instagram"></i></a>
                                                                            </div>
                                        -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>

            </div>
        </section>
        <!-- Hero Section End -->

        <!-- Banner Section Begin -->
        <section class="banner spad">
            <div class="container">
                <div class="row">
                    <c:forEach items="${listAK}" var="c">
                        <div class="col-lg-7 offset-lg-4">
                            <div class="banner__item">
                                <div class="banner__item__pic">
                                    <img style="width: 440px; height: 440px;" src="${c.img1}" alt="">
                                </div>
                                <div class="banner__item__text">
                                    <h2>New Jacket</h2>
                                    <a href="filter?cid=5&bid=&price1=&price2=">Shop now</a>
                                </div>
                            </div>
                        </div>
                    </c:forEach>

                    <c:forEach items="${listPK}" var="c">
                        <div class="col-lg-5">
                            <div class="banner__item banner__item--middle">
                                <div class="banner__item__pic">
                                    <img style="width: 458px; height: 400px;" src="${c.img1}" alt="">
                                </div>
                                <div class="banner__item__text">
                                    <h2>Accessories</h2>
                                    <a href="filter?cid=9&bid=&price1=&price2=">Shop now</a>
                                </div>
                            </div>
                        </div>
                    </c:forEach>

                    <c:forEach items="${listBT}" var="c">
                        <div class="col-lg-7">
                            <div class="banner__item banner__item--last">
                                <div class="banner__item__pic">
                                    <img style="width: 440px; height: 440px" src="${c.img1}" alt="">
                                </div>
                                <div class="banner__item__text">
                                    <h2>Baby Tee</h2>
                                    <a href="filter?cid=3&bid=&price1=&price2=">Shop now</a>
                                </div>
                            </div>
                        </div>
                    </c:forEach>

                </div>
            </div>
        </section>
        <!-- Banner Section End -->

        <!-- Product Section Begin -->
        <section class="product spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="filter__controls">
                            <li class="active" data-filter=".best-sellers">Best Sellers</li>
                            <li data-filter=".new-arrivals">New Arrivals</li>
                            <!--                        <li data-filter=".hot-sales">Hot Sales</li>-->
                        </ul>
                    </div>
                </div>
                <div class="row product__filter">
                    <c:forEach items="${listBS}" var="o">
                        <div class="col-lg-3 col-md-6 col-sm-6 col-md-6 col-sm-6 mix best-sellers">
                            <div class="product__item">
                                <div class="product__item__pic set-bg" data-setbg="${o.img}">
                                    <span class="label">New</span>
                                    <ul class="product__hover">
                                        <!--                                        <li><a href="#"><img src="img/icon/heart.png" alt=""></a></li>
                                                                                <li><a href="#"><img src="img/icon/compare.png" alt=""> <span>Compare</span></a></li>-->
                                        <li><a href="productdetails?pid=${o.productId}"><img src="img/icon/search.png" alt=""></a></li>
                                    </ul>
                                </div>
                                <div class="product__item__text">
                                    <h6>${o.pname}</h6>
                                    <c:if test="${sessionScope.acc.roleId eq 5 || sessionScope.acc.roleId eq null}">
                                        <a href="addtocart?id=${o.productId}" class="add-cart">+ Add To Cart</a>
                                    </c:if>
                                    <div class="rating">
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                    </div>
                                    <h5 class="original-price">${o.price} đ</h5>
                                    <div class="product__color__select">
                                        <label for="pc-1">
                                            <input type="radio" id="pc-1">
                                        </label>
                                        <label class="active black" for="pc-2">
                                            <input type="radio" id="pc-2">
                                        </label>
                                        <label class="grey" for="pc-3">
                                            <input type="radio" id="pc-3">
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>

                    <c:forEach items="${listNA}" var="list">
                        <div class="col-lg-3 col-md-6 col-sm-6 col-md-6 col-sm-6 mix new-arrivals">
                            <div class="product__item">
                                <div class="product__item__pic set-bg" data-setbg="${list.img1}">
                                    <span class="label">New</span>
                                    <ul class="product__hover">
                                        <!--                                        <li><a href="#"><img src="img/icon/heart.png" alt=""></a></li>
                                                                                <li><a href="#"><img src="img/icon/compare.png" alt=""> <span>Compare</span></a></li>-->
                                        <li><a href="productdetails?pid=${list.pid}"><img src="img/icon/search.png" alt=""></a></li>
                                    </ul>
                                </div>
                                <div class="product__item__text">
                                    <h6>${list.pname}</h6>
                                    <c:if test="${sessionScope.acc.roleId eq 5 || sessionScope.acc.roleId eq null}">
                                        <a href="addtocart?id=${list.pid}" class="add-cart">+ Add To Cart</a>
                                    </c:if>
                                    <div class="rating">
                                        <c:forEach begin="1" end="${list.rate}" var="i">
                                            <i class="fa fa-star"  style="color: #f7941d"></i>
                                        </c:forEach>
                                        <c:forEach begin="${list.rate + 1}" end="5" var="i">
                                            <i class="fa fa-star-o"></i>
                                        </c:forEach>
                                    </div>
                                    <h5 class="original-price">${list.price} đ</h5>
                                    <div class="product__color__select">
                                        <label for="pc-1">
                                            <input type="radio" id="pc-1">
                                        </label>
                                        <label class="active black" for="pc-2">
                                            <input type="radio" id="pc-2">
                                        </label>
                                        <label class="grey" for="pc-3">
                                            <input type="radio" id="pc-3">
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>

                </div>
            </div>
        </section>
        <!-- Product Section End -->

        <!-- Categories Section Begin -->

        <!-- Categories Section End -->

        <!-- Instagram Section Begin -->
        <section class="instagram spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="instagram__pic">
                            <div class="instagram__pic__item set-bg" data-setbg="img/instagram/instagram-1.jpg"></div>
                            <div class="instagram__pic__item set-bg" data-setbg="img/instagram/instagram-2.jpg"></div>
                            <div class="instagram__pic__item set-bg" data-setbg="img/instagram/instagram-3.jpg"></div>
                            <div class="instagram__pic__item set-bg" data-setbg="img/instagram/instagram-4.jpg"></div>
                            <div class="instagram__pic__item set-bg" data-setbg="img/instagram/instagram-5.jpg"></div>
                            <div class="instagram__pic__item set-bg" data-setbg="img/instagram/instagram-6.jpg"></div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="instagram__text">
                            <h2>Instagram</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                labore et dolore magna aliqua.</p>
                            <h3>#Male_Fashion</h3>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Instagram Section End -->

        <!-- Latest Blog Section Begin -->
        <section class="latest spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title">
                            <span>Latest News</span>
                            <h2>Fashion New Trends</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <c:forEach items="${listB}" var="c">
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="blog__item">
                                <div class="blog__item__pic set-bg" data-setbg="${c.thumbnail}"></div>
                                <div class="blog__item__text">
                                    <span><img src="img/icon/calendar.png" alt="">${c.date}</span>
                                    <h5 style="display: -webkit-box; -webkit-box-orient: vertical; overflow: hidden; -webkit-line-clamp: 2;">${c.title}</h5>
                                    <a href="blogdetail?blog_id=${c.blog_id}">Read More</a>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </section>
        <!-- Latest Blog Section End -->

        <!-- Footer Section Begin -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="footer__about">
                            <div class="footer__logo">
                                <a href="#"><img src="img/footer-logo.png" alt=""></a>
                            </div>
                            <p>The customer is at the heart of our unique business model, which includes design.</p>
                            <a href="#"><img src="img/payment.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-2 offset-lg-1 col-md-3 col-sm-6">
                        <div class="footer__widget">
                            <h6>Shopping</h6>
                            <ul>
                                <li><a href="#">Clothing Store</a></li>
                                <li><a href="#">Trending Shoes</a></li>
                                <li><a href="#">Accessories</a></li>
                                <li><a href="#">Sale</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-6">
                        <div class="footer__widget">
                            <h6>Shopping</h6>
                            <ul>
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">Payment Methods</a></li>
                                <li><a href="#">Delivery</a></li>
                                <li><a href="#">Return & Exchanges</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 offset-lg-1 col-md-6 col-sm-6">
                        <div class="footer__widget">
                            <h6>NewLetter</h6>
                            <div class="footer__newslatter">
                                <p>Be the first to know about new arrivals, look books, sales & promos!</p>
                                <form action="#">
                                    <input type="text" placeholder="Your email">
                                    <button type="submit"><span class="icon_mail_alt"></span></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </footer>
        <!-- Footer Section End -->

        <!-- Search Begin -->
        <div class="search-model">
            <div class="h-100 d-flex align-items-center justify-content-center">
                <div class="search-close-switch">+</div>
                <form class="search-model-form">
                    <input type="text" id="search-input" placeholder="Search here.....">
                </form>
            </div>
        </div>
        <!-- Search End -->

        <!-- Js Plugins -->
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery.nicescroll.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/jquery.countdown.min.js"></script>
        <script src="js/jquery.slicknav.js"></script>
        <script src="js/mixitup.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/main.js"></script>
        <script>
                                        document.addEventListener("DOMContentLoaded", function () {
                                            var originalPrices = document.querySelectorAll('.original-price');
                                            originalPrices.forEach(function (originalPrice) {
                                                originalPrice.textContent = formatPrice(originalPrice.textContent.trim());
                                            });

                                            function formatPrice(price) {
                                                return (parseFloat(price)).toLocaleString('vi-VN', {style: 'currency', currency: 'VND'}).replace('VND', '');
                                            }
                                        });
        </script>
    </body>

</html>