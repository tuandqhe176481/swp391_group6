
<!doctype html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
        <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/libs/css/style.css">
        <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="assets/vendor/vector-map/jqvmap.css">
        <link rel="stylesheet" href="assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css">
        <link rel="stylesheet" href="assets/vendor/fonts/flag-icon-css/flag-icon.min.css"><link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap"
              rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <title>My Order</title>
        

        <style>

            .filter {
                display: flex;
                align-items: center;
                margin-bottom: 10px;
            }
            .filter1 {
                display: flex;
                align-items: center;
            }

            .texta {
                margin-right: 10px;
                font-weight: bold;
                font-size: 18px;

            }

            select {
                padding: 8px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            /* T�y ch?nh c�c select n?m trong .filter */
            .filter select {
                margin-right: 10px;
            }

            /* T�y ch?nh m�u n?n khi hover tr�n select */
            .filter select:hover {
                background-color: #f5f5f5;
            }
            select:hover {
                background-color: #f5f5f5;
            }

            input[type="submit"] {
                padding: 10px 15px;
                background-color: #333;
                color: white;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            /* Hi?u ?ng hover cho n�t Filter */
            input[type="submit"]:hover {
                background-color: #454444f7;
            }

            .search-p{
                justify-content: right;
            }
            a.button {
                display: inline-block;
                padding: 8px 16px;
                text-align: center;
                text-decoration: none;
                border-radius: 4px;
                transition: background-color 0.3s;
            }


            a.button:hover {
                background-color: #ddd;
            }


            a.button:active {
                background-color: #ccc;
            }


            a.button.default {
                color: #333;
                background-color: #f0f0f0;
            }


            a.button.red {
                color: white;
                background-color: #e74c3c;
            }


            a.button.green {
                color: white;
                background-color: #2ecc71;
            }
            .date-form {
                display: flex;
                align-items: center;
            }

            .date-input {
                width: 45%;
                margin-right: 10px;
            }

            .date-divider {
                margin: 0 10px;
            }

            h4 {
                display: flex;
                align-items: center;
                justify-content: right;
            }

            input {
                padding: 8px;
                border: 1px solid #ccc;
                border-radius: 3px;
                outline: none;

            }

            a {
                color: #fff;
                text-decoration: none;
                margin: 0 10px;
            }


        </style>
    </head>

    <body>
        

        <!-- Offcanvas Menu Begin -->
        <div class="offcanvas-menu-overlay"></div>
        <div class="offcanvas-menu-wrapper">
            <div class="offcanvas__option">
                <div class="offcanvas__links">
                    <a href="#">Sign in</a>
                    <a href="#">FAQs</a>
                </div>
                <div class="offcanvas__top__hover">
                    <span>Usd <i class="arrow_carrot-down"></i></span>
                    <ul>
                        <li>USD</li>
                        <li>EUR</li>
                        <li>USD</li>
                    </ul>
                </div>
            </div>
            <div class="offcanvas__nav__option">
                <a href="#" class="search-switch"><img src="img/icon/search.png" alt=""></a>
                <a href="#"><img src="img/icon/heart.png" alt=""></a>
                <a href="#"><img src="img/icon/cart.png" alt=""> <span>0</span></a>
                <div class="price">$0.00</div>
            </div>
            <div id="mobile-menu-wrap"></div>
            <div class="offcanvas__text">
                <p>Free shipping, 30-day return or refund guarantee.</p>
            </div>
        </div>
        <!-- Offcanvas Menu End -->

        <!-- Header Section Begin -->
        <header class="header" style="background-color: pink">
            <div class="header__top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-7">
                            <div class="header__top__left">
                                <c:if test="${sessionScope.message != null}">
                                    <div id="message" class="notification" style="color: white ">
                                        ${sessionScope.message}
                                    </div>
                                    <script>
                                        // Hien thi thong bao 5s
                                        setTimeout(function () {
                                            var messageDiv = document.getElementById("message");
                                            if (messageDiv) {
                                                messageDiv.style.display = "none";

                                        <%
                                                    session.removeAttribute("message");
                                        %>
                                            }
                                        }, 5000);
                                    </script>
                                </c:if>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-5">
                            <div class="header__top__right">
                                <div class="header__top__links">
                                    <c:if test="${sessionScope.acc==null}">
                                        <a href="login">Sign in</a> 
                                    </c:if>


                                </div>
                                <c:if test="${sessionScope.acc!=null}">
                                    <div class="header__top__hover">

                                        <span>${sessionScope.acc.name} <i class="arrow_carrot-down"></i></span>
                                        <ul>
                                            <li><a style="color: black" href="userprofile?uid=${sessionScope.acc.email}">Profile</a> </li>
                                                <c:choose>
                                                    <c:when test="${sessionScope.acc.roleId eq 1}">
                                                    <li><a href="home">Dashboard</a></li>
                                                    </c:when>
                                                    <c:when test="${sessionScope.acc.roleId eq 2}">
                                                    <li><a href="home">Dashboard</a></li>
                                                    </c:when>
                                                    <c:when test="${sessionScope.acc.roleId eq 3}">
                                                    <li><a href="home">Dashboard</a></li>
                                                    </c:when>
                                                    <c:when test="${sessionScope.acc.roleId eq 4}">
                                                    <li><a href="dashboardmkt">Dashboard</a></li>
                                                    </c:when>
                                                    <c:when test="${sessionScope.acc.roleId eq 5}">
                                                    <li><a href="#" style="color: black">My Order</a> </li>
                                                    </c:when>
                                                </c:choose>
                                            <li><a href="logout"style="color: black">Log out</a></li>
                                        </ul>


                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div class="header__logo">
                            <a href="./home"><img src="img/logo.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li><a href="./home">Home</a></li>
                                <li><a href="./shop">Shop</a></li>
                                
                                <li><a href="./blog">Blog</a></li>
                                
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <div class="header__nav__option">
                            
                                <c:set value="${size}" var="size"/>
                                <c:set value="${total}" var="total"/>
                                <c:if test="${sessionScope.acc.roleId eq 5}">
                                <a href="viewcart"><img src="img/icon/cart.png" alt=""> <span>${size}</span></a>
                                <div class="price">$${total}</div>
                            </c:if>

                        </div>
                    </div>
                </div>
                <div class="canvas__open"><i class="fa fa-bars"></i></div>
            </div>
        </header>
      
           <div class="dashboard-main-wrapper">
               <div class="dashboard-wrapper">
                <div class="dashboard-ecommerce">
                    <div class="container-fluid dashboard-content " style="margin-left: -100px">
                        <!-- ============================================================== -->
                        <!-- pageheader  -->
                        <!-- ============================================================== -->

                        <!-- ============================================================== -->
                        <!-- end pageheader  -->
                        <!-- ============================================================== -->
                        <div class="ecommerce-widget">

                            <div class="row">
                                <!-- ============================================================== -->

                                <!-- ============================================================== -->

                                <!-- recent orders  -->
                                <!-- ============================================================== -->
                                <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                    <div class="filter1">





                                        <div class="input-group rounded search-p">
<!--                                            <form action="searchorder">
                                                <input type="search" name="search" class="form-control rounded" placeholder="Search" value="${search}" aria-label="Search" aria-describedby="search-addon" style="max-width: 95%" />

                                            </form>
                                            <span class="input-group-text border-0" id="search-addon">
                                                <i class="fas fa-search"></i>
                                            </span>-->
                                        </div> 
                                    </div>

                                    <div class="card">
                                        <div class="card-body p-0">
                                            <div class="table-responsive">

                                                <table class="table">
                                                    <thead class="bg-light">
                                                        <tr class="border-0">
                                                            <th class="border-0">Id</th>
                                                            <th class="border-0">Products</th>    
                                                            <th class="border-0">purchase date</th>
                                                            <th class="border-0">Receiver</th>                                                            
                                                            <th class="border-0">Total amount</th>                                                        
                                                            <th class="border-0">Status</th>
                                                                <c:if test="${sessionScope.acc.roleId eq 2}">
                                                                <th class="border-0">Sales</th>
                                                                </c:if>
                                                            <th class="border-0"></th>
                                                            

                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <h3 style="text-align: center;color: #003333;background-color: #ffffcc">My Order</h3>
                                                        <c:forEach items="${o}" var="l"  varStatus="loop">

                                                            <tr>

                                                                <td> <a href="orderinfo?oid=${l.oid}" style="color: black">${l.oid} </a> </td>
                                                                <c:if test="${l.quantity>1}">
                                                                    <td><a href="productdetails?pid=${l.pid}" style="color: black">${l.receiver}</a> and ${l.quantity-1} other products </td> 
                                                                </c:if>
                                                                <c:if test="${l.quantity <= 1}">
                                                                    <td><a href="productdetails?pid=${l.pid}" style="color: black">${l.receiver}</a></td>
                                                                    </c:if>

                                                                <td>${l.ordered_at}</td>
                                                                <td>${l.pname}</td>                                                                
                                                                <td>${l.totalAmount}</td>
                                                                <td>
                                                                    ${l.statusId == 1 ? 'Pending Confirmation' :
                                                                      (l.statusId == 2 ? 'Confirmed' :
                                                                      (l.statusId == 3 ? 'Shipping' :
                                                                      (l.statusId == 4 ? 'Completed' :
                                                                      (l.statusId == 5 ? 'Cancel' :
                                                                      'Unknown'))))}
                                                                </td>
                                                                <c:if test="${l.statusId==1}">
                                                                <td><a class="fas fa-edit edit-icon" href="cancelorder?oid=${l.oid}" style="color: black"></a></td>
                                                                </c:if>
                                                                <c:if test="${l.statusId!=1}">
                                                                <td></td>
                                                                </c:if>


                                                            </tr> 


                                                        </c:forEach>                                      

                                                    </tbody>

                                                </table>


                                                <%-- Ph�n trang cho product--%>
                                                <nav aria-label="Page navigation example">
                                                    <ul class="pagination justify-content-end">

                                                        <c:forEach begin="1" end="${num}" var="n">
                                                            <li class="page-item ${n ==page ? "active" : ''}"><a class="page-link" href="ordermanagersm?page=${n}">${n}</a></li>
                                                            </c:forEach>

                                                    </ul>                                                   
                                                </nav>
                                                <%-- Ph�n trang cho product availible--%>
                                                <nav aria-label="Page navigation example">
                                                    <ul class="pagination justify-content-end">
                                                        <c:forEach begin="1" end="${num1}" var="n1">
                                                            <li class="page-item ${n1 ==page1 ? "active" : ''}"><a class="page-link" href="filtersalemanager?status=${select}&&sale=${sale}&&startDate=${start}&&endDate=${end}&&page1=${n1}">${n1}</a></li>
                                                            </c:forEach>    
                                                    </ul>  
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ============================================================== -->
                                <!-- end recent orders  -->


                                <!-- ============================================================== -->
                                <!-- ============================================================== -->
                                <!-- customer acquistion  -->
                                <!-- ============================================================== -->
                                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">

                                </div>
                                <!-- ============================================================== -->
                                <!-- end customer acquistion  -->
                                <!-- ============================================================== -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <!-- jquery 3.3.1 js-->
    <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstrap bundle js-->
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js-->
    <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- chartjs js-->
    <script src="assets/vendor/charts/charts-bundle/Chart.bundle.js"></script>
    <script src="assets/vendor/charts/charts-bundle/chartjs.js"></script>

    <!-- main js-->
    <script src="assets/libs/js/main-js.js"></script>
    <!-- jvactormap js-->
    <script src="assets/vendor/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="assets/vendor/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- sparkline js-->
    <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <script src="assets/vendor/charts/sparkline/spark-js.js"></script>
    <!-- dashboard sales js-->
    <script src="assets/libs/js/dashboard-sales.js"></script>
    <script>
        function submitSale(selectedValue, oid) {
            window.location.href = 'changesale?user_id=' + selectedValue + "&oid=" + oid;
        }
    </script>




</body>

</html>