
<!doctype html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.io.*,java.util.*" %>
<%@ page import="javax.servlet.*, javax.servlet.http.*" %>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
        <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/libs/css/style.css">
        <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="assets/vendor/charts/chartist-bundle/chartist.css">
        <link rel="stylesheet" href="assets/vendor/charts/morris-bundle/morris.css">
        <link rel="stylesheet" href="assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="assets/vendor/charts/c3charts/c3.css">
        <link rel="stylesheet" href="assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <!------ Include the above in your HEAD tag ---------->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
        <title>Concept - Bootstrap 4 Admin Dashboard Template</title>

        <style>


        </style>
    </head>

    <body>
        <!-- ============================================================== -->
        <!-- main wrapper -->
        <!-- ============================================================== -->
        <div class="dashboard-main-wrapper">
            <!-- ============================================================== -->
            <!-- navbar -->
            <!-- ============================================================== -->
            <div class="dashboard-header">
                <nav class="navbar navbar-expand-lg bg-white fixed-top">
                    <a class="navbar-brand" href="index.html">Concept</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse " id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto navbar-right-top">
                            <li class="nav-item">
                                <div id="custom-search" class="top-search-bar">
                                    <input class="form-control" type="text" placeholder="Search..">
                                </div>
                            </li>
                            <li class="nav-item dropdown notification">
                                <a class="nav-link nav-icons" href="#" id="navbarDropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-fw fa-bell"></i> <span class="indicator"></span></a>

                        </ul>
                        </li>

                        <li class="nav-item dropdown nav-user">
                            <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="assets/images/avatar-1.jpg" alt="" class="user-avatar-md rounded-circle"></a>
                            <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                                <div class="nav-user-info">
                                    <h5 class="mb-0 text-white nav-user-name">John Abraham </h5>
                                    <span class="status"></span><span class="ml-2">Available</span>
                                </div>
                                <a class="dropdown-item" href="#"><i class="fas fa-user mr-2"></i>Account</a>
                                <a class="dropdown-item" href="#"><i class="fas fa-cog mr-2"></i>Setting</a>
                                <a class="dropdown-item" href="#"><i class="fas fa-power-off mr-2"></i>Logout</a>
                            </div>
                        </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <!-- ============================================================== -->
            <!-- end navbar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- left sidebar -->
            <!-- ============================================================== -->
            <div class="nav-left-sidebar sidebar-dark">
                <div class="menu-list">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav flex-column">
                                <li class="nav-divider">
                                    Menu
                                <li class="nav-item ">
                                    <a class="nav-link active" href="dashboardmkt" onclick="setActive(this)">
                                        <i class="fa fa-fw fa-user-circle"></i>
                                        Dashboard
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="productmanager" onclick="setActive(this)">
                                        <i class="fas fa-fw fa-chart-pie"></i>
                                        Product Manager
                                    </a>
                                </li>
                            </ul>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end left sidebar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- wrapper  -->
            <!-- ============================================================== -->
            <div class="dashboard-wrapper">
                <div class="dashboard-ecommerce">
                    <div class="container-fluid dashboard-content">

                        <form action="addblog" class="form-horizontal" method="post" enctype="multipart/form-data">
                            <fieldset>
                                <!-- Form Name -->
                                <legend>BLOG DETAILS</legend>

                                <div class="row">
                                    <!-- Image Section -->
                                    <div class="col-md-6">
                                        <!-- Thumbnail -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="filebutton1">Thumbnail</label>
                                            <div class="col-md-8">
                                                <img id="previewImg1" src="${b.thumbnail}" alt="Preview Image" style="max-width: 100%; max-height: 250px;">
                                                <input id="filebutton1" name="thumbnail" class="input-file" type="file" onchange="displayImage(this, 'previewImg1')">
                                            </div>
                                        </div>

                                    </div>



                                    <!-- Input Section -->
                                    <div class="col-md-5">
                                        <!-- Product ID -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_id">BLOG ID</label>
                                            <div class="col-md-8">
                                                <input id="product_id" name="pid" placeholder="BLOG ID" class="form-control input-md" type="text" readonly="" ">
                                            </div>
                                        </div>

                                        <!-- Categories -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="category">Categories</label>
                                            <div class="col-md-8">
                                                <select name="category" class="form-control input-md" id="category">
                                                    <option>Select Category</option>
                                                    <c:forEach items="${listBC}" var="bc">
                                                        <option value="${bc.bc_id}" >${bc.bc_name}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>


                                        <!-- Title -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_name">Title</label>
                                            <div class="col-md-8">
                                                <input id="product_name" name="name" placeholder="Title" class="form-control input-md" required="" type="text" ">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="author">Author</label>
                                            <div class="col-md-8">
                                                <input id="author" name="author" placeholder="Author" class="form-control input-md" type="text" readonly="" ">
                                            </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_name">Status</label>
                                            <div class="col-md-8">
                                                <select name="status" class="form-control input-md" id="product_name">
                                                    <option>Status</option>
                                                    <c:forEach items="${listSta}" var="s">
                                                        <option value="${s.sid}">${s.status}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_name">Brief</label>
                                            <div class="col-md-8">
                                                <textarea id="product_name" name="brief" placeholder="Brief" class="form-control input-md" required="" rows="4"></textarea>
                                            </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_name">Description</label>
                                            <div class="col-md-8">
                                                <textarea id="product_name" name="description" placeholder="BLOG DESCRIPTION" class="form-control input-md" required="" rows="4"></textarea>
                                            </div>
                                        </div>

                                        <!-- Button -->
                                        <div class="form-group">
                                            <div class="col-md-8 col-md-offset-4">
                                                <button id="singlebutton" name="singlebutton" class="btn btn-primary">ADD</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>       
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <div class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        Copyright � 2018 Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="text-md-right footer-links d-none d-sm-block">
                            <a href="javascript: void(0);">About</a>
                            <a href="javascript: void(0);">Support</a>
                            <a href="javascript: void(0);">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="assets/libs/js/dashboard-ecommerce.js"></script>


    <script>
                                                    function displayImage(input, imgId) {
                                                        var preview = document.getElementById(imgId);
                                                        var file = input.files[0];
                                                        var reader = new FileReader();

                                                        reader.onloadend = function () {
                                                            preview.src = reader.result;
                                                        }

                                                        if (file) {
                                                            reader.readAsDataURL(file);
                                                        } else {
                                                            preview.src = "";
                                                        }
                                                    }
    </script>

</body>

</html>