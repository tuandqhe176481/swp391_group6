
<!doctype html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
        <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/libs/css/style.css">
        <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="assets/vendor/charts/chartist-bundle/chartist.css">
        <link rel="stylesheet" href="assets/vendor/charts/morris-bundle/morris.css">
        <link rel="stylesheet" href="assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="assets/vendor/charts/c3charts/c3.css">
        <link rel="stylesheet" href="assets/vendor/fonts/flag-icon-css/flag-icon.min.css">

        
        <title>Concept - Bootstrap 4 Admin Dashboard Template</title>

        <style>
            .error-message {
                text-align: center; /* c?n gi?a n?i dung */
                font-weight: bold; /* l�m n?i b?t */
                color: red; /* m�u s?c */
                font-size: larger;
            }
            .filter {
                display: flex;
                align-items: center;
                margin-bottom: 10px;
            }
            .filter1 {
                display: flex;
                align-items: center;
            }

            .texta {
                margin-right: 10px;
                font-weight: bold;
                font-size: 18px;

            }

            select {
                padding: 8px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            /* T�y ch?nh c�c select n?m trong .filter */
            .filter select {
                margin-right: 10px;
            }

            /* T�y ch?nh m�u n?n khi hover tr�n select */
            .filter select:hover {
                background-color: #f5f5f5;
            }
            select:hover {
                background-color: #f5f5f5;
            }

            input[type="submit"] {
                padding: 10px 15px;
                background-color: #4CAF50;
                color: white;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            /* Hi?u ?ng hover cho n�t Filter */
            input[type="submit"]:hover {
                background-color: #45a049;
            }

            .search-p{
                justify-content: right;
            }
            a.button {
                display: inline-block;
                padding: 8px 16px;
                text-align: center;
                text-decoration: none;
                border-radius: 4px;
                transition: background-color 0.3s;
            }


            a.button:hover {
                background-color: #ddd;
            }


            a.button:active {
                background-color: #ccc;
            }


            a.button.default {
                color: #333;
                background-color: #f0f0f0;
            }


            a.button.yellow {
                color: white;
                background-color: greenyellow;
            }
            a.button.red {
                color: white;
                background-color: red;
            }
            
            .date-form {
                display: flex;
                align-items: center;
            }

            .date-input {
                width: 45%;
                margin-right: 10px;
            }

            .date-divider {
                margin: 0 10px;
            }

            h4 {
                display: flex;
                align-items: center;
                justify-content: right;
            }

            input {
                padding: 8px;
                border: 1px solid #ccc;
                border-radius: 3px;
                outline: none;

            }

            a {
                color: #fff;
                text-decoration: none;
                margin: 0 10px;
            }

           
        </style>
    </head>

    <body>
        <!-- ============================================================== -->
        <!-- main wrapper -->
        <!-- ============================================================== -->
        <div class="dashboard-main-wrapper">
            <!-- ============================================================== -->
            <!-- navbar -->
            <!-- ============================================================== -->
            <div class="dashboard-header">
                <nav class="navbar navbar-expand-lg bg-white fixed-top">
                    <a class="navbar-brand" href="home">GenzFashion</a>

                </nav>
            </div>
            <!-- ============================================================== -->
            <!-- end navbar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- left sidebar -->
            <!-- ============================================================== -->
            <div class="nav-left-sidebar sidebar-dark">
                <div class="menu-list">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav flex-column">
                                <li class="nav-divider">
                                    Dashboard-Inventory
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="dashboardinventory" onclick="setActive(this)">
                                        <i class="fa fa-fw fa-user-circle"></i>
                                        Dashboard
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end left sidebar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- wrapper  -->
            <!-- ============================================================== -->
            <div class="dashboard-wrapper">
                <div class="dashboard-ecommerce">
                    <div class="container-fluid dashboard-content ">
                        <!-- ============================================================== -->
                        <!-- pageheader  -->
                        <!-- ============================================================== -->
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="page-header">
                                    <h2 class="pageheader-title">Order Manager</h2>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end pageheader  -->
                        <!-- ============================================================== -->
                        <div class="ecommerce-widget">

                            <div class="row">
                                <!-- ============================================================== -->

                                <!-- ============================================================== -->

                                <!-- recent orders  -->
                                <!-- ============================================================== -->
                                <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                    <div class="filter1">
                                        <form action="filterinventory">
                                            <div class="filter">
                                                <a class="texta" style=" color: #454444f7;" >Filter:</a>
                                                 <div class="date-form">
                                                     <input value="${start}" type="datetime-local" name="startDate" class="date-input" />
                                                <span class="date-divider">To</span>
                                                <input value="${end}" type="datetime-local" name="endDate" class="date-input" />
                                                <input type="submit" value="Find" class="submit-btn" />
                                            </div>
                                                                                             
                                            </div>
                                        </form>

                                        <div class="input-group rounded search-p">
                                            <form action="searchinventory">
                                                <input type="search" name="search" class="form-control rounded" placeholder="Search" value="${search}" aria-label="Search" aria-describedby="search-addon" style="max-width: 95%" />

                                            </form>
                                            <span class="input-group-text border-0" id="search-addon">
                                                <i class="fas fa-search"></i>
                                            </span>
                                        </div> 
                                    </div>

                                    <div class="card">
                                        <div class="card-body p-0">
                                            <div class="table-responsive">

                                                <table class="table">
                                                    <thead class="bg-light">
                                                        <tr class="border-0">
                                                            <th class="border-0">Id</th>
                                                            <th class="border-0">Image</th>
                                                            <th class="border-0">Products</th>    
                                                            <th class="border-0">purchase date</th>
                                                            <th class="border-0">Receiver</th>                                                            
                                                            <th class="border-0">Total amount</th>                                                        
                                                            <th class="border-0">Status</th>
                                                            <th class="border-0"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <c:forEach items="${list}" var="l" varStatus="loop">

                                                            <tr>

                                                                <td> <a href="orderdetail?oid=${l.oid}" style="color: black; text-decoration: none;">${l.oid} </a> </td>
                                                                <td>
                                                                    <div class="m-r-10"><img src="${l.img}" alt="user" class="rounded" width="45"></div>
                                                                </td>
                                                                <c:if test="${l.quantityP>1}">
                                                                    <td>${l.productname} and ${l.quantityP-1} other products </td> 
                                                                </c:if>
                                                                <c:if test="${l.quantityP <= 1}">
                                                                    <td>${l.productname}</td>
                                                                </c:if>

                                                                <td>${l.ordered_at}</td>
                                                                <td>${l.receiver}</td>                                                                
                                                                <td>${l.totalAmount}</td>


                                                                <td>

                                                                    <c:forEach items="${listso}" var="so">
                                                                        <c:if test="${so.id==l.statusId}">${so.status}</c:if>
                                                                    </c:forEach>

                                                                </td>
                                                                <td>
                                                                    <c:if test="${l.statusId==2}">
                                                                        <a class="button yellow" href="shipping?oid=${l.oid}&status=${l.statusId}&value=1">Shipping</a>
                                                                        <a class="button red" href="shipping?oid=${l.oid}&status=${l.statusId}&value=0">Cancel</a>
                                                                    </c:if>
                                                                      

                                                                </td>




                                                            </tr> 


                                                        </c:forEach>    
                                                            <c:if test="${not empty error}">
                                                            <tr>
                                                                <td colspan="9" class="error-message" style="color: red">${error}</td>
                                                            </tr>
                                                        </c:if>

                                                    </tbody>

                                                </table>


                                                <%-- Ph�n trang cho product--%>
                                                <nav aria-label="Page navigation example">
                                                    <ul class="pagination justify-content-end">

                                                        <c:forEach begin="1" end="${num}" var="n">
                                                            <li class="page-item ${n ==page ? "active" : ''}"><a class="page-link" href="dashboardinventory?page=${n}">${n}</a></li>
                                                            </c:forEach>

                                                    </ul>                                                   
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ============================================================== -->
                                <!-- end recent orders  -->


                                <!-- ============================================================== -->
                                <!-- ============================================================== -->
                                <!-- customer acquistion  -->
                                <!-- ============================================================== -->
                                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">

                                </div>
                                <!-- ============================================================== -->
                                <!-- end customer acquistion  -->
                                <!-- ============================================================== -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            Copyright � 2018 Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="text-md-right footer-links d-none d-sm-block">
                                <a href="javascript: void(0);">About</a>
                                <a href="javascript: void(0);">Support</a>
                                <a href="javascript: void(0);">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="assets/libs/js/dashboard-ecommerce.js"></script>





</body>

</html>