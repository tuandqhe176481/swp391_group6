<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Male_Fashion Template">
        <meta name="keywords" content="Male_Fashion, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Male-Fashion | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap"
              rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <style>
            .product__item__text h6 {
                min-height: 40px; /* Set a minimum height for the product name container */
                display: flex;
                align-items: center; /* Vertically center the content */
            }

            .product__item__text h6:hover {
                white-space: normal; /* Display full text on hover */
            }
            .border-0{
                background-color: yellow;
            }
        </style>
    </head>

    <body   >
        <!-- Page Preloder -->

        <!-- Offcanvas Menu Begin -->
        <div class="offcanvas-menu-overlay"></div>
        <div class="offcanvas-menu-wrapper">
            <div class="offcanvas__option">
                <div class="offcanvas__links">
                    <a href="#">Sign in</a>
                </div>

            </div>
            <div class="offcanvas__nav__option">
                <a href="#" class="search-switch"><img src="img/icon/search.png" alt=""></a>
                <a href="#"><img src="img/icon/cart.png" alt=""> <span>0</span></a>
                <div class="price">$0.00</div>
            </div>
            <div id="mobile-menu-wrap"></div>
            <div class="offcanvas__text">
                <p>Free shipping, 30-day return or refund guarantee.</p>
            </div>
        </div>
        <!-- Offcanvas Menu End -->

        <!-- Header Section Begin -->
        <header class="header">
            <div class="header__top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-7">
                            <div class="header__top__left">
                                <p>Free shipping, 30-day return or refund guarantee.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-5">
                            <div class="header__top__right">

                                <c:if test="${sessionScope.acc!=null}">
                                    <div class="header__top__hover">

                                        <span>${sessionScope.acc.name} <i class="arrow_carrot-down"></i></span>
                                        <ul>
                                            <li><a href="userprofile?uid=${sessionScope.acc.email}">Profile</a> </li>
                                            <li><a href="#">My Order</a> </li>
                                            <li><a href="logout">Log out</a></li>
                                        </ul>


                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div class="header__logo">
                            <a href="./home"><img src="img/logo.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li class=""><a href="./home">Home</a></li>
                                <li><a href="./shop">Shop</a></li>

                                <li><a href="./blog">Blog</a></li>

                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <div class="header__nav__option">
                            <a href="#" class="search-switch"><img src="img/icon/search.png" alt=""></a>
                            <a href="#"><img src="img/icon/cart.png" alt=""> <span>0</span></a>
                            <div class="price">$0.00</div>
                        </div>
                    </div>
                </div>
                <div class="canvas__open"><i class="fa fa-bars"></i></div>
            </div>
        </header>

        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h2 class="pageheader-title">Order Information</h2>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end pageheader  -->
                    <!-- ============================================================== -->
                    <div class="ecommerce-widget">

                        <div class="row">
                            <!-- ============================================================== -->

                            <!-- ============================================================== -->

                            <!-- recent orders  -->
                            <!-- ============================================================== -->
                            <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">

                                <div class="card">

                                    <table>

                                        <thead class="bg-light">
                                            <tr class="border-0">
                                                <th class="border-0">ID</th>
                                                <th class="border-0">Image</th>
                                                <th class="border-0">Product</th>    
                                                <th class="border-0">Price</th>
                                                <th class="border-0">Quantity</th>                                                            
                                                <th class="border-0">Total</th>                                                        
                                        </thead>
                                        <tbody>

                                            <c:forEach  items="${list}" var="l">
                                                <tr>
                                                    <td class="product__cart__item">
                                                        <div class="product__cart__item__id">
                                                            <h6>${l.productId}</h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="product__cart__item__pic">
                                                            <img src="${l.img}" alt="" width="45">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="product__cart__item__text">
                                                            <h6><a href="productdetails?pid=${l.productId}">${l.pname}</a></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="product__cart__item__price">
                                                            <h6 class="original-price">${l.price}</h6>
                                                        </div>
                                                    </td>
                                                    <td class="quantity__item">
                                                        <div class="quantity">
                                                            <div>
                                                                &emsp;<span class="qty">${l.quantity}</span>&emsp;
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="cart__price original-price"> ${l.price*l.quantity}</td>


                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>    

                                <div class="row">

                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="total">
                                            Total: <span class="original-price">${o.totalAmount}</span>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">

                                    <div class="col-lg-2 col-md-2 col-sm-2"></div> 
                                    <div class="col-lg-8 col-md-8 col-sm-8"> 
                                        <form action="updateorder1">
                                            <table border="1" style="width: 100%;">
                                                <input type="hidden" name="oid" value="${o.oid}">
                                                <tr>
                                                    <td>Receiver:</td>

                                                    <td> ${o.receiver}</td>
                                                </tr>
                                                <tr>
                                                    <td>Ordered At:</td>
                                                    <td>${o.ordered_at}</td>
                                                </tr>
                                                <tr>
                                                    <td>Address:</td>
                                                    <td><input type="text" name="address" value="${o.address}"></td>
                                                </tr>
                                                <tr>
                                                    <td>Phone Number:</td>
                                                    <td><input type="text" name="phone" value="${o.phoneNumber}"></td>
                                                </tr>
                                                <tr>
                                                    <td>Payment:</td>
                                                    <td>${o.payment}</td>
                                                </tr>
                                                <tr>
                                                    <td>Notes:</td>
                                                    <td>${o.note}</td>
                                                </tr>


                                            </table>  
                                                   
                                            <c:if test="${o.statusId==1}">
                                            <button class="update-button" type="submit">Update</button>
                                            </c:if>
                                                   
                                        </form>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2"></div> 

                                </div>




                            </div>
                            <style>
                                /* Thi?t l?p CSS cho button */
                                .update-button {
                                    background-color: #4CAF50; /* M�u n?n */
                                    border: none; /* Kh�ng vi?n */
                                    color: white; /* M�u ch? */
                                    padding: 15px 32px; /* K�ch th??c padding */
                                    text-align: center; /* Canh gi?a ch? */
                                    text-decoration: none; /* Kh�ng g?ch ch�n */
                                    display: inline-block;
                                    font-size: 16px; /* C? ch? */
                                    margin: 4px 2px; /* Kho?ng c�ch */
                                    cursor: pointer; /* Con tr? khi r� chu?t */
                                    border-radius: 8px; /* Bo tr�n g�c */
                                    transition-duration: 0.4s; /* Th?i gian chuy?n ??i */
                                }

                                /* Hover effect */
                                .update-button:hover {
                                    background-color: #45a049; /* M�u n?n khi hover */
                                }
                            </style>
                            <!-- ============================================================== -->
                            <!-- end recent orders  -->


                            <!-- ============================================================== -->
                            <!-- ============================================================== -->
                            <!-- customer acquistion  -->
                            <!-- ============================================================== -->
                            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">

                            </div>
                            <!-- ============================================================== -->
                            <!-- end customer acquistion  -->
                            <!-- ============================================================== -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript">
        </script>
    </body>
</html>














































