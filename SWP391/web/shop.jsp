<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zxx">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Male_Fashion Template">
        <meta name="keywords" content="Male_Fashion, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Male-Fashion | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap"
              rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <style>
            .product__pagination {
                display: flex;
                justify-content: center;
                margin-top: 20px;
            }

            .pagination {
                display: flex;
                list-style: none;
                padding: 0;
                margin-left: 600px;
            }

            .page-item {
                margin-right: 5px;
            }

            .page-link {
                text-decoration: none;
                color: #333;
                background-color: #fff;
                border: 1px solid #ddd;
                padding: 8px 12px;
                border-radius: 4px;
            }

            .page-link:hover {
                background-color: #eee;
            }

            .page-item.active .page-link {
                background-color: black;
                color: #fff;
                border-color: #007bff;
                border-radius: 50%;
                width: 30px;
                height: 30px;
                display: flex;
                align-items: center;
                justify-content: center;

            }
            .product__item__text h6 {
                min-height: 40px; /* Set a minimum height for the product name container */
                display: flex;
                align-items: center; /* Vertically center the content */
            }

            .product__item__text h6:hover {
                white-space: normal; /* Display full text on hover */
            }
            /* Thêm lớp CSS để định dạng giá sale */
            .product__item__text .sale-price {
                color: red; /* Đặt màu để làm nổi bật giá sale */
                font-weight: bold; /* Tăng độ đậm cho giá sale */
            }

            /* Thêm lớp CSS để tạo nhãn cho giá sale */
            .product__item__text .sale-label {
                background-color: #e74c3c;
                color: #fff;
                padding: 3px 8px;
                border-radius: 5px;
                display: inline-block;
                margin-bottom: 5px;
            }

            .product__item__text h6 {
                min-height: 40px;
                display: flex;
                align-items: center;
            }

            .product__item__text h6:hover {
                white-space: normal;
            }
            /* CSS for the "Lastest" card */
            .card {
                border: 2px solid #ff6f61; /* Đổi màu viền */
                border-radius: 10px; /* Bo tròn góc */
                box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1); /* Đổ bóng */
            }

            .card-heading a {
                color: #ff6f61; /* Đổi màu chữ cho link */
            }

            .card-body {
                padding: 20px; /* Tăng khoảng cách nội dung bên trong */
            }

            .product__item {
                margin-bottom: 20px; /* Khoảng cách giữa các sản phẩm */
            }

            .label {
                background-color: #ff6f61; /* Đổi màu nền cho nhãn */
                color: white; /* Đổi màu chữ */
            }


            .quantity {
                margin-top: 10px; /* Khoảng cách giữa giá và số lượng */
            }

        </style>
    </head>

    <body>
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>

        <!-- Offcanvas Menu Begin -->
        <div class="offcanvas-menu-overlay"></div>
        <div class="offcanvas-menu-wrapper">
            <div class="offcanvas__option">
                <div class="offcanvas__links">
                    <a href="#">Sign in</a>
                    <a href="#">FAQs</a>
                </div>
                <div class="offcanvas__top__hover">
                    <span>Usd <i class="arrow_carrot-down"></i></span>
                    <ul>
                        <li>USD</li>
                        <li>EUR</li>
                        <li>USD</li>
                    </ul>
                </div>
            </div>
            <div class="offcanvas__nav__option">
                <a href="#" class="search-switch"><img src="img/icon/search.png" alt=""></a>
                <a href="#"><img src="img/icon/heart.png" alt=""></a>
                <a href="#"><img src="img/icon/cart.png" alt=""> <span>0</span></a>
                <div class="price">$0.00</div>
            </div>
            <div id="mobile-menu-wrap"></div>
            <div class="offcanvas__text">
                <p>Free shipping, 30-day return or refund guarantee.</p>
            </div>
        </div>
        <!-- Offcanvas Menu End -->

        <!-- Header Section Begin -->
        <header class="header">
            <div class="header__top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-7">
                            <div class="header__top__left">
                                <c:if test="${sessionScope.message != null}">
                                    <div id="message" class="notification" style="color: white ">
                                        ${sessionScope.message}
                                    </div>
                                    <script>
                                        // Hien thi thong bao 5s
                                        setTimeout(function () {
                                            var messageDiv = document.getElementById("message");
                                            if (messageDiv) {
                                                messageDiv.style.display = "none";

                                        <%
                                                    session.removeAttribute("message");
                                        %>
                                            }
                                        }, 5000);
                                    </script>
                                </c:if>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-5">
                            <div class="header__top__right">
                                <div class="header__top__links">
                                    <c:if test="${sessionScope.acc==null}">
                                        <a href="login">Sign in</a> 
                                    </c:if>


                                </div>
                                <c:if test="${sessionScope.acc!=null}">
                                    <div class="header__top__hover">

                                        <span>${sessionScope.acc.name} <i class="arrow_carrot-down"></i></span>
                                        <ul>
                                            <li><a href="userprofile?uid=${sessionScope.acc.email}">Profile</a> </li>
                                                <c:choose>
                                                    <c:when test="${sessionScope.acc.roleId eq 1}">
                                                    <li><a href="admindashboard">Dashboard</a></li>
                                                    </c:when>
                                                    <c:when test="${sessionScope.acc.roleId eq 2}">
                                                    <li><a href="salemanagerdashboard">Dashboard</a></li>
                                                    </c:when>
                                                    <c:when test="${sessionScope.acc.roleId eq 3}">
                                                    <li><a href="ordermanager">Dashboard</a></li>
                                                    </c:when>
                                                    <c:when test="${sessionScope.acc.roleId eq 4}">
                                                    <li><a href="dashboardmkt">Dashboard</a></li>
                                                    </c:when>
                                                    <c:when test="${sessionScope.acc.roleId eq 5}">
                                                    <li><a href="myorder?acid=${sessionScope.acc.user_id}">My Order</a> </li>
                                                    </c:when>
                                                </c:choose>
                                            <li><a href="logout">Log out</a></li>
                                        </ul>


                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div class="header__logo">
                            <a href="./home"><img src="img/logo.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li><a href="./home">Home</a></li>
                                <li  class="active"><a href="./shop">Shop</a></li>

                                <li><a href="./blog">Blog</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <div class="header__nav__option">
                            <a href="#" class="search-switch"><img src="img/icon/search.png" alt=""></a>
                                <c:set value="${size}" var="size"/>
                                <c:set value="${total}" var="total"/>
                                <c:if test="${sessionScope.acc.roleId eq 5}">
                                <a href="viewcart"><img src="img/icon/cart.png" alt=""> <span>${size}</span></a>
                                <div class="price original-price">${total}đ</div>
                            </c:if>

                        </div>
                    </div>
                </div>
                <div class="canvas__open"><i class="fa fa-bars"></i></div>
            </div>
        </header>
        <!-- Header Section End -->

        <!-- Breadcrumb Section Begin -->
        <section class="breadcrumb-option">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="breadcrumb__text">
                            <h4>Shop</h4>
                            <div class="breadcrumb__links">
                                <a href="./home">Home</a>
                                <span>Shop</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Breadcrumb Section End -->

        <!-- Shop Section Begin -->
        <section class="shop spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="shop__sidebar">
                            <div class="shop__sidebar__search">
                                <form action="search" >
                                    <input oninput="searchByName(this)" type="text" placeholder="Search" name = "search" value="${search}">
                                    <button type="submit"><span class="icon_search"></span></button>
                                </form>
                            </div>
                            <div class="shop__sidebar__accordion">
                                <div class="accordion" id="accordionExample">
                                    <div class="card">
                                        <div class="card-heading">
                                            <a data-toggle="collapse" data-target="#collapseOne">Categories</a>
                                        </div>
                                        <div id="collapseOne" class="collapse show" data-parent="#accordionExample">
                                            <div class="card-body">
                                                <div class="shop__sidebar__categories">
                                                    <ul class="nice-scroll">
                                                        <c:forEach items="${listc}" var="c">
                                                            <li><a href="filter?cid=${c.cid}&bid=${tagb}&price1=&price2=" style="${tag == c.cid ? 'color: black;' : ''}">
                                                                    ${c.cname}
                                                                </a>
                                                            </li> 
                                                        </c:forEach>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-heading">
                                            <a data-toggle="collapse" data-target="#collapseTwo">Branding</a>
                                        </div>
                                        <div id="collapseTwo" class="collapse show" data-parent="#accordionExample">
                                            <div class="card-body">
                                                <div class="shop__sidebar__brand">
                                                    <ul>
                                                        <c:forEach items="${listb}" var="b">
                                                            <li><a href="filter?bid=${b.bid}&cid=${tag}&price1=&price2=" style="${tagb == b.bid ? 'color: black;' : ''}" >${b.bname}</a></li>
                                                            </c:forEach>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-heading">
                                            <a data-toggle="collapse" data-target="#collapseThree">Filter Price</a>
                                        </div>
                                        <div id="collapseThree" class="collapse show" data-parent="#accordionExample">
                                            <div class="card-body">
                                                <div class="shop__sidebar__price">
                                                    <ul>
                                                        <li><a href="sale?sale=1" style="${tags == '1' ? 'color: black;' : ''}">Sale</a></li>
                                                        <li><a href="filter?price1=0&price2=50000&bid=${param.bid}&cid=${param.cid}" style="${tag1 == '0' ? 'color: black;' : ''}">0đ - 50,000đ</a></li>
                                                        <li><a href="filter?price1=50000&price2=200000&bid=${param.bid}&cid=${param.cid}" style="${tag1 == '50000' ? 'color: black;' : ''}">50,000đ - 200,000đ</a></li>
                                                        <li><a href="filter?price1=200000&price2=500000&bid=${param.bid}&cid=${param.cid}" style="${tag1 == '200000' ? 'color: black;' : ''}">200,000đ - 500,000đ</a></li>
                                                        <li><a href="filter?price1=500000&price2=1000000&bid=${param.bid}&cid=${param.cid}" style="${tag1 == '500000' ? 'color: black;' : ''}">500,000đ - 1,000,000đ</a></li>
                                                        <li><a href="filter?price1=1000000&price2=1500000&bid=${param.bid}&cid=${param.cid}" style="${tag1 == '1000000' ? 'color: black;' : ''}">1,000,000đ - 1,500,000đ</a></li>
                                                        <li><a href="filter?price1=1500000&price2=50000000&bid=${param.bid}&cid=${param.cid}" style="${tag1 == '1500000' ? 'color: black;' : ''}" >1,500,000đ+</a></li>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-heading">
                                            <a data-toggle="collapse" data-target="#collapseThree">Lastest</a>                                         
                                        </div>
                                        <div id="collapseFour" class="collapse show" data-parent="#accordionExample">
                                            <div class="card-body">
                                                <div class="shop__sidebar__size">         
                                                    <c:forEach  items="${listn}" var="n">
                                                        <div class="product__item">
                                                            <div class="product__item__pic set-bg" data-setbg="${n.img2}">
                                                                <c:if test="${n.isDiscount}">
                                                                    <span class="label" style="background-color: black ; color: white">Sale</span> 
                                                                </c:if>
                                                                <span class="label" style="background-color: black ; color: white">NEW</span> 
                                                                <ul class="product__hover">
                                                                    <li><a href="#"><img src="img/icon/heart.png" alt=""></a></li>
                                                                    <li><a href="#"><img src="img/icon/compare.png" alt=""> <span>Compare</span></a></li>
                                                                    <li><a href="productdetails?pid=${n.pid}"><img src="img/icon/search.png" alt=""></a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="product__item__text">
                                                                <h6>${n.pname}</h6>
                                                                <c:if test="${sessionScope.acc.roleId eq 5 || sessionScope.acc.roleId eq null}">
                                                                    <a href="addtocart?id=${n.pid}" class="add-cart">+ Add To Cart</a>
                                                                </c:if>
                                                                <div class="rating" style="color: yellow">
                                                                    <c:forEach begin="1" end="5" varStatus="star">
                                                                        <c:choose>
                                                                            <c:when test="${star.count <= n.rate}">
                                                                                <i class="fa fa-star" style="color: #ffe57f"></i>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <i class="fa fa-star-o" style="color: black"></i>
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </c:forEach>
                                                                </div>
                                                                <h5>
                                                                    <%-- Hiển thị giá giảm nếu có --%>
                                                                    <c:if test="${n.isDiscount}">
                                                                        <span class="original-price" style="text-decoration: line-through;">$${n.price}</span>
                                                                        <span class="sale-price">${n.priceSale} đ</span>
                                                                    </c:if>
                                                                    <%-- Nếu không có giảm giá, hiển thị giá bình thường --%>
                                                                    <c:if test="${not n.isDiscount}">
                                                                        ${n.price} đ
                                                                    </c:if>
                                                                </h5>

                                                            </div>
                                                        </div>
                                                    </c:forEach>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="shop__product__option">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="shop__product__option__left">

                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="shop__product__option__right">
                                        <p>Sort by Price:</p>
                                        <form>
                                            <select name="sort" onchange="form.submit()">
                                                <option value="" ${empty param.sort ? 'selected' : ''}>Default</option>
                                                <option value="LtH" ${param.sort == 'LtH' ? 'selected' : ''}>Low To High</option>
                                                <option value="HtL" ${param.sort == 'HtL' ? 'selected' : ''}>High To Low</option>
                                            </select>                          
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form name="f" action="" method="post">
                            <div class="row">
                                <h4 style="color: red">${error}</h4>
                                <c:forEach items="${listp}" var="p" varStatus="loop">
                                    <c:if test="${loop.index % 3 == 0}">
                                    </div>
                                    <div class="row">
                                    </c:if>
                                    <div id="content" class="col-lg-4 col-md-6 col-sm-6">
                                        <div class="product__item">
                                            <div class="product__item__pic set-bg" data-setbg="${p.img2}">
                                                <c:if test="${p.isDiscount}">
                                                    <span class="label" style="background-color: black ; color: white">Sale</span> 
                                                </c:if>
                                                <ul class="product__hover">
                                                    <li><a href="#"><img src="img/icon/feedback.jpg" style="width: 38px; height: 36px" > <span>Feedback</span></a></li>
                                                    <li><a href="productdetails?pid=${p.pid}"><img src="img/icon/search.png" alt=""></a></li>
                                                </ul>
                                            </div>
                                            <div class="product__item__text">
                                                <h6>${p.pname}</h6>
                                                <c:if test="${sessionScope.acc.roleId eq 5 || sessionScope.acc.roleId eq null}">
                                                    <a href="addtocart?id=${p.pid}" class="add-cart">+ Add To Cart</a>
                                                </c:if>
                                                <div class="rating" style="color: yellow">
                                                    <c:forEach begin="1" end="5" varStatus="star">
                                                        <c:choose>
                                                            <c:when test="${star.count <= p.rate}">
                                                                <i class="fa fa-star" style="color: #ffe57f"></i>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <i class="fa fa-star-o" style="color: black"></i>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                </div>
                                                <h5>
                                                    <%-- Hiển thị giá giảm nếu có --%>
                                                    <c:if test="${p.isDiscount}">
                                                        <span class="original-price" style="text-decoration: line-through;">${p.price} đ</span>
                                                        <span class="sale-price original-price">${p.priceSale} đ</span>
                                                    </c:if>
                                                    <%-- Nếu không có giảm giá, hiển thị giá bình thường --%>
                                                    <c:if test="${not p.isDiscount}">
                                                        <span class="original-price">${p.price} đ</span>
                                                    </c:if>
                                                </h5>

                                                <div class="quantity">
                                                    <div style="font-size: 11px;text-align: right;font-weight: bold">
                                                        Quantity: ${p.quantity}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                        </form>



                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <%-- Phân trang của Product shop --%>
                            <div class="product__pagination ">
                                <ul class="pagination">
                                    <c:forEach begin="1" end="${endP}" var="i">
                                        <li class="page-item ${i == currentIndex ? 'active' : ''}">
                                            <a class="page-link" href="shop?index=${i}<%=request.getParameter("sort") != null ? "&sort="+request.getParameter("sort") : ""%>">
                                                ${i}
                                            </a>
                                        </li>
                                    </c:forEach>
                                </ul>                            
                            </div>
                            <%-- Phân trang của Sale Product --%>
                            <div class="product__pagination ">
                                <ul class="pagination">
                                    <c:forEach begin="1" end="${endps}" var="i">
                                        <li class="page-item ${i == currentIndexSale ? 'active' : ''}">
                                            <a class="page-link" href="sale?index=${i}<%=request.getParameter("sort") != null ? "&sort="+request.getParameter("sort") : ""%>">
                                                ${i}
                                            </a>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </div>
                            <%-- Phân trang của brand --%>
                            <div class="product__pagination ">
                                <ul class="pagination">
                                    <c:forEach begin="1" end="${endpb}" var="i">
                                        <li class="page-item ${i == currentIndexSale ? 'active' : ''}">
                                            <a class="page-link" href="filter?bid=${tagb}&index=${i}">
                                                ${i}
                                            </a>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shop Section End -->

    <!-- Footer Section Begin -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer__about">
                        <div class="footer__logo">
                            <a href="#"><img src="img/footer-logo.png" alt=""></a>
                        </div>
                        <p>The customer is at the heart of our unique business model, which includes design.</p>
                        <a href="#"><img src="img/payment.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 offset-lg-1 col-md-3 col-sm-6">
                    <div class="footer__widget">
                        <h6>Shopping</h6>
                        <ul>
                            <li><a href="#">Clothing Store</a></li>
                            <li><a href="#">Trending Shoes</a></li>
                            <li><a href="#">Accessories</a></li>
                            <li><a href="#">Sale</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <div class="footer__widget">
                        <h6>Shopping</h6>
                        <ul>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Payment Methods</a></li>
                            <li><a href="#">Delivary</a></li>
                            <li><a href="#">Return & Exchanges</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 offset-lg-1 col-md-6 col-sm-6">
                    <div class="footer__widget">
                        <h6>NewLetter</h6>
                        <div class="footer__newslatter">
                            <p>Be the first to know about new arrivals, look books, sales & promos!</p>
                            <form action="#">
                                <input type="text" placeholder="Your email">
                                <button type="submit"><span class="icon_mail_alt"></span></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="footer__copyright__text">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        <p>Copyright ©
                            <script>
                                document.write(new Date().getFullYear());
                            </script>2020
                            All rights reserved | This template is made with <i class="fa fa-heart-o"
                                                                                aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        </p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Search Begin -->
    <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch">+</div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search End -->

    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>
<script type="text/javascript">
                                function buy(id) {

                                    var m = 1;
                                    document.f.action = "shop?id=" + id + "&num=" + m;
                                    document.f.submit();
                                    // document.getElementById('test').innerHTML=document.f.action;

                                }
                                document.addEventListener("DOMContentLoaded", function () {
                                    var originalPrices = document.querySelectorAll('.original-price');
                                    originalPrices.forEach(function (originalPrice) {
                                        originalPrice.textContent = formatPrice(originalPrice.textContent.trim());
                                    });

                                    function formatPrice(price) {
                                        return (parseFloat(price)).toLocaleString('vi-VN', {style: 'currency', currency: 'VND'}).replace('VND', '');
                                    }
                                });
</script>
