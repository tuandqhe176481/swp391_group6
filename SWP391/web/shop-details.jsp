    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">


<!DOCTYPE html>
<html lang="zxx">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Male_Fashion Template">
        <meta name="keywords" content="Male_Fashion, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Product Details </title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap"
              rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css" integrity="sha512-17EgCFERpgZKcm0j0fEq1YCJuyAWdz9KUtv1EjVuaOz8pDnh/0nZxmU6BBXwaaxqoi9PQXnRWqlcDB027hgv9A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" integrity="sha512-yHknP1/AwR+yx26cB1y0cjvQUMvEa2PFzt1c9LlS4pRQ5NOTZFWbhBig+X9G9eYW/8m0/4OXNx8pxJ6z57x0dw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    </head>

    <body>
        <!-- Page Preloder -->
       <div class="offcanvas-menu-overlay"></div>
        <div class="offcanvas-menu-wrapper">
            <div class="offcanvas__option">
                <div class="offcanvas__links">
                    <a href="#">Sign in</a>
                    <a href="#">FAQs</a>
                </div>
                <div class="offcanvas__top__hover">
                    <span>Usd <i class="arrow_carrot-down"></i></span>
                    <ul>
                        <li>USD</li>
                        <li>EUR</li>
                        <li>USD</li>
                    </ul>
                </div>
            </div>
            <div class="offcanvas__nav__option">
                <a href="#" class="search-switch"><img src="img/icon/search.png" alt=""></a>
                <a href="#"><img src="img/icon/heart.png" alt=""></a>
                <a href="#"><img src="img/icon/cart.png" alt=""> <span>0</span></a>
                <div class="price">$0.00</div>
            </div>
            <div id="mobile-menu-wrap"></div>
            <div class="offcanvas__text">
                <p>Free shipping, 30-day return or refund guarantee.</p>
            </div>
        </div>
        <!-- Offcanvas Menu End -->

        <!-- Header Section Begin -->
        <header class="header" style="background-color: pink">
            <div class="header__top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-7">
                            <div class="header__top__left">
                                <c:if test="${sessionScope.message != null}">
                                    <div id="message" class="notification" style="color: white ">
                                        ${sessionScope.message}
                                    </div>
                                    <script>
                                        // Hien thi thong bao 5s
                                        setTimeout(function () {
                                            var messageDiv = document.getElementById("message");
                                            if (messageDiv) {
                                                messageDiv.style.display = "none";

                                        <%
                                                    session.removeAttribute("message");
                                        %>
                                            }
                                        }, 5000);
                                    </script>
                                </c:if>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-5">
                            <div class="header__top__right">
                                <div class="header__top__links">
                                    <c:if test="${sessionScope.acc==null}">
                                        <a href="login">Sign in</a> 
                                    </c:if>


                                </div>
                                <c:if test="${sessionScope.acc!=null}">
                                    <div class="header__top__hover">

                                        <span>${sessionScope.acc.name} <i class="arrow_carrot-down"></i></span>
                                        <ul>
                                            <li><a style="color: black" href="userprofile?uid=${sessionScope.acc.email}">Profile</a> </li>
                                                <c:choose>
                                                    <c:when test="${sessionScope.acc.roleId eq 1}">
                                                    <li><a href="home">Dashboard</a></li>
                                                    </c:when>
                                                    <c:when test="${sessionScope.acc.roleId eq 2}">
                                                    <li><a href="home">Dashboard</a></li>
                                                    </c:when>
                                                    <c:when test="${sessionScope.acc.roleId eq 3}">
                                                    <li><a href="home">Dashboard</a></li>
                                                    </c:when>
                                                    <c:when test="${sessionScope.acc.roleId eq 4}">
                                                    <li><a href="dashboardmkt">Dashboard</a></li>
                                                    </c:when>
                                                    <c:when test="${sessionScope.acc.roleId eq 5}">
                                                    <li><a href="#" style="color: black">My Order</a> </li>
                                                    </c:when>
                                                </c:choose>
                                            <li><a href="logout"style="color: black">Log out</a></li>
                                        </ul>


                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div class="header__logo">
                            <a href="./home"><img src="img/logo.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li><a href="./home">Home</a></li>
                                <li><a href="./shop">Shop</a></li>
                               
                                <li><a href="./blog">Blog</a></li>
                               
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <div class="header__nav__option">
                            <a href="#" class="search-switch"><img src="img/icon/search.png" alt=""></a>
                                <c:set value="${size}" var="size"/>
                                <c:set value="${total}" var="total"/>
                                <c:if test="${sessionScope.acc.roleId eq 5}">
                                <a href="viewcart"><img src="img/icon/cart.png" alt=""> <span>${size}</span></a>
                                <div class="price">$${total}</div>
                            </c:if>

                        </div>
                    </div>
                </div>
                <div class="canvas__open"><i class="fa fa-bars"></i></div>
            </div>
        </header>
        <!-- Header Section End -->

        <!-- Shop Details Section Begin -->
        <section class="shop-details">
            <div class="product__details__pic">
                <div class="container">

                    <div class="row" >
                        <div class="col-lg-3 col-md-3">
                            <ul class="nav nav-tabs" role="tablist">

                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">
                                        <div class="product__thumb__pic set-bg" data-setbg="${p.img1}">
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">
                                        <div class="product__thumb__pic set-bg" data-setbg="${p.img2}">
                                        </div>
                                    </a>
                                </li>

                            </ul>
                        </div>

                        <div class="col-lg-6 col-md-9">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tabs-2" role="tabpanel">
                                    <div class="product__details__pic__item">
                                        <img src="${p.img1}" alt="">
                                    </div>
                                </div>
                                <div class="tab-pane" id="tabs-3" role="tabpanel">
                                    <div class="product__details__pic__item">
                                        <img src="${p.img2}" alt="">
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="col-lg-3" style="right: -150px ; background-color: #F3F2EE;">
                            <div class="shop__sidebar">
                                <div class="shop__sidebar__search">
                                    <form action="search">
                                        <input type="text" placeholder="Search" name = "search" value="${search}search" style=" background-color: #264e4ec4" >
                                        <button type="submit"><span class="icon_search"></span></button>
                                    </form>
                                </div>
                                <div class="shop__sidebar__accordion">
                                    <div class="accordion" id="accordionExample">
                                        <div class="card">
                                            <div class="card-heading">
                                                <a data-toggle="collapse" data-target="#collapseOne" style=" background-color: #264e4ec4 ; color: white">Categories</a>


                                            </div>
                                            <div id="collapseOne" class="collapse show" data-parent="#accordionExample">
                                                <div class="card-body"     style="background-color: beige">
                                                    <div class="shop__sidebar__categories">
                                                        <ul class="nice-scroll">
                                                            <c:forEach items="${listc}" var="c">
                                                                <li><a href="filter?cid=${c.cid}&bid=${tagb}&price1=&price2=" style="color: black;">
                                                                        ${c.cname}
                                                                    </a>
                                                                </li> 
                                                            </c:forEach>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card">
                                        </div>
                                        <div class="card">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="product__details__content">
                <div class="container">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-8">
                            <div class="product__details__text">
                                <h4>${p.pname}</h4>
                                <div class="rating">
                                    <c:forEach begin="1" end="${totalStars}" var="i">
                                        <i class="fa fa-star"  style="color: #f7941d"></i>
                                    </c:forEach>
                                    <c:forEach begin="${totalStars + 1}" end="5" var="i">
                                        <i class="fa fa-star-o"></i>
                                    </c:forEach>
                                </div>

                                <h3 class="original-price">${p.priceSale} ?<span class="original-price">${p.price} ?</span></h3>
                                
                                <div >
                                    Gender: ${(p.description == 0) ? "female" : "male"}
                                </div>
                                <div  class="product__details__option__color">
                                    <span >Color: ${p.color}</span>

                                </div>
                                <div>
                                    <h6>Quantity: ${p.quantity}</h6>
                                    <h6>Date: ${p.created_at}</h6>
                                </div>
                                

                                <div class="product__details__cart__option">
                                    
                                    <c:if test="${sessionScope.acc.roleId eq 5 || sessionScope.acc.roleId eq null}">
                                        <a href="addtocart?id=${p.pid}" class="add-cart">+ Add To Cart</a>
                                    </c:if>
                                </div>
                                <!-- Feedback -->
                                <c:if test="${sizeListF == 0}">
                                    <div class="product__details__last__option">
                                        <h5><span>Feedback</span></h5>
                                    </div>
                                    There is no reviews!
                                </c:if>
                                <c:if test="${sizeListF != 0}">
                                    <div class="product__details__last__option">
                                        <h5><span>Feedback</span></h5>
                                        <nav class="navbar navbar-default navbar-fixed-top">
                                            <div class="container">
                                                <div class="navbar-header">
                                                    <span class="sr-only">Toggle navigation</span>
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                    </button>

                                                </div>
                                        </nav>


                                        <div class="container" id="yourcmt" style="width: 1400px">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="rating-block">
                                                        <h4 style="width: 140%">Average user rating</h4>
                                                        <h2 class="bold padding-bottom-7" id="formattedRating"><small> </small><small>/ 5</small></h2>
                                                        <div class="rating">
                                                            <c:forEach begin="1" end="${totalStars}" var="i">
                                                                <i class="fa fa-star"  style="color: #f7941d"></i>
                                                            </c:forEach>
                                                            <c:forEach begin="${totalStars + 1}" end="5" var="i">
                                                                <i class="fa fa-star-o"></i>
                                                            </c:forEach>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>			
                                            <div>
                                            </div>
                                            <div class="row">

                                            </div>
                                            <div class="row" style="display: flex; flex-direction: column">
                                                <div class="col-sm-7">
                                                    <hr/>
                                                    <div class="review-block">
                                                        <c:forEach items="${listF}" var="f">
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    <img src="${f.acid.avatar}" class="img-rounded">
                                                                    <div class="review-block-name">${f.acid.name}</div>
                                                                    <div class="review-block-date">${f.time}</div>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <div class="rating">
                                                                        <c:forEach begin="1" end="${f.rate}" var="i">
                                                                            <i class="fa fa-star" style="color: #f7941d"></i>
                                                                        </c:forEach>
                                                                        <c:forEach begin="${f.rate + 1}" end="5" var="i">
                                                                            <i class="fa fa-star-o"></i>
                                                                        </c:forEach>
                                                                    </div>
                                                                    <div class="comment">${f.comment}</div>

                                                                </div>
                                                                <div class="col-sm-1">
                                                                    <div style="position: relative">
                                                                        <c:if test="${f.acid.user_id eq acid}">
                                                                            <div id="cmt__button">
                                                                                ...
                                                                            </div>
                                                                            <ul id="cmt__action" class="hidden">
                                                                                <li><a id="edit-cmt">Edit</a></li>
                                                                                <li><a href="delete_feedback?productid=${p.pid}" id="remove-cmt">Delete</a></li>
                                                                            </ul>
                                                                        </c:if>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <hr/>
                                                        </c:forEach>

                                                    </div>
                                                </div>
                                            </c:if>
                                            <style>
                                                #cmt__button {
                                                    width: 2rem;
                                                    height: 2rem;
                                                    font-size: 2rem;
                                                    text-align: center;
                                                    line-height: .2;
                                                    cursor: pointer
                                                }
                                                #cmt__action {
                                                    position: absolute;
                                                    background: #fff;
                                                    padding: .2rem;
                                                    border-radius: .2rem;
                                                    left: -1rem;
                                                    top: -4rem;
                                                }
                                                #cmt__action li {
                                                    border: none;
                                                    cursor: pointer
                                                }
                                                #cmt__action.hidden {
                                                    display:none;
                                                }
                                                #yourcmt form {
                                                    width: 100%;
                                                    display: flex;
                                                    align-items: center;
                                                    justify-content: space-between
                                                }
                                                #yourcmt form textarea {
                                                    width: 100%;
                                                }
                                                #line {
                                                    width: 100%;
                                                    height: .1rem;
                                                    background: grey;
                                                    margin-bottom: 3rem;
                                                }
                                                #cmt__action a {
                                                    color: black;
                                                }
                                            </style>
                                            <script>
                                                document.querySelector('#cmt__button').addEventListener(
                                                        "click",
                                                        function () {
                                                            document.querySelector('#cmt__action').classList.toggle('hidden');
                                                        }
                                                );

                                                document.querySelector('#edit-cmt').addEventListener(
                                                        "click",
                                                        function () {
                                                            document.querySelector('#yourcmt .comment').innerHTML = `
                                    
                                            <form action="productdetails" method="POST">
                                                <input name="pid" value="${p.pid}" hidden />
                                                <textarea name="editting__cmt">${f.comment}</textarea>
                                                <button class="button" type="submit">Save</button>
                                            </form>
                                    
                                    `;

                                                            document.querySelector('#cmt__action').classList.toggle('hidden');
                                                        }
                                                );
                                            </script>

                                            <c:if test="${sessionScope.acc.roleId ne null  && didBuy eq 'true' && !hasComment}">
                                                <hr>
                                                <div class="col-sm-7" style="display: contents;">
                                                    <h4>Your review:</h4>

                                                    <form action="AddCommentServlet" method="post">
                                                        <input type="hidden" name="pid" value="${p.pid}">
                                                        <div class="form-group">
                                                            <div id="rating" class="rating">
                                                                <span class="star" data-value="1">&#9733;</span>
                                                                <span class="star" data-value="2">&#9733;</span>
                                                                <span class="star" data-value="3">&#9733;</span>
                                                                <span class="star" data-value="4">&#9733;</span>
                                                                <span class="star" data-value="5">&#9733;</span>
                                                            </div>
                                                            <input type="hidden" name="rating" id="rating-value" value="0">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="comment">Comment:</label>
                                                            <textarea class="form-control" rows="5" id="comment" name="comment"></textarea>
                                                        </div>

                                                        <button type="submit" class="btn btn-default">Submit</button>
                                                    </form>

                                                </div>
                                            </div>

                                        </div> <!-- /container -->
                                    </c:if>


                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- Shop Details Section End -->


    <!-- Related Section Begin -->
    <section class="related spad">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="related-title">Related Product</h3>
                </div>
            </div>
            <div class="row filtering">

                <c:forEach items="${list}" var="l">
                    <div >

                        <div class="product__item">
                            <div class="product__item__pic set-bg" data-setbg="${l.img2}" style="margin-right:20px ">
                                <ul class="product__hover">
                                    <li><a href="#"><img src="img/icon/heart.png" alt=""></a></li>
                                    <li><a href="#"><img src="img/icon/compare.png" alt=""> <span>Compare</span></a></li>
                                    <li><a href="productdetails?pid=${l.pid}"><img src="img/icon/search.png" alt=""></a></li>
                                </ul>
                            </div>
                            <div class="product__item__text">
                                <h6>${l.pname}</h6>
                                <a href="#" class="add-cart">+ Add To Cart</a>
                                <div class="rating">
                                    <c:forEach begin="1" end="${l.rate}" var="i">
                                        <i class="fa fa-star"  style="color: #f7941d"></i>
                                    </c:forEach>
                                    <c:forEach begin="${l.rate + 1}" end="5" var="i">
                                        <i class="fa fa-star-o"></i>
                                    </c:forEach>
                                </div>
                                <h5 class="original-price">${l.price} ?</h5>
                            </div>
                        </div>
                    </div>
                </c:forEach>

            </div>
        </div>   


    </section>
    <!-- Related Section End -->

    <!-- Footer Section Begin -->
    <footer class="footer">
        <div class="container ">
            <div class="row ">
                <div class="col-lg-3 col-md-6 col-sm-6 ">
                    <div class="footer__about">
                        <div class="footer__logo">
                            <a href="#"><img src="img/footer-logo.png" alt=""></a>
                        </div>
                        <p>The customer is at the heart of our unique business model, which includes design.</p>
                        <a href="#"><img src="img/payment.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-2 offset-lg-1 col-md-3 col-sm-6">
                    <div class="footer__widget">
                        <h6>Shopping</h6>
                        <ul>
                            <li><a href="#">Clothing Store</a></li>
                            <li><a href="#">Trending Shoes</a></li>
                            <li><a href="#">Accessories</a></li>
                            <li><a href="#">Sale</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <div class="footer__widget">
                        <h6>Shopping</h6>
                        <ul>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Payment Methods</a></li>
                            <li><a href="#">Delivary</a></li>
                            <li><a href="#">Return & Exchanges</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 offset-lg-1 col-md-6 col-sm-6">
                    <div class="footer__widget">
                        <h6>NewLetter</h6>
                        <div class="footer__newslatter">
                            <p>Be the first to know about new arrivals, look books, sales & promos!</p>
                            <form action="#">
                                <input type="text" placeholder="Your email">
                                <button type="submit"><span class="icon_mail_alt"></span></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="footer__copyright__text">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        <p>Copyright �
                            <script>
                                document.write(new Date().getFullYear());
                            </script>2020
                            All rights reserved | This template is made with <i class="fa fa-heart-o"
                                                                                aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        </p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Search Begin -->
    <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch">+</div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search End -->
    <!-- Script ?? ?n form sau khi ?� comment -->
    <script>
        // L?y gi� tr? c?a averageRating t? server-side (Java code) v� ??nh d?ng n� v?i 2 s? th?p ph�n sau d?u ph?y
        var averageRating = ${averageRating}; // ??t gi� tr? averageRating ? ?�y
        var formattedRating = averageRating.toFixed(2) + " / 5";

        // Hi?n th? gi� tr? ?� ??nh d?ng trong ph?n t? c� id l� 'formattedRating'
        document.getElementById("formattedRating").textContent = formattedRating;
    </script>
    <script>
        // JavaScript code to handle star rating selection
        const stars = document.querySelectorAll('.star');
        const ratingValue = document.getElementById('rating-value');

        stars.forEach(star => {
            star.addEventListener('click', () => {
                const value = parseInt(star.getAttribute('data-value'));
                ratingValue.value = value;
                stars.forEach(s => {
                    if (parseInt(s.getAttribute('data-value')) <= value) {
                        s.classList.add('selected');
                    } else {
                        s.classList.remove('selected');
                    }
                });
            });
        });
    </script>

    <style>
        .star {
            font-size: 30px;
            cursor: pointer;
            color: #ccc;
        }

        .star.selected, .star:hover {
            color: #f7941d; /* M�u c?a sao khi ???c ch?n ho?c khi di chu?t v�o */
        }
    </style>
    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js" integrity="sha512-HGOnQO9+SP1V92SrtZfjqxxtLmVzqZpjFFekvzZVWoiASSQgSr4cw9Kqd2+l8Llp4Gm0G8GIFJ4ddwZilcdb8A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.js" integrity="sha512-eP8DK17a+MOcKHXC5Yrqzd8WI5WKh6F1TIk5QZ/8Lbv+8ssblcz7oGC8ZmQ/ZSAPa7ZmsCU4e/hcovqR8jfJqA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript">
        $('.filtering').slick({
            slidesToShow: 4,
            slidesToScroll: 2,
            arrows: true
        });

        var filtered = false;

        $('.js-filter').on('click', function () {
            if (filtered === false) {
                $('.filtering').slick('slickFilter', ':even');
                $(this).text('Unfilter Slides');
                filtered = true;
            } else {
                $('.filtering').slick('slickUnfilter');
                $(this).text('Filter Slides');
                filtered = false;
            }
        });
        
          document.addEventListener("DOMContentLoaded", function () {
                                    var originalPrices = document.querySelectorAll('.original-price');
                                    originalPrices.forEach(function (originalPrice) {
                                        originalPrice.textContent = formatPrice(originalPrice.textContent.trim());
                                    });

                                    function formatPrice(price) {
                                        return (parseFloat(price)).toLocaleString('vi-VN', {style: 'currency', currency: 'VND'}).replace('VND', '');
                                    }
                                });
    </script>

</body>

</html>