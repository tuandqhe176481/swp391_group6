<!doctype html>
<html lang="en">
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ page import="java.util.List" %>
    <%@ page import="model.Order" %>
    <%@ page import="model.BrandTotal" %>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
        <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/libs/css/style.css">
        <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="assets/vendor/vector-map/jqvmap.css">
        <link rel="stylesheet" href="assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css">
        <link rel="stylesheet" href="assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
        <title>Concept - Bootstrap 4 Admin Dashboard Template</title>
        <style>
            h4 {
                display: flex;
                align-items: center;
                justify-content: right;
            }

            input {
                padding: 8px;
                border: 1px solid #ccc;
                border-radius: 3px;
                outline: none;

            }

            a {
                color: #fff;
                text-decoration: none;
                margin: 0 10px;
            }
            .card-body {
                display: flex;
                align-items: center;
            }
            .text-muted {
                flex-grow: 1;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }
        </style>
    </head>

    <body>
        <!-- ============================================================== -->
        <!-- main wrapper -->
        <!-- ============================================================== -->
        <div class="dashboard-main-wrapper" id="dataContainer"  >
            <!-- ============================================================== -->
            <!-- navbar -->
            <!-- ============================================================== -->
            <div class="dashboard-header">
                <nav class="navbar navbar-expand-lg bg-white fixed-top">
                    <a class="navbar-brand" href="home">Concept</a>

                </nav>
            </div>
            <!-- ============================================================== -->
            <!-- end navbar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- left sidebar -->
            <!-- ============================================================== -->
            <div class="nav-left-sidebar sidebar-dark">
                <div class="menu-list">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav flex-column">
                                <li class="nav-divider">
                                    Menu
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="admindashboard">
                                        <i class="fa fa-fw fa-user-circle"></i>
                                        Dashboard
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="usermanager">
                                        <i class="fas fa-fw fa-chart-pie"></i>
                                        User Manager
                                    </a>
                                </li>
                                   <li class="nav-item">
                                    <a class="nav-link" href="settinglist">
                                        <i class="fas fa-fw fa-chart-pie"></i>
                                       Setting List
                                    </a>
                                </li>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end left sidebar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- wrapper  -->
            <!-- ============================================================== -->
            <div class="dashboard-wrapper">
                <div class="container-fluid  dashboard-content">
                    <!-- ============================================================== -->
                    <!-- pagehader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h3 class="mb-2">Admin Dashboard</h3>


                                <div class="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="admindashboard" class="breadcrumb-link">Dashboard</a></li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- pagehader  -->
                    <!-- ============================================================== -->
                    <h4>
                        <form action="admindashboard">
                            <a><input value="${start}" type="datetime-local" name="startDate"/></a>
                            <a>To</a>
                            <a><input value="${end}"  type="datetime-local" name="endDate"/></a>
                            <a><input type="submit" value="Find" /></a> 
                        </form>
                    </h4>

                    <div class="row">
                        <!-- metric -->

                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body">
                                    <img src="img/icon/pendingConfirm.png" alt="alt" style="float: left; width: 30%;"/>
                                    <div style="float: left; width: 70%;" >
                                        <h5 class="text-muted"  style="margin-left: 10px; margin-top: 0; margin-right: 10px; text-align: right;">Order Pending</h5>
                                        <div class="metric-value d-inline-block" style="margin-left: 10px; text-align: right;">
                                            <h1 class="mb-1 text-primary" style="margin-left: 40px; margin-top: 0; margin-right: 10px; text-align: right;">${count1}</h1>
                                        </div>
                                    </div>
                                </div>
                                <div id="sparkline"></div>
                            </div>
                        </div>
                        <!-- /. metric -->
                        <!-- metric -->
                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body">
                                    <img src="img/icon/confirm.png" alt="alt" style="float: left; width: 30%;"/>
                                    <div style="float: left; width: 70%;">
                                        <h5 class="text-muted" style="margin-left: 10px; margin-top: 0; margin-right: 10px; text-align: right;">Order Confirmed</h5>
                                        <div class="metric-value d-inline-block" style="margin-left: 10px; text-align: right;">
                                            <h1 class="mb-1 text-primary" style="margin-left: 40px; margin-top: 0; margin-right: 10px; text-align: right;">${count2} </h1>
                                        </div>
                                    </div>
                                </div>
                                <div id="sparkline"></div>
                            </div>
                        </div>
                        <!-- /. metric -->
                        <!-- metric -->
                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body">
                                    <img src="img/icon/shipping.png" alt="alt" style="float: left; width: 30%;"/>
                                    <div style="float: left; width: 70%;">
                                        <h5 class="text-muted" style="margin-left: 10px; margin-top: 0; margin-right: 10px; text-align: right;">Order Shipping</h5>
                                        <div class="metric-value d-inline-block" style="margin-left: 10px; text-align: right;">
                                            <h1 class="mb-1 text-primary" style="margin-left: 40px; margin-top: 0; margin-right: 10px; text-align: right;">${count3}</h1>
                                        </div>
                                    </div>
                                    <div style="clear: both;"></div>
                                </div>
                                <div id="sparkline"></div>
                            </div>
                        </div>




                        <!-- /. metric -->
                        <!-- metric -->
                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body">
                                    <img src="img/icon/completed.png" alt="alt" style="float: left; width: 30%;"/>
                                    <div style="float: left; width: 70%;">
                                        <h5 class="text-muted" style="margin-left: 10px; margin-top: 0; margin-right: 10px; text-align: right;">Order Completed</h5>
                                        <div class="metric-value d-inline-block"  style="margin-left: 10px; text-align: right;">
                                            <h1 class="mb-1 text-primary" style="margin-left: 40px; margin-top: 0; margin-right: 10px; text-align: right;">${count4} </h1>
                                        </div>
                                    </div>
                                </div>
                                <div id="sparkline"></div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body">
                                    <img src="img/icon/cancel.png" alt="alt" style="float: left; width: 30%;"/>
                                    <div style="float: left; width: 70%;">
                                        <h5 class="text-muted" style="margin-left: 10px; margin-top: 0; margin-right: 10px; text-align: right;">Order Cancel</h5>
                                        <div class="metric-value d-inline-block" style="margin-left: 10px; text-align: right;">
                                            <h1 class="mb-1 text-primary" style="margin-left: 40px; margin-top: 0; margin-right: 10px; text-align: right;">${count5} </h1>
                                        </div>
                                    </div>
                                </div>
                                <div id="sparkline"></div>
                            </div>
                        </div>

                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body">
                                    <img src="img/icon/newCus.png" alt="alt" style="float: left; width: 30%;"/>
                                    <div style="float: left; width: 70%;">
                                        <h5 class="text-muted" style="margin-left: 10px; margin-top: 0; margin-right: 10px; text-align: right;">Newly registered customer</h5>
                                        <div class="metric-value d-inline-block" style="margin-left: 10px; text-align: right;">
                                            <h1 class="mb-1 text-primary" style="margin-left: 40px; margin-top: 0; margin-right: 10px; text-align: right;">${newCusCreate.name} </h1>
                                        </div>
                                    </div>
                                </div>
                                <div id="sparkline"></div>
                            </div>
                        </div>

                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body">
                                    <img src="img/icon/newBuy.png" alt="alt" style="float: left; width: 30%;"/>
                                    <div style="float: left; width: 70%;">
                                        <h5 class="text-muted" style="margin-left: 10px; margin-top: 0; margin-right: 10px; text-align: right;">New customer buy</h5>
                                        <div class="metric-value d-inline-block" style="margin-left: 10px; text-align: right;">
                                            <h1 class="mb-1 text-primary" style="margin-left: 40px; margin-top: 0; margin-right: 10px; text-align: right;">${newCusOrder.name} </h1>
                                        </div>
                                    </div>
                                </div>
                                <div id="sparkline"></div>
                            </div>
                        </div>

                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body">
                                    <img src="img/icon/revenue.png" alt="alt" style="float: left; width: 30%;"/>
                                    <div style="float: left; width: 70%;">
                                        <h5 class="text-muted" style="margin-left: 0px; margin-top: 0; margin-right: 50px; text-align: right;">Revenue</h5>
                                        <div class="metric-value d-inline-block" style="margin-left: 10px; text-align: right;">
                                            <h1 class="mb-1 text-primary" style="margin-left: 40px; margin-top: 0; margin-right: 10px; text-align: right;">$${total} </h1>
                                        </div>
                                    </div>
                                </div>
                                <div id="sparkline"></div>
                            </div>
                        </div>
                        <!-- /. metric -->
                    </div>
                    <!-- ============================================================== -->
                    <!-- revenue  -->
                    <!-- ============================================================== -->
                    <div class="row" >
                        <div class="col-xl-8 col-lg-12 col-md-8 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Top Selling Products</h5>
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead class="bg-light">
                                                <tr class="border-0">
                                                    <th class="border-0">#</th>
                                                    <th class="border-0">Image</th>
                                                    <th class="border-0">Product Name</th>
                                                    <th class="border-0">Product Id</th>
                                                    <th class="border-0">Price</th>
                                                    <th class="border-0">Quantity Sold</th>
                                                    <th class="border-0">Price Sold</th>


                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${listo}" var="o">
                                                    <tr>
                                                        <td>1</td>
                                                        <td>
                                                            <div class="m-r-10"><img src="${o.img}" alt="user" class="rounded" width="45"></div>
                                                        </td>
                                                        <td>${o.pname}</td>
                                                        <td>${o.productId}</td>
                                                        <td class="original-price">${o.productPrice} ?</td>
                                                        <td>${o.quantity}</td>
                                                        <td class="original-price">${o.price} ?</td>


                                                    </tr>

                                                </c:forEach>




                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end reveune  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- total sale  -->
                        <!-- ============================================================== -->
                        <div class="col-xl-4 col-lg-12 col-md-4 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Total Sale</h5>
                                <div >
                                    <canvas id="myPieChart" width="220" height="155"></canvas>
                                    <div class="chart-widget-list" style="text-align: center;">
                                        <h5 style="display: block;">Brand Total Amount</h5>    
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end total sale  -->
                        <!-- ============================================================== -->
                    </div>
                    <div class="row">
                        <!-- ============================================================== -->
                        <!-- top selling products  -->
                        <!-- ============================================================== -->
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                        </div>

                        <!-- ============================================================== -->
                        <!-- end revenue locations  -->
                        <!-- ============================================================== -->
                    </div>

                </div>
                <!-- ============================================================== -->
                <!-- footer -->
                <!-- ============================================================== -->
                <div class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                Copyright � 2018 Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="text-md-right footer-links d-none d-sm-block">
                                    <a href="javascript: void(0);">About</a>
                                    <a href="javascript: void(0);">Support</a>
                                    <a href="javascript: void(0);">Contact Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end footer -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- end wrapper  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end main wrapper  -->
        <!-- ============================================================== -->
        <!-- Optional JavaScript -->
        <!-- jquery 3.3.1 js-->
        <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
        <!-- bootstrap bundle js-->
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
        <!-- slimscroll js-->
        <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
        <!-- chartjs js-->
        <script src="assets/vendor/charts/charts-bundle/Chart.bundle.js"></script>
        <script src="assets/vendor/charts/charts-bundle/chartjs.js"></script>

        <!-- main js-->
        <script src="assets/libs/js/main-js.js"></script>
        <!-- jvactormap js-->
        <script src="assets/vendor/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
        <script src="assets/vendor/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- sparkline js-->
        <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
        <script src="assets/vendor/charts/sparkline/spark-js.js"></script>
        <!-- dashboard sales js-->
        <script src="assets/libs/js/dashboard-sales.js"></script>

        <script>
            <%
                List<BrandTotal> list = (List<BrandTotal>) request.getAttribute("listbt");
            %>
                                        function generateYearOptions() {
                                            var currentYear = new Date().getFullYear();
                                            var selectElement = $("#yearSelect");

                                            for (var i = 0; i <= 10; i++) {
                                                var year = currentYear - i;
                                                var option = $("<option>").val(year).text(year);
                                                selectElement.append(option);
                                            }
                                        }

                                        // G?i h�m khi trang ???c t?i
                                        $(document).ready(function () {
                                            generateYearOptions();
                                        });
                                        var xValues = [
            <%
                for (BrandTotal b : list) {
                 out.println("\"" + b.getBrandName() + "\"" + ",");
                 }
            %>
                                        ];


                                        var yValues = [
            <%
                for (BrandTotal b : list) {
                    out.println(b.getTotalAmount() + ",");
                 }
            %>
                                        ];
                                        var barColors = [
                                            "#b91d47",
                                            "#00aba9",
                                            "#2b5797",
                                            "#e8c3b9",
                                            "#1e7145"
                                        ];

                                        new Chart("myPieChart", {
                                            type: "pie",
                                            data: {
                                                labels: xValues,
                                                datasets: [{
                                                        backgroundColor: barColors,
                                                        data: yValues
                                                    }]
                                            },
                                            options: {
                                                title: {
                                                    display: false,

                                                }
                                            }
                                        });
        </script>
   <script>
            document.addEventListener("DOMContentLoaded", function () {
                var originalPrices = document.querySelectorAll('.original-price');
                originalPrices.forEach(function (originalPrice) {
                    originalPrice.textContent = formatPrice(originalPrice.textContent.trim());
                });

                function formatPrice(price) {
                    return (parseFloat(price)).toLocaleString('vi-VN', {style: 'currency', currency: 'VND'}).replace('VND', '');
                }
            });
        </script>
    </body>

</html>