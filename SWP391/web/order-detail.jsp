<!doctype html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
        <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/libs/css/style.css">
        <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="assets/vendor/charts/chartist-bundle/chartist.css">
        <link rel="stylesheet" href="assets/vendor/charts/morris-bundle/morris.css">
        <link rel="stylesheet" href="assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="assets/vendor/charts/c3charts/c3.css">
        <link rel="stylesheet" href="assets/vendor/fonts/flag-icon-css/flag-icon.min.css">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
        <title>Concept - Bootstrap 4 Admin Dashboard Template</title>

        <style>
            .filter {
                display: flex;
                align-items: center;
                margin-bottom: 10px;
            }
            .filter1 {
                display: flex;
                align-items: center;
            }

            .texta {
                margin-right: 10px;
                font-weight: bold;
                font-size: 18px;

            }

            select {
                padding: 8px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            /* T�y ch?nh c�c select n?m trong .filter */
            .filter select {
                margin-right: 10px;
            }

            /* T�y ch?nh m�u n?n khi hover tr�n select */
            .filter select:hover {
                background-color: #f5f5f5;
            }
            select:hover {
                background-color: #f5f5f5;
            }

            input[type="submit"] {
                padding: 10px 15px;
                background-color: #4CAF50;
                color: white;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            /* Hi?u ?ng hover cho n�t Filter */
            input[type="submit"]:hover {
                background-color: #45a049;
            }

            .search-p{
                justify-content: right;
            }
            .product__cart__item__pic {
                width: 90px;
                height: 90px;
                overflow: hidden;
            }

            .product__cart__item__pic img {
                width: 100%;
                height: 100%;
                object-fit: cover;
            }

            .shopping__cart__table table tbody tr td.quantity__item {
                width: 270px;
            }
            .shopping__cart__table table tbody tr td.quantity__price {
                width: 270px;
            }
            .shopping__cart__table table tbody tr td.product__cart__item {
                width: 450px;
            }
            .total {
                font-size: 24px;
                font-weight: bold;
                text-align: right;
            }
            h4 {
                display: flex;
                align-items: center;
                justify-content: right;
            }

            input {
                padding: 8px;
                border: 1px solid #ccc;
                border-radius: 3px;
                outline: none;

            }

            a {
                color: #fff;
                text-decoration: none;
                margin: 0 10px;
            }
            .navbar-brand {
                display: inline-block;
                margin-right: 1rem;
                line-height: inherit;
                white-space: nowrap;
                padding: 11px 20px;
                font-size: 30px;
                text-transform: uppercase;
                font-weight: 700;
                color: #007bff;
            }
        </style>
    </head>

    <body>
        <!-- ============================================================== -->
        <!-- main wrapper -->
        <!-- ============================================================== -->
        <div class="dashboard-main-wrapper">
            <!-- ============================================================== -->
            <!-- navbar -->
            <!-- ============================================================== -->
            <div class="dashboard-header">
                <nav class="navbar navbar-expand-lg bg-white fixed-top">
                    <a class="navbar-brand" href="home">GenzFashion</a>

                </nav>
            </div>
            <!-- ============================================================== -->
            <!-- end navbar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- left sidebar -->
            <!-- ============================================================== -->
            <div class="nav-left-sidebar sidebar-dark">
                <div class="menu-list">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <c:if test="${sessionScope.acc.roleId eq 2}">
                                <ul class="navbar-nav flex-column">
                                    <li class="nav-divider">
                                        Dashboard-Sales-Manager
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="salemanagerdashboard" onclick="setActive(this)">
                                            <i class="fa fa-fw fa-user-circle"></i>
                                            Dashboard
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link active" href="ordermanagersm" onclick="setActive(this)">
                                            <i class="fas fa-fw fa-chart-pie"></i>
                                            Order Manager
                                        </a>
                                    </li>

                                </ul>    
                            </c:if>
                            <c:if test="${sessionScope.acc.roleId eq 3}">
                                <ul class="navbar-nav flex-column">
                                    <li class="nav-divider">
                                        Dashboard-Sales
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link active" href="ordermanager" onclick="setActive(this)">
                                            <i class="fas fa-fw fa-chart-pie"></i>
                                            Order Manager
                                        </a>
                                    </li>

                                </ul>
                            </c:if>
                            <c:if test="${sessionScope.acc.roleId eq 6}">
                                <ul class="navbar-nav flex-column">
                                    <li class="nav-divider">
                                        Dashboard-Inventory
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link active" href="dashboardinventory" onclick="setActive(this)">
                                            <i class="fa fa-fw fa-user-circle"></i>
                                            Dashboard
                                        </a>
                                    </li>

                                </ul>
                            </c:if>


                        </div>
                    </nav>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end left sidebar -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- wrapper  -->
            <!-- ============================================================== -->
            <div class="dashboard-wrapper">
                <div class="dashboard-ecommerce">
                    <div class="container-fluid dashboard-content ">
                        <!-- ============================================================== -->
                        <!-- pageheader  -->
                        <!-- ============================================================== -->
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="page-header">
                                    <h2 class="pageheader-title">Order Detail</h2>
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end pageheader  -->
                        <!-- ============================================================== -->
                        <div class="ecommerce-widget">

                            <div class="row">
                                <!-- ============================================================== -->

                                <!-- ============================================================== -->

                                <!-- recent orders  -->
                                <!-- ============================================================== -->
                                <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">

                                    <div class="card">

                                        <table>

                                            <thead class="bg-light">
                                                <tr class="border-0">
                                                    <th class="border-0">ID</th>
                                                    <th class="border-0">Image</th>
                                                    <th class="border-0">Product</th>    
                                                    <th class="border-0">Price</th>
                                                    <th class="border-0">Quantity</th>                                                            
                                                    <th class="border-0">Total</th>                                                        
                                            </thead>
                                            <tbody>

                                                <c:forEach  items="${list}" var="l">
                                                    <tr>
                                                        <td class="product__cart__item">
                                                            <div class="product__cart__item__id">
                                                                <h6>${l.productId}</h6>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="product__cart__item__pic">
                                                                <img src="${l.img}" alt="">
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="product__cart__item__text">
                                                                <h6>${l.pname}</h6>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="product__cart__item__price">
                                                                <h6 class="original-price">${l.price}</h6>
                                                            </div>
                                                        </td>
                                                        <td class="quantity__item">
                                                            <div class="quantity">
                                                                <div>
                                                                    &emsp;<span class="qty">${l.quantity}</span>&emsp;
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="cart__price original-price"> ${l.price*l.quantity}</td>


                                                    </tr>
                                                </c:forEach>




                                            </tbody>
                                        </table>
                                    </div>    
                                    <c:set value="${order}" var="o"/>
                                    <div class="row">

                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="total">
                                                Total: <span class="original-price">${o.totalAmount}</span>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-sm-2"></div> 
                                        <div class="col-lg-8 col-md-8 col-sm-8"> 
                                            <table border="1" style="width: 100%;">
                                                <tr>
                                                    <th style="width: 40%;"></th> 
                                                    <th style="width: 60%;"></th> 
                                                </tr>
                                                <tr>
                                                    <td>Receiver:</td>
                                                    <td>${o.receiver}</td>
                                                </tr>
                                                <tr>
                                                    <td>Ordered At:</td>
                                                    <td>${o.ordered_at}</td>
                                                </tr>
                                                <tr>
                                                    <td>Address:</td>
                                                    <td>${o.address}</td>
                                                </tr>
                                                <tr>
                                                    <td>Phone Number:</td>
                                                    <td>${o.phoneNumber}</td>
                                                </tr>
                                                <tr>
                                                    <td>Payment:</td>
                                                    <td>${o.payment}</td>
                                                </tr>
                                                <tr>
                                                    <td>Notes:</td>
                                                    <td>${o.note}</td>
                                                </tr>
                                                <tr>
                                                    <td>Status:</td>
                                                    <td>${o.status}</td>
                                                </tr>

                                            </table>    
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2"></div> 
                                    </div>




                                </div>
                                <!-- ============================================================== -->
                                <!-- end recent orders  -->


                                <!-- ============================================================== -->
                                <!-- ============================================================== -->
                                <!-- customer acquistion  -->
                                <!-- ============================================================== -->
                                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">

                                </div>
                                <!-- ============================================================== -->
                                <!-- end customer acquistion  -->
                                <!-- ============================================================== -->
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            Copyright � 2018 Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="text-md-right footer-links d-none d-sm-block">
                                <a href="javascript: void(0);">About</a>
                                <a href="javascript: void(0);">Support</a>
                                <a href="javascript: void(0);">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="assets/libs/js/dashboard-ecommerce.js"></script>



  <script>
            document.addEventListener("DOMContentLoaded", function () {
                var originalPrices = document.querySelectorAll('.original-price');
                originalPrices.forEach(function (originalPrice) {
                    originalPrice.textContent = formatPrice(originalPrice.textContent.trim());
                });

                function formatPrice(price) {
                    return (parseFloat(price)).toLocaleString('vi-VN', {style: 'currency', currency: 'VND'}).replace('VND', '');
                }
            });
        </script>


</body>

</html>