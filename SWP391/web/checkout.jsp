<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zxx">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Male_Fashion Template">
        <meta name="keywords" content="Male_Fashion, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Male-Fashion | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap"
              rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
    </head>

    <body>
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>

        <!-- Offcanvas Menu Begin -->
        <div class="offcanvas-menu-overlay"></div>
        <div class="offcanvas-menu-wrapper">
            <div class="offcanvas__option">
                <div class="offcanvas__links">
                    <a href="#">Sign in</a>
                    <a href="#">FAQs</a>
                </div>
                <div class="offcanvas__top__hover">
                    <span>Usd <i class="arrow_carrot-down"></i></span>
                    <ul>
                        <li>USD</li>
                        <li>EUR</li>
                        <li>USD</li>
                    </ul>
                </div>
            </div>
            <div class="offcanvas__nav__option">
                <a href="#" class="search-switch"><img src="img/icon/search.png" alt=""></a>
                <a href="#"><img src="img/icon/heart.png" alt=""></a>
                <a href="#"><img src="img/icon/cart.png" alt=""> <span>0</span></a>
                <div class="price">$0.00</div>
            </div>
            <div id="mobile-menu-wrap"></div>
            <div class="offcanvas__text">
                <p>Free shipping, 30-day return or refund guarantee.</p>
            </div>
        </div>
        <!-- Offcanvas Menu End -->

        <!-- Header Section Begin -->
        <header class="header">
            <div class="header__top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-7">
                            <div class="header__top__left">
                                <p>Free shipping, 30-day return or refund guarantee.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-5">
                            <div class="header__top__right">
                                <div class="header__top__links">
                                    <c:if test="${sessionScope.acc==null}">
                                        <a href="login">Sign in</a> 
                                    </c:if>


                                </div>
                                <c:if test="${sessionScope.acc!=null}">
                                    <div class="header__top__hover">

                                        <span>${sessionScope.acc.name} <i class="arrow_carrot-down"></i></span>
                                        <ul>
                                            <li><a href="userprofile?uid=${sessionScope.acc.email}">Profile</a> </li>
                                            <li><a href="#">My Order</a> </li>
                                            <li><a href="logout">Log out</a></li>
                                        </ul>


                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div class="header__logo">
                            <a href="./home.jsp"><img src="img/logo.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li class=""><a href="./home">Home</a></li>
                                <li><a class="active" href="./shop">Shop</a></li>
                                <li><a href="./blog">Blog</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <div class="header__nav__option">
                            <c:set value="${total}" var="total"/>
                            <a href="#" class="search-switch"><img src="img/icon/search.png" alt=""></a>
                                <c:set value="${size}" var="size"/>
                            <a href="viewcart"><img src="img/icon/cart.png" alt=""> <span>${size}</span></a>
                            <div class="price original-price">${total}đ</div>
                        </div>
                    </div>
                </div>
                <div class="canvas__open"><i class="fa fa-bars"></i></div>
            </div>
        </header>
        <!-- Header Section End -->

        <!-- Breadcrumb Section Begin -->
        <section class="breadcrumb-option">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="breadcrumb__text">
                            <h4>Check Out</h4>
                            <div class="breadcrumb__links">
                                <a href="./home">Home</a>
                                <a href="./shop">Shop</a>
                                <a href="./viewcart">Shopping Cart</a>
                                <span>Check Out</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Breadcrumb Section End -->

        <!-- Checkout Section Begin -->
        <section class="checkout spad">
            <div class="container">
                <div class="checkout__form">
                    <form id="frmCreateOrder" method="post">
                        <div class="row">
                            <div class="col-lg-7 col-md-6">
                                <h6 class="coupon__code"><span class="icon_tag_alt"></span> Have a coupon? <a href="#">Click
                                        here</a> to enter your code</h6>
                                <h6 class="checkout__title">Billing Details</h6>
                                <c:set value="${requestScope.ap}" var="ap"/>


                                <div class="checkout__input">
                                    <p>Your Name<span>*</span></p>
                                    <input type="text" name="receiver" placeholder="Input Your Name" value="${ap.name}" required="">
                                </div>

                                <div class="checkout__input">
                                    <p>Address<span>*</span></p>
                                    <input type="text" name="address" placeholder="Street Address" value="${ap.address}" required="">
                                </div>


                                <div class="checkout__input">
                                    <p>Phone<span>*</span></p>
                                    <input type="number" name="phone" placeholder="Phone Number" value="${ap.phone_number}" required="">
                                </div>

                                <div class="checkout__input">
                                    <p>Email<span>*</span></p>
                                    <input type="text" name="email" placeholder="Your Email" value="${ap.email}" readonly="">
                                </div>

                                <div class="checkout__input">
                                    <p>Order notes<span>*</span></p>
                                    <input type="text" name="note"
                                           placeholder="Notes about your order, e.g. special notes for delivery.">
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="continue__btn">
                                            <a href="viewcart">Change Cart</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-5 col-md-6">
                                <div class="checkout__order">
                                    <h4 class="order__title">Your order</h4>
                                    <div class="checkout__order__products">Product <span>Total</span></div>

                                    <c:set value="${sessionScope.cart}" var="o"/>
                                    <c:forEach  items="${listi}" var="i">
                                        <ul class="checkout__total__products">
                                            <li style="font-size: 12px">x${i.quantity}&ensp;
                                                ${i.product.pname} <span class="original-price">${i.price*i.quantity}đ</span></li>
                                        </ul>
                                    </c:forEach>
                                    <ul class="checkout__total__all"> 
                                        <li> Total<span class="original-price">${total}đ</span></li>
                                    </ul>
                                    <input type="hidden" name="amount" value="${total}">
                                    <input type="hidden" name="payment" id="payment" value="">
                                    <div class="checkout__payment">
                                        <input type="radio" name="payment" id="onlinePayment" value="Online" required="">
                                        <label for="onlinePayment">PLACE ORDER Online</label>
                                        <br>
                                        <input type="radio" name="payment" id="codPayment" value="COD" required="">
                                        <label for="codPayment">PLACE ORDER COD</label>
                                    </div>
                                    <button type="button" class="site-btn" onclick="submitForm()">PLACE ORDER</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- Checkout Section End -->

        <!-- Footer Section Begin -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="footer__about">
                            <div class="footer__logo">
                                <a href="#"><img src="img/footer-logo.png" alt=""></a>
                            </div>
                            <p>The customer is at the heart of our unique business model, which includes design.</p>
                            <a href="#"><img src="img/payment.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-2 offset-lg-1 col-md-3 col-sm-6">
                        <div class="footer__widget">
                            <h6>Shopping</h6>
                            <ul>
                                <li><a href="#">Clothing Store</a></li>
                                <li><a href="#">Trending Shoes</a></li>
                                <li><a href="#">Accessories</a></li>
                                <li><a href="#">Sale</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-6">
                        <div class="footer__widget">
                            <h6>Shopping</h6>
                            <ul>
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">Payment Methods</a></li>
                                <li><a href="#">Delivary</a></li>
                                <li><a href="#">Return & Exchanges</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 offset-lg-1 col-md-6 col-sm-6">
                        <div class="footer__widget">
                            <h6>NewLetter</h6>
                            <div class="footer__newslatter">
                                <p>Be the first to know about new arrivals, look books, sales & promos!</p>
                                <form action="#">
                                    <input type="text" placeholder="Your email">
                                    <button type="submit"><span class="icon_mail_alt"></span></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </footer>
        <!-- Footer Section End -->


        <!-- Search Begin -->
        <div class="search-model">
            <div class="h-100 d-flex align-items-center justify-content-center">
                <div class="search-close-switch">+</div>
                <form class="search-model-form">
                    <input type="text" id="search-input" placeholder="Search here.....">
                </form>
            </div>
        </div>
        <!-- Search End -->

        <!-- Js Plugins -->
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery.nicescroll.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/jquery.countdown.min.js"></script>
        <script src="js/jquery.slicknav.js"></script>
        <script src="js/mixitup.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/main.js"></script>
        <script src="js/jquery-1.11.3.min.js"></script>


        <script type="text/javascript">
                                        $("#frmCreateOrder").submit(function () {
                                            var postData = $("#frmCreateOrder").serialize();
                                            var submitUrl = $("#frmCreateOrder").attr("action");
                                            $.ajax({
                                                type: "POST",
                                                url: submitUrl,
                                                data: postData,
                                                dataType: 'JSON',
                                                success: function (x) {
                                                    if (x.code === '00') {
                                                        if (window.vnpay) {
                                                            vnpay.open({width: 768, height: 600, url: x.data});
                                                        } else {
                                                            location.href = x.data;
                                                        }
                                                        return false;
                                                    } else {
                                                        alert(x.Message);
                                                    }
                                                }
                                            });
                                            return false;
                                        });
        </script>     
        <script>

            function submitForm() {
                var paymentMethod = document.querySelector('input[name="payment"]:checked').value;
                if (paymentMethod === "Online") {
                    setActionAndSubmit('vnpayajax', 'Online');
                } else if (paymentMethod === "COD") {
                    setActionAndSubmit('confirmorder', 'COD');
                }
            }
            function setActionAndSubmit(action, paymentMethod) {
                document.getElementById('payment').value = paymentMethod;
                document.getElementById('frmCreateOrder').action = action;
                document.getElementById('frmCreateOrder').submit();
            }

          
        </script>
        <script>
            document.addEventListener("DOMContentLoaded", function () {
                var originalPrices = document.querySelectorAll('.original-price');
                originalPrices.forEach(function (originalPrice) {
                    originalPrice.textContent = formatPrice(originalPrice.textContent.trim());
                });

                function formatPrice(price) {
                    return (parseFloat(price)).toLocaleString('vi-VN', {style: 'currency', currency: 'VND'}).replace('VND', '');
                }
            });
        </script>
        <script>

            function submitForm() {
                var paymentMethod = document.querySelector('input[name="payment"]:checked').value;
                if (paymentMethod === "Online") {
                    setActionAndSubmit('vnpayajax', 'Online');
                    var postData = $("#frmCreateOrder").serialize();
                                            var submitUrl = $("#frmCreateOrder").attr("action");
                                            $.ajax({
                                                type: "POST",
                                                url: submitUrl,
                                                data: postData,
                                                dataType: 'JSON',
                                                success: function (x) {
                                                    if (x.code === '00') {
                                                        if (window.vnpay) {
                                                            vnpay.open({width: 768, height: 600, url: x.data});
                                                        } else {
                                                            location.href = x.data;
                                                        }
                                                        return false;
                                                    } else {
                                                        alert(x.Message);
                                                    }
                                                }
                                            });
                                            return false;
                } else if (paymentMethod === "COD") {
                    setActionAndSubmit('confirmorder', 'COD');
                }
            }
            function setActionAndSubmit(action, paymentMethod) {
                document.getElementById('payment').value = paymentMethod;
                document.getElementById('frmCreateOrder').action = action;
                document.getElementById('frmCreateOrder').submit();
            }

          
        </script>
    </body>

</html>