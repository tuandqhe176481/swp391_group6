/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.salemanager;

import controller.auth.Authorization;
import dal.MaketingDao;
import dal.SaleManagerDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import model.Accounts;
import model.Order;
import model.OrderDetail;
import model.SaleTotal;

/**
 *
 * @author PV
 */
@WebServlet(name = "DashboardSaleManagerServlet", urlPatterns = {"/salemanagerdashboard"})
public class DashboardSaleManagerServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DashboardSaleManagerServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DashboardSaleManagerServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (!Authorization.isSaleManager((Accounts) session.getAttribute("acc"))) {
            Authorization.redirectToHome(session, response);
        } else {
            SaleManagerDAO dao = new SaleManagerDAO();
            MaketingDao daoM = new MaketingDao();
            int sales = dao.getSales();
            int orders = dao.getOrder();
            int total = dao.getTotalAmount();

            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            if (startDate == null || startDate.equals("")) {
                LocalDate sevenDaysAgo = LocalDate.now().minusDays(7);
                startDate = sevenDaysAgo.format(DateTimeFormatter.ISO_LOCAL_DATE);
            }

            if (endDate == null || endDate.equals("")) {
                LocalDate today = LocalDate.now();
                endDate = today.format(DateTimeFormatter.ISO_LOCAL_DATE);
            }

            List<Order> listo = daoM.getRevenue("2024");
            request.setAttribute("listr", listo);

            int erning = daoM.getEarningDay(endDate);
            System.out.println(erning);
            request.setAttribute("erning", erning);

            Order highest = daoM.getHighestMonth();
            Order lowest = daoM.getLowestMonth();
            request.setAttribute("high", highest);
            request.setAttribute("low", lowest);

            List<OrderDetail> listod = daoM.getProductBestSeller();
            request.setAttribute("listod", listod);

            int s1 = dao.getCountStatus(1);
            int s2 = dao.getCountStatus(2);
            int s3 = dao.getCountStatus(3);
            int s4 = dao.getCountStatus(4);
            int s5 = dao.getCountStatus(5);
            request.setAttribute("s1", s1);
            request.setAttribute("s2", s2);
            request.setAttribute("s3", s3);
            request.setAttribute("s4", s4);
            request.setAttribute("s5", s5);

            List<SaleTotal> list = dao.getTotalBySale();
            request.setAttribute("list", list);
            request.setAttribute("sales", sales);
            request.setAttribute("orders", orders);
            request.setAttribute("total", total);
            request.getRequestDispatcher("dashboard-sale-manager.jsp").forward(request, response);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
