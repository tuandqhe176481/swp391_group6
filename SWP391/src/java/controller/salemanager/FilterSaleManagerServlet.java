/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.salemanager;

import controller.auth.Authorization;
import dal.SaleDAO;
import dal.SaleManagerDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import model.Accounts;
import model.OrderInfo;
import model.StatusOrder;

/**
 *
 * @author PV
 */
@WebServlet(name = "FilterSaleManagerServlet", urlPatterns = {"/filtersalemanager"})
public class FilterSaleManagerServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FilterSaleManagerServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FilterSaleManagerServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (!Authorization.isSaleManager((Accounts) session.getAttribute("acc"))) {
            Authorization.redirectToHome(session, response);
        } else {
            SaleDAO dao = new SaleDAO();
            SaleManagerDAO daoM = new SaleManagerDAO();
            List<StatusOrder> listso = dao.getStatusOrder();

            String statusid = request.getParameter("status");
            int sid = Integer.parseInt(statusid);
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");

            DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
            DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            LocalDate earningDate = LocalDate.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String datee = earningDate.format(formatter);
            System.out.println(datee);

            String startDateStr = request.getParameter("startDate");
            String endDateStr = request.getParameter("endDate");

            LocalDateTime startDateTime;
            LocalDateTime endDateTime;

            if (startDateStr == null || startDateStr.isEmpty()) {
                LocalDateTime sevenDaysAgo = LocalDateTime.now().minusDays(7);
                startDateTime = sevenDaysAgo;
            } else {
                startDateTime = LocalDateTime.parse(startDateStr, inputFormatter);
            }

            if (endDateStr == null || endDateStr.isEmpty()) {
                LocalDateTime today = LocalDateTime.now();
                endDateTime = today;
            } else {
                endDateTime = LocalDateTime.parse(endDateStr, inputFormatter);
            }

            String formattedStartDate = startDateTime.format(outputFormatter);
            String formattedEndDate = endDateTime.format(outputFormatter);
            String sale = request.getParameter("sale");

            List<OrderInfo> list = new ArrayList<>();
            if (sid == 0 && startDate == null && endDate == null) {
                if (!sale.equals("0")) {
                    list = dao.getOrderSales(Integer.parseInt(sale));
                } else {
                    list = daoM.getAllOrder();
                }

            } else if (sid != 0 && startDate == null && endDate == null) {
                if (!sale.equals("0")) {
                    list = dao.getOrderByStatus(Integer.parseInt(sale), statusid);
                } else {
                    list = daoM.getOrderByS(statusid);
                }

            } else if (sid == 0) {
                if (!sale.equals("0")) {
                    list = dao.getOrderSalesDate(Integer.parseInt(sale), formattedStartDate, formattedEndDate);
                } else {
                    list = daoM.getOrderDate(formattedStartDate, formattedEndDate);
                }

            } else if (sale.equals("0")) {

                list = daoM.getOrderBySAndD(statusid, formattedStartDate, formattedEndDate);
            } else {

                list = dao.getOrderByStatusAndDate(Integer.parseInt(sale), statusid, formattedStartDate, formattedEndDate);
            }
            if(list.size()==0){
            request.setAttribute("error", "No results found !");
            }

            int page, numperpage = 7;
            int size = list.size();
            int num = (size % 7 == 0 ? (size / 7) : ((size / 7) + 1));
            String xpage = request.getParameter("page1");
            if (xpage == null) {
                page = 1;
            } else {
                page = Integer.parseInt(xpage);
            }
            int start, end;
            start = (page - 1) * numperpage;
            end = Math.min(page * numperpage, size);
            List<OrderInfo> list1 = dao.getListByPage(list, start, end);

            List<Accounts> listsale = daoM.getAllSales();
            request.setAttribute("page1", page);
            request.setAttribute("num1", num);

            int select = Integer.parseInt(request.getParameter("status"));
            request.setAttribute("listsale", listsale);
            request.setAttribute("sale", sale);
            request.setAttribute("start", startDateStr);
            request.setAttribute("end", endDateStr);
            request.setAttribute("select", select);
            request.setAttribute("listso", listso);
            request.setAttribute("list", list1);
            request.getRequestDispatcher("order-manager2.jsp").forward(request, response);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
