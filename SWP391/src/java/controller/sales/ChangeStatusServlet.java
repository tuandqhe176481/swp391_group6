/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.sales;

import controller.auth.Authorization;
import dal.DAO;
import dal.SaleDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Accounts;
import model.Email;
import model.Product;

/**
 *
 * @author PV
 */
@WebServlet(name="ChangeStatusServlet", urlPatterns={"/changestatus"})
public class ChangeStatusServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ChangeStatusServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ChangeStatusServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (!Authorization.isSaler((Accounts) session.getAttribute("acc"))) {
            Authorization.redirectToHome(session, response);
        } else{
            String oid=request.getParameter("oid");
        int status=Integer.parseInt(request.getParameter("status")) ;
        int value=Integer.parseInt(request.getParameter("value"));
        SaleDAO dao=new SaleDAO();
        DAO dao1 = new DAO();
        if(status==1 && value==2){
        dao.changeOrder(oid,2);
        }
        else if(status==3 && value==1){
        dao.changeOrder(oid,4);
        String to = dao1.getEmailByOrderId(oid);
            if (to != null && !to.isEmpty()) {
                List<Product> orderedProducts = dao1.getOrderedProductsByOrderID(oid);
                StringBuilder content = new StringBuilder();
                content.append("<h2>Your order has been completed</h2>");
                content.append("<p>If you want to give feedback on the product you purchased, click on the link of each product and scroll down to the feedback section</p>");
                for (Product product : orderedProducts) {
                    content.append("<p>- ").append(product.getPname()).append("</p>");
                    content.append("<p>Link: <a href=\"http://localhost:9999/SWP391/productdetails?pid=").append(product.getPid()).append("\">").append("Feedback</a></p>");
                }

                String subject = "Your order completion";
                boolean result = Email.sendEmail(to, subject, content.toString());
                if (result) {
                    System.out.println("send email successful");
                } else {
                    System.out.println("send email error");
                }
            }
        }
        else if(value==0){
        dao.changeOrder(oid,5);
        dao.backProduct(oid);
        }
        request.getRequestDispatcher("ordermanager").forward(request, response);
        }
        
        
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
