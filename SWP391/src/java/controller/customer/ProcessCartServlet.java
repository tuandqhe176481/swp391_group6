/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import dal.CustomerDAO;
import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Accounts;
import model.Item;
import model.Product;

/**
 *
 * @author PV
 */
@WebServlet(name = "ProcessCartServlet", urlPatterns = {"/processcart"})
public class ProcessCartServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Accounts a = (Accounts) session.getAttribute("acc");
        CustomerDAO cd = new CustomerDAO();
        DAO d = new DAO();
        
        
        String tnum = request.getParameter("num");
        String tid = request.getParameter("id");
        int id, num;
        try {
            id = Integer.parseInt(tid);
            num = Integer.parseInt(tnum);
            Product p = d.getProductByPid(tid);
            int price ;
            if(p.isIsDiscount()){
            price=p.getPriceSale();
            }
            else{
            price=p.getPrice();
            }
            if ((num == -1) && (cd.checkQuantity(a.getUser_id(), id) == 1)) {
                cd.removeItem(id, price, a.getUser_id());
            } else if (num == -1 ) {  
                cd.decreaseItem(id, price, a.getUser_id());
            } else if(num == 1) {
                cd.increaseItem(id, price, a.getUser_id());
            }

        } catch (Exception e) {
        }
        List<Item> listi = cd.getCart(a.getUser_id());
        double total = cd.totalAmount(a.getUser_id());
        request.setAttribute("listi", listi);
        request.setAttribute("total", total);
        request.setAttribute("size", listi.size());
        request.getRequestDispatcher("shopping-cart.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
