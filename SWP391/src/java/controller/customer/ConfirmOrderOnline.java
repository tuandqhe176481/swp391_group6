/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import controller.auth.Authorization;
import dal.CustomerDAO;
import dal.DAO;
import dal.SaleDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Accounts;
import model.Email;
import model.Item;

/**
 *
 * @author PV
 */
@WebServlet(name = "ConfirmOrderOnline", urlPatterns = {"/confirmorderonline"})
public class ConfirmOrderOnline extends HttpServlet {

    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (!Authorization.isCustomer((Accounts) session.getAttribute("acc"))) {
            Authorization.redirectToHome(session, response);
        } else {

            CustomerDAO cd = new CustomerDAO();
            Accounts acc = (Accounts) session.getAttribute("acc");
            List<Item> listi = cd.getCart(acc.getUser_id());
            String address, phone, note, receiver, payment;
            address = (String) session.getAttribute("address");
            phone = (String) session.getAttribute("phone");
            note = (String) session.getAttribute("note");
            receiver = (String) session.getAttribute("receiver");
            payment = (String) session.getAttribute("payment");

            

            int saleId = cd.getMinOrderSale();

            cd.addOrder(acc, listi, address, phone, note, receiver, saleId, payment);
            double total1 = cd.totalAmount(acc.getUser_id());
            int total = (int) total1;

            Email e = new Email();
            StringBuilder emailContent = new StringBuilder();
            emailContent.append("<style>")
                    .append(".total span {")
                    .append("font-size: 24px;")
                    .append(".product__cart__item__pic {")
                    .append("width: 20px;")
                    .append("height: 20px;")
                    .append("overflow: hidden;")
                    .append("}")
                    .append(".product__cart__item__pic img {")
                    .append("width: 100%;")
                    .append("height: 100%;")
                    .append("object-fit: cover;")
                    .append("}")
                    .append("</style>")
                    .append("<div class='confirmation-message'>")
                    .append("<p style='color: #3498db;font-weight: bold;font-size: 32px;'>You have placed your order successfully</p>")
                    .append("<p>Thank you for choosing our products. The product will reach you as soon as possible.</p>")
                    .append("<p style='font-style: italic;color: #e74c3c;'>from Genzfashion1756 with love <span class='heart'>&#10084;</span></p>")
                    .append("</div>")
                    .append("<div class='row'>")
                    .append("<div class='col-lg-12'>")
                    .append("<div class='shopping__cart__table'>")
                    .append("<table>")
                    .append("<thead>")
                    .append("<tr>")
                    .append("<th>Product</th>")
                    .append("<th>Price</th>")
                    .append("<th>Quantity</th>")
                    .append("<th>Total</th>")
                    .append("<th></th>")
                    .append("</tr>")
                    .append("</thead>")
                    .append("<tbody>");

            for (Item item : listi) {
                emailContent.append("<tr>")
                        .append("<td class='product__cart__item'>")
                        .append("<div class='product__cart__item__text'>")
                        .append(item.getProduct().getPname())
                        .append("</div>")
                        .append("</td>")
                        .append("<td>")
                        .append("<div class='product__cart__item__price'>")
                        .append(item.getPrice())
                        .append("</div>")
                        .append("</td>")
                        .append("<td class='quantity__item'>")
                        .append("<div class='quantity'>")
                        .append("<div>")
                        .append("&emsp;<span class='qty'>").append(item.getQuantity()).append("</span>&emsp;")
                        .append("</div>")
                        .append("</div>")
                        .append("</td>")
                        .append("<td class='cart__price'> ").append(item.getPrice() * item.getQuantity()).append("</td>")
                        .append("</tr>");

            }

            emailContent.append("</tbody>")
                    .append("</table>")
                    .append("</div>")
                    .append("</div>")
                    .append("</div>")
                    .append("<div class='total' style='text-align: right;>")
                    .append(" <span style='font-size: 24px;'>Total Amount:").append(total).append("</span>")
                    .append("</div>");

            e.sendEmail(acc.getEmail(), "Your order successfully", emailContent.toString());

            

            
            request.setAttribute("listi", listi);
            request.setAttribute("total", total);
            request.setAttribute("size", listi.size());
            cd.cartSubmit(acc.getUser_id());
            request.getRequestDispatcher("order-success.jsp").forward(request, response);

        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
