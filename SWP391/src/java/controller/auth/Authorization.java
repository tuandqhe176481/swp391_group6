/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.auth;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import model.*;

/**
 *
 * @author pv
 */
public class Authorization extends HttpServlet {
    //cac ham check role

    public static boolean isAdmin(Accounts acc) {
        return acc != null && acc.getRoleId() == 1;
    }

    public static boolean isSaleManager(Accounts acc) {
        return acc != null && acc.getRoleId() == 2;
    }

    public static boolean isSaler(Accounts acc) {
        return acc != null && acc.getRoleId() == 3;
    }

    public static boolean isMarketer(Accounts acc) {
        return acc != null && acc.getRoleId() == 4;
    }

    public static boolean isCustomer(Accounts acc) {
        return acc != null && acc.getRoleId() == 5;
    }
    
    public static boolean isInventory(Accounts acc) {
        return acc != null && acc.getRoleId() == 6;
    }

    public static void redirectToHome(HttpSession session, HttpServletResponse response)
            throws ServletException, IOException {
        //day ve trang home va thong bao
        session.setAttribute("message", "Bạn không có quyền truy cập!");
        response.sendRedirect("home");
    }
}
