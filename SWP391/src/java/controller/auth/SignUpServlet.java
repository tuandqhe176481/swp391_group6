/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.auth;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Accounts;
import model.Email;


@WebServlet(name = "SignUpServlet", urlPatterns = {"/signup"})
public class SignUpServlet extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SignUpServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SignUpServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("user");
        String pass = request.getParameter("pass");
        String re_pass = request.getParameter("re_pass");
        String phone_number = request.getParameter("phone_number");
        String email = request.getParameter("email");
        String address = request.getParameter("address");
        String gt_raw = request.getParameter("gt");
        boolean gt;
        if (gt_raw.equals("1")) {
            gt = true;
        } else {
            gt = false;
        }
        if (!pass.equals(re_pass)) {
            request.setAttribute("error", "password and re-enter password are not the same");
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        } else {
            DAO d = new DAO();
            Accounts a = d.checkAccount(email);
            if (a == null) {
                Accounts a1=d.checkAccountName(username);
                if(a1==null){
                HttpSession session = request.getSession();
                session.setAttribute("username", username);
                session.setAttribute("pass", pass);
                session.setAttribute("phone_number", phone_number);
                session.setAttribute("email", email);
                session.setAttribute("address", address);
                session.setAttribute("gt", gt);
                Email e = new Email();
                String verifyLink = "http://localhost:9999/SWP391/verifyaccount"; // Thay đổi URL theo link xác nhận của bạn

                String emailContent = "<!DOCTYPE html>\n"
                        + "<html>\n"
                        + "<head>\n"
                        + "</head>\n"
                        + "<body>\n"
                        + "<p>Please verify your email by clicking the following link:</p>\n"
                        + "<a href=\"" + verifyLink + "\">Verify Email</a>\n"
                        + "\n"
                        + "</body>\n"
                        + "</html>";

                e.sendEmail(email, "Verify your email", emailContent);
                request.setAttribute("Notification", "You need confirm email to login");
                request.getRequestDispatcher("login.jsp").forward(request, response);
                }
                else{
                request.setAttribute("error", "this username is exist ");
                request.getRequestDispatcher("signup.jsp").forward(request, response);
                }
            } else {
                request.setAttribute("error", "this email is exist ");
                request.getRequestDispatcher("signup.jsp").forward(request, response);
            }
        }

    }

  
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
