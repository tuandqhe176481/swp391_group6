/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.auth;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Accounts;

/**
 *
 * @author PV
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String pass = request.getParameter("pass");
        String r = request.getParameter("rem");

        Cookie cemail = new Cookie("cemail", email);
        Cookie cpass = new Cookie("cpass", pass);
        Cookie cr = new Cookie("crem", r);

        if (r != null) {
            cemail.setMaxAge(60 * 60 * 24 * 7);
            cpass.setMaxAge(60 * 60 * 24 * 7);
            cr.setMaxAge(60 * 60 * 24 * 7);
        } else {
            cemail.setMaxAge(0);
            cpass.setMaxAge(0);
            cr.setMaxAge(0);
        }

        response.addCookie(cemail);
        response.addCookie(cpass);
        response.addCookie(cr);

        DAO d = new DAO();
        Accounts a = d.login(email, pass);

        if (a == null) {
            request.setAttribute("error", "your email or password incorrect");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        } else {
            if (a.getStatusId() == 2) {
                request.setAttribute("error", "your account is inactive");
                request.getRequestDispatcher("login.jsp").forward(request, response);
            } else {
                HttpSession session = request.getSession();
                session.setAttribute("acc", a);
                int roleId = a.getRoleId();
                switch (roleId) {
                    case 1:
                        response.sendRedirect("admindashboard");
                        break;
                    case 2:
                        response.sendRedirect("salemanagerdashboard");
                        break;
                    case 3:
                        response.sendRedirect("ordermanager");
                        break;
                    case 4:
                        response.sendRedirect("dashboardmkt");
                        break;
                    case 5:
                        response.sendRedirect("home");
                        break;
                    case 6:
                        response.sendRedirect("dashboardinventory");
                        break;
                    default:
                        break;
                }

            }

        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
