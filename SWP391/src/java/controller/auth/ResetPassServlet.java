/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.auth;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Accounts;
import model.Email;

@WebServlet(name = "ResetPassServlet", urlPatterns = {"/resetpass"})
public class ResetPassServlet extends HttpServlet {

    private String emailc;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        emailc = request.getParameter("emailc");
        DAO d = new DAO();
        Accounts a = d.checkAccount(emailc);
        if (a != null) {

            Email e = new Email();
            long expirationTimeMillis = System.currentTimeMillis() + (1 * 60 * 1000);
            String verifyLink = "http://localhost:9999/SWP391/newresetpass?expires=" + expirationTimeMillis; // Thay đổi URL theo link xác nhận của bạn

            String emailContent = "<!DOCTYPE html>\n"
                    + "<html>\n"
                    + "<head>\n"
                    + "</head>\n"
                    + "<body>\n"
                    + "<p>Please Reset your password by clicking the following link:</p>\n"
                    + "<a href=\"" + verifyLink + "\">Reset Password</a>\n"
                    + "\n"
                    + "</body>\n"
                    + "</html>";

            e.sendEmail(emailc, "Reset your password", emailContent);
            request.setAttribute("Notification", "You need confirm email to Reset Password");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        } else {
            request.setAttribute("error", "this email don't exist");
            request.getRequestDispatcher("resetpass.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String newpass = request.getParameter("newpass");
        String re_newpass = request.getParameter("re_newpass");
        if (!newpass.equals(re_newpass)) {
            request.setAttribute("error", "password and re-enter password are not the same");
            request.getRequestDispatcher("newpass.jsp").forward(request, response);
        } else {
            DAO d = new DAO();
            d.changePass(emailc, newpass);
            request.setAttribute("Notification", "Reset password successfully");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
