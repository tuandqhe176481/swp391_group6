/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.client;

import dal.CustomerDAO;
import dal.DAO;
import dal.PagingDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.Comparator;
import java.util.List;
import model.Accounts;
import model.Brand;
import model.Item;
import model.Product;
import model.ProductCategories;

/**
 *
 * @author pc
 */
@WebServlet(name = "ShopServlet", urlPatterns = {"/shop"})
public class ShopServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        Accounts acc = (Accounts) session.getAttribute("acc");
        DAO dao = new DAO();

        //Lấy toàn bộ categories
        List<ProductCategories> listc = dao.getAllCategories();
        request.setAttribute("listc", listc);

        //Lấy toàn bộ brand
        List<Brand> listb = dao.getAllBrand();
        request.setAttribute("listb", listb);

        //Lấy product mới nhất 
        List<Product> listn = dao.getLastestProduct();
        request.setAttribute("listn", listn);

        String pageindex = request.getParameter("index");

        if (pageindex == null) {
            pageindex = "1";
        }
        int index = Integer.parseInt(pageindex);
        PagingDAO daop = new PagingDAO();
        int count = daop.getTotalProduct();
        int endp = count / 12;
        if (endp % 12 != 0) {
            endp++;
        }
        request.setAttribute("endP", endp);
        List<Product> list = daop.pagingProduct(index);

        String sort = request.getParameter("sort");
        if (sort != null) {
            if (sort.equals("HtL")) {
                list.sort(Comparator.comparing(Product::getPrice).reversed());
            } else if (sort.equals("LtH")) {
                list.sort(Comparator.comparing(Product::getPrice));
            }
        }
        
        if (acc != null) {
            CustomerDAO cd = new CustomerDAO();
            List<Item> listi = cd.getCart(acc.getUser_id());
            double total = cd.totalAmount(acc.getUser_id());
            request.setAttribute("listi", listi);
            session.setAttribute("total", total);
            session.setAttribute("size", listi.size());
        }

        request.setAttribute("listp", list);
        request.setAttribute("currentIndex", index);
        request.getRequestDispatcher("shop.jsp").forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
