/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.client;

import dal.DAO;
import dal.PagingDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Comparator;
import java.util.List;
import model.Brand;
import model.Product;
import model.ProductCategories;

/**
 *
 * @author pc
 */
@WebServlet(name = "SaleControl", urlPatterns = {"/sale"})
public class SaleControl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        DAO dao = new DAO();
        //Lấy toàn bộ categories
        List<ProductCategories> listc = dao.getAllCategories();
        request.setAttribute("listc", listc);

        //Lấy toàn bộ brand
        List<Brand> listb = dao.getAllBrand();
        request.setAttribute("listb", listb);

        String sale = request.getParameter("sale");
        String indexpage = request.getParameter("index");
        if (indexpage == null) {
            indexpage = "1";
        }
        int index = Integer.parseInt(indexpage);
        request.setAttribute("tags", sale);
        PagingDAO daop = new PagingDAO();
        int count = daop.getTotalProductSale();
        int endps = count / 12;
        if (endps % 12 != 0) {
            endps++;
        }
        List<Product> listps = daop.pagingSaleProduct(index);

        String sort = request.getParameter("sort");
        if (sort != null) {
            if (sort.equals("HtL")) {
                listps.sort(Comparator.comparing(Product::getPrice).reversed());
            } else if (sort.equals("LtH")) {
                listps.sort(Comparator.comparing(Product::getPrice));
            }
        }
        request.setAttribute("listp", listps);
        request.setAttribute("currentIndexSale", index);
        request.setAttribute("endps", endps);
        request.getRequestDispatcher("shop.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * <<<<<<< HEAD
     *
     * @return a String containing servlet description =======
     * @return a String containing servlet description >>>>>>>
     * ca8899b6057216ad17aaf644785a3845f7757681
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
