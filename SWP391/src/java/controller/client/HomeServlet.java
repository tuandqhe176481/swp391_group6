/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.client;

import dal.CustomerDAO;
import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Accounts;
import model.Banner;
import model.Blog;
import model.Item;
import model.OrderDetail;
import model.Product;

/**
 *
 * @author pv
 */
@WebServlet(name = "HomeServlet", urlPatterns = {"/home"})
public class HomeServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        Accounts acc = (Accounts) session.getAttribute("acc");

        DAO dao = new DAO();
        List<OrderDetail> listBS = dao.getProductBestSeller();
        List<Product> listNA = dao.getProductNewArrival();
        List<Banner> listBA = dao.getBanners();
        List<Product> listPK = dao.getLatestProducts(9);
        List<Product> listAK = dao.getLatestProducts(5);
        List<Product> listBT = dao.getLatestProducts(3);
        List<Blog> listB = dao.get3LatestBlogs();

        if (acc != null) {
            CustomerDAO cd = new CustomerDAO();
            List<Item> listi = cd.getCart(acc.getUser_id());
            double total = cd.totalAmount(acc.getUser_id());
            request.setAttribute("listi", listi);
            session.setAttribute("total", total);
            session.setAttribute("size", listi.size());
        }

        request.setAttribute("listBS", listBS);
        request.setAttribute("listNA", listNA);
        request.setAttribute("listBA", listBA);
        request.setAttribute("listPK", listPK);
        request.setAttribute("listAK", listAK);
        request.setAttribute("listBT", listBT);
        request.setAttribute("listB", listB);
        request.getRequestDispatcher("home.jsp").forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
