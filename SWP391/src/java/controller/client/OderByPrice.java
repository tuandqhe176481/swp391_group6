/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.client;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Product;

/**
 *
 * @author pc
 */
@WebServlet(name = "OderByPrice", urlPatterns = {"/order"})
public class OderByPrice extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       
    }
    public static List<Product> highToLow(List<Product> listMen) {
        for (int i = 0; i < listMen.size(); i++) {
            for (int j = i; j < listMen.size(); j++) {
                if (listMen.get(i).getPrice() < listMen.get(j).getPrice()) {
                    Product temp = listMen.get(i);
                    listMen.set(i, listMen.get(j));
                    listMen.set(j, temp);
                }
            }
        }
        return listMen;
    }

    public static List<Product> lowToHigh(List<Product> listMen) {
        for (int i = 0; i < listMen.size(); i++) {
            for (int j = i; j < listMen.size(); j++) {
                if (listMen.get(i).getPrice() > listMen.get(j).getPrice()) {
                    Product temp = listMen.get(i);
                    listMen.set(i, listMen.get(j));
                    listMen.set(j, temp);
                }
            }
        }
        return listMen;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
   protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        DAO d = new DAO();
//
//        // Lấy tham số "sort" từ dropdown
//        String sort = request.getParameter("sort");
//        if (sort != null) {
//            request.getSession().setAttribute("sort", sort);
//        } else {
//            sort = (String) request.getSession().getAttribute("sort");
//        }
//        int currentPage = 1;
//        // Số sản phẩm trên mỗi trang
//        int productsPerPage = 6 * 3;
//        String pageStr = request.getParameter("page");
//        if (pageStr != null) {
//            currentPage = Integer.parseInt(pageStr);
//            request.setAttribute("page", currentPage);
//        } else {
//            currentPage = 1;
//            request.setAttribute("page", currentPage);
//        }
//        // Tính vị trí bắt đầu của dãy sản phẩm
//        int start = (currentPage - 1) * productsPerPage;
//
//        // Tùy thuộc vào tham số "sort" để sắp xếp danh sách sản phẩm
//        List<Product> productSort = null;
//        // Lấy danh sách sản phẩm cho các danh mục khác
//        List<Product> listMen = d.getMen();
//        List<Product> listWomen = d.getWomen();
//        List<Product> listSport = d.getSport();
//        List<Product> listLuxury = d.getLuxury();
//        if (sort != null && sort.equals("High To Low")) {
//            // Sắp xếp sản phẩm theo giá từ cao đến thấp
//            productSort = d.getProductsSortedByPriceHighToLow();
//            request.setAttribute("sort", sort);
//            listMen = highToLow(listMen);
//            listWomen = highToLow(listWomen);
//            listSport = highToLow(listSport);
//            listLuxury = highToLow(listLuxury);
//            if (start < productSort.size()) {
//                productSort = productSort.subList(start, Math.min(start + productsPerPage, productSort.size()));
//            } else {
//                productSort = productSort.subList(start, start + productsPerPage);
//            }
//
//            // kiem tra start + productsPerPage neu no lon qua thi tra ve max lenght
//        } else if (sort != null && sort.equals("Low To High")) {
//            // Sắp xếp sản phẩm theo giá từ thấp đến cao
//            productSort = d.getProductsSortedByPriceLowToHigh();
//            request.setAttribute("sort", sort);
//            listMen = lowToHigh(listMen);
//            listWomen = lowToHigh(listWomen);
//            listSport = lowToHigh(listSport);
//            listLuxury = lowToHigh(listLuxury);
//            if (start < productSort.size()) {
//                productSort = productSort.subList(start, Math.min(start + productsPerPage, productSort.size()));
//            } else {
//                productSort = productSort.subList(start, start + productsPerPage);
//            }
//
//        }
//
//        // Số trang hiện tại (1-based)
//        // Lấy danh sách sản phẩm trong khoảng
//        List<Product> products = d.getProductsByRange(start, start + productsPerPage);
//
//        // Tính tổng số sản phẩm
//        int totalProducts = d.getTotalProducts();
//
//        // Số trang cần hiển thị
//        int totalPages = (int) Math.ceil((double) totalProducts / productsPerPage);
//        if (productSort != null) {
//            products = productSort;
//        }
//        // Đặt danh sách sản phẩm vào request và forward tới shop.jsp
//        request.setAttribute("totalPages", totalPages);
//        request.setAttribute("currentPage", currentPage);
//        request.getRequestDispatcher("shop.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
