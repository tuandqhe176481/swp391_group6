/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.client;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Brand;
import model.Product;
import model.ProductCategories;

/**
 *
 * @author pc
 */
@WebServlet(name = "FilterControl", urlPatterns = {"/filter"})
public class FilterControl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        DAO dao = new DAO();
        //Lấy toàn bộ categories
        List<ProductCategories> listc = dao.getAllCategories();
        request.setAttribute("listc", listc);

        //Lấy toàn bộ brand
        List<Brand> listb = dao.getAllBrand();
        request.setAttribute("listb", listb);

        String price1 = request.getParameter("price1");
        String price2 = request.getParameter("price2");

        String cateid = request.getParameter("cid");
        String brandid = request.getParameter("bid");

        try {
            if (cateid.isEmpty() && brandid.isEmpty()) { // nếu khoogn có cả cateid và brandid thì chạy hàm search bằng price
                int priceMin = Integer.parseInt(price1);
                int priceMax = Integer.parseInt(price2);
                List<Product> listf = dao.getProductByPrice1(priceMin, priceMax);
                if (listf.isEmpty()) {
                    request.setAttribute("tag1", price1);
                    request.setAttribute("tag2", price2);
                    request.setAttribute("error", "No result found");
                    request.getRequestDispatcher("shop.jsp").forward(request, response);
                }
                request.setAttribute("listp", listf);
                request.setAttribute("tag1", price1);
                request.setAttribute("tag2", price2);
                request.getRequestDispatcher("shop.jsp").forward(request, response);

            } else if (cateid.isEmpty() && !(price1.isEmpty() && price2.isEmpty())) { // nếu cateid không có và có giá trị của price thì sẽ chạy hàm search bằng cid và price
                int priceMin = Integer.parseInt(price1);
                int priceMax = Integer.parseInt(price2);
                int bid = Integer.parseInt(brandid);
                List<Product> listfb = dao.getProductByPriceAndBrand(priceMin, priceMax, bid);
                if (listfb.isEmpty()) {
                    request.setAttribute("error", "No result found");
                    request.setAttribute("tagb", bid);
                    request.setAttribute("tag1", price1);
                    request.getRequestDispatcher("shop.jsp").forward(request, response);
                }
                request.setAttribute("listp", listfb);
                request.setAttribute("tagb", bid);
                request.setAttribute("tag1", price1);
                request.getRequestDispatcher("shop.jsp").forward(request, response);

            } else if (brandid.isEmpty() && !(price1.isEmpty() && price2.isEmpty())) {// nếu brandid không có thì và price có giá trị thì sẽ chạy hàm search bằng price và cateid
                int priceMin = Integer.parseInt(price1);
                int priceMax = Integer.parseInt(price2);
                int cid = Integer.parseInt(cateid);
                List<Product> listfc = dao.getProductByPriceAndCate(priceMin, priceMax, cid);
                if (listfc.isEmpty()) {
                    request.setAttribute("error", "No result found");
                    request.setAttribute("tag", cid);
                    request.setAttribute("tag1", price1);
                    request.getRequestDispatcher("shop.jsp").forward(request, response);
                }
                request.setAttribute("listp", listfc);
                request.setAttribute("tag", cid);
                request.setAttribute("tag1", price1);
                request.getRequestDispatcher("shop.jsp").forward(request, response);

            } else if (cateid.isEmpty() && price1.isEmpty() && price2.isEmpty()) { // nếu cateid rỗng và price rỗng thì search bằng brand
                List<Product> listbs = dao.getProductByBid(brandid);
                if (listbs.isEmpty()) {
                    request.setAttribute("error", "No result found");
                    request.setAttribute("tagb", brandid);
                    request.getRequestDispatcher("shop.jsp").forward(request, response);
                }
                request.setAttribute("listp", listbs);
                request.setAttribute("tagb", brandid);
                request.getRequestDispatcher("shop.jsp").forward(request, response);

            } else if (brandid.isEmpty() && price1.isEmpty() && price2.isEmpty()) { // nếu brandid rỗng và price rỗng thì search bằng categories
                List<Product> listcs = dao.getProductByCid(cateid);
                if (listcs.isEmpty()) {
                    request.setAttribute("error", "No result found");
                    request.setAttribute("tag", cateid);
                    request.getRequestDispatcher("shop.jsp").forward(request, response);
                }
                request.setAttribute("tag", cateid);
                request.setAttribute("listp", listcs);
                request.getRequestDispatcher("shop.jsp").forward(request, response);

            } else if (price1.isEmpty() && price2.isEmpty()) { // nếu price rỗng và cateid, brandid đều có giá trị thì chạy hàm search bằng categories và brand
                int cid = Integer.parseInt(cateid);
                int bid = Integer.parseInt(brandid);
                List<Product> listbc = dao.getProductBybidAndcid(cid, bid);
                if (listbc.isEmpty()) {
                    request.setAttribute("error", "No result found");
                    request.setAttribute("tag", cid);
                    request.setAttribute("tagb", bid);
                    request.getRequestDispatcher("shop.jsp").forward(request, response);
                }
                request.setAttribute("listp", listbc);
                request.setAttribute("tag", cid);
                request.setAttribute("tagb", bid);
                request.getRequestDispatcher("shop.jsp").forward(request, response);

            } else { // nếu có đủ cả brandid, cid, price1, price2 thì chạy hàm search kết hợp
                int priceMin = Integer.parseInt(price1);
                int priceMax = Integer.parseInt(price2);
                int cid = Integer.parseInt(cateid);
                int bid = Integer.parseInt(brandid);
                List<Product> listfilter = dao.getProductByPrice2(priceMin, priceMax, cid, bid);
                if (listfilter.isEmpty()) {
                    request.setAttribute("error", "No result found");
                    request.setAttribute("tag1", price1);
                    request.setAttribute("tag2", price2);
                    request.setAttribute("tag", cid);
                    request.setAttribute("tagb", bid);
                    request.getRequestDispatcher("shop.jsp").forward(request, response);
                }
                request.setAttribute("listp", listfilter);
                request.setAttribute("tag1", price1);
                request.setAttribute("tag2", price2);
                request.setAttribute("tag", cid);
                request.setAttribute("tagb", bid);
                request.getRequestDispatcher("shop.jsp").forward(request, response);
            }
        } catch (Exception e) {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response <<<<<<< HEAD
     * @throws ServletException if a servlet-specific error occurs =======
     * @throws ServletException if a servlet-specific error occurs >>>>>>>
     * ca8899b6057216ad17aaf644785a3845f7757681
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
