/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.client;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Accounts;
import model.Email;
import model.Feedback;
import model.Product;
import model.ProductCategories;

/**
 *
 * @author pc
 */
@WebServlet(name = "ProductDetailsServlet", urlPatterns = {"/productdetails"})
public class ProductDetailsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        DAO dao = new DAO();
        int pid = Integer.parseInt(request.getParameter("pid"));
        Accounts currentAccounts = (Accounts) request.getSession().getAttribute("acc");
        int acid = (currentAccounts != null) ? currentAccounts.getUser_id() : 0;
        int oid = dao.getOrderIdByProductId(pid);
        List<Product> list = dao.getProductRelated(pid);
        Product p = dao.getProductDetails(pid);
        List<ProductCategories> listc = dao.getAllCategories();
        request.setAttribute("p", p);
        request.setAttribute("list", list);
        request.setAttribute("listc", listc);

        List<Feedback> listF = dao.getFeedbackOfProduct(pid);
        request.setAttribute("listF", listF);
        request.setAttribute("sizeListF", listF.size());

        double averageRating = dao.getAverageRatingOfProduct(pid);
        int fullStars = (int) averageRating; // Số sao đầy đủ
        double remaining = averageRating - fullStars; // Phần thập phân
        double extraStar = 0; // Số sao thêm (nếu có)
        if (remaining > 0.75) {
            extraStar = 1;
        } else if (remaining >= 0.5 && remaining <= 0.75) {
            extraStar = 0.5;
        }

        // Tổng số sao
        double totalStars = fullStars + extraStar;
        request.setAttribute("totalStars", totalStars);
        request.setAttribute("averageRating", averageRating);
        request.setAttribute("didBuy", dao.DidBuyTheBook(acid, pid));
        request.setAttribute("acid", acid);
        boolean hasComment = dao.checkIfUserHasCommented(pid, acid);
        request.setAttribute("hasComment", hasComment);

        request.getRequestDispatcher("shop-details.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAO dao = new DAO();
        HttpSession session = request.getSession();
        Accounts currentAccounts = (Accounts) request.getSession().getAttribute("acc");
        int acid = (currentAccounts != null) ? currentAccounts.getUser_id() : 0;
        int pid = Integer.parseInt(request.getParameter("pid"));
        int oid = dao.getOrderIdByProductId(pid);
        String editting__cmt = request.getParameter("editting__cmt");
        new DAO().editFeedback(acid, pid, editting__cmt);

//        if (oid != -1 && dao.checkOrderCompleted(oid) == true) {
//            sendEmailIfOrderCompleted(pid);
//        }
        response.sendRedirect("productdetails?pid=" + pid);
    }

//    private void sendEmailIfOrderCompleted(int pid) {
//        DAO dao = new DAO();
//        
//        
//            String to = dao.getEmailByOrderId(oid);
//            if (to != null && !to.isEmpty()) {
//                List<Product> orderedProducts = dao.getOrderedProductsByOrderID(oid);
//                StringBuilder content = new StringBuilder();
//                content.append("<h2>Your order has been completed</h2>");
//                content.append("<p>If you want to give feedback on the product you purchased, click on the link of each product and scroll down to the feedback section</p>");
//                for (Product product : orderedProducts) {
//                    content.append("<p>- ").append(product.getPname()).append("</p>");
//                    content.append("<p>Link: <a href=\"http://localhost:9999/SWP391/productdetails?pid=").append(product.getPid()).append("\">").append("Feedback</a></p>");
//                }
//
//                String subject = "Your order completion";
//                boolean result = Email.sendEmail(to, subject, content.toString());
//                if (result) {
//                    System.out.println("send email successful");
//                } else {
//                    System.out.println("send email error");
//                }
//            }
//
//        
//    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
