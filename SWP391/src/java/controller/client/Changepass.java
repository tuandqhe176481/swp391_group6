/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.client;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Accounts;

/**
 *
 * @author pc
 */
@WebServlet(name = "Changepass", urlPatterns = {"/changepass"})
public class Changepass extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        DAO dao = new DAO();
        String email = ((Accounts) request.getSession().getAttribute("acc")).getEmail();
        String oldpass = request.getParameter("oldpass");
        String newpass = request.getParameter("newpass");
        String re_newpass = request.getParameter("re_newpass");
        System.out.println(email);
        Accounts a1 = dao.login(email, oldpass);
        Accounts a = dao.getUserInfor(email);
        request.setAttribute("a", a);
        if (a1 == null) {
            request.setAttribute("error", "wrong old pass");
            request.getRequestDispatcher("userprofile.jsp").forward(request, response);
        } else {
            if (!newpass.equals(re_newpass)) {
                a1 = dao.getUserInfor(email);
                request.setAttribute("a", a1);
                request.setAttribute("error", "password and repeat password are not the same");
                request.getRequestDispatcher("userprofile.jsp").forward(request, response);
            } else {
                DAO d = new DAO();

                d.changePass(email, newpass);

                request.setAttribute("Notification1", "Change password successfully");
              request.getRequestDispatcher("userprofile.jsp").forward(request, response);
            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
