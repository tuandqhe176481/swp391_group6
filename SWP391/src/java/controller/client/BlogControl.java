/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.client;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Blog;
import model.BlogCategories;

/**
 *
 * @author pv
 */
@WebServlet(name = "BlogControl", urlPatterns = {"/blog"})
public class BlogControl extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        DAO dao = new DAO();
        String bc_id = request.getParameter("bc_id");
        List<BlogCategories> listBC = dao.getAllBlogCategories();
        List<Blog> listB = dao.getAllBlogPosts();

        request.setAttribute("listBC", listBC);
        request.setAttribute("tag", bc_id);

        // Sắp xếp
        String sort = request.getParameter("sort");
        if (sort != null) {
            if (sort.equals("latest")) {
                listB = dao.getLatestBlogs();
            } else if (sort.equals("oldest")) {
                listB = dao.getOldestBlogs();
            }
        }

        // Phân trang
        String page = request.getParameter("page");
        int itemsPerPage = 4;
        int currentPage = 1;

        if (page != null && !page.isEmpty()) {
            try {
                currentPage = Integer.parseInt(page);
                if (currentPage < 1) {
                    currentPage = 1;
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        int startIndex = (currentPage - 1) * itemsPerPage;
        int endIndex = Math.min(startIndex + itemsPerPage, listB.size());

        List<Blog> currentPageList = listB.subList(startIndex, endIndex);

        request.setAttribute("totalPages", Math.ceil((double) listB.size() / (double) itemsPerPage));
        request.setAttribute("currentPage", currentPage);
        request.setAttribute("listB", currentPageList);

        request.getRequestDispatcher("blog.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
