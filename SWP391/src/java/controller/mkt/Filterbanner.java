/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.mkt;

import controller.auth.Authorization;
import dal.DAO;
import dal.MaketingDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Accounts;
import model.Banner;
import model.Brand;
import model.Product;
import model.ProductCategories;
import model.Status;

/**
 *
 * @author pc
 */
@WebServlet(name = "FilterBanner", urlPatterns = {"/filterbanner"})
public class Filterbanner extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if (!Authorization.isMarketer((Accounts) session.getAttribute("acc"))) {
            Authorization.redirectToHome(session, response);
        } else {
            DAO dao = new DAO();
            MaketingDao daok = new MaketingDao();

            List<Brand> listba = daok.getAllBrand();
            request.setAttribute("listba", listba);

            List<Banner> listb = daok.getBanner();
            request.setAttribute("p", listb);

            String brand_id = request.getParameter("brandid");
            String statusid = request.getParameter("status");

            if (!brand_id.equals("Brand") && statusid.equals("Status")) {
                int bid = Integer.parseInt(brand_id);
                System.out.println(bid);
                List<Banner> b = daok.getBannerByBrandID(bid);

                if (b.isEmpty()) {
                    request.setAttribute("mess", "No results found");
                    request.getRequestDispatcher("mkt-sliderlist.jsp").forward(request, response);
                } else {
                    request.setAttribute("tagc", bid);
                    request.setAttribute("listc", b);
                    request.getRequestDispatcher("mkt-sliderlist.jsp").forward(request, response);
                }
            } else if (brand_id.equals("brandid") && !statusid.equals("Status")) {
                response.sendRedirect("sliderlist");

            } else {

                int bid = Integer.parseInt(brand_id);
                int status = Integer.parseInt(statusid);

                List<Banner> list = daok.filterBanner(bid, status);
                request.setAttribute("listc", status);
                request.setAttribute("tagc", bid);
                request.setAttribute("listc", list);
                if (list.isEmpty()) {
                    request.setAttribute("mess", "No results found");

                }

                request.getRequestDispatcher("mkt-sliderlist.jsp").forward(request, response);
            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
