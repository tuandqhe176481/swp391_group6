/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.mkt;

import controller.auth.Authorization;
import dal.DAO;
import dal.MaketingDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.SimpleFormatter;
import model.Accounts;
import model.Blog;
import model.BlogCategories;
import model.Status;

/**
 *
 * @author pv
 */
@MultipartConfig
@WebServlet(name = "EditBlogControl", urlPatterns = {"/editblog"})
public class EditBlogControl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if (!Authorization.isMarketer((Accounts) session.getAttribute("acc"))) {
            Authorization.redirectToHome(session, response);
        } else {
            MaketingDao mDao = new MaketingDao();
            DAO dao = new DAO();

            String blog_id = request.getParameter("blog_id");
            int blogid = Integer.parseInt(blog_id);

            List<BlogCategories> listBC = dao.getAllBlogCategories();
            request.setAttribute("listBC", listBC);

            List<Status> listSta = mDao.getAllStatus();
            request.setAttribute("listSta", listSta);

            Blog b = mDao.getBlogDetails(blogid);
            request.setAttribute("b", b);

            request.getRequestDispatcher("editBlog.jsp").forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (!Authorization.isMarketer((Accounts) session.getAttribute("acc"))) {
            Authorization.redirectToHome(session, response);
        } else {
            Accounts currentAccounts = (Accounts) request.getSession().getAttribute("acc");
            int acid = currentAccounts.getUser_id();
            MaketingDao mDao = new MaketingDao();
            String b_id = request.getParameter("blog_id");
            String bc_id = request.getParameter("category");
            String title = request.getParameter("name");
            String blog_status = request.getParameter("status");
            String description = request.getParameter("description");
            String brief = request.getParameter("brief");
            String author = request.getParameter("author");
            String thumbnail = request.getParameter("thumbnail");
            Date currentDate = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String created_at = sdf.format(currentDate);

            String uploadedFile1 = null;
            try {
                Part filePart = request.getPart("thumbnail");
                if (filePart.getSize() > 0) {
                    String fileName = getFileName(filePart);
                    String savePath = getServletContext().getRealPath("/") + "images/";
                    File fileSaveDir = new File(savePath);
                    if (!fileSaveDir.exists()) {
                        fileSaveDir.mkdir();
                    }

                    File file = new File(savePath + fileName);
                    filePart.write(file.getAbsolutePath());
                    uploadedFile1 = "images/" + fileName;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                int blogid = Integer.parseInt(b_id);
                int bc_id1 = Integer.parseInt(bc_id);
                int status_id = Integer.parseInt(blog_status);
                if (uploadedFile1 == null) {
                    uploadedFile1 = mDao.getBlogThumbnailById(blogid);
                }
                mDao.updateBlog(bc_id1, title, author, created_at, description, uploadedFile1, brief, acid, status_id, blogid);

            } catch (Exception e) {
            }
            response.sendRedirect("blogmanager");
        }

    }

    private String getFileName(final Part part) {
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
