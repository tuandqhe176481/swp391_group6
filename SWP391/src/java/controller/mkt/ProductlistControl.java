
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.mkt;

import controller.auth.Authorization;
import controller.client.*;
import dal.DAO;
import dal.MaketingDao;
import dal.PagingDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Accounts;
import model.Product;
import model.ProductCategories;
import model.Status;

/**
 *
 * @author pc
 */
@WebServlet(name = "DashboardControl", urlPatterns = {"/productmanager"})
public class ProductlistControl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if (!Authorization.isMarketer((Accounts) session.getAttribute("acc"))) {
            Authorization.redirectToHome(session, response);
        } else {
            DAO dao = new DAO();
            MaketingDao daok = new MaketingDao();
            PagingDAO daop = new PagingDAO();
            //List product
            int count = daop.getTotalProduct();
            int endp = count / 12;
            if (endp % 12 != 0) {
                endp++;
            }
            String indexpage = request.getParameter("index");
            if (indexpage == null) {
                indexpage = "1";
            }
            int index = Integer.parseInt(indexpage);
            List<Product> list = daop.pagingProductforMkt(index);
            request.setAttribute("endp", endp);
            request.setAttribute("listp", list);
            request.setAttribute("currentpage", index);

            //List Customer

            List<ProductCategories> listca = daok.getAllCategories();
            request.setAttribute("listca", listca);

            List<Status> liststa = daok.getAllStatus();
            request.setAttribute("liststa", liststa);

            request.getRequestDispatcher("mkt-product.jsp").forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
