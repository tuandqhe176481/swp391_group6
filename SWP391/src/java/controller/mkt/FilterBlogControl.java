/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.mkt;

import controller.auth.Authorization;
import dal.DAO;
import dal.MaketingDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Accounts;
import model.Blog;
import model.BlogCategories;
import model.Status;

/**
 *
 * @author pv
 */
@WebServlet(name = "FilterBlogControl", urlPatterns = {"/filterblogmanager"})
public class FilterBlogControl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if (!Authorization.isMarketer((Accounts) session.getAttribute("acc"))) {
            Authorization.redirectToHome(session, response);
        } else {
            DAO dao = new DAO();
            MaketingDao mDao = new MaketingDao();

            List<BlogCategories> listBC = dao.getAllBlogCategories();
            request.setAttribute("listBC", listBC);

            List<Status> listS = mDao.getAllStatus();
            request.setAttribute("listS", listS);

            List<String> listA = dao.getAuthors();
            request.setAttribute("listA", listA);

            String bc_id = request.getParameter("blog_categories");
            String status_id = request.getParameter("status");
            String author = request.getParameter("author");

            if ((bc_id.equals("Categories")) && !status_id.equals("Status") && (author.equals("Author"))) {
                int sid = Integer.parseInt(status_id);
                int count = mDao.countBlogAvai();
                int endp = count / 6;
                if (endp % 6 != 0) {
                    endp++;
                }
                request.setAttribute("endps", endp);
                String pageIndex = request.getParameter("indexS");
                if (pageIndex == null) {
                    pageIndex = "1";
                }
                int indexS = Integer.parseInt(pageIndex);
                List<Blog> listBl = mDao.pagingBlogAvai(sid, indexS);
                request.setAttribute("listB", listBl);
                request.setAttribute("tags", sid);
                request.setAttribute("bc_id", bc_id);
                request.setAttribute("author", author);
                request.setAttribute("currentPage1", indexS);
                request.getRequestDispatcher("mkt-post.jsp").forward(request, response);
            } else if (!bc_id.equals("Categories") && status_id.equals("Status") && author.equals("Author")) {
                int cid = Integer.parseInt(bc_id);
                List<Blog> listb = mDao.getBlogByBCid(cid);
                if (listb.isEmpty()) {
                    request.setAttribute("mess", "No result found");
                }
                request.setAttribute("tags", cid);
                request.setAttribute("listB", listb);
                request.setAttribute("author", author);
                request.getRequestDispatcher("mkt-post.jsp").forward(request, response);
            } else if (bc_id.equals("Categories") && status_id.equals("Status") && !author.equals("Author")) {
                List<Blog> listByAuthor = mDao.getBlogByAuthor(author);
                if (listByAuthor.isEmpty()) {
                    request.setAttribute("mess", "No result found");
                }
                request.setAttribute("listB", listByAuthor);
                request.setAttribute("author", author);
                request.getRequestDispatcher("mkt-post.jsp").forward(request, response);
            } else if (!bc_id.equals("Categories") && !status_id.equals("Status") && author.equals("Author")) {
                int cid = Integer.parseInt(bc_id);
                int sid = Integer.parseInt(status_id);
                List<Blog> listByCaAndSt = mDao.getBlogByBCidAndSid(cid, sid);
                request.setAttribute("listB", listByCaAndSt);
                request.setAttribute("tags", sid);
                request.setAttribute("tagc", cid);
                request.getRequestDispatcher("mkt-post.jsp").forward(request, response);
            } else if (!bc_id.equals("Categories") && status_id.equals("Status") && !author.equals("Author")) {
                int cid = Integer.parseInt(bc_id);
                List<Blog> listByCaAndAu = mDao.getBlogByCidAndAuthor(cid, author);
                request.setAttribute("listB", listByCaAndAu);
                request.setAttribute("tagc", cid);
                request.setAttribute("author", author);
                request.getRequestDispatcher("mkt-post.jsp").forward(request, response);
            } else if (bc_id.equals("Categgories") && !status_id.equals("Status") && !author.equals("Author")) {
                int sid = Integer.parseInt(status_id);
                List<Blog> listBySidAndAu = mDao.getBlogBySidAndAuthor(sid, author);
                request.setAttribute("listB", listBySidAndAu);
                request.setAttribute("tags", sid);
                request.setAttribute("author", author);
                request.getRequestDispatcher("mkt-post").forward(request, response);
            } else if (!bc_id.equals("Categories") && !status_id.equals("Status") && !author.equals("Author")) {
                int cid = Integer.parseInt(bc_id);
                int sid = Integer.parseInt(status_id);
                List<Blog> listByCaAndStaAndAu = mDao.getBlogByCidAndSidAndAuthor(cid, sid, author);
                request.setAttribute("listB", listByCaAndStaAndAu);
                request.setAttribute("tags", sid);
                request.setAttribute("tagc", cid);
                request.setAttribute("author", author);
                request.getRequestDispatcher("mkt-post.jsp").forward(request, response);
            } else {
                response.sendRedirect("blogmanager");
            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
