/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.mkt;

import controller.auth.Authorization;
import dal.DAO;
import dal.MaketingDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import model.Accounts;
import model.Brand;
import model.Product;
import model.ProductCategories;
import model.Status;
//import org.apache.commons.fileupload.servlet.ServletFileUpload;

@MultipartConfig
@WebServlet(name = "EditProductControl", urlPatterns = {"/editproduct"})
public class EditProductControl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if (!Authorization.isMarketer((Accounts) session.getAttribute("acc"))) {
            Authorization.redirectToHome(session, response);
        } else {
            MaketingDao daok = new MaketingDao();
            DAO dao = new DAO();

            String productId = request.getParameter("pid");
            int pid = Integer.parseInt(productId);

            List<ProductCategories> listca = daok.getAllCategories();
            request.setAttribute("listca", listca);

            List<Status> liststa = daok.getAllStatus();
            request.setAttribute("liststa", liststa);

            //Lấy toàn bộ brand
            List<Brand> listb = dao.getAllBrand();
            request.setAttribute("listb", listb);

            Product p = daok.getProductDetails(pid);
            request.setAttribute("p", p);
            request.getRequestDispatcher("editProduct.jsp").forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        MaketingDao daok = new MaketingDao();
        String cateid = request.getParameter("category");
        String name = request.getParameter("name");
        String prate = request.getParameter("rate");
        String productprice = request.getParameter("price");
        String productbrand = request.getParameter("brand");
        String productpriceSale = request.getParameter("pricesale");
        String productquantity = request.getParameter("quantity");
        String productdiscount = request.getParameter("discount");
        String productsoldout = request.getParameter("soldout");
        String productstatus = request.getParameter("status");
        String size = request.getParameter("size");
        String color = request.getParameter("color");
        String productgender = request.getParameter("gender");
        String productdescription = request.getParameter("description");
        String img1 = request.getParameter("img1");
        String img2 = request.getParameter("img2");
        String productId = request.getParameter("pid");
        Date currentDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String created_at = sdf.format(currentDate);

        String uploadedFile1 = null;
        try {
            Part filePart = request.getPart("img1");
            if (filePart.getSize() > 0) {
                String fileName = getFileName(filePart);

                String savePath = getServletContext().getRealPath("/") + "images/";
                File fileSaveDir = new File(savePath);
                if (!fileSaveDir.exists()) {
                    fileSaveDir.mkdir();
                }

                File file = new File(savePath + fileName);
                filePart.write(file.getAbsolutePath());

                uploadedFile1 = "images/" + fileName;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("brand: " + uploadedFile1);

        String uploadedFile2 = null;
        try {
            Part filePart = request.getPart("img2");
            if (filePart.getSize() > 0) {
                String fileName = getFileName(filePart);

                String savePath = getServletContext().getRealPath("/") + "images/";
                File fileSaveDir = new File(savePath);
                if (!fileSaveDir.exists()) {
                    fileSaveDir.mkdir();
                }

                File file = new File(savePath + fileName);
                filePart.write(file.getAbsolutePath());

                uploadedFile2 = "images/" + fileName;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            int pid = Integer.parseInt(productId);
            int cid = Integer.parseInt(cateid);
            float rate = Float.parseFloat(prate);
            int price = Integer.parseInt(productprice);
            int brand = Integer.parseInt(productbrand);
            int priceSale = Integer.parseInt(productpriceSale);
            int quantity = Integer.parseInt(productquantity);
            Boolean isDiscount = Boolean.parseBoolean(productdiscount);
            Boolean isSoldout = Boolean.parseBoolean(productsoldout);
            Boolean gender = Boolean.parseBoolean(productgender);
            int status_id = Integer.parseInt(productstatus);
            if (uploadedFile1 == null) {
                uploadedFile1 = daok.getProductImage1ById(pid); // Lấy đường dẫn ảnh 1 cũ
            }
            if (uploadedFile2 == null) {
                uploadedFile2 = daok.getProductImage2ById(pid); // Lấy đường dẫn ảnh 2 cũ
            }
            daok.updateProduct(cid, name, rate, uploadedFile1, uploadedFile2, price, brand, priceSale, quantity, isDiscount, isSoldout, created_at, status_id, pid);
            daok.updateProductDetail(size, color, gender, productdescription, pid);
        } catch (Exception e) {
        }
        response.sendRedirect("productmanager");
    }

    private String getFileName(final Part part) {
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
