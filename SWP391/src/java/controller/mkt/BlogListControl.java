/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.mkt;

import controller.auth.Authorization;
import dal.DAO;
import dal.MaketingDao;
import dal.PagingDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Accounts;
import model.Blog;
import model.BlogCategories;
import model.Status;

/**
 *
 * @author pv
 */
@WebServlet(name="BlogListControl", urlPatterns={"/blogmanager"})
public class BlogListControl extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if (!Authorization.isMarketer((Accounts) session.getAttribute("acc"))) {
            Authorization.redirectToHome(session, response);
        } else{
            DAO dao = new DAO();
            MaketingDao mDao = new MaketingDao();
            PagingDAO pDao = new PagingDAO();
            
            //list blog
            int count = pDao.getTotalBlogPost();
            int endp = count/6;
            if(endp % 6 != 0){
                endp++;
            }
            String indexPage = request.getParameter("index");
            if(indexPage == null){
                indexPage = "1";
            }
            int index  = Integer.parseInt(indexPage);
            List<Blog> list = pDao.pagingBlogPostForMkt(index);
            request.setAttribute("endp", endp);
            request.setAttribute("listB", list);
            request.setAttribute("currentPage", index);
            
            //list Blog Category
            List<BlogCategories> listBC = dao.getAllBlogCategories();
            request.setAttribute("listBC", listBC);
            
            
            
            //list author
            List<String> listA = dao.getAuthors();
            request.setAttribute("listA", listA);
            
            //list status
            List<Status> listS = mDao.getAllStatus();
            request.setAttribute("listS", listS);
            request.getRequestDispatcher("mkt-post.jsp").forward(request, response);
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
