/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import controller.auth.Authorization;
import dal.AdminDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Accounts;
import model.Setting;

/**
 *
 * @author pc
 */
@WebServlet(name = "SearchSettingControl", urlPatterns = {"/searchsetting"})
public class SearchSettingControl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            if (!Authorization.isAdmin((Accounts) session.getAttribute("acc"))) {
                Authorization.redirectToHome(session, response);
            } else {
                String search = request.getParameter("search");

                AdminDAO dao = new AdminDAO();

                int countsearch = dao.countSettingSearch(search);
                int itemsPerPage = 6; // Số lượng mục trên mỗi trang
                int endsearch = (countsearch + itemsPerPage - 1) / itemsPerPage; // Số lượng trang cần thiết
                request.setAttribute("endsearch", endsearch);

                String indexsearch = request.getParameter("indexse");
                if (indexsearch == null) {
                    indexsearch = "1";
                }
                int indexse = Integer.parseInt(indexsearch);

                List<Setting> listset = dao.searchByValue(search, indexse);
                if (listset.isEmpty()) {
                    request.setAttribute("error", "No result found");
                    request.setAttribute("search", search);
                    request.getRequestDispatcher("settingList.jsp").forward(request, response);
                } else {
                    request.setAttribute("list", listset);
                    request.setAttribute("search", search);
                    request.setAttribute("indexse", indexse);
                    request.getRequestDispatcher("settingList.jsp").forward(request, response);
                }
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
