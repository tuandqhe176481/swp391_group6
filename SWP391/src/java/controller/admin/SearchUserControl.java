/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import controller.auth.Authorization;
import dal.AdminDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.AccountStatus;
import model.Accounts;
import model.Role;

/**
 *
 * @author pc
 */
@WebServlet(name = "SearchUserControl", urlPatterns = {"/searchuser"})
public class SearchUserControl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if (!Authorization.isAdmin((Accounts) session.getAttribute("acc"))) {
            Authorization.redirectToHome(session, response);
        } else {
            AdminDAO dao = new AdminDAO();
            //Lấy thông tin gender, status, role
            List<Role> listr = dao.getAllRole();
            request.setAttribute("listr", listr);

            List<AccountStatus> listas = dao.getAllStatus();
            request.setAttribute("listas", listas);

            String search = request.getParameter("search");
            int countSearch = dao.countSearchUser(search);
            System.out.println("count: " + countSearch);
            int endSearch = countSearch / 12;
            if (endSearch % 2 != 0) {
                endSearch++;
            }
            request.setAttribute("endSearch", endSearch);
            System.out.println("search: " + endSearch);
            String index = request.getParameter("indexSearch");
            if (index == null) {
                index = "1";
            }
            int indexSearch = Integer.parseInt(index);
            List<Accounts> list = dao.searchUser(search, indexSearch);

            // Kiểm tra nếu danh sách rỗng
            if (list.isEmpty()) {
                // Nếu danh sách rỗng, gửi thông báo lỗi đến trang userList-manager.jsp
                request.setAttribute("error", "No result found !");
                request.setAttribute("search", search);
                request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
            } else {
                // Nếu danh sách không rỗng, gửi danh sách người dùng đến trang userList-manager.jsp
                request.setAttribute("listacc", list);
                request.setAttribute("search", search);
                request.setAttribute("indexSearch", indexSearch);
                request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
