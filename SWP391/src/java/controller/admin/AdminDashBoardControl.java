/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import controller.auth.Authorization;
import dal.AdminDAO;
import dal.MaketingDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import model.Accounts;
import model.BrandTotal;
import model.OrderDetail;

/**
 *
 * @author pc
 */
@WebServlet(name = "AdminDashBoardControl", urlPatterns = {"/admindashboard"})
public class AdminDashBoardControl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if (!Authorization.isAdmin((Accounts) session.getAttribute("acc"))) {
            Authorization.redirectToHome(session, response);
        } else {
            AdminDAO daoa = new AdminDAO();
            MaketingDao daok = new MaketingDao();

            DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
            DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

            String startDateStr = request.getParameter("startDate");
            String endDateStr = request.getParameter("endDate");

            LocalDateTime startDate;
            LocalDateTime endDate;

            if (startDateStr == null || startDateStr.isEmpty()) {
                LocalDateTime sevenDaysAgo = LocalDateTime.now().minusDays(7);
                startDate = sevenDaysAgo;
            } else {
                startDate = LocalDateTime.parse(startDateStr, inputFormatter);
            }

            if (endDateStr == null || endDateStr.isEmpty()) {
                LocalDateTime today = LocalDateTime.now();
                endDate = today;
            } else {
                endDate = LocalDateTime.parse(endDateStr, inputFormatter);
            }

            String formattedStartDate = startDate.format(outputFormatter);
            String formattedEndDate = endDate.format(outputFormatter);

            System.out.println(formattedEndDate);
            System.out.println(formattedEndDate);

            //Dem so don hang
            int count1 = daoa.countOrderStatus(1, formattedStartDate, formattedEndDate);
            request.setAttribute("count1", count1);
            int count2 = daoa.countOrderStatus(2, formattedStartDate, formattedEndDate);
            request.setAttribute("count2", count2);
            int count3 = daoa.countOrderStatus(3, formattedStartDate, formattedEndDate);
            request.setAttribute("count3", count3);
            int count4 = daoa.countOrderStatus(4, formattedStartDate, formattedEndDate);
            request.setAttribute("count4", count4);
            int count5 = daoa.countOrderStatus(5, formattedStartDate, formattedEndDate);
            request.setAttribute("count5", count5);

            // thong ke theo brand
            List<BrandTotal> listbt = daok.getTotalByBrand();
            request.setAttribute("listbt", listbt);

            //thong ke khách hàng
            Accounts newCusCreate = daoa.countCusNewCreate(formattedStartDate, formattedEndDate);
            Accounts newCusOrder = daoa.countCusOrder(formattedStartDate, formattedEndDate);
            request.setAttribute("newCusCreate", newCusCreate);
            request.setAttribute("newCusOrder", newCusOrder);

            //Tong doanh thu
            int totalAmount = daoa.countTotalAmount(formattedStartDate, formattedEndDate);
            request.setAttribute("total", totalAmount);

            //Lay san pham best seller
            List<OrderDetail> listo = daok.getProductBestSeller();
            request.setAttribute("listo", listo);
            request.setAttribute("start", startDateStr);
            request.setAttribute("end", endDateStr);
            request.getRequestDispatcher("adminDashBoard.jsp").forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
