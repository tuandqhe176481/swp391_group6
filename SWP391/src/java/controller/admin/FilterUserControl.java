/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import controller.auth.Authorization;
import dal.AdminDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.AccountStatus;
import model.Accounts;
import model.Role;

/**
 *
 * @author pc
 */
@WebServlet(name = "FilterUserControl", urlPatterns = {"/filteruser"})
public class FilterUserControl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if (!Authorization.isAdmin((Accounts) session.getAttribute("acc"))) {
            Authorization.redirectToHome(session, response);
        } else {
            AdminDAO dao = new AdminDAO();
            //Lấy thông tin gender, status, role
            List<Role> listr = dao.getAllRole();
            request.setAttribute("listr", listr);

            List<AccountStatus> listas = dao.getAllStatus();
            request.setAttribute("listas", listas);

            String status = request.getParameter("status");
            String role = request.getParameter("role");
            String gender = request.getParameter("gender");

            if (gender.equals("Gender") && status.equals("Status") && role.equals("Role")) {
                response.sendRedirect("usermanager");
            } else if (!gender.equals("Gender") && status.equals("Status") && role.equals("Role")) {
                int countf = dao.countFelmale();
                int countm = dao.countMale();

                int itemsPerPage = 12; // Số lượng mục trên mỗi trang
                int endF = (countf + itemsPerPage - 1) / itemsPerPage; // Số lượng trang cần thiết
                int endM = (countm + itemsPerPage - 1) / itemsPerPage; // Số lượng trang cần thiết
                request.setAttribute("endF", endF);
                request.setAttribute("endM", endM);

                String index1 = request.getParameter("indexf");
                String index2 = request.getParameter("indexm");
                if (index1 == null) {
                    index1 = "1";
                }
                if (index2 == null) {
                    index2 = "1";
                }
                int indexf = Integer.parseInt(index1);
                int indexm = Integer.parseInt(index2);

                if (gender.equals("Male")) {
                    List<Accounts> listm = dao.getAllAccountMale(indexm);
                    if (listm.isEmpty()) {
                        request.setAttribute("error", "No result found !");
                        request.setAttribute("gender", gender);
                        request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
                    } else {
                        request.setAttribute("indexm", indexm);
                        request.setAttribute("listacc", listm);
                        request.setAttribute("gender", gender);
                        request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
                    }
                } else if (gender.equals("Female")) {
                    List<Accounts> listf = dao.getAllAccountFelmale(indexf);
                    if (listf.isEmpty()) {
                        request.setAttribute("error", "No result found !");
                        request.setAttribute("gender", gender);
                        request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
                    } else {
                        request.setAttribute("gender", gender);
                        request.setAttribute("indexf", indexf);
                        request.setAttribute("listacc", listf);
                        request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
                    }
                }

            } else if ((gender.equals("Gender") || gender == null) && (status.equals("Status") || status == null) && !role.equals("Role")) {
                int role_id = Integer.parseInt(role);
                int countCus = dao.countCustomer(role_id);

                int itemsPerPage = 12; // Số lượng mục trên mỗi trang
                int endC = (countCus + itemsPerPage - 1) / itemsPerPage; // Số lượng 

                request.setAttribute("endC", endC);

                String index3 = request.getParameter("indexc");
                if (index3 == null) {
                    index3 = "1";
                }
                int indexc = Integer.parseInt(index3);
                List<Accounts> listC = dao.getAccountByRole(role_id, indexc);
                if (listC.isEmpty()) {
                    request.setAttribute("error", "No result found !");
                    request.setAttribute("role", role_id);
                    request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
                } else {
                    request.setAttribute("indexc", indexc);
                    request.setAttribute("role", role_id);
                    request.setAttribute("listacc", listC);
                    request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
                }
            } else if ((gender.equals("Gender") || gender == null) && !(status.equals("Status")) && (role.equals("Role") || role == null)) {

                //List accout active
                int countActive = dao.countActiveAccount();
                int itemsPerPage = 12; // Số lượng mục trên mỗi trang
                int endA = (countActive + itemsPerPage - 1) / itemsPerPage; // Số lượng 

                request.setAttribute("endA", endA);
                String index4 = request.getParameter("indexa");
                if (index4 == null) {
                    index4 = "1";
                }
                int indexa = Integer.parseInt(index4);

                //List account inactive
                int countInActive = dao.countInActiveAccount();
                int endI = (countInActive + itemsPerPage - 1) / itemsPerPage; // Số lượng 
                request.setAttribute("endI", endI);
                String index5 = request.getParameter("indexi");
                if (index5 == null) {
                    index5 = "1";
                }
                int indexI = Integer.parseInt(index5);
                if (status.equals("1")) {
                    List<Accounts> lista = dao.getAccountsActive(indexa);
                    if (lista.isEmpty()) {
                        request.setAttribute("error", "No result found !");
                        request.setAttribute("status", "1");
                        request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
                    } else {
                        request.setAttribute("status", "1");
                        request.setAttribute("indexa", indexa);
                        request.setAttribute("listacc", lista);
                        request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
                    }

                } else if (status.equals("2")) {
                    List<Accounts> listi = dao.getAccountsInActive(indexI);
                    if (listi.isEmpty()) {
                        request.setAttribute("error", "No result found !");
                        request.setAttribute("status", "2");
                        request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
                    } else {
                        request.setAttribute("status", "2");
                        request.setAttribute("indexi", indexI);
                        request.setAttribute("listacc", listi);
                        request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
                    }
                }
            } else if (!(gender.equals("Gender") || gender == null) && (status.equals("Status") || status == null) && !(role.equals("Role") || role == null)) {
                int role_id = Integer.parseInt(role);
                int countGR = dao.countAccountByGenderAndRole(gender, role_id);
                int itemsPerPage = 12; // Số lượng mục trên mỗi trang
                int endGR = (countGR + itemsPerPage - 1) / itemsPerPage; // Số lượng 
                request.setAttribute("endGR", endGR);
                String index6 = request.getParameter("indexgr");
                if (index6 == null) {
                    index6 = "1";
                }
                int indexGR = Integer.parseInt(index6);
                List<Accounts> listgr = dao.getAccountByGenderAndRole(gender, role_id, indexGR);
                if (listgr.isEmpty()) {
                    request.setAttribute("error", "No result found !");
                    request.setAttribute("role", role_id);
                    request.setAttribute("gender", gender);
                    request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
                } else {
                    request.setAttribute("listacc", listgr);
                    request.setAttribute("gender", gender);
                    request.setAttribute("indexgr", indexGR);
                    request.setAttribute("role", role_id);
                    request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
                }
            } else if (!(gender.equals("Gender") || gender == null) && !(status.equals("Status") || status == null) && (role.equals("Role") || role == null)) {
                int status_id = Integer.parseInt(status);
                int countGS = dao.countAccountByGenderAndStatus(gender, status_id);
                int itemsPerPage = 12; // Số lượng mục trên mỗi trang
                int endGS = (countGS + itemsPerPage - 1) / itemsPerPage; // Số lượng 
                request.setAttribute("endGS", endGS);
                String index7 = request.getParameter("indexgs");
                if (index7 == null) {
                    index7 = "1";
                }
                int indexGS = Integer.parseInt(index7);
                List<Accounts> listgs = dao.getAccountByGenderAndStatus(gender, status_id, indexGS);
                if (listgs.isEmpty()) {
                    request.setAttribute("error", "No result found !");
                    request.setAttribute("gender", gender);
                    request.setAttribute("status", status_id);
                    request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
                } else {
                    request.setAttribute("listacc", listgs);
                    request.setAttribute("gender", gender);
                    request.setAttribute("status", status_id);
                    request.setAttribute("indexgs", indexGS);
                    request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
                }
            } else if ((gender.equals("Gender") || gender == null) && !(status.equals("Status") || status == null) && !(role.equals("Role") || role == null)) {
                int status_id = Integer.parseInt(status);
                int role_id = Integer.parseInt(role);

                int countRS = dao.countAccountByRoleAndStatus(role_id, status_id);
                int itemsPerPage = 12; // Số lượng mục trên mỗi trang
                int endRS = (countRS + itemsPerPage - 1) / itemsPerPage; // Số lượng 
                request.setAttribute("endRS", endRS);
                String index8 = request.getParameter("indexrs");
                if (index8 == null) {
                    index8 = "1";
                }
                int indexRS = Integer.parseInt(index8);
                List<Accounts> listrs = dao.getAccountByRoleAndStatus(role_id, status_id, indexRS);
                if (listrs.isEmpty()) {
                    request.setAttribute("error", "No result found !");
                    request.setAttribute("status", status_id);
                    request.setAttribute("role", role_id);
                    request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
                } else {
                    request.setAttribute("listacc", listrs);
                    request.setAttribute("status", status_id);
                    request.setAttribute("role", role_id);
                    request.setAttribute("indexrs", indexRS);
                    request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
                }
            } else {
                int status_id = Integer.parseInt(status);
                int role_id = Integer.parseInt(role);
                int count = dao.countAccountByGenderAndRoleAndStatus(gender, role_id, status_id);
                int itemsPerPage = 12; // Số lượng mục trên mỗi trang
                int endRS = (count + itemsPerPage - 1) / itemsPerPage; // Số lượng 
                request.setAttribute("end", endRS);
                String index9 = request.getParameter("index");
                if (index9 == null) {
                    index9 = "1";
                }
                int index = Integer.parseInt(index9);
                List<Accounts> list = dao.getAccountByGenderAndRoleAndStatus(gender, role_id, status_id, index);
                if (list.isEmpty()) {
                    request.setAttribute("error", "No result found !");
                    request.setAttribute("gender", gender);
                    request.setAttribute("status", status_id);
                    request.setAttribute("role", role_id);
                    request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
                } else {
                    request.setAttribute("listacc", list);
                    request.setAttribute("gender", gender);
                    request.setAttribute("status", status_id);
                    request.setAttribute("role", role_id);
                    request.setAttribute("index", index);
                    request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
                }
            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
