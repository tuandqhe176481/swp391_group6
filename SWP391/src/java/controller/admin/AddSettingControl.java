/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import controller.auth.Authorization;
import dal.AdminDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Accounts;
import model.SettingCategories;

/**
 *
 * @author pc
 */
@WebServlet(name = "AddSettingControl", urlPatterns = {"/addsetting"})
public class AddSettingControl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        AdminDAO dao = new AdminDAO();
        List<SettingCategories> listsc = dao.getAllSettingCate();
        request.setAttribute("listsc", listsc);
        request.getRequestDispatcher("addSetting.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (!Authorization.isAdmin((Accounts) session.getAttribute("acc"))) {
            Authorization.redirectToHome(session, response);
        } else {
            AdminDAO dao = new AdminDAO();
            String type1 = request.getParameter("type");
            String value = request.getParameter("value");
            String order1 = request.getParameter("order");
            String status = request.getParameter("status");

            int type = Integer.parseInt(type1);
            int order = Integer.parseInt(order1);
            System.out.println(type1);
            System.out.println(order1);

            switch (type) {
                case 1:
                    dao.addRole(value, status);
                    dao.addSetting(type, value, order, status);
                    response.sendRedirect("settinglist");
                    break;
                case 2:
                    dao.addBrand(value, status);
                    dao.addSetting(type, value, order, status);
                    response.sendRedirect("settinglist");
                    break;
                case 3:
                    dao.addCate(value, status);
                    dao.addSetting(type, value, order, status);
                    response.sendRedirect("settinglist");
                    break;
                case 4:
                    dao.addBlogCate(value, status);
                    dao.addSetting(type, value, order, status);
                    response.sendRedirect("settinglist");
                    break;
                default:
                    break;
            }
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
