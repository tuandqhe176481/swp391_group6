/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import controller.auth.Authorization;
import dal.AdminDAO;
import dal.DAO;
import dal.MaketingDao;
import dal.SaleDAO;
import dal.SaleManagerDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.AccountStatus;
import model.Accounts;
import model.BlogCategories;
import model.Brand;
import model.ProductCategories;
import model.Role;
import model.Setting;
import model.SettingCategories;
import model.Status;
import model.StatusOrder;

/**
 *
 * @author pc
 */
@WebServlet(name = "SettingListControl", urlPatterns = {"/settinglist"})
public class SettingListControl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if (!Authorization.isAdmin((Accounts) session.getAttribute("acc"))) {
            Authorization.redirectToHome(session, response);
        } else {
            AdminDAO dao = new AdminDAO();
            int count = dao.countSetting();
            int end = count / 6;
            if (end % 2 != 0) {
                end++;
            }
            request.setAttribute("end", end);
            String index = request.getParameter("index");
            if (index == null) {
                index = "1";
            }
            int indexs = Integer.parseInt(index);
            List<Setting> list = dao.getAllSetting(indexs);
            request.setAttribute("list", list);
            request.setAttribute("index", index);

            List<SettingCategories> listsc = dao.getAllSettingCate();
            request.setAttribute("listsc", listsc);
            request.getRequestDispatcher("settingList.jsp").forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
