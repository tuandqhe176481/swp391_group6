/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import controller.auth.Authorization;
import dal.AdminDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Accounts;
import model.Setting;
import model.SettingCategories;

/**
 *
 * @author pc
 */
@WebServlet(name = "FilterSettingControl", urlPatterns = {"/filtersetting"})
public class FilterSettingControl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if (!Authorization.isAdmin((Accounts) session.getAttribute("acc"))) {
            Authorization.redirectToHome(session, response);
        } else {
            AdminDAO dao = new AdminDAO();
            List<SettingCategories> listsc = dao.getAllSettingCate();
            request.setAttribute("listsc", listsc);

            String status = request.getParameter("status");
            String categories = request.getParameter("categories");
            int cate = Integer.parseInt(categories);
            if (categories.equals("0") && status.equals("Status")) {
                response.sendRedirect("settinglist");
            } else if (!categories.equals("0") && status.equals("Status")) {
                int countcate = dao.countSettingCate(cate);
                int itemsPerPage = 6; // Số lượng mục trên mỗi trang
                int endcate = (countcate + itemsPerPage - 1) / itemsPerPage; // Số lượng trang cần thiết
                System.out.println(endcate);
                request.setAttribute("endcate", endcate);

                String indexcate = request.getParameter("indexc");
                if (indexcate == null) {
                    indexcate = "1";
                }
                int indexc = Integer.parseInt(indexcate);
                List<Setting> listcate = dao.getAllSettingByCate(indexc, cate);
                if (listcate == null) {
                    request.setAttribute("mess", "No result found");
                    request.getRequestDispatcher("settingList.jsp").forward(request, response);
                } else {
                    request.setAttribute("list", listcate);
                    request.setAttribute("indexc", indexc);
                    request.setAttribute("categories", cate);
                    request.getRequestDispatcher("settingList.jsp").forward(request, response);
                }

            } else if (categories.equals("0") && !status.equals("Status")) {
                int countstatus = dao.countSettingStatus(status);
                int itemsPerPage = 6; // Số lượng mục trên mỗi trang
                int endstatus = (countstatus + itemsPerPage - 1) / itemsPerPage; // Số lượng trang cần thiết
                request.setAttribute("endstatus", endstatus);
                System.out.println(countstatus);
                String indexstatus = request.getParameter("indexs");
                if (indexstatus == null) {
                    indexstatus = "1";
                }
                int indexs = Integer.parseInt(indexstatus);
                System.out.println(status);
                List<Setting> liststatus = dao.getAllSettingByStatus(indexs, status);
                if (liststatus.isEmpty()) {
                    request.setAttribute("error", "No result found !");
                    request.setAttribute("status", status);
                    request.getRequestDispatcher("settingList.jsp").forward(request, response);
                } else {
                    request.setAttribute("list", liststatus);
                    request.setAttribute("indexs", indexs);
                    request.setAttribute("status", status);
                    request.getRequestDispatcher("settingList.jsp").forward(request, response);
                }
            } else if (!categories.equals("0") && !status.equals("Status")) {

                int countstacate = dao.countSettingByCateAndStatus(cate, status);
                int itemsPerPage = 6;
                int endstacate = (countstacate + itemsPerPage - 1) / itemsPerPage;
                request.setAttribute("endstacate", endstacate);
                String indexstacate = request.getParameter("indexsc");

                if (indexstacate == null) {
                    indexstacate = "1";

                }
                int indexsc = Integer.parseInt(indexstacate);
                System.out.println(endstacate);
                System.out.println(cate);
                System.out.println(status);
                List<Setting> liststacate = dao.getAllSettingByCateAndStatus(indexsc, cate, status);
                if (liststacate.isEmpty()) {
                    request.setAttribute("error", "No result found !");
                    request.setAttribute("status", status);
                    request.setAttribute("categories", cate);
                    request.getRequestDispatcher("settingList.jsp").forward(request, response);
                } else {
                    request.setAttribute("list", liststacate);
                    request.setAttribute("indexsc", indexsc);
                    request.setAttribute("status", status);
                    request.setAttribute("categories", cate);
                    request.getRequestDispatcher("settingList.jsp").forward(request, response);
                }

            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
