/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import controller.auth.Authorization;
import dal.AdminDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Accounts;
import model.Setting;
import model.SettingCategories;

/**
 *
 * @author pc
 */
@WebServlet(name = "EditSettingControl", urlPatterns = {"/editsetting"})
public class EditSettingControl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            if (!Authorization.isAdmin((Accounts) session.getAttribute("acc"))) {
                Authorization.redirectToHome(session, response);
            } else {
                AdminDAO dao = new AdminDAO();
                List<SettingCategories> listsc = dao.getAllSettingCate();
                request.setAttribute("listsc", listsc);

                String id = request.getParameter("id");
                int sid = Integer.parseInt(id);
                Setting s = dao.getSettingById(sid);
                request.setAttribute("s", s);

                request.getRequestDispatcher("editSetting.jsp").forward(request, response);
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AdminDAO dao = new AdminDAO();
        String type = request.getParameter("type");
        String value = request.getParameter("value");
        String order_raw = request.getParameter("order");
        String status = request.getParameter("status");
        String id = request.getParameter("id");

        int type_id = Integer.parseInt(type);
        int order = Integer.parseInt(order_raw);
        int sid = Integer.parseInt(id);

        System.out.println(type);
        System.out.println(value);
        System.out.println(order_raw);
        System.out.println(status);
        System.out.println(id);

        dao.updateSetting(type_id, value, order, status, sid);
        response.sendRedirect("settinglist");

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
