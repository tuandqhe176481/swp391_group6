/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import controller.auth.Authorization;
import dal.AdminDAO;
import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import model.AccountStatus;
import model.Accounts;
import model.Email;
import model.Role;

@MultipartConfig
@WebServlet(name = "addUserControl", urlPatterns = {"/adduser"})
public class addUserControl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if (!Authorization.isAdmin((Accounts) session.getAttribute("acc"))) {
            Authorization.redirectToHome(session, response);
        } else {
            //Lấy thông tin gender, status, role
            AdminDAO dao = new AdminDAO();
            List<Role> listr = dao.getAllRole();
            request.setAttribute("listr", listr);

            List<AccountStatus> listas = dao.getAllStatus();
            request.setAttribute("listas", listas);
            request.getRequestDispatcher("addUserManager.jsp").forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AdminDAO daoa = new AdminDAO();
        DAO dao = new DAO();
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String pass = request.getParameter("pass");
        String phone = request.getParameter("phone");
        String gender = request.getParameter("gender");
        String address = request.getParameter("address");
        String role = request.getParameter("role");
        String status = request.getParameter("status");
        String img = request.getParameter("img");
        LocalDate date = LocalDate.now();
        String created_at = date.format(DateTimeFormatter.ISO_LOCAL_DATE);
        String imgUpload = null;
        try {
            Part filePart = request.getPart("img");
            if (filePart.getSize() > 0) {
                String fileName = getFileName(filePart);

                String savePath = getServletContext().getRealPath("/") + "images/";
                File fileSaveDir = new File(savePath);
                if (!fileSaveDir.exists()) {
                    fileSaveDir.mkdir();
                }

                File file = new File(savePath + fileName);
                filePart.write(file.getAbsolutePath());

                imgUpload = "images/" + fileName;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        int role_id = Integer.parseInt(role);
        int status_id = Integer.parseInt(status);
        System.out.println(name);
        System.out.println(email);
        System.out.println(pass);
        System.out.println(gender);
        System.out.println(phone);
        System.out.println(status_id);
        System.out.println(role_id);
        System.out.println(imgUpload);
        System.out.println(address);
        System.out.println(created_at);
        Accounts a = dao.checkAccount(email);
        if (a == null) {
            daoa.addUser(email, pass, name, role_id, created_at, status_id, phone, gender, address, imgUpload);
            Email e = new Email();
            e.sendEmail(email, "WelCome to my shop!!!", "<html lang=\"en\">\n"
                    + "<body>\n"
                    + "<h1> GenzFashion send you your accounts" + "</h1>"
                    + "  <p>We are excited to have you join us.</p>\n"
                    + "    <p><strong>Your account details:</strong></p>"
                    + "<ul>"
                    + " <li><strong>Email:</strong></li>\n" + email
                    + "<li><strong>Password:</strong> </li>" + pass
                    + " </ul>"
                    + "</body>\n"
                    + "</html>");
            response.sendRedirect("usermanager");
        } else {
            request.setAttribute("error", "this username is exist ");
            request.getRequestDispatcher("userList-manager.jsp").forward(request, response);
        }

    }

    private String getFileName(final Part part) {
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
