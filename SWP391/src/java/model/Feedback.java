/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;

/**
 *
 * @author pv
 */
public class Feedback {
    private int feedback_id;
    private Accounts acid;
    private Product pid;
    private double rate;
    private Date time;
    private String comment;

    public Feedback() {
    }

    public Feedback(int feedback_id, Accounts acid, Product pid, double rate, Date time, String comment) {
        this.feedback_id = feedback_id;
        this.acid = acid;
        this.pid = pid;
        this.rate = rate;
        this.time = time;
        this.comment = comment;
    }

    public int getFeedback_id() {
        return feedback_id;
    }

    public void setFeedback_id(int feedback_id) {
        this.feedback_id = feedback_id;
    }

    public Accounts getAcid() {
        return acid;
    }

    public void setAcid(Accounts acid) {
        this.acid = acid;
    }

    public Product getPid() {
        return pid;
    }

    public void setPid(Product pid) {
        this.pid = pid;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    
}
