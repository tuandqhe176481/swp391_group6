/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author PV
 */
public class OrderDetail {
      private int id;
    private int acId;
    private int productId;
    private String pname;
    private int productPrice;
    private int price;
    private String img;
    private int quantity;

    public OrderDetail() {
    }

    public OrderDetail(int productId, int quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }
    

    public OrderDetail(int id, int productId, String pname, int price, String img, int quantity) {
        this.id = id;
        this.productId = productId;
        this.pname = pname;
        this.price = price;
        this.img = img;
        this.quantity = quantity;
    }
    
    
       public OrderDetail(int productId, String pname, int productPrice, String img, int quantity, int price) {
        this.productId = productId;
        this.pname = pname;
        this.productPrice = productPrice;
        this.img = img;
        this.quantity = quantity;
              this.price = price;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public int getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(int productPrice) {
        this.productPrice = productPrice;
    }

    public OrderDetail(int id, int acId, int productId, int price, int quantity) {
        this.id = id;
        this.acId = acId;
        this.productId = productId;
        this.price = price;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAcId() {
        return acId;
    }

    public void setAcId(int acId) {
        this.acId = acId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    
    
    
}
