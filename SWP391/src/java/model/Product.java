/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author pc
 */
public class Product {

    private int pid;
    private int cid;
    private String pname;
    private float rate;
    private String img1;
    private String img2;
    private int price;
    private String price1;
    private int brandid;
    private int priceSale;
     private String priceSale1;
    private int quantity;
    private boolean isDiscount;
    private boolean isSoldout;
    private String created_at;
    private int status;
    private String statuss;
    private int pdid;
    private String size;
    private String color;
    private Boolean gender;
    private String description;

    public Product(int pid, int cid, String pname, float rate, String img1, String img2, String price1, int brandid, String priceSale1, int quantity, boolean isDiscount, boolean isSoldout, String created_at, int status) {
        this.pid = pid;
        this.cid = cid;
        this.pname = pname;
        this.rate = rate;
        this.img1 = img1;
        this.img2 = img2;
        this.price1 = price1;
        this.brandid = brandid;
        this.priceSale1 = priceSale1;
        this.quantity = quantity;
        this.isDiscount = isDiscount;
        this.isSoldout = isSoldout;
        this.created_at = created_at;
        this.status = status;
    }

    public String getPrice1() {
        return price1;
    }

    public void setPrice1(String price1) {
        this.price1 = price1;
    }

    public String getPriceSale1() {
        return priceSale1;
    }

    public void setPriceSale1(String priceSale1) {
        this.priceSale1 = priceSale1;
    }

    public Product() {
    }

    public Product(int pid, String size, String color, Boolean gender, String description) {
        this.pid = pid;
        this.size = size;
        this.color = color;
        this.gender = gender;
        this.description = description;
    }

    
      public Product(int pid, int cid, String pname, float rate, String img1, String img2, int price, int brandid, int priceSale, int quantity, boolean isDiscount, boolean isSoldout, String created_at) {
        this.pid = pid;
        this.cid = cid;
        this.pname = pname;
        this.rate = rate;
        this.img1 = img1;
        this.img2 = img2;
        this.price = price;
        this.brandid = brandid;
        this.priceSale = priceSale;
        this.quantity = quantity;
        this.isDiscount = isDiscount;
        this.isSoldout = isSoldout;
        this.created_at = created_at;
    }
 

    
    
    public Product(int pid, int cid, String pname, float rate, String img1, String img2, int price, int brandid, int priceSale, int quantity, boolean isDiscount, boolean isSoldout, String created_at, int status, String size, String color, Boolean gender, String description) {
        this.pid = pid;
        this.cid = cid;
        this.pname = pname;
        this.rate = rate;
        this.img1 = img1;
        this.img2 = img2;
        this.price = price;
        this.brandid = brandid;
        this.priceSale = priceSale;
        this.quantity = quantity;
        this.isDiscount = isDiscount;
        this.isSoldout = isSoldout;
        this.created_at = created_at;
        this.status = status;
        this.size = size;
        this.color = color;
        this.gender = gender;
        this.description = description;
    }



    public int getPdid() {
        return pdid;
    }

    public void setPdid(int pdid) {
        this.pdid = pdid;
    }

    public Product(int pid, int cid, String pname, float rate, String img1, String img2, int price, int brandid, int priceSale, int quantity, boolean isDiscount, boolean isSoldout, String created_at, int status) {
        this.pid = pid;
        this.cid = cid;
        this.pname = pname;
        this.rate = rate;
        this.img1 = img1;
        this.img2 = img2;
        this.price = price;
        this.brandid = brandid;
        this.priceSale = priceSale;
        this.quantity = quantity;
        this.isDiscount = isDiscount;
        this.isSoldout = isSoldout;
        this.created_at = created_at;
        this.status = status;
    }

    public Product(int pid, int cid, String pname, float rate, String img1, String img2, int price, int brandid, int priceSale, int quantity, boolean isDiscount, boolean isSoldout, String created_at, String statuss) {
        this.pid = pid;
        this.cid = cid;
        this.pname = pname;
        this.rate = rate;
        this.img1 = img1;
        this.img2 = img2;
        this.price = price;
        this.brandid = brandid;
        this.priceSale = priceSale;
        this.quantity = quantity;
        this.isDiscount = isDiscount;
        this.isSoldout = isSoldout;
        this.created_at = created_at;
        this.statuss = statuss;
    }
    
    
    

    public String getStatuss() {
        return statuss;
    }

    public void setStatuss(String statuss) {
        this.statuss = statuss;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public String getImg1() {
        return img1;
    }

    public void setImg1(String img1) {
        this.img1 = img1;
    }

    public String getImg2() {
        return img2;
    }

    public void setImg2(String img2) {
        this.img2 = img2;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getBrandid() {
        return brandid;
    }

    public void setBrandid(int brandid) {
        this.brandid = brandid;
    }

    public int getPriceSale() {
        return priceSale;
    }

    public void setPriceSale(int priceSale) {
        this.priceSale = priceSale;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isIsDiscount() {
        return isDiscount;
    }

    public void setIsDiscount(boolean isDiscount) {
        this.isDiscount = isDiscount;
    }

    public boolean isIsSoldout() {
        return isSoldout;
    }

    public void setIsSoldout(boolean isSoldout) {
        this.isSoldout = isSoldout;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
