/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author pv
 */
public class Blog {

    int blog_id;
    String title;
    String author;
    String date;
    String content;
    int blog_categories_id;
    String blog_categories_name;
    String thumbnail;
    String brief;
    int user_id;
    int status_id;
    String status_name;

    public String getBlog_categories_name() {
        return blog_categories_name;
    }

    public void setBlog_categories_name(String blog_categories_name) {
        this.blog_categories_name = blog_categories_name;
    }

    public Blog(int blog_id, String title, String author, String date, String content, String blog_categories_name, String thumbnail, String brief, int user_id, String status_name) {
        this.blog_id = blog_id;
        this.title = title;
        this.author = author;
        this.date = date;
        this.content = content;
        this.blog_categories_name = blog_categories_name;
        this.thumbnail = thumbnail;
        this.brief = brief;
        this.user_id = user_id;
        this.status_name = status_name;
    }

    public Blog() {
    }

    public Blog(int blog_id, String title, String author, String date, String content, int blog_categories_id, String thumbnail, String brief, int user_id) {
        this.blog_id = blog_id;
        this.title = title;
        this.author = author;
        this.date = date;
        this.content = content;
        this.blog_categories_id = blog_categories_id;
        this.thumbnail = thumbnail;
        this.brief = brief;
        this.user_id = user_id;
    }

    public int getStatus_id() {
        return status_id;
    }

    public void setStatus_id(int status_id) {
        this.status_id = status_id;
    }

    public String getStatus_name() {
        return status_name;
    }

    public void setStatus_name(String status_name) {
        this.status_name = status_name;
    }

    public Blog(int blog_id, String title, String author, String date, String content, int blog_categories_id, String thumbnail, String brief, int user_id, String status_name) {
        this.blog_id = blog_id;
        this.title = title;
        this.author = author;
        this.date = date;
        this.content = content;
        this.blog_categories_id = blog_categories_id;
        this.thumbnail = thumbnail;
        this.brief = brief;
        this.user_id = user_id;
        this.status_name = status_name;
    }

    public int getBlog_id() {
        return blog_id;
    }

    public void setBlog_id(int blog_id) {
        this.blog_id = blog_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getBlog_categories_id() {
        return blog_categories_id;
    }

    public void setBlog_categories_id(int blog_categories_id) {
        this.blog_categories_id = blog_categories_id;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

}
