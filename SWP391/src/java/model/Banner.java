/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author pv
 */
public class Banner {
    private int banId;
    private String banImg;
    private int status;
    private String createdAt;
    private int roleId;
    private int bid;
    private String bName;
    public Banner() {
    }
    public Banner(int banId, String banImg, int status, String createdAt, int roleId, int bid) {
        this.banId = banId;
        this.banImg = banImg;
        this.status = status;
        this.createdAt = createdAt;
        this.roleId = roleId;
        this.bid = bid;
        
    }
    public Banner( String banImg, int status, String createdAt, int roleId, int bid) {
        this.banId = banId;
        this.banImg = banImg;
        this.status = status;
        this.createdAt = createdAt;
        this.roleId = roleId;
        this.bid = bid;
        
    }
    public Banner(int banId, String banImg, int status, String createdAt, int roleId, int bid,String bName) {
        this.banId = banId;
        this.banImg = banImg;
        this.status = status;
        this.createdAt = createdAt;
        this.roleId = roleId;
        this.bid = bid;
        this.bName=bName;
    }

    public String getbName() {
        return bName;
    }

    public void setbName(String bName) {
        this.bName = bName;
    }

   
    public int getBanId() {
        return banId;
    }

    public void setBanId(int banId) {
        this.banId = banId;
    }

    public String getBanImg() {
        return banImg;
    }

    public void setBanImg(String banImg) {
        this.banImg = banImg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getBid() {
        return bid;
    }

    public void setBid(int bid) {
        this.bid = bid;
    }
    
    
    
    
}
