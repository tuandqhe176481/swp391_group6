/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author pv
 */
public class BlogCategories {
    int bc_id;
    String bc_name;

    public BlogCategories() {
    }

    public BlogCategories(int bc_id, String bc_name) {
        this.bc_id = bc_id;
        this.bc_name = bc_name;
    }

    public int getBc_id() {
        return bc_id;
    }

    public void setBc_id(int bc_id) {
        this.bc_id = bc_id;
    }

    public String getBc_name() {
        return bc_name;
    }

    public void setBc_name(String bc_name) {
        this.bc_name = bc_name;
    }

    
    
}
