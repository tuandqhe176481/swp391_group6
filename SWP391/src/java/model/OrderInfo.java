/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author PV
 */
public class OrderInfo {

    private int oid;
    private String ordered_at;
    private Double totalAmount;
    private int statusId;
    private String receiver;
    private String img;
    private String productname;
    private int quantityP;
    private int saleId;

    public OrderInfo(int oid, String ordered_at, Double totalAmount, int statusId, String receiver, String img, String productname) {
        this.oid = oid;
        this.ordered_at = ordered_at;
        this.totalAmount = totalAmount;
        this.statusId = statusId;
        this.receiver = receiver;
        this.img = img;
        this.productname = productname;
    }

    public OrderInfo(int oid, String ordered_at, Double totalAmount, int statusId, String receiver, String img, String productname, int quantityP) {
        this.oid = oid;
        this.ordered_at = ordered_at;
        this.totalAmount = totalAmount;
        this.statusId = statusId;
        this.receiver = receiver;
        this.img = img;
        this.productname = productname;
        this.quantityP = quantityP;
    }

    public OrderInfo(int oid, String ordered_at, Double totalAmount, int statusId, String receiver, String img, String productname, int quantityP, int saleId) {
        this.oid = oid;
        this.ordered_at = ordered_at;
        this.totalAmount = totalAmount;
        this.statusId = statusId;
        this.receiver = receiver;
        this.img = img;
        this.productname = productname;
        this.quantityP = quantityP;
        this.saleId = saleId;
    }

    
    
    
    
    

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public String getOrdered_at() {
        return ordered_at;
    }

    public void setOrdered_at(String ordered_at) {
        this.ordered_at = ordered_at;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public int getQuantityP() {
        return quantityP;
    }

    public void setQuantityP(int quantityP) {
        this.quantityP = quantityP;
    }

    public int getSaleId() {
        return saleId;
    }

    public void setSaleId(int saleId) {
        this.saleId = saleId;
    }

    
    
    
    
    
}
