/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author pc
 */
public class Setting {

    private int id;
    private int type;
    private String type_name;
    private String value;
    private int order;
    private String status;

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public Setting() {
    }

    public Setting(int id,String type_name, String value, int order, String status) {
        this.id = id;
        this.type_name = type_name;
        this.value = value;
        this.order = order;
        this.status = status;
    }

    public Setting(int id, int type,String type_name, String value, int order, String status) {
        this.id = id;
        this.type = type;
        this.type_name = type_name;
        this.value = value;
        this.order = order;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
