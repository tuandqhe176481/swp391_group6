/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author PV
 */
public class Order {
    
    
        private int oid;
        private int acid;
        private String ordered_at;
        private int totalAmount;
        private String address;
        private String phoneNumber;
        private int statusId;
        private String note;
        private String receiver;
        private int discount;   
        private int odid;
        private int pid;
        private String pname;
        private int oi;
        private String img;
        private String email;
        private float price;
        private int quantity;
        private int saleId;
        private String status;
        private String payment;

    public Order() {
    }

    public Order(int oid, int acid, String ordered_at, int totalAmount, String address, String phoneNumber, int statusId, String note, String receiver, String payment,int saleId,String status) {
        this.oid = oid;
        this.acid = acid;
        this.ordered_at = ordered_at;
        this.totalAmount = totalAmount;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.statusId = statusId;
        this.note = note;
        this.receiver = receiver;
        this.payment=payment;
        this.saleId = saleId;
        this.status=status;
    }

    public Order(int oid, int pid, String pname, String img, float price, int quantity) {
        this.oid = oid;
        this.pid = pid;
        this.pname = pname;
        this.img = img;
        this.price = price;
        this.quantity = quantity;
    }
    
    
    
     public Order(int oid, String ordered_at, int totalAmount, int statusId, String receiver, String pname, String img,int quantity, int pid) {
        this.oid = oid;
        this.ordered_at = ordered_at;
        this.totalAmount = totalAmount;
        this.statusId = statusId;
        this.receiver = receiver;
        this.pname = pname;
        this.img = img;
        this.quantity=quantity;
        this.pid = pid;
    }
   public Order(String ordered_at, int totalAmount) {
        this.ordered_at = ordered_at;
        this.totalAmount = totalAmount;
    }

    public Order(int oid, int acid, String ordered_at, int totalAmount, String address, String phoneNumber, int statusId, String note, String receiver, int discount, int odid, int pid, float price, int quantity) {
        this.oid = oid;
        this.acid = acid;
        this.ordered_at = ordered_at;
        this.totalAmount = totalAmount;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.statusId = statusId;
        this.note = note;
        this.receiver = receiver;
        this.discount = discount;
        this.odid = odid;
        this.pid = pid;
        this.price = price;
        this.quantity = quantity;
    }

    public Order(int oid, int acid, String ordered_at, int totalAmount, String address, String phoneNumber, int statusId, String note, String receiver, String payment, int odid, int pid, String pname, float price, int quantity) {
        this.oid = oid;
        this.acid = acid;
        this.ordered_at = ordered_at;
        this.totalAmount = totalAmount;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.statusId = statusId;
        this.note = note;
        this.receiver = receiver;
        this.payment = this.payment;
        this.odid = odid;
        this.pid = pid;
        this.pname = pname;
        this.price = price;
        this.quantity = quantity;
    }

   

    public Order(int oid, int acid, String ordered_at, int totalAmount, String address, String phoneNumber, int statusId, String note, String receiver, String payment, int odid, int pid, String pname, String img, String email, float price, int quantity) {
        this.oid = oid;
        this.acid = acid;
        this.ordered_at = ordered_at;
        this.totalAmount = totalAmount;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.statusId = statusId;
        this.note = note;
        this.receiver = receiver;
        this.payment = payment;
        this.odid = odid;
        this.pid = pid;
        this.pname = pname;
        this.img = img;
        this.email = email;
        this.price = price;
        this.quantity = quantity;
    }
 public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
    

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

   

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public int getAcid() {
        return acid;
    }

    public void setAcid(int acid) {
        this.acid = acid;
    }

    public String getOrdered_at() {
        return ordered_at;
    }

    public void setOrdered_at(String ordered_at) {
        this.ordered_at = ordered_at;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getOdid() {
        return odid;
    }

    public void setOdid(int odid) {
        this.odid = odid;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getOi() {
        return oi;
    }

    public void setOi(int oi) {
        this.oi = oi;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getSaleId() {
        return saleId;
    }

    public void setSaleId(int saleId) {
        this.saleId = saleId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }
    
    

    
    

}

   
