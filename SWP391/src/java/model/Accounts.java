/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author PV
 */
public class Accounts {

    private int user_id;
    private String email;
    private String pass;
    private String name;
    private int roleId;
    private String role_name;
    private String status_name;
    private int statusId;
    private String createAt;
    private String phone_number;
    private String gender;
    private String address;
    private String avatar;
    private int countorder;

    public Accounts(int accountId, String avatar, String name) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }

    public String getStatus_name() {
        return status_name;
    }

    public void setStatus_name(String status_name) {
        this.status_name = status_name;
    }

    public Accounts() {
    }

    public Accounts(String name) {
        this.name = name;
    }

    public Accounts(String email, String pass) {
        this.email = email;
        this.pass = pass;
    }

    public Accounts(String email, String name, String pass) {
        this.email = email;
        this.name = name;
        this.pass = pass;
    }

    public Accounts(int user_id, String email, String pass, String name, int roleId) {
        this.user_id = user_id;
        this.email = email;
        this.pass = pass;
        this.name = name;
        this.roleId = roleId;
    }

    public Accounts(int user_id, String email, String name, String role_name, String createAt, String status_name, String phone_number, String gender, String address, String avatar) {
        this.user_id = user_id;
        this.email = email;
        this.name = name;
        this.role_name = role_name;
        this.createAt = createAt;
        this.status_name = status_name;
        this.phone_number = phone_number;
        this.gender = gender;
        this.address = address;
        this.avatar = avatar;
    }

    public Accounts(int user_id, String email, String name, String role_name, String createAt, String status_name, String phone_number, String gender, String address) {
        this.user_id = user_id;
        this.email = email;
        this.name = name;
        this.role_name = role_name;
        this.createAt = createAt;
        this.status_name = status_name;
        this.phone_number = phone_number;
        this.gender = gender;
        this.address = address;
    }

    public Accounts(int user_id, String email, String pass, String name, int roleId, int statusId, String createAt, String phone_number, String gender, String address, String avatar) {
        this.user_id = user_id;
        this.email = email;
        this.pass = pass;
        this.name = name;
        this.roleId = roleId;
        this.statusId = statusId;
        this.createAt = createAt;
        this.phone_number = phone_number;
        this.gender = gender;
        this.address = address;
        this.avatar = avatar;
    }

    public Accounts(int user_id, String email, String pass, String name, int roleId, String createAt, int statusId, String phone_number, String gender, String address, String avatar) {
        this.user_id = user_id;
        this.email = email;
        this.pass = pass;
        this.name = name;
        this.roleId = roleId;
        this.createAt = createAt;
        this.statusId = statusId;
        this.phone_number = phone_number;
        this.gender = gender;
        this.address = address;
        this.avatar = avatar;
    }

    public Accounts(int user_id, String email, String pass, String name, int roleId, int statusId, String createAt, String phone_number, String gender, String address, String avatar, int countorder) {
        this.user_id = user_id;
        this.email = email;
        this.pass = pass;
        this.name = name;
        this.roleId = roleId;
        this.statusId = statusId;
        this.createAt = createAt;
        this.phone_number = phone_number;
        this.gender = gender;
        this.address = address;
        this.avatar = avatar;
        this.countorder = countorder;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String Email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getCountorder() {
        return countorder;
    }

    public void setCountorder(int countorder) {
        this.countorder = countorder;
    }

}
