/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Accounts;
import model.Order;
import model.OrderDetail;
import model.OrderInfo;
import model.StatusOrder;

/**
 *
 * @author PV
 */
public class SaleDAO extends DBContext {

    public int countProductByOid(int oid) {
        String sql = "select count(*) as quantity from orderDetails where oid=?";
        int quantity = 0;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, oid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                quantity = rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return quantity;
    }

    public List<OrderInfo> getOrderSales(int saleId) {
        List<OrderInfo> list = new ArrayList<>();
        String sql = "WITH OrderedOrders AS (\n" +
"                    SELECT o.oid, ordered_at, TotalAmount, status, pname,o.receiver, img,o.saleId,\n" +
"                           ROW_NUMBER() OVER (PARTITION BY o.oid ORDER BY o.ordered_at) AS row_num\n" +
"                    FROM orders o\n" +
"                    JOIN orderDetails od ON o.oid = od.oid\n" +
"                    JOIN Products p ON od.pid = p.pid \n" +
"                where o.saleId= ?\n" +
"                )\n" +
"                SELECT oid, ordered_at, TotalAmount, status, pname, receiver,img,saleId\n" +
"                FROM OrderedOrders\n" +
"                WHERE row_num = 1"
                + "ORDER BY ordered_at DESC, status ASC;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, saleId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int quantity = countProductByOid(rs.getInt(1));
                list.add(new OrderInfo(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getInt(4), rs.getString(6), rs.getString(7), rs.getString(5), quantity,rs.getInt(8)));
            }
        } catch (Exception e) {
        }
        return list;

    }

    public List<OrderInfo> getOrderInventory() {
        List<OrderInfo> list = new ArrayList<>();
        String sql = "WITH OrderedOrders AS (\n"
                + "    SELECT o.oid, ordered_at, TotalAmount, status, pname,o.receiver, img,\n"
                + "           ROW_NUMBER() OVER (PARTITION BY o.oid ORDER BY o.ordered_at) AS row_num\n"
                + "    FROM orders o\n"
                + "    JOIN orderDetails od ON o.oid = od.oid\n"
                + "    JOIN Products p ON od.pid = p.pid "
                + "where o.status in (2,3)\n"
                + ")\n"
                + "SELECT oid, ordered_at, TotalAmount, status, pname, receiver,img\n"
                + "FROM OrderedOrders\n"
                + "WHERE row_num = 1"
                + "ORDER BY ordered_at DESC, status ASC;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int quantity = countProductByOid(rs.getInt(1));
                list.add(new OrderInfo(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getInt(4), rs.getString(6), rs.getString(7), rs.getString(5), quantity));
            }
        } catch (Exception e) {
        }
        return list;

    }
    
    public List<OrderInfo> getOrderInventoryByDate(String start,String end) {
        List<OrderInfo> list = new ArrayList<>();
        String sql = "WITH OrderedOrders AS (\n"
                + "    SELECT o.oid, ordered_at, TotalAmount, status, pname,o.receiver, img,\n"
                + "           ROW_NUMBER() OVER (PARTITION BY o.oid ORDER BY o.ordered_at) AS row_num\n"
                + "    FROM orders o\n"
                + "    JOIN orderDetails od ON o.oid = od.oid\n"
                + "    JOIN Products p ON od.pid = p.pid "
                + "where o.status=2 and o.ordered_at BETWEEN ? AND ?\n"
                + ")\n"
                + "SELECT oid, ordered_at, TotalAmount, status, pname, receiver,img\n"
                + "FROM OrderedOrders\n"
                + "WHERE row_num = 1"
                + "ORDER BY ordered_at DESC, status ASC;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1,start);
            st.setString(2,end);            
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int quantity = countProductByOid(rs.getInt(1));
                list.add(new OrderInfo(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getInt(4), rs.getString(6), rs.getString(7), rs.getString(5), quantity));
            }
        } catch (Exception e) {
        }
        return list;

    }

    public List<StatusOrder> getStatusOrder() {
        List<StatusOrder> list = new ArrayList<>();
        String sql = "select * from statusOrder";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new StatusOrder(rs.getInt(1), rs.getString(2)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Accounts> getAllSales() {
        List<Accounts> list = new ArrayList<>();
        String sql = "select * from Accounts a where a.role_id=3";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getInt(7), rs.getString(6), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public void changeOrder(String oid, int status) {
        String sql = "update orders set status=? where oid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, status);
            st.setString(2, oid);
            ResultSet rs = st.executeQuery();
        } catch (Exception e) {
        }
    }

    public List<OrderDetail> getOrderDetailByOid(String oid) {
        List<OrderDetail> list = new ArrayList<>();
        String sql = "select od.oid,od.pid,p.pname,p.img,od.price,od.quantity from orderDetails od\n"
                + "join Products p on od.pid=p.pid\n"
                + "where oid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, oid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new OrderDetail(rs.getInt(1), rs.getInt(2), rs.getString(3), (int) rs.getDouble(5), rs.getString(4), rs.getInt(6)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public Order getOrderByOid(String oid) {

        String sql = "select o.*,so.status from orders o join statusOrder so on o.status=so.id where oid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, oid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new Order(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getString(8), rs.getString(9), rs.getString(10),rs.getInt(11), rs.getString(12));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void backProduct(String oid) {
        List<OrderDetail> list = new ArrayList<>();
        String sql = "select * from orderDetails where oid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, oid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new OrderDetail(rs.getInt(2), rs.getInt(5)));
            }
            for (OrderDetail od : list) {
                String sql2 = "update Products set quantity+=? where pid=?";
                PreparedStatement st2 = connection.prepareStatement(sql2);
                st2.setInt(1, od.getQuantity());
                st2.setInt(2, od.getProductId());
                st2.executeUpdate();
            }
        } catch (Exception e) {
        }
    }
    public List<OrderInfo> getOrderByStatus(int sale ,String status) {
        List<OrderInfo> list = new ArrayList<>();
        String sql = "WITH OrderedOrders AS (\n"
                + "    SELECT o.oid, ordered_at, TotalAmount, status, pname,o.receiver, img,o.saleId,\n"
                + "           ROW_NUMBER() OVER (PARTITION BY o.oid ORDER BY o.ordered_at) AS row_num\n"
                + "    FROM orders o\n"
                + "    JOIN orderDetails od ON o.oid = od.oid\n"
                + "    JOIN Products p ON od.pid = p.pid "
                + "where o.saleId= ? and o.status=?\n"
                + ")\n"
                + "SELECT oid, ordered_at, TotalAmount, status, pname, receiver,img,saleId\n"
                + "FROM OrderedOrders\n"
                + "WHERE row_num = 1"
                + "ORDER BY ordered_at DESC, status ASC;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, sale);
            st.setString(2,status);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int quantity = countProductByOid(rs.getInt(1));
                list.add(new OrderInfo(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getInt(4), rs.getString(6), rs.getString(7), rs.getString(5), quantity,rs.getInt(8)));
            }
        } catch (Exception e) {
        }
        return list;

    }
    
    public List<OrderInfo> getOrderByStatusAndDate(int sale ,String status,String start,String end) {
        List<OrderInfo> list = new ArrayList<>();
        String sql = "WITH OrderedOrders AS (\n"
                + "    SELECT o.oid, ordered_at, TotalAmount, status, pname,o.receiver, img,o.saleId,\n"
                + "           ROW_NUMBER() OVER (PARTITION BY o.oid ORDER BY o.ordered_at) AS row_num\n"
                + "    FROM orders o\n"
                + "    JOIN orderDetails od ON o.oid = od.oid\n"
                + "    JOIN Products p ON od.pid = p.pid "
                + "where o.saleId= ? and o.status=? and o.ordered_at BETWEEN ? AND ?\n"
                + ")\n"
                + "SELECT oid, ordered_at, TotalAmount, status, pname, receiver,img,saleId\n"
                + "FROM OrderedOrders\n"
                + "WHERE row_num = 1"
                + "ORDER BY ordered_at DESC, status ASC;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, sale);
            st.setString(2,status);
            st.setString(3, start);
            st.setString(4, end);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int quantity = countProductByOid(rs.getInt(1));
                list.add(new OrderInfo(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getInt(4), rs.getString(6), rs.getString(7), rs.getString(5), quantity,rs.getInt(8)));
            }
        } catch (Exception e) {
        }
        return list;

    }
    
    public List<OrderInfo> getOrderSalesDate(int saleId,String start,String end) {
        List<OrderInfo> list = new ArrayList<>();
        String sql = "WITH OrderedOrders AS (\n"
                + "    SELECT o.oid, ordered_at, TotalAmount, status, pname,o.receiver, img,o.saleId,\n"
                + "           ROW_NUMBER() OVER (PARTITION BY o.oid ORDER BY o.ordered_at) AS row_num\n"
                + "    FROM orders o\n"
                + "    JOIN orderDetails od ON o.oid = od.oid\n"
                + "    JOIN Products p ON od.pid = p.pid "
                + "where o.saleId= ? and o.ordered_at BETWEEN ? AND ?\n"
                + ")\n"
                + "SELECT oid, ordered_at, TotalAmount, status, pname, receiver,img,saleId\n"
                + "FROM OrderedOrders\n"
                + "WHERE row_num = 1"
                + "ORDER BY ordered_at DESC, status ASC;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, saleId);
            st.setString(2, start);
            st.setString(3, end);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int quantity = countProductByOid(rs.getInt(1));
                list.add(new OrderInfo(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getInt(4), rs.getString(6), rs.getString(7), rs.getString(5), quantity,rs.getInt(8)));
            }
        } catch (Exception e) {
        }
        return list;

    }
    
    public List<OrderInfo> getOrderByName(int saleId,String name) {
        List<OrderInfo> list = new ArrayList<>();
        String sql = "WITH OrderedOrders AS (\n"
                + "    SELECT o.oid, ordered_at, TotalAmount, status, pname,o.receiver, img,\n"
                + "           ROW_NUMBER() OVER (PARTITION BY o.oid ORDER BY o.ordered_at) AS row_num\n"
                + "    FROM orders o\n"
                + "    JOIN orderDetails od ON o.oid = od.oid\n"
                + "    JOIN Products p ON od.pid = p.pid "
                + "where o.saleId= ? and o.receiver like ?\n"
                + ")\n"
                + "SELECT oid, ordered_at, TotalAmount, status, pname, receiver,img\n"
                + "FROM OrderedOrders\n"
                + "WHERE row_num = 1"
                + "ORDER BY ordered_at DESC, status ASC;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, saleId);
            st.setString(2, '%'+name+'%');
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int quantity = countProductByOid(rs.getInt(1));
                list.add(new OrderInfo(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getInt(4), rs.getString(6), rs.getString(7), rs.getString(5), quantity));
            }
        } catch (Exception e) {
        }
        return list;

    }
    
    public List<OrderInfo> getListByPage(List<OrderInfo> list, int start, int end) {
        ArrayList<OrderInfo> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public static void main(String[] args) {
        SaleDAO dao = new SaleDAO();
        int i = dao.countProductByOid(4);
        System.out.println(i);
        List<OrderInfo> list =dao.getOrderByName(7, "gau");
        System.out.println(list.size());

    }
}
