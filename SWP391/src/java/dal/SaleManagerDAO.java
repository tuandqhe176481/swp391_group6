/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Accounts;
import model.OrderInfo;
import model.SaleTotal;

/**
 *
 * @author PV
 */
public class SaleManagerDAO extends DBContext {
    
    public int countProductByOid(int oid) {
        String sql = "select count(*) as quantity from orderDetails where oid=?";
        int quantity = 0;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, oid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                quantity = rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return quantity;
    }
    
    public List<OrderInfo> getAllOrder() {
        List<OrderInfo> list = new ArrayList<>();
        String sql = "WITH OrderedOrders AS (\n"
                + "                    SELECT o.oid, ordered_at, TotalAmount, status, pname,o.receiver, img,o.saleId,\n"
                + "                   ROW_NUMBER() OVER (PARTITION BY o.oid ORDER BY o.ordered_at) AS row_num\n"
                + "                   FROM orders o\n"
                + "                    JOIN orderDetails od ON o.oid = od.oid\n"
                + "                   JOIN Products p ON od.pid = p.pid)\n"
                + "                SELECT oid, ordered_at, TotalAmount, status, pname, receiver,img,saleId\n"
                + "                FROM OrderedOrders\n"
                + "                WHERE row_num = 1"
                + "ORDER BY ordered_at DESC, status ASC;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int quantity = countProductByOid(rs.getInt(1));
                list.add(new OrderInfo(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getInt(4), rs.getString(6), rs.getString(7), rs.getString(5), quantity, rs.getInt(8)));
            }
        } catch (Exception e) {
        }
        return list;
        
    }
    
    public List<Accounts> getAllSales() {
        List<Accounts> list = new ArrayList<>();
        String sql = "select * from Accounts where role_id=3";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getInt(7), rs.getString(6), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11), countOrderSale(rs.getInt(1))));
            }
        } catch (Exception e) {
        }
        return list;
    }
    
    public int countOrderSale(int saleId) {
        String sql = "select count(saleId) from orders \n"
                + "  where saleId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, saleId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            
        }
        return 0;
    }
    
    public void changeSale(String id, String oid) {
        String sql = "update orders set saleId =? where oid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            st.setString(2, oid);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }
    
    public List<OrderInfo> getOrderByStatus(String status) {
        List<OrderInfo> list = new ArrayList<>();
        String sql = "WITH OrderedOrders AS (\n"
                + "    SELECT o.oid, ordered_at, TotalAmount, status, pname,o.receiver, img,\n"
                + "           ROW_NUMBER() OVER (PARTITION BY o.oid ORDER BY o.ordered_at) AS row_num\n"
                + "    FROM orders o\n"
                + "    JOIN orderDetails od ON o.oid = od.oid\n"
                + "    JOIN Products p ON od.pid = p.pid "
                + "where o.status=?\n"
                + ")\n"
                + "SELECT oid, ordered_at, TotalAmount, status, pname, receiver,img\n"
                + "FROM OrderedOrders\n"
                + "WHERE row_num = 1"
                + "ORDER BY ordered_at DESC, status ASC;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, status);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int quantity = countProductByOid(rs.getInt(1));
                list.add(new OrderInfo(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getInt(4), rs.getString(6), rs.getString(7), rs.getString(5), quantity));
            }
        } catch (Exception e) {
        }
        return list;
        
    }
    
    public List<OrderInfo> getOrderByS(String status) {
        List<OrderInfo> list = new ArrayList<>();
        String sql = "WITH OrderedOrders AS (\n"
                + "    SELECT o.oid, ordered_at, TotalAmount, status, pname,o.receiver, img,o.saleId,\n"
                + "           ROW_NUMBER() OVER (PARTITION BY o.oid ORDER BY o.ordered_at) AS row_num\n"
                + "    FROM orders o\n"
                + "    JOIN orderDetails od ON o.oid = od.oid\n"
                + "    JOIN Products p ON od.pid = p.pid "
                + "where o.status=?\n"
                + ")\n"
                + "SELECT oid, ordered_at, TotalAmount, status, pname, receiver,img,saleId\n"
                + "FROM OrderedOrders\n"
                + "WHERE row_num = 1"
                + "ORDER BY ordered_at DESC, status ASC;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, status);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int quantity = countProductByOid(rs.getInt(1));
                list.add(new OrderInfo(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getInt(4), rs.getString(6), rs.getString(7), rs.getString(5), quantity, rs.getInt(8)));
            }
        } catch (Exception e) {
        }
        return list;
        
    }
    
    public List<OrderInfo> getOrderDate(String start, String end) {
        List<OrderInfo> list = new ArrayList<>();
        String sql = "WITH OrderedOrders AS (\n"
                + "    SELECT o.oid, ordered_at, TotalAmount, status, pname,o.receiver, img,o.saleId,\n"
                + "           ROW_NUMBER() OVER (PARTITION BY o.oid ORDER BY o.ordered_at) AS row_num\n"
                + "    FROM orders o\n"
                + "    JOIN orderDetails od ON o.oid = od.oid\n"
                + "    JOIN Products p ON od.pid = p.pid "
                + "where o.ordered_at BETWEEN ? AND ?\n"
                + ")\n"
                + "SELECT oid, ordered_at, TotalAmount, status, pname, receiver,img,saleId\n"
                + "FROM OrderedOrders\n"
                + "WHERE row_num = 1"
                + "ORDER BY ordered_at DESC, status ASC;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, start);
            st.setString(2, end);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int quantity = countProductByOid(rs.getInt(1));
                list.add(new OrderInfo(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getInt(4), rs.getString(6), rs.getString(7), rs.getString(5), quantity, rs.getInt(8)));
            }
        } catch (Exception e) {
        }
        return list;
        
    }
    
    public List<OrderInfo> getOrderBySAndD(String status, String start, String end) {
        List<OrderInfo> list = new ArrayList<>();
        String sql = "WITH OrderedOrders AS (\n"
                + "    SELECT o.oid, ordered_at, TotalAmount, status, pname,o.receiver, img,o.saleId,\n"
                + "           ROW_NUMBER() OVER (PARTITION BY o.oid ORDER BY o.ordered_at) AS row_num\n"
                + "    FROM orders o\n"
                + "    JOIN orderDetails od ON o.oid = od.oid\n"
                + "    JOIN Products p ON od.pid = p.pid "
                + "where o.status=? and o.ordered_at BETWEEN ? AND ?\n"
                + ")\n"
                + "SELECT oid, ordered_at, TotalAmount, status, pname, receiver,img,saleId\n"
                + "FROM OrderedOrders\n"
                + "WHERE row_num = 1"
                + "ORDER BY ordered_at DESC, status ASC;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, status);
            st.setString(2, start);
            st.setString(3, end);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int quantity = countProductByOid(rs.getInt(1));
                list.add(new OrderInfo(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getInt(4), rs.getString(6), rs.getString(7), rs.getString(5), quantity, rs.getInt(8)));
            }
        } catch (Exception e) {
        }
        return list;
        
    }
    
    public int getSales() {
        String sql = " select count(*) from accounts where role_id = 3";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            
        }
        return 0;
    }
    
    public int getOrder() {
        String sql = "select count(*) from orders where status <> 5";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            
        }
        return 0;
    }
    
    public int getTotalAmount() {
        String sql = "SELECT SUM(TotalAmount) FROM orders WHERE status = 4;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            
        }
        return 0;
    }
    
    public int getCountStatus(int status) {
        String sql = "SELECT Count(*) FROM orders WHERE status = ?;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, status);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            
        }
        return 0;
    }
    
    public List<SaleTotal> getTotalBySale() {
        List<SaleTotal> list = new ArrayList<>();
        String sql = " SELECT acc.username,Sum(o.TotalAmount) \n"
                + "from orders o join Accounts acc\n"
                + "on o.saleId=acc.acid\n"
                + "where o.status=4\n"
                + "group by acc.username";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new SaleTotal(rs.getString(1), rs.getInt(2)));
            }
        } catch (Exception e) {
        }
        return list;
    }
    
     public List<OrderInfo> getOrderByName1(String name) {
        List<OrderInfo> list = new ArrayList<>();
        String sql = "WITH OrderedOrders AS (\n"
                + "    SELECT o.oid, ordered_at, TotalAmount, status, pname,o.receiver, img,\n"
                + "           ROW_NUMBER() OVER (PARTITION BY o.oid ORDER BY o.ordered_at) AS row_num\n"
                + "    FROM orders o\n"
                + "    JOIN orderDetails od ON o.oid = od.oid\n"
                + "    JOIN Products p ON od.pid = p.pid "
                + "where o.receiver like ?\n"
                + ")\n"
                + "SELECT oid, ordered_at, TotalAmount, status, pname, receiver,img\n"
                + "FROM OrderedOrders\n"
                + "WHERE row_num = 1"
                + "ORDER BY ordered_at DESC, status ASC;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, '%'+name+'%');
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int quantity = countProductByOid(rs.getInt(1));
                list.add(new OrderInfo(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getInt(4), rs.getString(6), rs.getString(7), rs.getString(5), quantity));
            }
        } catch (Exception e) {
        }
        return list;

    }
    
     public List<OrderInfo> getOrderByName2(String name) {
        List<OrderInfo> list = new ArrayList<>();
        String sql = "WITH OrderedOrders AS (\n"
                + "    SELECT o.oid, ordered_at, TotalAmount, status, pname,o.receiver, img,\n"
                + "           ROW_NUMBER() OVER (PARTITION BY o.oid ORDER BY o.ordered_at) AS row_num\n"
                + "    FROM orders o\n"
                + "    JOIN orderDetails od ON o.oid = od.oid\n"
                + "    JOIN Products p ON od.pid = p.pid "
                + "where o.receiver like ? and o.status=2 \n"
                + ")\n"
                + "SELECT oid, ordered_at, TotalAmount, status, pname, receiver,img\n"
                + "FROM OrderedOrders\n"
                + "WHERE row_num = 1"
                + "ORDER BY ordered_at DESC, status ASC;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, '%'+name+'%');
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int quantity = countProductByOid(rs.getInt(1));
                list.add(new OrderInfo(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getInt(4), rs.getString(6), rs.getString(7), rs.getString(5), quantity));
            }
        } catch (Exception e) {
        }
        return list;

    }
    public static void main(String[] args) {
        SaleManagerDAO dao = new SaleManagerDAO();
        dao.changeSale("9", "1");
    }
    
}
