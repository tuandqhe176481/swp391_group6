/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import com.sun.jmx.remote.internal.ArrayQueue;
import java.sql.Connection;
import context.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Banner;
import model.Brand;
import model.Product;
import model.ProductCategories;
import model.Accounts;
import model.Blog;
import model.BlogCategories;
import model.Feedback;
import model.Order;
import model.OrderDetail;

/**
 *
 * @author pc
 */
public class DAO extends DBContext {

    public List<Product> getAllProduct() {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT * FROM Products ORDER BY CONVERT(DATE, created_at, 103) DESC where quantity > 0";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<ProductCategories> getAllCategories() {
        List<ProductCategories> list = new ArrayList<>();
        String sql = "select * from Categories where status = 'Active'";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new ProductCategories(rs.getInt(1), rs.getString(2)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Brand> getAllBrand() {
        List<Brand> list = new ArrayList<>();
        String sql = "select * from Brand where status = 'Active'";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Brand(rs.getInt(1), rs.getString(2)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getProductByCid(String cid) {
        List<Product> list = new ArrayList<>();
        String sql = "select * from Products where cid = " + cid;

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getProductByBid(String bid) {
        List<Product> list = new ArrayList<>();
        String sql = "select * from Products where quantity > 0 and status_id = 1 and  brandid = " + bid;

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public Product getProductByPid(String pid) {

        String sql = "select * from Products where pid = " + pid;

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Product> getProductSale(String sale) {
        List<Product> list = new ArrayList<>();
        String sql = "select * from Products\n"
                + "where quantity > 0 and status_id = 1 and  isDiscount = ?";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, sale);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getProductBybidAndcid(int cid, int bid) {
        List<Product> list = new ArrayList<>();
        String sql = "  select * from Products\n"
                + "  where quantity > 0 and status_id = 1 and  cid = ? and brandid = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, cid);
            ps.setInt(2, bid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> searchByName(String search) {
        List<Product> list = new ArrayList<>();
        String sql = "select * from Products where quantity > 0 and status_id = 1 and pname like ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + search + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getProductByPrice1(int price1, int price2) {
        List<Product> list = new ArrayList<>();
        String sql = "select * from Products\n"
                + " where quantity > 0 and status_id = 1 and  \n"
                + "   price between ? and ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, price1);
            ps.setInt(2, price2);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getProductByPrice2(int price1, int price2, int cid, int bid) {
        List<Product> list = new ArrayList<>();
        String sql = "select * from Products\n"
                + "  where quantity > 0 and status_id = 1 and  cid = ? and brandid = ?\n"
                + "  and price between ? and ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, cid);
            ps.setInt(2, bid);
            ps.setInt(3, price1);
            ps.setInt(4, price2);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getProductByPriceAndCate(int price1, int price2, int cid) {
        List<Product> list = new ArrayList<>();
        String sql = " select * from Products\n"
                + "  where quantity > 0 and status_id = 1 and  cid = ?\n"
                + "  and price between ? and ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, cid);
            ps.setInt(2, price1);
            ps.setInt(3, price2);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getProductByPriceAndBrand(int price1, int price2, int bid) {
        List<Product> list = new ArrayList<>();
        String sql = " select * from Products\n"
                + "  where quantity > 0 and status_id = 1 and  brandid = ?\n"
                + "  and price between ? and ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, bid);
            ps.setInt(2, price1);
            ps.setInt(3, price2);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getLastestProduct() {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT TOP 2 * FROM Products where quantity > 0 and status_id = 1\n"
                + "order by CONVERT(DATE, created_at, 103) DESC";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<OrderDetail> getProductBestSeller() {
        List<OrderDetail> list = new ArrayList<>();
        String sql = " SELECT TOP (4) \n"
                + "    od.pid,\n"
                + "    p.pname,\n"
                + "	p.price,\n"
                + "	p.img2,\n"
                + "    SUM(od.quantity) AS quantitySold,\n"
                + "	SUM(od.price) as priceSold\n"
                + "FROM \n"
                + "    orderDetails od\n"
                + "JOIN \n"
                + "    Products p ON od.pid = p.pid\n"
                + "GROUP BY	\n"
                + "    od.pid, p.pname,p.price,p.img2\n"
                + "ORDER BY \n"
                + "   quantitySold DESC;";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new OrderDetail(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getInt(5), rs.getInt(6)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Product> getProductNewArrival() {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT TOP 4 * FROM Products ORDER BY created_at DESC";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(
                        rs.getInt("pid"),
                        rs.getInt("cid"),
                        rs.getString("pname"),
                        rs.getFloat("rate"),
                        rs.getString("img"),
                        rs.getString("img2"),
                        rs.getInt("price"),
                        rs.getInt("brandid"),
                        rs.getInt("priceSale"),
                        rs.getInt("quantity"),
                        rs.getBoolean("isDiscount"),
                        rs.getBoolean("isSoldout"),
                        rs.getString("created_at")
                ));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return list;
    }

    public List<Banner> getBanners() {
        List<Banner> list = new ArrayList<>();
        String sql = "SELECT * FROM Banner WHERE role_id = 4 AND status = 1 ORDER BY created_at DESC";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Banner(
                        rs.getInt("banid"),
                        rs.getString("img"),
                        rs.getInt("status"),
                        rs.getString("created_at"),
                        rs.getInt("role_id"),
                        rs.getInt("brandid")
                ));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return list;
    }

    public Accounts checkAccount(String email) {
        String sql = "select * from Accounts where email=? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Accounts(email, rs.getString(4));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public Accounts checkAccountName(String name) {
        String sql = "select * from Accounts where username=? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, name);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Accounts(name);
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void signup(String user, String pass, String phone, String email, String address, boolean gt) {
        LocalDate curDate = java.time.LocalDate.now();
        String date = curDate.toString();
        String sql = "INSERT INTO Accounts (email, password, username,role_id,created_at, status_id,phone_number, gender, address) "
                + "VALUES (?, ?, ?, ?, ?, ?,?,?,?)";

        try ( PreparedStatement st = connection.prepareStatement(sql)) {

            // Set parameters
            st.setString(1, email);
            st.setString(2, pass);
            st.setString(3, user);
            st.setInt(4, 5);
            st.setString(5, date);
            st.setInt(6, 1);
            st.setString(7, phone);
            st.setBoolean(8, gt);
            st.setString(9, address);
            st.executeUpdate();

        } catch (Exception e) {
        }
    }

    public Accounts login(String email, String pass) {
        String sql = "select * from Accounts where email=? and password=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            st.setString(2, pass);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                String name = rs.getString("username");
                return new Accounts(rs.getInt(1), email, rs.getString(3), rs.getString(4), rs.getInt(5), rs.getInt(7), rs.getString(6), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11));
            }

        } catch (Exception e) {
        }
        return null;
    }

    public void changePass(String email, String newpass) {
        String sql = "UPDATE Accounts SET password = ? WHERE email = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, newpass);
            st.setString(2, email);
            st.executeUpdate();

        } catch (Exception e) {
        }
    }

    public List<Product> getLatestProducts(int categoryId) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT TOP 1 * FROM Products "
                + "WHERE cid = ? "
                + "ORDER BY created_at DESC";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, categoryId); // Thay thế đối số trong WHERE
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(
                        rs.getInt("pid"),
                        rs.getInt("cid"),
                        rs.getString("pname"),
                        rs.getFloat("rate"),
                        rs.getString("img"),
                        rs.getString("img2"),
                        rs.getInt("price"),
                        rs.getInt("brandid"),
                        rs.getInt("priceSale"),
                        rs.getInt("quantity"),
                        rs.getBoolean("isDiscount"),
                        rs.getBoolean("isSoldout"),
                        rs.getString("created_at")
                ));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return list;
    }

    public List<Product> getProductByPrice(int price1, int price2) {
        List<Product> list = new ArrayList<>();
        String sql = "select * from Products\n"
                + "where price between ? and ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, price1);
            ps.setInt(2, price2);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getProductRelated(int pid) {
        List<Product> list = new ArrayList<>();
        String sql = " SELECT TOP 8 *\n"
                + "FROM Products\n"
                + "where quantity > 0 and status_id = 1 and       brandid = (SELECT brandid FROM Products WHERE pid = ?)\n"
                + "ORDER BY price DESC;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, pid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;

    }

    public Product getProductDetails(int pdid) {

        String sql = "SELECT *\n"
                + "FROM Products p\n"
                + "JOIN ProductDetail pd ON p.pid = pd.pid\n"
                + "WHERE p.pid = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, pdid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13), rs.getInt(14), rs.getString(15), rs.getString(16), rs.getBoolean(17), rs.getString(18));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<BlogCategories> getAllBlogCategories() {
        List<BlogCategories> list = new ArrayList<>();
        String sql = "SELECT * FROM BlogCategories";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new BlogCategories(
                        rs.getInt("bc_id"),
                        rs.getString("bc_name")
                ));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Blog> getAllBlogPosts() {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT post_id, title, author, updated_date, content, bc.bc_name,\n"
                + "thumbnail, brief, acid, status_name\n"
                + "From BlogPosts bp join BlogCategories bc on bp.bc_id = bc.bc_id\n"
                + "join Status s on s.status_id = bp.status_id \n"
                + "WHERE bp.status_id = 1";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Blog(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getInt(9), rs.getString(10)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Blog> getLatestBlogs() {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT * FROM BlogPosts ORDER BY updated_date DESC";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Blog(
                        rs.getInt("post_id"),
                        rs.getString("title"),
                        rs.getString("author"),
                        rs.getString("updated_date"),
                        rs.getString("content"),
                        rs.getInt("bc_id"),
                        rs.getString("thumbnail"),
                        rs.getString("brief"),
                        rs.getInt("acid")
                ));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Blog> getOldestBlogs() {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT * FROM BlogPosts ORDER BY updated_date ASC";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Blog(
                        rs.getInt("post_id"),
                        rs.getString("title"),
                        rs.getString("author"),
                        rs.getString("updated_date"),
                        rs.getString("content"),
                        rs.getInt("bc_id"),
                        rs.getString("thumbnail"),
                        rs.getString("brief"),
                        rs.getInt("acid")
                ));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Blog> searchByTitleBlog(String title) {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT * FROM BlogPosts where title like ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + title + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Blog(
                        rs.getInt("post_id"),
                        rs.getString("title"),
                        rs.getString("author"),
                        rs.getString("updated_date"),
                        rs.getString("content"),
                        rs.getInt("bc_id"),
                        rs.getString("thumbnail"),
                        rs.getString("brief"),
                        rs.getInt("acid")
                ));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Blog> searchByBlogCategories(int bc_id) {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT * FROM BlogPosts where bc_id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, bc_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Blog(
                        rs.getInt("post_id"),
                        rs.getString("title"),
                        rs.getString("author"),
                        rs.getString("updated_date"),
                        rs.getString("content"),
                        rs.getInt("bc_id"),
                        rs.getString("thumbnail"),
                        rs.getString("brief"),
                        rs.getInt("acid")
                ));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public Blog getBlogDetail(int blog_id) {
        String sql = "SELECT * FROM BlogPosts where post_id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, blog_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return (new Blog(
                        rs.getInt("post_id"),
                        rs.getString("title"),
                        rs.getString("author"),
                        rs.getString("updated_date"),
                        rs.getString("content"),
                        rs.getInt("bc_id"),
                        rs.getString("thumbnail"),
                        rs.getString("brief"),
                        rs.getInt("acid")
                ));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Blog> get3LatestBlogs() {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT TOP 3 * FROM BlogPosts ORDER BY updated_date DESC;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Blog(
                        rs.getInt("post_id"),
                        rs.getString("title"),
                        rs.getString("author"),
                        rs.getString("updated_date"),
                        rs.getString("content"),
                        rs.getInt("bc_id"),
                        rs.getString("thumbnail"),
                        rs.getString("brief"),
                        rs.getInt("acid")
                ));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public Accounts getUserInfor(String email) {
        String sql = "select * from Accounts where email=?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getInt(7), rs.getString(6), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;

    }

    public void changeInfor(String name, String phone_number, String adress, String email) {
        String sql = "  update Accounts\n"
                + "  set username= ?,\n"
                + " phone_number = ?,\n"
                + " [address] = ?\n"
                + "\n"
                + "where email= ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, name);
            st.setString(2, phone_number);
            st.setString(3, adress);
            st.setString(4, email);
            st.executeUpdate();

        } catch (Exception e) {
        }
    }

    public List<Order> getMyOrder(int acid) {
        String sql = "SELECT \n"
                + "    o.oid,\n"
                + "    o.acid,\n"
                + "    o.ordered_at,\n"
                + "    o.TotalAmount,\n"
                + "    o.[address],\n"
                + "    o.phone_number,\n"
                + "    o.status,\n"
                + "    o.note,\n"
                + "    o.receiver,\n"
                + "    o.payment,\n"
                + "    od.odid,\n"
                + "	p.pid,\n"
                + "    p.pname ,\n"
                + "    od.price,\n"
                + "    od.quantity\n"
                + "FROM \n"
                + "    orders o\n"
                + "JOIN \n"
                + "    orderDetails od ON o.oid = od.oid\n"
                + "JOIN \n"
                + "    Products p ON od.pid = p.pid\n"
                + "	where acid = ?";
        List<Order> list = new ArrayList<>();
        try {

            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, acid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Order(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getInt(11), rs.getInt(12), rs.getString(13), rs.getFloat(14), rs.getInt(15)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

   

      public void changeOrderInfor(String phone_number, String address, String oid) {
        String sql = "   update orders set phone_number=?, address=? where oid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, phone_number);
            st.setString(2, address);
            st.setString(3, oid);
            st.executeUpdate();

        } catch (Exception e) {
        }
    }

    public List<String> getAuthors() {
        List<String> authors = new ArrayList<>();
        String sql = "SELECT DISTINCT author FROM BlogPosts";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                authors.add(rs.getString("author"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return authors;
    }

    ////////////////////////////////////////FEEDBACK
    public int addFeedback(int acid, int pid, double rating, String comment) {
        String query = "INSERT INTO Feedback (acid, pid, rating, comment, created_at) VALUES (?, ?, ?, ?, GETDATE())";

        int generatedId = -1; // Giá trị khởi tạo, chỉ ra thất bại

        try {
            PreparedStatement st = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            st.setInt(1, acid);
            st.setInt(2, pid);
            st.setDouble(3, rating);
            st.setString(4, comment);
            st.executeUpdate();

            ResultSet rs = st.getGeneratedKeys();
            if (rs.next()) {
                generatedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return generatedId;
    }

    public Accounts getAccountById(int acid) {
        Accounts account = null;
        String query = "SELECT * FROM Accounts WHERE acid = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setInt(1, acid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                int accountId = rs.getInt("acid");
                String email = rs.getString("email");
                String password = rs.getString("password");
                String username = rs.getString("username");
                int roleId = rs.getInt("role_id"); // Sửa tên cột từ "role_name" thành "role_id"
                String createdAt = rs.getString("created_at");
                int statusId = rs.getInt("status_id"); // Thêm lấy dữ liệu cho cột status_id
                String phoneNumber = rs.getString("phone_number");
                String gender = rs.getString("gender");
                String address = rs.getString("address");
                String avatar = rs.getString("avartar"); // Sửa tên cột từ "avartar" thành "avatar"

                // Tạo đối tượng Accounts
                account = new Accounts(accountId, email, password, username, roleId, createdAt, statusId, phoneNumber, gender, address, avatar);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return account;
    }

    public Product getProductByPid1(int pid) {

        String sql = "select * from Products where pid = " + pid;

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Feedback> getFeedbackOfProduct(int productId) {
        List<Feedback> feedbackList = new ArrayList<>();
        String query = "SELECT f.*, a.avartar, a.username, a.acid FROM Feedback f INNER JOIN Accounts a ON f.acid = a.acid WHERE f.pid = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setInt(1, productId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int feedbackId = rs.getInt("feedback_id");
                double rating = rs.getDouble("rating");
                Date createdAt = rs.getDate("created_at");
                String comment = rs.getString("comment");
                String avatar = rs.getString("avartar");
                String name = rs.getString("username");
                int userId = rs.getInt("acid");

                // Tạo đối tượng Account với avatar và username
                Accounts account = new Accounts();
                account.setAvatar(avatar);
                account.setName(name);
                account.setUser_id(userId);

                // Tạo đối tượng Feedback
                Feedback feedback = new Feedback(feedbackId, account, null, rating, createdAt, comment);
                feedbackList.add(feedback);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return feedbackList;
    }

    public double getAverageRatingOfProduct(int productId) {
        String query = "SELECT AVG(rating) AS averageRating FROM Feedback WHERE pid = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setInt(1, productId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getDouble("averageRating");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public boolean DidBuyTheBook(int acid, int pid) {
        String query = "select acid, pid from orders o, orderdetails d \n"
                + "where o.oid = d.oid and status = 4 and \n"
                + "acid = ? and pid = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setInt(1, acid);
            st.setInt(2, pid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public boolean checkIfUserHasCommented(int pid, int acid) {
        boolean hasCommented = false;
        String query = "SELECT COUNT(*) FROM Feedback WHERE pid = ? AND acid = ?";

        try ( PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, pid);
            statement.setInt(2, acid);

            try ( ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    int count = resultSet.getInt(1);
                    hasCommented = count > 0;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return hasCommented;
    }

    public void deleteFeedback(int acid, int pid) {
        String query = "delete from Feedback where acid = ? and pid = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setInt(1, acid);
            st.setInt(2, pid);
            ResultSet rs = st.executeQuery();
        } catch (Exception e) {
        }

    }

    public void cancelOrder(int oid) {
        String sql = "update orders set status=5 where oid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, oid);
            ResultSet rs = st.executeQuery();
        } catch (Exception e) {
        }
    }

    public void editFeedback(int acid, int pid, String newComment) {
        String query = "update Feedback set comment = ? where acid = ? and pid = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st = connection.prepareStatement(query);
            st.setString(1, newComment);
            st.setInt(2, acid);
            st.setInt(3, pid);
            ResultSet rs = st.executeQuery();
        } catch (Exception e) {
        }
    }

    public List<Product> getOrderedProductsByOrderID(String orderID) {
        List<Product> orderedProducts = new ArrayList<>();
        String query = "SELECT p.* FROM Products p "
                + "JOIN orderDetails od ON p.pid = od.pid "
                + "WHERE od.oid = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st = connection.prepareStatement(query);
            st.setString(1, orderID);
            try ( ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    Product product = new Product();
                    product.setPid(rs.getInt("pid"));
                    product.setPname(rs.getString("pname"));
                    // Set other attributes as needed
                    orderedProducts.add(product);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orderedProducts;
    }

    public List<Order> getOrdersByStatus(int statusId) {
        List<Order> orders = new ArrayList<>();
        String query = "SELECT * FROM orders WHERE status = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setInt(1, statusId);
            try ( ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    Order order = new Order();
                    // Lấy thông tin của đơn hàng từ ResultSet và thêm vào danh sách
                    orders.add(order);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }

    public boolean checkOrderCompleted(int orderId) {
        String query = "SELECT status FROM orders WHERE oid = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setInt(1, orderId);
            try ( ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    int status = rs.getInt("status");
                    return (status == 4);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false; // Trả về false nếu có lỗi xảy ra hoặc không tìm thấy đơn hàng
    }

    public int getOrderIdByProductId(int productId) {
        String query = "SELECT oid FROM orderDetails WHERE pid = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setInt(1, productId);
            try ( ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt("oid");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public String getEmailByOrderId(String orderId) {
        String email = null;
        String query = "SELECT email FROM Accounts WHERE acid IN (SELECT acid FROM Orders WHERE oid = ?)";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, orderId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                email = rs.getString("email");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return email;
    }

    public List<Order> getOrderById(int acid) {
        List<Order> list = new ArrayList<>();
        String sql = "WITH OrderedOrders AS (  SELECT o.oid, ordered_at, TotalAmount, status, pname,o.receiver, img,p.pid,\n"
                + "ROW_NUMBER() OVER (PARTITION BY o.oid ORDER BY o.ordered_at) AS row_num\n"
                + "FROM orders o\n"
                + "JOIN orderDetails od ON o.oid = od.oid\n"
                + "JOIN Products p ON od.pid = p.pid\n"
                + "where o.acid=?)\n"
                + "SELECT oid, ordered_at, TotalAmount, status, pname, receiver,img,pid\n"
                + "FROM OrderedOrders\n"
                + "WHERE row_num = 1\n"
                + "ORDER BY ordered_at DESC, status ASC;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, acid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int quantity = countProductByOid(rs.getInt(1));
                list.add(new Order(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getString(7), quantity, rs.getInt(8)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Order> getOInfor(int oid) {
        List<Order> list = new ArrayList<>();
        String sql = "select od.oid,od.pid,p.pname,p.img,od.price,od.quantity from orderDetails od\n"
                + "join Products p on od.pid=p.pid\n"
                + "where oid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, oid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Order(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getFloat(5), rs.getInt(6)));
            }
        } catch (Exception e) {
        }
        return list;
    }
     public Order getOrderInfor(int oid) {
        String sql = "SELECT     o.oid,    o.acid,   o.ordered_at,  o.TotalAmount,    o.[address],   o.phone_number,   o.status,   o.note,   o.receiver,   o.payment,    od.odid,\n"
                + "p.pid,\n"
                + "				p.pname,p.img ,   a.email , od.price,    od.quantity FROM    orders o JOIN   orderDetails od ON o.oid = od.oid JOIN     Products p ON od.pid = p.pid \n"
                + "				JOIN Accounts a ON o.acid=a.acid\n"
                + "				where o.oid=? ";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, oid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Order(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getInt(11), rs.getInt(12), rs.getString(13), rs.getString(14), rs.getString(15), rs.getFloat(16), rs.getInt(17));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
    public int countProductByOid(int oid) {
        String sql = "select count(*) as quantity from orderDetails where oid=?";
        int quantity = 0;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, oid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                quantity = rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return quantity;
    }

    public String getUserAvartaById(int acid) {
        String sql = "  select avartar from Accounts where acid = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, acid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (Exception e) {
        }
        return null;
    }

     public void changavatar(String avatar, String email) {
        String sql = "UPDATE Accounts SET [avartar] = ? WHERE email = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, avatar);
            st.setString(2, email);
            st.executeUpdate();

        } catch (Exception e) {
        }
    }
    public static void main(String[] args) {
        DAO dao = new DAO();
//        List<OrderDetail> listNA = dao.getOInfor("12");
//        for (OrderDetail product : listNA) {
//            System.out.println(product);
//        }
        Order o = dao.getOrderInfor(14);
        System.out.println(o);

    }
}
