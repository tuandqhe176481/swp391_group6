package dal;

import context.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.AccountStatus;
import model.Accounts;
import model.Role;
import model.Setting;
import model.SettingCategories;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author pc
 */
public class AdminDAO extends DBContext {

    public List<Accounts> getAllAccount(int index) {
        List<Accounts> list = new ArrayList<>();
        String sql = "  SELECT [acid]\n"
                + "      ,[email]\n"
                + "      ,[username]\n"
                + "      ,[role_name]\n"
                + "      ,[created_at]\n"
                + "      ,[status_name]\n"
                + "      ,[phone_number]\n"
                + "      ,[gender]\n"
                + "      ,[address]\n"
                + "	  from Accounts a\n"
                + "	  join AccountStatuses acs on a.status_id = acs.status_id\n"
                + "	  join Roles r on a.role_id = r.role_id\n"
                + "	  ORDER BY created_at DESC\n"
                + "	 OFFSET ? rows FETCH next 12 rows only;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, (index - 1) * 12);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9)));
            }

        } catch (Exception e) {
        }
        return list;
    }

    public int countAccount() {
        String sql = "select count(*) from Accounts";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<AccountStatus> getAllStatus() {
        List<AccountStatus> list = new ArrayList<>();
        String sql = "select * from AccountStatuses";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new AccountStatus(rs.getInt(1), rs.getString(2)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Role> getAllRole() {
        List<Role> list = new ArrayList<>();
        String sql = "select * from Roles";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Role(rs.getInt(1), rs.getString(2)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public int countFelmale() {
        String sql = "select count(*) from Accounts where gender = 'Female'";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Accounts> getAllAccountFelmale(int index) {
        List<Accounts> list = new ArrayList<>();
        String sql = "  SELECT [acid]\n"
                + ",[email]\n"
                + ",[username]\n"
                + ",[role_name]\n"
                + ",[created_at]\n"
                + ",[status_name]\n"
                + ",[phone_number]\n"
                + ",[gender]\n"
                + ",[address]\n"
                + "from Accounts a\n"
                + "join AccountStatuses acs on a.status_id = acs.status_id\n"
                + "join Roles r on a.role_id = r.role_id\n"
                + "where gender = 'Female'\n"
                + "ORDER BY created_at DESC\n"
                + "OFFSET ? rows FETCH next 12 rows only;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, (index - 1) * 12);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9)));
            }

        } catch (Exception e) {
        }
        return list;
    }

    public int countMale() {
        String sql = "select count(*) from Accounts where gender = 'Male'";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public int countCustomer(int role_id) {
        String sql = "select count(*) from Accounts where role_id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, role_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Accounts> getAccountByRole(int role, int index) {
        List<Accounts> list = new ArrayList<>();
        String sql = "SELECT [acid]\n"
                + ",[email]\n"
                + ",[username]\n"
                + ",[role_name]\n"
                + ",[created_at]\n"
                + ",[status_name]\n"
                + ",[phone_number]\n"
                + ",[gender]\n"
                + ",[address]\n"
                + "from Accounts a\n"
                + "join AccountStatuses acs on a.status_id = acs.status_id\n"
                + "join Roles r on a.role_id = r.role_id\n"
                + "where a.role_id = ?\n"
                + "ORDER BY created_at DESC\n"
                + "OFFSET ? rows FETCH next 12 rows only;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, role);
            ps.setInt(2, (index - 1) * 12);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9)));
            }

        } catch (Exception e) {
        }
        return list;
    }

    public List<Accounts> getAllAccountMale(int index) {
        List<Accounts> list = new ArrayList<>();
        String sql = "  SELECT [acid]\n"
                + ",[email]\n"
                + ",[username]\n"
                + ",[role_name]\n"
                + ",[created_at]\n"
                + ",[status_name]\n"
                + ",[phone_number]\n"
                + ",[gender]\n"
                + ",[address]\n"
                + "from Accounts a\n"
                + "join AccountStatuses acs on a.status_id = acs.status_id\n"
                + "join Roles r on a.role_id = r.role_id\n"
                + "where gender = 'Male'\n"
                + "ORDER BY created_at DESC\n"
                + "OFFSET ? rows FETCH next 12 rows only;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, (index - 1) * 12);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9)));
            }

        } catch (Exception e) {
        }
        return list;
    }

    public int countActiveAccount() {
        String sql = "select count(*) from Accounts where status_id = 1";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public int countInActiveAccount() {
        String sql = "select count(*) from Accounts where status_id = 2";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Accounts> getAccountsActive(int index) {
        List<Accounts> list = new ArrayList<>();
        String sql = "  SELECT [acid]\n"
                + ",[email]\n"
                + ",[username]\n"
                + ",[role_name]\n"
                + ",[created_at]\n"
                + ",[status_name]\n"
                + ",[phone_number]\n"
                + ",[gender]\n"
                + ",[address]\n"
                + "from Accounts a\n"
                + "join AccountStatuses acs on a.status_id = acs.status_id\n"
                + "join Roles r on a.role_id = r.role_id\n"
                + "where a.status_id = 1\n"
                + "ORDER BY created_at DESC\n"
                + "OFFSET ? rows FETCH next 12 rows only;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, (index - 1) * 12);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9)));
            }

        } catch (Exception e) {
        }
        return list;
    }

    public List<Accounts> getAccountsInActive(int index) {
        List<Accounts> list = new ArrayList<>();
        String sql = "  SELECT [acid]\n"
                + ",[email]\n"
                + ",[username]\n"
                + ",[role_name]\n"
                + ",[created_at]\n"
                + ",[status_name]\n"
                + ",[phone_number]\n"
                + ",[gender]\n"
                + ",[address]\n"
                + "from Accounts a\n"
                + "join AccountStatuses acs on a.status_id = acs.status_id\n"
                + "join Roles r on a.role_id = r.role_id\n"
                + "where a.status_id = 2\n"
                + "ORDER BY created_at DESC\n"
                + "OFFSET ? rows FETCH next 12 rows only;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, (index - 1) * 12);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9)));
            }

        } catch (Exception e) {
        }
        return list;
    }

    public int countAccountByGenderAndRole(String gender, int role) {
        String sql = "select count(*) from Accounts where gender = ? and role_id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, gender);
            ps.setInt(2, role);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Accounts> getAccountByGenderAndRole(String gender, int role, int index) {
        List<Accounts> list = new ArrayList<>();
        String sql = "  SELECT [acid]\n"
                + ",[email]\n"
                + ",[username]\n"
                + ",[role_name]\n"
                + ",[created_at]\n"
                + ",[status_name]\n"
                + ",[phone_number]\n"
                + ",[gender]\n"
                + ",[address]\n"
                + "from Accounts a\n"
                + "join AccountStatuses acs on a.status_id = acs.status_id\n"
                + "join Roles r on a.role_id = r.role_id\n"
                + "where a.gender = ? and a.role_id = ?\n"
                + "ORDER BY created_at DESC\n"
                + "OFFSET ? rows FETCH next 12 rows only;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, gender);
            ps.setInt(2, role);
            ps.setInt(3, (index - 1) * 12);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public int countAccountByGenderAndStatus(String gender, int status) {
        String sql = "select count(*) from Accounts where gender = ? and status_id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, gender);
            ps.setInt(2, status);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Accounts> getAccountByGenderAndStatus(String gender, int status, int index) {
        List<Accounts> list = new ArrayList<>();
        String sql = "  SELECT [acid]\n"
                + ",[email]\n"
                + ",[username]\n"
                + ",[role_name]\n"
                + ",[created_at]\n"
                + ",[status_name]\n"
                + ",[phone_number]\n"
                + ",[gender]\n"
                + ",[address]\n"
                + "from Accounts a\n"
                + "join AccountStatuses acs on a.status_id = acs.status_id\n"
                + "join Roles r on a.role_id = r.role_id\n"
                + "where gender = ? and a.status_id = ?\n"
                + "ORDER BY created_at DESC\n"
                + "OFFSET ? rows FETCH next 12 rows only;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, gender);
            ps.setInt(2, status);
            ps.setInt(3, (index - 1) * 12);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public int countAccountByRoleAndStatus(int role, int status) {
        String sql = "select count(*) from Accounts where role_id = ? and status_id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, role);
            ps.setInt(2, status);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Accounts> getAccountByRoleAndStatus(int role, int status, int index) {
        List<Accounts> list = new ArrayList<>();
        String sql = "  SELECT [acid]\n"
                + ",[email]\n"
                + ",[username]\n"
                + ",[role_name]\n"
                + ",[created_at]\n"
                + ",[status_name]\n"
                + ",[phone_number]\n"
                + ",[gender]\n"
                + ",[address]\n"
                + "from Accounts a\n"
                + "join AccountStatuses acs on a.status_id = acs.status_id\n"
                + "join Roles r on a.role_id = r.role_id\n"
                + "where a.role_id = ? and a.status_id = ?\n"
                + "ORDER BY created_at DESC\n"
                + "OFFSET ? rows FETCH next 12 rows only;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, role);
            ps.setInt(2, status);
            ps.setInt(3, (index - 1) * 12);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public int countAccountByGenderAndRoleAndStatus(String gender, int role, int status) {
        String sql = "select count(*) from Accounts where gender = ? and role_id = ? and status_id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, gender);
            ps.setInt(2, role);
            ps.setInt(3, status);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Accounts> getAccountByGenderAndRoleAndStatus(String gender, int role, int status, int index) {
        List<Accounts> list = new ArrayList<>();
        String sql = "   SELECT [acid]\n"
                + ",[email]\n"
                + ",[username]\n"
                + ",[role_name]\n"
                + ",[created_at]\n"
                + ",[status_name]\n"
                + ",[phone_number]\n"
                + ",[gender]\n"
                + ",[address]\n"
                + "from Accounts a\n"
                + "join AccountStatuses acs on a.status_id = acs.status_id\n"
                + "join Roles r on a.role_id = r.role_id\n"
                + "where a.gender = ? and a.role_id = ? and a.status_id = ?\n"
                + "ORDER BY created_at DESC\n"
                + "OFFSET ? rows FETCH next 12 rows only;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, gender);
            ps.setInt(2, role);
            ps.setInt(3, status);
            ps.setInt(4, (index - 1) * 12);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public int countSearchUser(String search) {
        String sql = "select count(*) from Accounts\n"
                + "WHERE [username] LIKE ?\n"
                + "OR [phone_number] LIKE ?\n"
                + "OR [email] LIKE ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + search + "%");
            ps.setString(2, "%" + search + "%");
            ps.setString(3, "%" + search + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Accounts> searchUser(String search, int index) {
        List<Accounts> list = new ArrayList<>();
        String sql = "  SELECT [acid]\n"
                + ",[email]\n"
                + ",[username]\n"
                + ",[role_name]\n"
                + ",[created_at]\n"
                + ",[status_name]\n"
                + ",[phone_number]\n"
                + ",[gender]\n"
                + ",[address]\n"
                + "from Accounts a\n"
                + "join AccountStatuses acs on a.status_id = acs.status_id\n"
                + "join Roles r on a.role_id = r.role_id\n"
                + "WHERE [username] LIKE ?\n"
                + "   OR [phone_number] LIKE ?\n"
                + "   OR [email] LIKE ?\n"
                + "   ORDER BY created_at DESC\n"
                + "OFFSET ? rows FETCH next 12 rows only;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + search + "%");
            ps.setString(2, "%" + search + "%");
            ps.setString(3, "%" + search + "%");
            ps.setInt(4, (index - 1) * 12);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public Accounts getAccountById(int id) {
        String sql = "SELECT [acid]\n"
                + ",[email]\n"
                + ",[username]\n"
                + ",[role_name]\n"
                + ",[created_at]\n"
                + ",[status_name]\n"
                + ",[phone_number]\n"
                + ",[gender]\n"
                + ",[address]\n"
                + ",[avartar]\n"
                + "from Accounts a\n"
                + "join AccountStatuses acs on a.status_id = acs.status_id\n"
                + "join Roles r on a.role_id = r.role_id\n"
                + "where acid = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void editUserById(String name, String email, String pass, String phone, String address, String gender, int role, int status, String avartar, String created_at, int id) {
        String sql = "   Update Accounts\n"
                + "  set\n"
                + "		[email] = ?\n"
                + "      ,[password] =?\n"
                + "      ,[username]=?\n"
                + "      ,[role_id]=?\n"
                + "      ,[created_at]=?\n"
                + "      ,[status_id]=?\n"
                + "      ,[phone_number]=?\n"
                + "      ,[gender]=?\n"
                + "      ,[address]=?\n"
                + "      ,[avartar]=?\n"
                + "	  from Accounts\n"
                + "	  where acid=?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, email);
            ps.setString(2, pass);
            ps.setString(3, name);
            ps.setInt(4, role);
            ps.setString(5, created_at);
            ps.setInt(6, status);
            ps.setString(7, phone);
            ps.setString(8, gender);
            ps.setString(9, address);
            ps.setString(10, avartar);
            ps.setInt(11, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public String getProductImage1ById(int id) {
        String sql = " 	 select [avartar] from Accounts\n"
                + " where acid = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void addUser(String email, String pass, String name, int role, String created_at, int status, String phone, String gender, String address, String avartar) {
        String sql = "insert into Accounts\n"
                + "values(?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, email);
            ps.setString(2, pass);
            ps.setString(3, name);
            ps.setInt(4, role);
            ps.setString(5, created_at);
            ps.setInt(6, status);
            ps.setString(7, phone);
            ps.setString(8, gender);
            ps.setString(9, address);
            ps.setString(10, avartar);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public int countOrderStatus(int status, String startDate, String endDate) {
        String sql = "SELECT \n"
                + "COUNT(*) \n"
                + "FROM\n"
                + " [SWP391_Group6].[dbo].[orders]\n"
                + "WHERE\n"
                + "[status] = ? and ordered_at between ? and ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, status);
            ps.setString(2, startDate);
            ps.setString(3, endDate);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public Accounts countCusOrder(String startDate, String endDate) {
        String sql = " SELECT top 1 \n"
                + "    a.[acid],\n"
                + "    a.[email],\n"
                + "    a.[password],\n"
                + "    a.[username],\n"
                + "    a.[role_id],\n"
                + "    a.[created_at],\n"
                + "    a.[status_id],\n"
                + "    a.[phone_number],\n"
                + "    a.[gender],\n"
                + "    a.[address],\n"
                + "    a.[avartar]\n"
                + "FROM \n"
                + "    [SWP391_Group6].[dbo].[Accounts] a\n"
                + "WHERE\n"
                + "    a.[acid] IN (\n"
                + "        SELECT DISTINCT [acid] \n"
                + "        FROM [SWP391_Group6].[dbo].[orders]\n"
                + "        WHERE [ordered_at] BETWEEN ? AND ?\n"
                + "    )\n"
                + "ORDER BY \n"
                + "    a.[created_at] DESC;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, startDate);
            ps.setString(2, endDate);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getString(6), rs.getInt(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public Accounts countCusNewCreate(String startDate, String endDate) {
        String sql = " SELECT top 1\n"
                + "    [acid],\n"
                + "    [email],\n"
                + "    [password],\n"
                + "    [username],\n"
                + "    [role_id],\n"
                + "    [created_at],\n"
                + "    [status_id],\n"
                + "    [phone_number],\n"
                + "    [gender],\n"
                + "    [address],\n"
                + "    [avartar]\n"
                + "FROM \n"
                + "    [SWP391_Group6].[dbo].[Accounts]\n"
                + "WHERE\n"
                + "    [created_at] BETWEEN ? AND ?\n"
                + "ORDER BY \n"
                + "    [created_at] DESC;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, startDate);
            ps.setString(2, endDate);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getString(6), rs.getInt(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public int countTotalAmount(String startDate, String endDate) {
        String sql = "SELECT SUM(TotalAmount)\n"
                + "FROM [SWP391_Group6].[dbo].[orders]\n"
                + "where ordered_at between ? and ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, startDate);
            ps.setString(2, endDate);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Setting> getAllSetting(int index) {
        List<Setting> list = new ArrayList<>();
        String sql = "SELECT s.[id], sc.[setting_name], s.[value], s.[order], s.[status]\n"
                + "FROM [Settings] s \n"
                + "JOIN SettingsCategories sc ON s.[type] = sc.setting_type\n"
                + "ORDER BY s.[id] \n"
                + "OFFSET ? ROWS FETCH NEXT 6 ROWS ONLY;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, (index - 1) * 6);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Setting(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getString(5)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<SettingCategories> getAllSettingCate() {
        List<SettingCategories> list = new ArrayList<>();
        String sql = "select * from SettingsCategories";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new SettingCategories(rs.getInt(1), rs.getString(2)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public int countSetting() {
        String sql = "select count(*) from Settings";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public int countSettingCate(int type) {
        String sql = "select count(*) from Settings where type = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, type);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Setting> getAllSettingByCate(int index, int type) {
        List<Setting> list = new ArrayList<>();
        String sql = "SELECT s.[id], sc.[setting_name], s.[value], s.[order], s.[status]\n"
                + "FROM [Settings] s \n"
                + "JOIN SettingsCategories sc ON s.[type] = sc.setting_type\n"
                + "where type = ?\n"
                + "ORDER BY s.[id] \n"
                + "OFFSET ? ROWS FETCH NEXT 6 ROWS ONLY;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(2, (index - 1) * 6);
            ps.setInt(1, type);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Setting(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getString(5)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Setting> getAllSettingByStatus(int index, String status) {
        List<Setting> list = new ArrayList<>();
        String sql = "SELECT s.[id], sc.[setting_name], s.[value], s.[order], s.[status]\n"
                + "FROM [Settings] s \n"
                + "JOIN SettingsCategories sc ON s.[type] = sc.setting_type\n"
                + "where status = ?\n"
                + "ORDER BY s.[id] \n"
                + "OFFSET ? ROWS FETCH NEXT 6 ROWS ONLY;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(2, (index - 1) * 6);
            ps.setString(1, status);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Setting(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getString(5)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public int countSettingStatus(String status) {
        String sql = "SELECT COUNT(*) FROM Settings WHERE status = ?;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, status);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public int countSettingByCateAndStatus(int type, String status) {
        String sql = "select count(*) from Settings where type = ? and  status = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, type);
            ps.setString(2, status);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Setting> getAllSettingByCateAndStatus(int index, int type, String status) {
        List<Setting> list = new ArrayList<>();
        String sql = "SELECT s.[id], sc.[setting_name], s.[value], s.[order], s.[status]\n"
                + "FROM [Settings] s \n"
                + "JOIN SettingsCategories sc ON s.[type] = sc.setting_type\n"
                + "where type = ? and  status = ?\n"
                + "ORDER BY s.[id] \n"
                + "OFFSET ? ROWS FETCH NEXT 6 ROWS ONLY;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(3, (index - 1) * 6);
            ps.setInt(1, type);
            ps.setString(2, status);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Setting(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getString(5)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public Accounts getAccounts(int id) {
        String sql = "select * from Accounts where acid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getInt(7), rs.getString(6), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11));
            }

        } catch (Exception e) {
        }
        return null;
    }

    public List<Setting> searchByValue(String search, int index) {
        List<Setting> list = new ArrayList<>();
        String sql = "SELECT s.[id], sc.[setting_name], s.[value], s.[order], s.[status]\n"
                + "FROM [Settings] s \n"
                + "JOIN SettingsCategories sc ON s.[type] = sc.setting_type\n"
                + "where [value] like ?\n"
                + "ORDER BY s.[id]\n"
                + "OFFSET ? ROWS FETCH NEXT 6 ROWS ONLY;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + search + "%");
            ps.setInt(2, (index - 1) * 6);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Setting(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getString(5)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public int countSettingSearch(String search) {
        String sql = "select count(*) from Settings where [value] like ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + search + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public Setting getSettingById(int id) {
        String sql = "SELECT s.[id],s.type ,sc.[setting_name], s.[value], s.[order], s.[status]\n"
                + "FROM [Settings] s \n"
                + "JOIN SettingsCategories sc ON s.[type] = sc.setting_type\n"
                + "where s.[id] = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Setting(rs.getInt(1),rs.getInt(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getString(6));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void addRole(String role, String status) {
            String sql = "insert into Roles\n"
                    + "values(?,?);";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, role);
            ps.setString(2, status);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void addSetting(int type, String value, int order, String status) {
        String sql = "insert into Settings\n"
                + "values(?,?,?,?);";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, type);
            ps.setString(2, value);
            ps.setInt(3, order);
            ps.setString(4, status);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void addBrand(String brand, String status) {
        String sql = "insert into Brand\n"
                + "values(?,?);";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, brand);
            ps.setString(2, status);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void addCate(String cate, String status) {
        String sql = "insert into Categories\n"
                + "values(?,?);";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, cate);
            ps.setString(2, status);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void addBlogCate(String bcate, String status) {
        String sql = "insert into BlogCategories\n"
                + "values(?,?);";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, bcate);
            ps.setString(2, status);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void updateSetting(int type, String value, int order, String status, int id) {
        String sql = "  update Settings\n"
                + "  set type = ?,\n"
                + "  value = ?,\n"
                + "  [order] = ?,\n"
                + "  status = ?\n"
                + "  where id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, type);
            ps.setString(2, value);
            ps.setInt(3, order);
            ps.setString(4, status);
            ps.setInt(5, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public static void main(String[] args) {

        AdminDAO dao = new AdminDAO();

        int count = dao.countTotalAmount("2024-02-13", "2024-03-13");
        System.out.println(count);

    }
}
