/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import jdk.management.resource.internal.TotalResourceContext;
import model.Accounts;
import model.Banner;
import model.Blog;
import model.Brand;
import model.BrandTotal;
import model.FeedbackList;
import model.Order;
import model.OrderDetail;
import model.Product;
import model.ProductCategories;
import model.Status;

/**
 *
 * @author pc
 */
public class MaketingDao extends context.DBContext {

    public int getCustomer() {
        String sql = " select count(*) from accounts where role_id = 5";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {

        }
        return 0;
    }

    public List<Accounts> getAllCustomer() {
        List<Accounts> list = new ArrayList<>();
        String sql = "  select * from accounts where role_id = 5	";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getString(6), rs.getInt(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Accounts> searchCustomer(String search) {
        List<Accounts> list = new ArrayList<>();
        String sql = "select * from accounts where role_id = 5 and username like ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + search + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getString(6), rs.getInt(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public int getNewCustomer(String startDate, String endDate) {
        String sql = "SELECT COUNT(*) FROM Accounts WHERE created_at BETWEEN ? AND ? AND role_id = 5";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, startDate);
            ps.setString(2, endDate);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {

        }
        return 0;
    }

    public int getTotalPost(String startDate, String endDate) {
        String sql = "select count(*) from BlogPosts where updated_date  BETWEEN ? AND ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, startDate);
            ps.setString(2, endDate);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {

        }
        return 0;
    }

    public int getAllPost() {
        String sql = "select count(*) from BlogPosts";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {

        }
        return 0;
    }

    public List<ProductCategories> getAllCategories() {
        List<ProductCategories> list = new ArrayList<>();
        String sql = "select * from categories";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new ProductCategories(rs.getInt(1), rs.getString(2)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Status> getAllStatus() {
        List<Status> list = new ArrayList<>();
        String sql = "select * from Status";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Status(rs.getInt(1), rs.getString(2)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Product> getProductByStatus(int status_id) {
        List<Product> list = new ArrayList<>();
        String sql = "select pid\n"
                + "                      ,cid\n"
                + "                      ,pname\n"
                + "                      ,rate\n"
                + "                     ,img\n"
                + "                      ,img2\n"
                + "                    ,price\n"
                + "                      ,brandid\n"
                + "                      ,priceSale\n"
                + "                     ,quantity\n"
                + "                      ,isDiscount\n"
                + "                     ,isSoldout\n"
                + "                    ,created_at\n"
                + "                     ,s.status_name\n"
                + "                 from Products p \n"
                + "                Inner join Status s on p.status_id = s.status_id\n"
                + "				where p.status_id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, status_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13), rs.getString(14)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getProductByCid(int cid) {
        List<Product> list = new ArrayList<>();
        String sql = "select pid\n"
                + "                      ,cid\n"
                + "                      ,pname\n"
                + "                      ,rate\n"
                + "                     ,img\n"
                + "                      ,img2\n"
                + "                    ,price\n"
                + "                      ,brandid\n"
                + "                      ,priceSale\n"
                + "                     ,quantity\n"
                + "                      ,isDiscount\n"
                + "                     ,isSoldout\n"
                + "                    ,created_at\n"
                + "                     ,s.status_name\n"
                + "                 from Products p \n"
                + "                Inner join Status s on p.status_id = s.status_id\n"
                + "				where cid = ?";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, cid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13), rs.getString(14)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getProductByCidAndSid(int cid, int sid) {
        List<Product> list = new ArrayList<>();
        String sql = "				\n"
                + "select pid\n"
                + "                      ,cid\n"
                + "                      ,pname\n"
                + "                      ,rate\n"
                + "                     ,img\n"
                + "                      ,img2\n"
                + "                    ,price\n"
                + "                      ,brandid\n"
                + "                      ,priceSale\n"
                + "                     ,quantity\n"
                + "                      ,isDiscount\n"
                + "                     ,isSoldout\n"
                + "                    ,created_at\n"
                + "                     ,s.status_name\n"
                + "                 from Products p \n"
                + "                Inner join Status s on p.status_id = s.status_id\n"
                + "				where  p.cid = ?  and p.status_id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, cid);
            ps.setInt(2, sid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13), rs.getString(14)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public int countProductAvai(int status) {
        String sql = "select count(*) from products where status_id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, status);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {

        }
        return 0;
    }

    public List<Product> pagingProductAvai(int sid, int index) {
        List<Product> list = new ArrayList<>();
        String sql = "select pid\n"
                + "                      ,cid\n"
                + "                      ,pname\n"
                + "                      ,rate\n"
                + "                     ,img\n"
                + "                      ,img2\n"
                + "                    ,price\n"
                + "                      ,brandid\n"
                + "                      ,priceSale\n"
                + "                     ,quantity\n"
                + "                      ,isDiscount\n"
                + "                     ,isSoldout\n"
                + "                    ,created_at\n"
                + "                     ,s.status_name\n"
                + "                 from Products p \n"
                + "                Inner join Status s on p.status_id = s.status_id\n"
                + "				where p.status_id = ?\n"
                + "                ORDER BY pid Asc 				\n"
                + "                OFFSET ? rows FETCH next 12 rows only";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, sid);
            ps.setInt(2, (index - 1) * 12);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13), rs.getString(14)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Product> searchByName(String search) {
        List<Product> list = new ArrayList<>();
        String sql = "select pid\n"
                + "                      ,cid\n"
                + "                      ,pname\n"
                + "                      ,rate\n"
                + "                     ,img\n"
                + "                      ,img2\n"
                + "                    ,price\n"
                + "                      ,brandid\n"
                + "                      ,priceSale\n"
                + "                     ,quantity\n"
                + "                      ,isDiscount\n"
                + "                     ,isSoldout\n"
                + "                    ,created_at\n"
                + "                     ,s.status_name\n"
                + "                 from Products p \n"
                + "                Inner join Status s on p.status_id = s.status_id\n"
                + "			where pname like ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + search + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13), rs.getString(14)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public int addProduct(int cid, String name, float rate, String img1, String img2, int price, int brand, int priceSale, int quantity, Boolean isDiscount, Boolean isSoldout, String created_at, int status_id) {
        String sql = "INSERT INTO Products\n"
                + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?);";

        int generatedId = -1; // Initial value, indicating failure

        try {
            PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, cid);
            ps.setString(2, name);
            ps.setFloat(3, rate);
            ps.setString(4, img1);
            ps.setString(5, img2);
            ps.setInt(6, price);
            ps.setInt(7, brand);
            ps.setInt(8, priceSale);
            ps.setInt(9, quantity);
            ps.setBoolean(10, isDiscount);
            ps.setBoolean(11, isSoldout);
            ps.setString(12, created_at);
            ps.setInt(13, status_id);
            ps.executeUpdate();

            // Retrieve the generated keys
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                generatedId = rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace(); // Handle the exception appropriately
        }

        return generatedId;
    }

    public void addProductDetail(String size, String color, Boolean gender, String description, int pid) {
        String sql = "insert into ProductDetail\n"
                + "values(?,?,?,?,?);";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, size);
            ps.setString(2, color);
            ps.setBoolean(3, gender);
            ps.setString(4, description);
            ps.setInt(5, pid);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public Product getProductDetails(int pdid) {

        String sql = "SELECT p.pid\n"
                + "      ,[cid]\n"
                + "      ,[pname]\n"
                + "      ,[rate]\n"
                + "      ,[img]\n"
                + "      ,[img2]\n"
                + "      ,[price]\n"
                + "      ,[brandid]\n"
                + "      ,[priceSale]\n"
                + "      ,[quantity]\n"
                + "      ,[isDiscount]\n"
                + "      ,[isSoldout]\n"
                + "      ,[created_at]\n"
                + "      ,[status_id]\n"
                + "	  ,[size]\n"
                + "      ,[color]\n"
                + "      ,[gender]\n"
                + "      ,[description]\n"
                + "FROM Products p\n"
                + "JOIN ProductDetail pd ON p.pid = pd.pid\n"
                + "WHERE p.pid = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, pdid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13), rs.getInt(14), rs.getString(15), rs.getString(16), rs.getBoolean(17), rs.getString(18));

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void updateProduct(int cid, String name, float rate, String img1, String img2, int price, int brand, int priceSale, int quantity, Boolean isDiscount, Boolean isSoldout, String created_at, int status_id, int pid) {
        String sql = "Update Products\n"
                + "set"
                + "      [cid]=?\n"
                + "      ,[pname]=?\n"
                + "      ,[rate]=?\n"
                + "      ,[img]=?\n"
                + "      ,[img2]=?\n"
                + "      ,[price]=?\n"
                + "      ,[brandid]=?\n"
                + "      ,[priceSale]=?\n"
                + "      ,[quantity]=?\n"
                + "      ,[isDiscount]=?\n"
                + "      ,[isSoldout]=?\n"
                + "      ,[created_at]=?\n"
                + "      ,[status_id]=?\n"
                + "	  where pid = ?";
        try {
            try {
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setInt(1, cid);
                ps.setString(2, name);
                ps.setFloat(3, rate);
                ps.setString(4, img1);
                ps.setString(5, img2);
                ps.setInt(6, price);
                ps.setInt(7, brand);
                ps.setInt(8, priceSale);
                ps.setInt(9, quantity);
                ps.setBoolean(10, isDiscount);
                ps.setBoolean(11, isSoldout);
                ps.setString(12, created_at);
                ps.setInt(13, status_id);
                ps.setInt(14, pid);
                ps.executeUpdate();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }
    }

    public void updateProductDetail(String size, String color, Boolean gender, String description, int pid) {
        String sql = "Update ProductDetail\n"
                + "  set size = ?,\n"
                + "  color = ?,\n"
                + "  gender = ?,\n"
                + "  [description] = ?\n"
                + "  where pid = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, size);
            ps.setString(2, color);
            ps.setBoolean(3, gender);
            ps.setString(4, description);
            ps.setInt(5, pid);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public String getProductImage1ById(int pid) {
        String sql = "  select [img] from Products\n"
                + "  where pid = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, pid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (Exception e) {
        }
        return null;
    }

    public String getProductImage2ById(int pid) {
        String sql = "  select [img2] from Products\n"
                + "  where pid = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, pid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void deleteProduct(int pid) {
        String sql1 = "delete from ProductDetail where pid = ? ";
        
        String sql = "delete from Products\n"
                + "where pid =  ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql1);
            ps.setInt(1, pid);
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, pid);
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }

    public int countRevenue() {
        String sql = "  SELECT\n"
                + "    SUM(TotalAmount) AS GrandTotalAmount\n"
                + "FROM\n"
                + "    [SWP391_Group6].[dbo].[orders];";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {

        }
        return 0;
    }

    public int countTotalOrder() {
        String sql = "SELECT\n"
                + "COUNT(*) AS TotalOrders\n"
                + "FROM\n"
                + "    [SWP391_Group6].[dbo].[orders];";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {

        }
        return 0;
    }

    public int countRevenue7Day(String startDate, String endDate) {
        String sql = "  SELECT\n"
                + "    SUM(TotalAmount) AS GrandTotalAmount\n"
                + "FROM\n"
                + "   orders\n"
                + "	where ordered_at between ? and ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, startDate);
            ps.setString(2, endDate);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {

        }
        return 0;
    }

    public List<Order> getRevenue(String year) {
        List<Order> list = new ArrayList<>();
        String sql = "SELECT\n"
                + "    MONTH([ordered_at]) AS OrderMonth,\n"
                + "    SUM(TotalAmount) AS MonthlyRevenue\n"
                + "FROM\n"
                + "    [SWP391_Group6].[dbo].[orders]\n"
                + "WHERE\n"
                + "    YEAR([ordered_at]) = ?\n"
                + "GROUP BY\n"
                + "    MONTH([ordered_at])\n"
                + "ORDER BY\n"
                + "    OrderMonth;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, year);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Order(rs.getString(1), rs.getInt(2)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public int getEarningDay(String day) {
        String sql = "select SUM([TotalAmount]) AS DailyRevenue\n"
                + "FROM\n"
                + "[SWP391_Group6].[dbo].[orders]\n"
                + "where CONVERT(date, ordered_at)= ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, day);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {

        }
        return 0;
    }

    public Order getHighestMonth() {
        String sql = "SELECT top 1\n"
                + "  MONTH([ordered_at]) AS OrderMonth,\n"
                + "  SUM(TotalAmount) AS MonthlyRevenue\n"
                + "FROM\n"
                + "    [SWP391_Group6].[dbo].[orders]\n"
                + "GROUP BY\n"
                + "    YEAR([ordered_at]), MONTH([ordered_at])\n"
                + "ORDER BY\n"
                + "MonthlyRevenue desc";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Order(rs.getString(1), rs.getInt(2));
            }
        } catch (Exception e) {

        }
        return null;
    }

    public Order getLowestMonth() {
        String sql = "SELECT top 1\n"
                + "  MONTH([ordered_at]) AS OrderMonth,\n"
                + "  SUM(TotalAmount) AS MonthlyRevenue\n"
                + "FROM\n"
                + "    [SWP391_Group6].[dbo].[orders]\n"
                + "GROUP BY\n"
                + "    YEAR([ordered_at]), MONTH([ordered_at])\n"
                + "ORDER BY\n"
                + "MonthlyRevenue asc";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Order(rs.getString(1), rs.getInt(2));
            }
        } catch (Exception e) {

        }
        return null;
    }

    public List<BrandTotal> getTotalByBrand() {
        List<BrandTotal> list = new ArrayList<>();
        String sql = " SELECT b.brandname, Sum(o.totalAmount) AS total\n"
                + "FROM orderDetails od\n"
                + "JOIN Products p ON od.pid = p.pid\n"
                + "join Brand b on p.brandid = b.brandid\n"
                + "join orders o on od.oid = o.oid\n"
                + "GROUP BY b.brandname;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new BrandTotal(rs.getString(1), rs.getInt(2)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<OrderDetail> getProductBestSeller() {
        List<OrderDetail> list = new ArrayList<>();
        String sql = " SELECT TOP (4) \n"
                + "    od.pid,\n"
                + "    p.pname,\n"
                + "	p.price,\n"
                + "	p.img2,\n"
                + "    SUM(od.quantity) AS quantitySold,\n"
                + "	SUM(od.price*od.quantity) as priceSold\n"
                + "FROM \n"
                + "    orderDetails od\n"
                + "JOIN \n"
                + "    Products p ON od.pid = p.pid\n"
                + "GROUP BY	\n"
                + "    od.pid, p.pname,p.price,p.img2\n"
                + "ORDER BY \n"
                + "   quantitySold DESC;";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new OrderDetail(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getInt(5), rs.getInt(6)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public int countBlogAvai() {
        String sql = "select count(*) from BlogPosts where status_id = 1";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {

        }
        return 0;
    }

    public List<Blog> pagingBlogAvai(int sid, int index) {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT post_id, title, author, updated_date, content, bc.bc_name,\n"
                + "thumbnail, brief, acid, status_name\n"
                + "From BlogPosts bp join BlogCategories bc on bp.bc_id = bc.bc_id\n"
                + "join Status s on s.status_id = bp.status_id\n"
                + "WHERE bp.status_id = ? "
                + "ORDER BY post_id ASC "
                + "OFFSET ? ROWS FETCH NEXT 6 ROWS ONLY";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, sid);
            ps.setInt(2, (index - 1) * 6);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Blog(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getInt(9), rs.getString(10)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Blog> getBlogByBCid(int bcid) {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT post_id, title, author, updated_date, content, bc.bc_name,\n"
                + "thumbnail, brief, acid, status_name\n"
                + "From BlogPosts bp join BlogCategories bc on bp.bc_id = bc.bc_id\n"
                + "join Status s on s.status_id = bp.status_id \n"
                + "WHERE bp.bc_id = ?";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, bcid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Blog(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getInt(9), rs.getString(10)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Blog> getBlogByAuthor(String author) {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT post_id, title, author, updated_date, content, bc.bc_name,\n"
                + "thumbnail, brief, acid, status_name\n"
                + "From BlogPosts bp join BlogCategories bc on bp.bc_id = bc.bc_id\n"
                + "join Status s on s.status_id = bp.status_id WHERE author like ? ";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + author + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Blog(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getInt(9), rs.getString(10)));

            }
        } catch (SQLException e) {
            // Xử lý ngoại lệ
            System.out.println(e);
        }
        return list;
    }

    public List<Blog> getBlogByBCidAndSid(int cid, int sid) {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT post_id, title, author, updated_date, content, bc.bc_name,\n"
                + "thumbnail, brief, acid, status_name\n"
                + "From BlogPosts bp join BlogCategories bc on bp.bc_id = bc.bc_id\n"
                + "join Status s on s.status_id = bp.status_id \n"
                + "where bp.bc_id = ? and bp.status_id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, cid);
            ps.setInt(2, sid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Blog(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getInt(9), rs.getString(10)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Blog> getBlogByCidAndAuthor(int cid, String author) {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT post_id, title, author, updated_date, content, bc.bc_name,\n"
                + "thumbnail, brief, acid, status_name\n"
                + "From BlogPosts bp join BlogCategories bc on bp.bc_id = bc.bc_id\n"
                + "join Status s on s.status_id = bp.status_id \n"
                + "where bp.bc_id = ? and bp.author = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, cid);
            ps.setString(2, author);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Blog(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getInt(9), rs.getString(10)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Blog> getBlogBySidAndAuthor(int sid, String author) {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT post_id, title, author, updated_date, content, bc.bc_name,\n"
                + "thumbnail, brief, acid, status_name\n"
                + "From BlogPosts bp join BlogCategories bc on bp.bc_id = bc.bc_id\n"
                + "join Status s on s.status_id = bp.status_id \n"
                + "where bp.status_id = ? and bp.author = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, sid);
            ps.setString(2, author);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Blog(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getInt(9), rs.getString(10)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Blog> getBlogByCidAndSidAndAuthor(int cid, int sid, String author) {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT post_id, title, author, updated_date, content, bc.bc_name,\n"
                + "thumbnail, brief, acid, status_name\n"
                + "From BlogPosts bp join BlogCategories bc on bp.bc_id = bc.bc_id\n"
                + "join Status s on s.status_id = bp.status_id \n"
                + "where bp.bc_id = ? and bp.status_id = ? and bp.author = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, cid);
            ps.setInt(2, sid);
            ps.setString(3, author);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Blog(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getInt(9), rs.getString(10)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Blog> searchByTitle(String search) {
        List<Blog> resultList = new ArrayList<>();
        String sql = "SELECT post_id, title, author, updated_date, content, bc.bc_name,\n"
                + "thumbnail, brief, acid, status_name\n"
                + "From BlogPosts bp join BlogCategories bc on bp.bc_id = bc.bc_id\n"
                + "join Status s on s.status_id = bp.status_id \n"
                + "WHERE title LIKE ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + search + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                resultList.add(new Blog(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getInt(9), rs.getString(10)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return resultList;
    }

    public int addBlog(String title, String author, String updated_date, String content, int bc_id, String thumbnail, String brief, int acid, int status_id) {
        String sql = "INSERT INTO BlogPosts (title, author, updated_date, content, bc_id, thumbnail, brief, acid, status_id) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";

        int generatedId = -1;

        try {
            PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, title);
            ps.setString(2, author);
            ps.setString(3, updated_date);
            ps.setString(4, content);
            ps.setInt(5, bc_id);
            ps.setString(6, thumbnail);
            ps.setString(7, brief);
            ps.setInt(8, acid);
            ps.setInt(9, status_id);
            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                generatedId = rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return generatedId;
    }

    public Blog getBlogDetails(int bid) {

        String sql = "SELECT post_id, title, author, updated_date, content, bc.bc_name,\n"
                + "thumbnail, brief, acid, status_name\n"
                + "From BlogPosts bp join BlogCategories bc on bp.bc_id = bc.bc_id\n"
                + "join Status s on s.status_id = bp.status_id \n"
                + "WHERE bp.post_id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, bid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Blog(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getInt(9), rs.getString(10));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public String getBlogThumbnailById(int bid) {
        String sql = "  select thumbnail from BlogPosts\n"
                + "  where post_id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, bid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void updateBlog(int bc_id, String title, String author, String updated_date, String content, String thumbnail, String brief, int acid, int status_id, int post_id) {
        String sql = "UPDATE BlogPosts\n"
                + "SET\n"
                + "    bc_id = ?,\n"
                + "    title = ?,\n"
                + "    author = ?,\n"
                + "    updated_date = ?,\n"
                + "    content = ?,\n"
                + "    thumbnail = ?,\n"
                + "    brief = ?,\n"
                + "    acid = ?,\n"
                + "    status_id = ?\n"
                + "WHERE\n"
                + "    post_id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, bc_id);
            ps.setString(2, title);
            ps.setString(3, author);
            ps.setString(4, updated_date);
            ps.setString(5, content);
            ps.setString(6, thumbnail);
            ps.setString(7, brief);
            ps.setInt(8, acid);
            ps.setInt(9, status_id);
            ps.setInt(10, post_id);
            ps.executeUpdate();
        } catch (SQLException e) {
            // Xử lý ngoại lệ hoặc thông báo lỗi
            e.printStackTrace();
        }
    }

    public void deleteBlog(int bid) {
        String sql = "delete from BlogPosts\n"
                + "where post_id =  ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, bid);
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }

    }

    public void deleteBanner(int banid) {
        String sql = "delete from Banner where banid=?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, banid);
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }

    public void addBanner(String banImg, int status, String createdAt, int roleId, int bid) {
        String sql = "insert into Banner values (?,?,?,?,?);";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);

            ps.setString(1, banImg);
            ps.setInt(2, status);
            ps.setString(3, createdAt);
            ps.setInt(4, roleId);
            ps.setInt(5, bid);
            ps.executeUpdate();

        } catch (Exception e) {
        }
    }

    public void editBanner(int status, int banid) {
        String sql = "   update Banner \n"
                + "   set \n"
                + "		[status]=?\n"
                + "	where banid=?";
        try {
            try {
                PreparedStatement ps = connection.prepareStatement(sql);
//                ps.setString(1,img);
                ps.setInt(1, status);

                ps.setInt(2, banid);
                ps.executeUpdate();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }
    }

    public List<Banner> getBanner() {
        List<Banner> list = new ArrayList<>();
        String sql = "  select banid,\n"
                + "	img,\n"
                + "	b.status,\n"
                + "	created_at,\n"
                + "	role_id,\n"
                + "	b.brandid,\n"
                + "	br.brandname\n"
                + "\n"
                + "from Banner b join Brand br on b.brandid=br.brandid ";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Banner(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getInt(5), rs.getInt(6), rs.getString(7)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Brand> getAllBrand() {
        List<Brand> list = new ArrayList<>();
        String sql = "select * from Brand";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Brand(rs.getInt(1), rs.getString(2)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public Banner getBannerByID(int banid) {
        String sql = "select banid,img,b.status,created_at,role_id,b.brandid,brandname \n"
                + "from Banner b \n"
                + "join Brand br \n"
                + "on b.brandid=br.brandid \n"
                + "where banid=?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, banid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Banner(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getInt(5), rs.getInt(6), rs.getString(7));

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Banner> getBannerByBrandID(int bid) {
        List<Banner> list = new ArrayList<>();
        String sql = "select banid,img,b.status,created_at,role_id,b.brandid,brandname \n"
                + "from Banner b \n"
                + "join Brand br \n"
                + "on b.brandid=br.brandid \n"
                + "where b.brandid=?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, bid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Banner(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getInt(5), rs.getInt(6), rs.getString(7)));

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Banner> getBannerByStatus(int status) {
        List<Banner> list = new ArrayList<>();
        String sql = "select banid,img,b.status,created_at,role_id,b.brandid,brandname \n"
                + "from Banner b \n"
                + "join Brand br \n"
                + "on b.brandid=br.brandid \n"
                + "where b.status=?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, status);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Banner(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getInt(5), rs.getInt(6), rs.getString(7)));

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Banner> filterBanner(int bid, int status) {
        List<Banner> list = new ArrayList<>();
        String sql = "select * from Banner b inner join Brand br on b.brandid =br.brandid where b.brandid=? and b.status=?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, bid);
            ps.setInt(2, status);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Banner(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getInt(5), rs.getInt(6), rs.getString(7)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public Accounts getUserById(String email) {
        String sql = "select * from Accounts\n"
                + "where email = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Accounts(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getInt(7), rs.getString(6), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
     public List<FeedbackList> getFeedback() {

        List<FeedbackList> list = new ArrayList<>();
        String sql = "   select f.feedback_id,f.acid,f.pid,f.rating ,f.created_at,f.comment,a.username,p.pname,a.email  from Accounts a\n"
                + "  JOIN \n"
                + "    Feedback f ON f.acid = a.acid\n"
                + "JOIN \n"
                + "    Products p ON f.pid = p.pid;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new FeedbackList(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getDouble(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
     public void deleteComment(int feedback_id){
        String sql = "delete from Feedback where feedback_id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, feedback_id);
            ResultSet rs = st.executeQuery();
        } catch (Exception e) {
        }
    }
    public static void main(String[] args) {
        MaketingDao dao = new MaketingDao();
        List<Banner> list = dao.getBanner();
        for (Banner a : list) {
            System.out.println(a.getBanId());
        }
    }
}
