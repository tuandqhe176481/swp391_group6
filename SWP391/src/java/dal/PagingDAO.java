/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Blog;
import model.Product;

/**
 *
 * @author pc
 */
public class PagingDAO extends context.DBContext {

    public int getTotalProduct() {
        String sql = " select count(*) from Products where quantity > 0 and status_id = 1";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Product> pagingProduct(int index) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT * FROM Products\n"
                + "WHERE quantity > 0 AND status_id = 1\n"
                + "ORDER BY CONVERT(DATE, created_at, 103) DESC\n"
                + "OFFSET ? ROWS FETCH NEXT 12 ROWS ONLY;";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, (index - 1) * 12);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13), rs.getInt(14)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public int getTotalProductSale() {
        String sql = " select	count(*) from Products\n"
                + " where isDiscount = 1";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Product> pagingSaleProduct(int index) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT * FROM Products\n"
                + "WHERE isDiscount = 1 and quantity > 0 and status_id = 1 \n"
                + "order by CONVERT(DATE, created_at, 103) DESC\n"
                + "OFFSET ? ROWS FETCH NEXT 12 ROWS ONLY;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, (index - 1) * 12);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13), rs.getInt(14)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public int getTotalProductbyBrandid(int bid) {
        String sql = " select	count(*) from Products\n"
                + " where quantity > 0 and status_id = 1 and brandid = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, bid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Product> pagingProductByBrand(int bid, int index) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT * FROM Products\n"
                + "WHERE quantity > 0 and status_id = 1 and brandid = ?\n"
                + "order by pid\n"
                + "OFFSET ? ROWS FETCH NEXT 12 ROWS ONLY;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, bid);
            ps.setInt(2, (index - 1) * 12);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13), rs.getInt(14)));
            }
        } catch (Exception e) {
        }
        return list;

    }

    public List<Product> pagingProductforMkt(int index) {
        List<Product> list = new ArrayList<>();
        String sql = "select pid\n"
                + "      ,cid\n"
                + "      ,pname\n"
                + "      ,rate\n"
                + "      ,img\n"
                + "      ,img2\n"
                + "      ,price\n"
                + "      ,brandid\n"
                + "      ,priceSale\n"
                + "      ,quantity\n"
                + "      ,isDiscount\n"
                + "      ,isSoldout\n"
                + "      ,created_at\n"
                + "      ,s.status_name\n"
                + "	 from Products p \n"
                + "Inner join Status s on p.status_id = s.status_id\n"
                + "ORDER BY pid Asc \n"
                + "OFFSET ? rows FETCH next 12 rows only";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, (index - 1) * 12);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getBoolean(11), rs.getBoolean(12), rs.getString(13), rs.getString(14)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Blog> pagingBlogPostForMkt(int index) {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT post_id, title, author, updated_date, content, bc.bc_name,\n"
                + "thumbnail, brief, acid, status_name\n"
                + "From BlogPosts bp join BlogCategories bc on bp.bc_id = bc.bc_id\n"
                + "join Status s on s.status_id = bp.status_id\n"
                + "ORDER BY post_id ASC \n"
                + "OFFSET ? ROWS FETCH NEXT 6 ROWS ONLY";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, (index - 1) * 6);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Blog(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getInt(9), rs.getString(10)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public int getTotalBlogPost() {
        String sql = " select count (*) from BlogPosts";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public static void main(String[] args) {
        PagingDAO dao = new PagingDAO();
        List<Product> list = dao.pagingProductforMkt(1);
        for (Product product : list) {
            System.out.println(list);
        }
    }
}
