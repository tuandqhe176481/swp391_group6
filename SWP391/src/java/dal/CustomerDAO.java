/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import context.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import model.Accounts;
import model.Item;
import model.Product;

/**
 *
 * @author PV
 */
public class CustomerDAO extends DBContext {

    public Accounts getAccounts(String email) {
        String sql = "select * from Accounts where email=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new Accounts(rs.getInt(1), email, rs.getString(3), rs.getString(4), rs.getInt(5), rs.getInt(7), rs.getString(6), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11));
            }

        } catch (Exception e) {
        }
        return null;
    }

    public void addOrder(Accounts a, List<Item> list, String address, String phone_number, String note, String receiver, int saleId, String payment) {

        Calendar cld = Calendar.getInstance(TimeZone.getTimeZone("Etc/GMT+7"));
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        String vnp_CreateDate = formatter.format(cld.getTime());

        try {
            double total = totalAmount(a.getUser_id());

            String sql = "insert into orders(acid,ordered_at,TotalAmount,address,phone_number,status,note,receiver,payment,saleId) values(?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, a.getUser_id());
            st.setTimestamp(2, new Timestamp(formatter.parse(vnp_CreateDate).getTime()));
            st.setInt(3, (int) total);
            st.setString(4, address);
            st.setString(5, phone_number);
            st.setInt(6, 1);
            st.setString(7, note);
            st.setString(8, receiver);
            st.setString(9, payment);
            st.setInt(10, saleId);
            st.executeUpdate();
            //lay id cua Order vua add
            String sql1 = "select top 1 oid from orders order by oid desc";
            PreparedStatement st1 = connection.prepareStatement(sql1);
            ResultSet rs = st1.executeQuery();
            if (rs.next()) {
                int oid = rs.getInt(1);
                for (Item i : list) {
                    String sql2 = "insert into orderDetails values(?,?,?,?)";
                    PreparedStatement st2 = connection.prepareStatement(sql2);
                    st2.setInt(1, i.getProduct().getPid());
                    st2.setInt(2, oid);
                    st2.setInt(3, i.getPrice());
                    st2.setInt(4, i.getQuantity());
                    st2.executeUpdate();
                }
            }
            // cap nhat so luong
            String sql3 = "update Products set quantity=quantity-? where pid=?";
            PreparedStatement st3 = connection.prepareStatement(sql3);
            for (Item i : list) {
                st3.setInt(1, i.getQuantity());
                st3.setInt(2, i.getProduct().getPid());
                st3.executeUpdate();
            }

        } catch (Exception e) {
        }

    }

    public void createCart(int acid) {
        String sql = "INSERT INTO Cart VALUES (?, ?) ";
        try ( PreparedStatement st = connection.prepareStatement(sql)) {
            // Set parameters
            st.setInt(1, acid);
            st.setDouble(2, 0);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void addToCart(int acid, Item item) {
        String sql = "select cart_id from Cart where acid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, acid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                int cart_id = rs.getInt(1);

                String sql1 = "select * from CartDetails where pid=? and cart_id=?";
                PreparedStatement st1 = connection.prepareStatement(sql1);
                st1.setInt(1, item.getProduct().getPid());
                st1.setInt(2, cart_id);
                ResultSet rs1 = st1.executeQuery();
                if (rs1.next()) {
                    String sql4 = "update CartDetails set quantity+=? where pid=? and cart_id=?";
                    PreparedStatement st4 = connection.prepareStatement(sql4);
                    st4.setInt(1, 1);
                    st4.setInt(2, item.getProduct().getPid());
                    st4.setInt(3, cart_id);
                    st4.executeUpdate();
                } else {

                    String sql2 = "INSERT INTO CartDetails VALUES (?,?,?,?) ";
                    PreparedStatement st2 = connection.prepareStatement(sql2);
                    st2.setInt(1, cart_id);
                    st2.setInt(2, item.getProduct().getPid());
                    st2.setInt(3, item.getQuantity());
                    st2.setDouble(4, (double) item.getPrice());
                    st2.executeUpdate();
                }
                String sql3 = "update Cart set total_order_price+=? where acid=?";
                PreparedStatement st3 = connection.prepareStatement(sql3);
                st3.setDouble(1, (double) item.getPrice());
                st3.setInt(2, acid);
                st3.executeUpdate();

            }
        } catch (Exception e) {
        }

    }

    public void removeItem(int pid, int price, int acid) {
        String sql3 = "select cart_id from Cart where acid=?";
        try {
            PreparedStatement st3 = connection.prepareStatement(sql3);
            st3.setInt(1, acid);
            ResultSet rs3 = st3.executeQuery();
            if (rs3.next()) {
                int cart_id = rs3.getInt(1);
                String sql = "select quantity From CartDetails where pid=? and cart_id=?";
                PreparedStatement st = connection.prepareStatement(sql);
                st.setInt(1, pid);
                st.setInt(2, cart_id);
                ResultSet rs = st.executeQuery();
                if (rs.next()) {
                    int quantity = rs.getInt(1);

                    String sql1 = "delete from CartDetails where pid=? and cart_id=? ";
                    PreparedStatement st1 = connection.prepareStatement(sql1);
                    st1.setInt(1, pid);
                    st1.setInt(2, cart_id);
                    st1.executeUpdate();

                    String sql2 = "update Cart set total_order_price-=? where acid=?";
                    PreparedStatement st2 = connection.prepareStatement(sql2);
                    st2.setDouble(1, (double) (price * quantity));
                    st2.setInt(2, acid);
                    st2.executeUpdate();
                }
            }
        } catch (Exception e) {
        }
    }

    public void increaseItem(int pid, int price, int acid) {
        String sql2 = "select cart_id from Cart where acid=?";
        try {
            PreparedStatement st2 = connection.prepareStatement(sql2);
            st2.setInt(1, acid);
            ResultSet rs = st2.executeQuery();
            if (rs.next()) {
                int cart_id = rs.getInt(1);
                String sql = "update CartDetails set quantity+=? where pid=? and cart_id=?";
                PreparedStatement st = connection.prepareStatement(sql);
                st.setInt(1, 1);
                st.setInt(2, pid);
                st.setInt(3, cart_id);
                st.executeUpdate();

                String sql1 = "update Cart set total_order_price+=? where acid=?";
                PreparedStatement st1 = connection.prepareStatement(sql1);
                st1.setDouble(1, (double) price);
                st1.setInt(2, acid);
                st1.executeUpdate();
            }
        } catch (Exception e) {
        }
    }

    public void decreaseItem(int pid, int price, int acid) {
        String sql2 = "select cart_id from Cart where acid=?";
        try {
            PreparedStatement st2 = connection.prepareStatement(sql2);
            st2.setInt(1, acid);
            ResultSet rs = st2.executeQuery();
            if (rs.next()) {
                int cart_id = rs.getInt(1);
                String sql = "update CartDetails set quantity-=? where pid=? and cart_id=?";
                PreparedStatement st = connection.prepareStatement(sql);
                st.setInt(1, 1);
                st.setInt(2, pid);
                st.setInt(3, cart_id);
                st.executeUpdate();

                String sql1 = "update Cart set total_order_price-=? where acid=?";
                PreparedStatement st1 = connection.prepareStatement(sql1);
                st1.setDouble(1, (double) price);
                st1.setInt(2, acid);
                st1.executeUpdate();
            }
        } catch (Exception e) {
        }
    }

    public int checkQuantity(int acid, int pid) {
        String sql = "select cart_id from Cart where acid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, acid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                int cart_id = rs.getInt(1);
                String sql1 = "select quantity from CartDetails where pid=? and cart_id=?";
                PreparedStatement st1 = connection.prepareStatement(sql1);
                st1.setInt(1, pid);
                st1.setInt(2, cart_id);
                ResultSet rs1 = st1.executeQuery();
                if (rs1.next()) {
                    int quantity = rs1.getInt(1);
                    return quantity;
                }

            }

        } catch (Exception e) {
        }
        return 0;
    }

    public List<Item> getCart(int acid) {
        List<Item> list = new ArrayList<>();
        String sql = "select cart_id from Cart where acid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, acid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                int cart_id = rs.getInt(1);

                String sql1 = "select cd.quantity,cd.total_cost,p.* from CartDetails cd  join Products p on cd.pid=p.pid \n"
                        + "where cd.cart_id=?";
                PreparedStatement st1 = connection.prepareStatement(sql1);
                st1.setInt(1, cart_id);
                ResultSet rs1 = st1.executeQuery();
                while (rs1.next()) {
                    list.add(new Item(new Product(
                            rs1.getInt("pid"),
                            rs1.getInt("cid"),
                            rs1.getString("pname"),
                            rs1.getFloat("rate"),
                            rs1.getString("img"),
                            rs1.getString("img2"),
                            rs1.getInt("price"),
                            rs1.getInt("brandid"),
                            rs1.getInt("priceSale"),
                            rs1.getInt(12),
                            rs1.getBoolean("isDiscount"),
                            rs1.getBoolean("isSoldout"),
                            rs1.getString("created_at")),
                            rs1.getInt(1), (int) rs1.getDouble(2)));
                }

            }

        } catch (Exception e) {
        }
        return list;
    }

    public Double totalAmount(int acid) {
        String sql = "select total_order_price from Cart where acid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, acid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                double total = rs.getDouble(1);
                return total;
            }
        } catch (Exception e) {
        }
        return 0.0;
    }

    public void cartSubmit(int acid) {
        try {
            String sql = "update Cart set total_order_price=? where acid=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setDouble(1, 0);
            st.setInt(2, acid);
            st.executeUpdate();

            String sql1 = "select cart_id from Cart where acid=?";
            PreparedStatement st1 = connection.prepareStatement(sql1);
            st1.setInt(1, acid);
            ResultSet rs1 = st1.executeQuery();
            if (rs1.next()) {
                int cart_id = rs1.getInt(1);

                String sql2 = "delete from CartDetails where cart_id=?";
                PreparedStatement st2 = connection.prepareStatement(sql2);
                st2.setInt(1, cart_id);
                st2.executeUpdate();
            }

        } catch (Exception e) {
        }
    }

    public int getMinOrderSale() {
        String sql = "SELECT top 1 saleId, COUNT(saleId) AS sale_count\n"
                + "FROM orders\n"
                + "GROUP BY saleId\n"
                + "ORDER BY sale_count ASC";
        try {
            PreparedStatement st=connection.prepareStatement(sql);
            ResultSet rs=st.executeQuery();
            if(rs.next()){
            return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public static void main(String[] args) {
        CustomerDAO cd = new CustomerDAO();

        List<Item> list = cd.getCart(18);
        System.out.println(list.size());
        Double total = cd.totalAmount(18);
        System.out.println(total);
        int quantity = cd.checkQuantity(18, 38);
        System.out.println(quantity);
        cd.cartSubmit(18);
        int i=cd.getMinOrderSale();
        System.out.println(i);
    }

}
