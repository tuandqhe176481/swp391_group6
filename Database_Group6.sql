Create Database SWP391_Group6
-- Bảng Categories
CREATE TABLE Categories (
   cid INT PRIMARY KEY IDENTITY(1,1),
    cname NVARCHAR(150),
);

-- Bảng Brand
CREATE TABLE Brand (
    brandid INT PRIMARY KEY IDENTITY(1,1),
    brandname NVARCHAR(50) NOT NULL
    -- Các cột khác nếu cần
);
CREATE TABLE Status (
    status_id int PRIMARY KEY IDENTITY(1,1),
    status_name VARCHAR(MAX) NOT NULL
);
-- Bảng Products
CREATE TABLE Products (
  pid INT PRIMARY KEY IDENTITY(1,1),
	cid int,
    pname NVARCHAR(50),
    rate FLOAT,
    img NVARCHAR(MAX),
	img2 NVARCHAR(MAX),
    price INTEGER,
	brandid INT,
    priceSale INTEGER,
    quantity INTEGER,
    isDiscount BIT,
    isSoldout BIT,
    created_at DATE,
    status_id int,
	FOREIGN KEY (status_id) REFERENCES Status(status_id),
	FOREIGN KEY (cid) REFERENCES Categories (cid),
	FOREIGN KEY (brandid) REFERENCES Brand (brandid)
);


-- Bảng AccountStatuses
CREATE TABLE AccountStatuses (
     status_id INT PRIMARY KEY IDENTITY(1,1),
    status_name NVARCHAR(50) NOT NULL
    -- Các cột khác nếu cần
);

-- Bảng Roles
CREATE TABLE Roles (
    role_id INT PRIMARY KEY IDENTITY(1,1),
    role_name NVARCHAR(50) NOT NULL
    -- Các cột khác nếu cần
);

-- Bảng Accounts
CREATE TABLE Accounts (
     acid INT PRIMARY KEY IDENTITY(1,1),
    email NVARCHAR(50),
    password NVARCHAR(50),
    username NVARCHAR(50),
    role_id INT,
    created_at date,
    status_id INT,
    phone_number VARCHAR(20),
    gender varchar(20),
	address nvarchar(MAX),
	avartar nvarchar(MAX),
    FOREIGN KEY (role_id) REFERENCES Roles(role_id),
    FOREIGN KEY (status_id) REFERENCES AccountStatuses(status_id)
);
CREATE TABLE statusOrder (
    id INT IDENTITY(1,1),
    status NVARCHAR(50),
    PRIMARY KEY (id)
);
-- Bảng orders
CREATE TABLE orders (
	oid INT IDENTITY(1,1),
    acid INT,
    ordered_at DATETIME,
    TotalAmount INT,
    address NVARCHAR(max),
    phone_number NCHAR(20),
    status INT,
    note NVARCHAR(250),
    receiver NVARCHAR(50),
    payment NVARCHAR(20),
	saleId int,
    PRIMARY KEY (oid),
    FOREIGN KEY (acid) REFERENCES Accounts(acid),
	FOREIGN KEY (status) REFERENCES statusOrder(id)
);


-- Bảng orderDetails
CREATE TABLE orderDetails (
     odid INT PRIMARY KEY IDENTITY(1,1),
    pid INT,
    oid INT,
    price FLOAT,
    quantity INT,
    FOREIGN KEY (pid) REFERENCES Products(pid),
    FOREIGN KEY (oid) REFERENCES orders(oid)
);

CREATE TABLE Cart (
     cart_id INT PRIMARY KEY IDENTITY(1,1),
    acid INT,
    total_order_price DECIMAL(10, 2) DEFAULT 0.0,
    FOREIGN KEY (acid) REFERENCES Accounts(acid)

);

CREATE TABLE CartDetails (
     cart_details_id INT PRIMARY KEY IDENTITY(1,1),
    cart_id INT,
    pid INT,
    quantity INT,
    total_cost DECIMAL(10, 2),
    FOREIGN KEY (cart_id) REFERENCES Cart(cart_id),
    FOREIGN KEY (pid) REFERENCES Products(pid)
);

--Bảng banner
CREATE TABLE Banner (
     banid INT PRIMARY KEY IDENTITY(1,1),
    img NVARCHAR(150),
    status INTEGER,
    created_at NVARCHAR(MAX),
    role_id INT, -- Thêm cột role_id
	brandid INT,
    FOREIGN KEY (role_id) REFERENCES Roles(role_id)
);


-- Bảng ProductDetail
CREATE TABLE ProductDetail (
     pdid INT PRIMARY KEY IDENTITY(1,1),
    size NVARCHAR(MAX),
    color NVARCHAR(MAX),
    gender BIT,
    description NVARCHAR(MAX),
    pid INTEGER,
    FOREIGN KEY (pid) REFERENCES Products(pid)
);

-- Bảng Feedback
CREATE TABLE Feedback (
    feedback_id INT PRIMARY KEY IDENTITY(1,1),
    acid INT,
    pid INT,
    rating INTEGER,
    comment NVARCHAR(MAX),
    created_at DATETIME,
    FOREIGN KEY (acid) REFERENCES Accounts(acid),
    FOREIGN KEY (pid) REFERENCES Products(pid)
);

--Bảng BLOG_CATEGORIES
CREATE TABLE BlogCategories (
    bc_id INT PRIMARY KEY IDENTITY(1,1),
    bc_name NVARCHAR(MAX) NOT NULL
);

--Bảng BLOG
CREATE TABLE BlogPosts (
    post_id INT PRIMARY KEY IDENTITY(1,1),
    title NVARCHAR(MAX) NOT NULL,
    author NVARCHAR(100),
    updated_date NVARCHAR(MAX),
	content NVARCHAR(MAX),
    bc_id INT,
    thumbnail NVARCHAR(MAX),
    brief NVARCHAR(MAX),
    acid INT,
	status_id int,
	FOREIGN KEY (status_id) REFERENCES Status(status_id),
    FOREIGN KEY (bc_id) REFERENCES BlogCategories(bc_id),
    FOREIGN KEY (acid) REFERENCES Accounts(acid)
);
CREATE TABLE SettingsCategories (
    setting_type INT PRIMARY KEY IDENTITY(1, 1), -- Thêm cột setting_type làm khóa chính
    setting_name NVARCHAR(255) NOT NULL
);

CREATE TABLE Settings (
    id INT PRIMARY KEY IDENTITY(1, 1),
    [type] INT NOT NULL,
    [value] NVARCHAR(255) NOT NULL,
    [order] INT NOT NULL,
    [status] NVARCHAR(50) NOT NULL,
    FOREIGN KEY ([type]) REFERENCES SettingsCategories(setting_type)
);









INSERT INTO SettingsCategories
values('User Role'),
('Brand'),
('Product Categories'),
('Blog Categories'),
('Order Status'),
('Accounts Status');

/****** Script for SelectTopNRows command from SSMS  ******/
-- Thêm cột 'status' vào bảng [SWP391_Group6].[dbo].[Status]
ALTER TABLE [SWP391_Group6].[dbo].[Status]
ADD [status] NVARCHAR(MAX);

-- Thêm cột 'status' vào bảng [SWP391_Group6].[dbo].[AccountStatuses]
ALTER TABLE [SWP391_Group6].[dbo].[AccountStatuses]
ADD [status] NVARCHAR(MAX);

-- Thêm cột 'status' vào bảng [SWP391_Group6].[dbo].[statusOrder]
ALTER TABLE [SWP391_Group6].[dbo].[statusOrder]
ADD [statuss] NVARCHAR(MAX);

-- Thêm cột 'status' vào bảng [SWP391_Group6].[dbo].[Roles]
ALTER TABLE [SWP391_Group6].[dbo].[Roles]
ADD [status] NVARCHAR(MAX);

-- Thêm cột 'status' vào bảng [SWP391_Group6].[dbo].[Categories]
ALTER TABLE [SWP391_Group6].[dbo].[Categories]
ADD [status] NVARCHAR(MAX);

-- Thêm cột 'status' vào bảng [SWP391_Group6].[dbo].[Brand]
ALTER TABLE [SWP391_Group6].[dbo].[Brand]
ADD [status] NVARCHAR(MAX);

-- Thêm cột 'status' vào bảng [SWP391_Group6].[dbo].[BlogCategories]
ALTER TABLE [SWP391_Group6].[dbo].[BlogCategories]
ADD [status] NVARCHAR(MAX);

--ADD ROLE
INSERT INTO [SWP391_Group6].[dbo].[Roles] ([role_name])
VALUES ('Admin');
INSERT INTO [SWP391_Group6].[dbo].[Roles] ([role_name])
VALUES ('Sale Manager');
INSERT INTO [SWP391_Group6].[dbo].[Roles] ([role_name])
VALUES ('Saler');
INSERT INTO [SWP391_Group6].[dbo].[Roles] ([role_name])
VALUES ('Marketer');
INSERT INTO [SWP391_Group6].[dbo].[Roles] ([role_name])
VALUES ('Customer');
INSERT INTO [SWP391_Group6].[dbo].[Roles] ([role_name])
VALUES ('Inventory Manager');

--Add statusOrder
INSERT INTO statusOrder (status) VALUES 
('Pending Confirmation'),
('Confirmed'),
('Shipping'),
('Completed'),
('Cancel');
--ADD BRAND	
insert into Brand (brandname)
values('Adidas'), ('Funt'),('Teelab'),('Gucci'),('Dirty Coin'),('$Maker');

--ADD BANNER
INSERT INTO Banner (img, status, role_id, brandid) 

VALUES ('https://www.celestineagency.com/sites/default/files/images/features/3554810d092486526eb3570bc73ecdf682469c19-7500x4217.jpg', 1, 4, 1);
INSERT INTO Banner (img, status, role_id, brandid) 
VALUES ('https://theme.hstatic.net/200000528913/1000992880/14/ms_banner_img1.jpg?v=614', 1, 4, 2);
INSERT INTO Banner (img, status, role_id, brandid) 
VALUES ('https://bizweb.dktcdn.net/100/415/697/themes/902041/assets/slider_3.jpg?1705474695254', 1, 4, 3);
INSERT INTO Banner (img, status, role_id, brandid) 
VALUES ('https://authenticvietnam.vn/wp-content/uploads/banner-that-lung-gucci-cua-nuoc-nao-chinh-hang-chia-se-tu-gucci-vietnam.jpg', 1, 4, 4);
INSERT INTO Banner (img, status, role_id, brandid) 
VALUES ('https://sneakerdaily.vn/wp-content/uploads/2021/04/chuyen-tha-nhu-dua-ao-dirtycoins-420k-duoc-resell-xuong-4-trieu-2.jpeg', 1, 4, 5);
INSERT INTO Banner (img, status, role_id, brandid) 
VALUES ('https://smakerclothing.com/upload/hinhanh/cover-2393.jpg', 1, 4, 6);


--ADD CATEGORIES
insert into Categories(cname)
values (N'Áo thun'),('Áo Polo'),(N'BaBy Tee'),(N'Áo sơ mi'),(N'Áo khoác'),(N'Hoodie'),(N'Quần nam'),(N'Quần nữ'),(N'Phụ kiện');

insert into Status (status_name)
values('Available'),
('Discontinued');

--ADD PRODUCT
--$maker
  INSERT INTO dbo.Products (cid, pname, rate, img, img2, price, brandid, pricesale, quantity, isDiscount, isSoldout, created_at, status_id)
VALUES
(1, N'NEED MONEY FOR RARI', 5, 'https://smakerclothing.com/upload/sanpham/dsc038638113_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/dsc03868-41815701_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-06-24', 1),

(2, N'CHENILLE LOGO POLO IN GREY', 5, 'https://smakerclothing.com/upload/sanpham/dscf51378225_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/dscf5135-47815095_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-08-07', 1),

(3, N'FIRE BUTTERFLY', 5, 'https://smakerclothing.com/upload/sanpham/dscf32359404_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/dscf3236-59895382_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-06-28', 1),

(4, N'SƠ MI $MAKER', 5, 'https://smakerclothing.com/upload/hinhthem/beauty1589772470597-76668953_225x300.jpg', 'https://smakerclothing.com/upload/hinhthem/beauty1589772512075-99935196_225x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-07-19', 1),

(5, N'$MAKER  JACKET', 5, 'https://smakerclothing.com/upload/sanpham/dscf54913843_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/dscf5494-59160417_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-06-20', 1),

(6, N'$MAKER HOODIE IN PINK', 5, 'https://smakerclothing.com/upload/sanpham/dscf41605220_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/dscf4164-3502612_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-09-21', 1),

(7, N'$MAKER JOGGERS', 5, 'https://smakerclothing.com/upload/sanpham/dscf38463434_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/dscf3849-49680923_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-06-10', 1),

(8, N'$MAKER CLOUND IN BLUE', 5, 'https://smakerclothing.com/upload/sanpham/dscf41427614_320x320.jpg', 'https://smakerclothing.com/upload/hinhthem/dscf4143-31817573_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-06-22', 1),

(9, N' THE MONEY BAG', 5, 'https://smakerclothing.com/upload/sanpham/dsc025837097_320x320.jpg', 'https://smakerclothing.com/upload/hinhthem/dsc02606-93345900_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-06-25', 1),

(9, N'SUNSET FRAME SUNGLASSES', 5, 'https://smakerclothing.com/upload/sanpham/dscf63018353_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/dscf6305-98865199_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-07-17', 1),

(9, N'LOGO SOCKS IN WHITE', 5, 'https://smakerclothing.com/upload/hinhthem/img5006-76520104_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/img5001-74664339_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-07-15', 1),

(1, N'TETVOVEN MERCH', 5, 'https://smakerclothing.com/upload/sanpham/dsc023108066_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/dsc02316-30758029_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-07-15', 1),

(8, N'TENVOVEN MERCH', 5, 'https://smakerclothing.com/upload/sanpham/dsc022952592_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/dsc02303-72340436_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-07-15', 1),

(7, N'PATTERN LOGO MESH SHORT', 5, 'https://smakerclothing.com/upload/sanpham/dsc037789878_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/dsc03782-69950891_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-07-15', 1),

(8, N'CUSTOMIZE EMBOSSED', 5, 'https://smakerclothing.com/upload/sanpham/dsc037520302_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/dsc03762-47443005_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-07-15', 1),

(7, N'PATTERN LOGO" MESH SHORT IN GREEN', 5, 'https://smakerclothing.com/upload/sanpham/dsc037671017_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/dsc03773-11537028_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-07-15', 1),

(8, N'SPLASHED FLARE JEANS', 5, 'https://smakerclothing.com/upload/sanpham/dsc037877393_320x320.jpg', 'https://smakerclothing.com/upload/hinhthem/dsc03799-68907839_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-07-15', 1),

(1, N'"NEED MONEY FOR BIRKIN"', 5, 'https://smakerclothing.com/upload/sanpham/dsc038778086_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/dsc03883-6773379_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-07-15', 1),

(1, N'"LOGO CORNROW"', 5, 'https://smakerclothing.com/upload/sanpham/dsc038528134_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/dsc03856-85371355_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-07-15', 1),

(1, N'"I GOT MONEY IN SAUDI" ', 5, 'https://smakerclothing.com/upload/sanpham/dscf94911775_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/dscf9493-572477_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-07-15', 1),

(1, N'"420 PICNIC" ', 5, 'https://smakerclothing.com/upload/hinhthem/dsc00752-43895257_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/img6677-39456857_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-07-15', 1),

(1, N'Andriu RightHands', 5, 'https://smakerclothing.com/upload/sanpham/dscf74751427_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/dscf7478-54999878_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-07-15', 1),

(5, N'DAMONEYTEAM', 5, 'https://smakerclothing.com/upload/sanpham/dsc086841061_320x320.jpg', 'https://smakerclothing.com/upload/hinhthem/dsc08688-4746230_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-07-15', 1),

(4, N'Ting Ting Hodie', 5, 'https://smakerclothing.com/upload/sanpham/img_62659213_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/img6261-14214957_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-07-15', 1),

(2, N'$MAKER POLO', 5, 'https://smakerclothing.com/upload/sanpham/dscf94250684_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/dscf9433-75782071_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-07-15', 1),

(2, N'$M 4 LIFE', 5, 'https://smakerclothing.com/upload/sanpham/dscf82148230_300x300.jpg', 'https://smakerclothing.com/upload/hinhthem/dscf8221-73047773_300x300.jpg', CAST(RAND()*(2000-800)+800 AS INT), 6, 0, 100, 0, 1, '2023-07-15', 1);

---Gucci
INSERT INTO dbo.Products 
VALUES
(2, 'COTTON POLO WITH INTERLOCKING G', 4, 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1699344933/737656_XJF4V_1043_002_100_0000_Light-Cotton-polo-with-Interlocking-G.jpg', 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1699344931/737656_XJF4V_1043_001_100_0000_Light-Cotton-polo-with-Interlocking-G.jpg', 850, 4, 849, 154, 0, 0, '2024-01-11', 1),
    (5, 'WOOL CARDIGAN WITH GUCCI INTARSIA', 5, 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1700156997/771707_XKDLV_1140_002_100_0000_Light-Wool-cardigan-with-Gucci-intarsia.jpg', 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1700156996/771707_XKDLV_1140_001_100_0000_Light-Wool-cardigan-with-Gucci-intarsia.jpg', 2500, 4, 2400, 66, 1, 0, '2023-09-12', 1),
    (5, 'G CHECK WOOL JACKET', 5, 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1696433545/770378_ZAOV3_1102_002_100_0000_Light-G-check-wool-jacket.jpg', 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1696433545/770378_ZAOV3_1102_001_100_0000_Light-G-check-wool-jacket.jpg', 3500, 4, 3400, 98, 0, 1, '2023-12-04', 1),
    (7, 'G CHECK WOOL PANT', 5, 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1696432526/680704_ZAOV3_1102_002_100_0000_Light-G-check-wool-pant.jpg', 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1696432525/680704_ZAOV3_1102_001_100_0000_Light-G-check-wool-pant.jpg', 1350, 4, 1200, 111, 0, 1, '2023-11-16', 1),
    (6, 'COTTON HOODED SWEATSHIRT WITH PRINT', 5, 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1700156013/768454_XJF3Y_1230_002_100_0000_Light-Cotton-hooded-sweatshirt-with-print.jpg', 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1700156012/768454_XJF3Y_1230_001_100_0000_Light-Cotton-hooded-sweatshirt-with-print.jpg', 1350, 4, 1200, 131, 1, 0, '2023-11-16', 1),
    (6, 'COTTON JERSEY SWEATSHIRT WITH WEB', 5, 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1695394804/645320_XJF63_1148_002_100_0000_Light-Cotton-jersey-sweatshirt-with-Web.jpg', 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1695394803/645320_XJF63_1148_001_100_0000_Light-Cotton-jersey-sweatshirt-with-Web.jpg', 1550, 4, 1400, 131, 0, 0, '2024-01-01', 1),
    (6, 'COTTON JERSEY HOODED SWEATSHIRT', 5, 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1697473049/770839_XJGA2_9314_002_100_0000_Light-Cotton-jersey-hooded-sweatshirt.jpg', 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1697473901/770839_XJGA2_9314_001_100_0000_Light-Cotton-jersey-hooded-sweatshirt.jpg', 1650, 4, 1400, 187, 0, 0, '2024-01-01', 1),
    (1, 'COTTON JERSEY PRINTED T-SHIRT', 5, 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1699378313/771758_XJF7C_9095_002_100_0000_Light-Cotton-jersey-printed-T-shirt.jpg', 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1699378312/771758_XJF7C_9095_001_100_0000_Light-Cotton-jersey-printed-T-shirt.jpg', 1350, 4, 1200, 211, 0, 0, '2024-01-01', 1),
    (2, 'COTTON POLO SHIRT WITH GG EMBROIDERY', 5, 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1695141038/768631_XJF5A_4560_001_100_0000_Light-Cotton-polo-shirt-with-GG-embroidery.jpg', 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1697044593/768631_XJF5A_4560_002_100_0000_Light-Cotton-polo-shirt-with-GG-embroidery.jpg', 1280, 4, 1200, 244, 1, 0, '2023-12-01', 1),
    (1, 'T-SHIRT WITH GUCCI EMBLEMS', 5, 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1699378351/777495_XJGD0_9086_002_100_0000_Light-T-shirt-with-Gucci-emblems.jpg', 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1699378350/777495_XJGD0_9086_001_100_0000_Light-T-shirt-with-Gucci-emblems.jpg', 1180, 4, 1100, 444, 0, 0, '2023-12-06', 1),
    (9, 'T-SHIRT WITH GUCCI EMBLEMS', 5, 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1698774512/778089_J1691_1012_003_100_0000_Light-Rectangular-frame-sunglases.jpg', 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1689786088/778089_J1691_1012_002_100_0000_Light-Rectangular-frame-sunglases.jpg', 380, 4, 350, 512, 1, 0, '2023-12-09', 1),
    (9, 'OVAL FRAME SUNGLASSES', 5, 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1700077828/778325_J0740_2340_003_100_0000_Light-Oval-frame-sunglasses.jpg', 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1689786164/778325_J0740_2340_001_100_0000_Light-Oval-frame-sunglasses.jpg', 370, 4, 350, 552, 0, 0, '2023-04-09', 1),
    (9, 'GUCCI PRINT COTTON BASEBALL HAT', 5, 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1699552054/773135_4HA7F_4200_003_100_0000_Light-Gucci-print-cotton-baseball-hat.jpg', 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1695659492/773135_4HA7F_4200_001_100_0000_Light-Gucci-print-cotton-baseball-hat.jpg', 270, 4, 250, 652, 1, 0, '2023-09-09', 1),
    (9, 'GG POLYESTER BASEBALL HAT', 5, 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1695918665/768392_4HA5M_8660_003_100_0000_Light-GG-polyester-baseball-hat.jpg', 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1697733941/768392_4HA5M_8660_001_100_0000_Light-GG-polyester-baseball-hat.jpg', 470, 4, 450, 122, 0, 0, '2023-12-19', 1),
    (4, 'COTTON SHIRT WITH GUCCI EMBROIDERY', 5, 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1696433516/769558_ZAJP1_4850_002_100_0000_Light-Cotton-shirt-with-Gucci-embroidery.jpg', 'https://media.gucci.com/style/DarkGray_Center_0_0_2400x2400/1696433515/769558_ZAJP1_4850_001_100_0000_Light-Cotton-shirt-with-Gucci-embroidery.jpg', 820, 4, 750, 422, 1, 0, '2023-12-29', 1);

Insert into ProductDetail
  values('XL','Blue',0,N'Kiểu dáng ‎771450 XDCS8 4100 Lựa chọn trang phục nam cho Cruise 2024 lấy cảm hứng từ những năm 1990. Chiếc áo khoác denim có cấu trúc này được làm bằng vải denim màu xanh đậm và được bổ sung thêm các miếng bông in nổi GG.',1),
  ('L','Black',0,N'Kiểu dáng ‎737656 XJF4V 1043
Bộ sưu tập Cruise 2024 dành cho nam giới mang phong cách hiện đại. Quần tây rộng rãi được kết hợp với áo khoác vừa vặn, trong khi quần túi hộp thoải mái mặc với áo khoác ngoài mang lại nét thể thao. Được trình bày bằng áo jersey cotton dày màu đen, chiếc áo polo kiểu dáng đẹp này được xác định bằng sọc Web màu xanh lá cây và đỏ và miếng vá thêu GG cổ điển.',2),
('L','Grey wool',0,N'Phong cách ‎771707 XKDLV 1140
Nhìn từ lựa chọn Trượt tuyết Cruise 2024 Aprés kết hợp chức năng với nét sành điệu. Da độn, lông cừu và các loại vải khác được cập nhật chi tiết logo mới. Một chiếc intarsia nổi bật của Gucci làm nổi bật chiếc áo len len màu xám này, mang phong cách đường phố vào tủ quần áo hàng ngày của bạn.',3),
('L','Grey',0,N'Phong cách ‎770378 ZAOV3 1102
Lựa chọn trang phục nam cho Cruise 2024 lấy cảm hứng từ những năm 1990. Vải Moleskin và các họa tiết di sản lấy cảm hứng từ kho lưu trữ của House làm phong phú thêm những chiếc áo khoác mới, kết hợp với quần denim để tạo nét hiện đại. Chiếc áo khoác vừa vặn này được làm bằng len ca-rô G màu xám và xanh lam, để lộ hình dáng vừa vặn với hai bên ngực.',4),
('M','Grey',0,N'Phong cách ‎770378 ZAOV3 1102
Lựa chọn trang phục nam cho Cruise 2024 lấy cảm hứng từ những năm 1990. Vải Moleskin và các họa tiết di sản lấy cảm hứng từ kho lưu trữ của House làm phong phú thêm những chiếc áo khoác mới, kết hợp với quần denim để tạo nét hiện đại. Chiếc quần vừa vặn này được làm bằng len ca-rô G màu xám và xanh lam, để lộ hình dáng vừa vặn.',5),
('XL','Grey',0,N'Kiểu dáng ‎768454 XJF3Y 1230
Bộ sưu tập Cruise 2024 dành cho nam giới mang phong cách hiện đại. Chiếc áo nỉ có mũ trùm đầu rộng rãi này được chế tác từ vải cotton chải màu xám và nổi bật với họa tiết in nổi Gucci Made in Italy.',6),
('XL','Whash',0,N'Kiểu dáng ‎645320 XJF63 1148
Màu sắc và họa tiết gắn liền với mùa lạnh hơn truyền cảm hứng cho tủ quần áo đô thị mới cho Bộ sưu tập Cruise 2024 của Gucci. Chiếc áo nỉ có mũ trùm đầu này được làm bằng vải jersey cotton nỉ màu xám nhạt đã được giặt sạch và được xác định bằng một sọc Web màu xanh lá cây và đỏ ở giữa.',7),
('XL','White',1,N'Phong cách ‎770839 XJGA2 9314
Màu sắc và họa tiết gắn liền với mùa lạnh hơn truyền cảm hứng cho tủ quần áo đô thị mới cho Bộ sưu tập Cruise 2024 của Gucci. Được chế tác từ áo jersey cotton nỉ màu trắng nhạt, chiếc áo nỉ có mũ này để lộ hình thêu Gucci ở giữa và hình in chữ G lồng vào nhau theo màu của House Web.',8),
('L','White',1,N'Kiểu dáng ‎771758 XJF7C 9095
Chào đón những ngày ấm áp hơn, bộ sưu tập Cruise 2024 dành cho nam mang đến những món đồ thể thao hơn với họa tiết sống động. Web tiếp tục mang đến nét cổ điển, trong khi các chi tiết logo mang đậm nét hiện đại. Chiếc áo phông jersey cotton vừa màu trắng nhạt này được làm nổi bật với họa tiết Gucci và chữ G lồng vào nhau tương phản, tạo nên phong cách táo bạo.',9),
('L','Dark blue',1,N'Kiểu dáng ‎768631 XJF5A 4560
quần tây được kết hợp với áo khoác vừa vặn, trong khi quần túi hộp thoải mái mặc với áo khoác ngoài mang lại nét thể thao. Được chế tác từ vải cotton co giãn màu xanh đậm, chiếc áo polo này có hình thêu GG và cổ áo tương phản.',10),
('L','White and blue fabric',1,N'Phong cách ‎777495 XJGD0 9086
Màu sắc và họa tiết gắn liền với mùa lạnh hơn truyền cảm hứng cho tủ quần áo đô thị mới cho Bộ sưu tập Cruise 2024 của Gucci. Chiếc áo phông ngắn tay này nổi bật với họa tiết biểu tượng Gucci và viền sọc.',11),
('L','Black injection frame',1,N'Phong cách ‎778089 J1691 1012
Những khung hình chữ nhật đẹp mắt tạo điểm nhấn cho bất kỳ bộ trang phục nào và hình bóng điêu khắc của khung phun màu đen này làm tăng thêm sự thú vị về mặt thị giác. Chi tiết Double G bằng kim loại tông vàng tô điểm cho càng kính, hoàn chỉnh với thấu kính màu xám nguyên khối.',12),
('L','Shiny tortoiseshell',0,N'Style ‎778325 J0740 2340
This pair of oval-shaped frame sunglasses is crafter by shiny tortoiseshell acetate. A gold-toned Gucci engraved detail enhances the style, while the solid blue lens adds a contemporary feel.',13),
('L','Ebony and beige GG',0,N'Kiểu dáng ‎768392 4HA5M 8660
Được giới thiệu cho Cruise 2024, đặc điểm chính của polyester tái chế mới là logo GG. Vải nhẹ nhờ khả năng vận hành của sợi và chất lượng cao, giúp kiểu dáng không bị nhăn. Kết quả cuối cùng là các phụ kiện mang tính kỹ thuật nhưng vẫn trang nhã, mang lại tính thực dụng và cảm giác hiện đại. Ở đây, nó xuất hiện với màu gỗ mun và màu be trên chiếc mũ bóng chày này.',15);

---adidas
---adidas
INSERT INTO dbo.Products 
VALUES
(1, N'ÁO THUN STT', 5, 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/238f5e550f744acf861d533951759203_9366/Ao_Thun_SST_Mau_xanh_da_troi_IS2830_21_model.jpg', 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/8fe9b641391b4309b55ac388d5825b7a_9366/Ao_Thun_SST_Mau_xanh_da_troi_IS2830_23_hover_model.jpg', 621, 1, 550, 100, 0, 1, '2023-02-07', 1);

INSERT INTO dbo.Products  
VALUES
(5, N'ÁO TRACK TOP FIREBIRD DENIM CAO CẤP', 5, 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/c723d65fd6e34581aaa8b0cb0cf5d1ce_9366/Ao_Track_Top_Firebird_Denim_Cao_Cap_DJen_IT7461_21_model.jpg', 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/3cfab0ca45a1440ab7f23eefdd27a14e_9366/Ao_Track_Top_Firebird_Denim_Cao_Cap_DJen_IT7461_23_hover_model.jpg', 600, 1, 500, 100, 1, 1, '2023-01-12', 1);

INSERT INTO dbo.Products 
VALUES
(7, N'QUẦN TRACK PANT FIREBIRD DENIM CAO CẤP', 5, 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/062f097402974d2f88a677551b6bc08f_9366/Quan_Track_Pant_Firebird_Denim_Cao_Cap_DJen_IT7483_21_model.jpg', 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/0557881191e54472a0663273e885ae55_9366/Quan_Track_Pant_Firebird_Denim_Cao_Cap_DJen_IT7483_23_hover_model.jpg', 640, 1, 520, 100, 1, 1, '2022-06-25', 1);

INSERT INTO dbo.Products
VALUES
(5, N'ÁO KHOÁC GIẤU LƯNG CHỮ T POWER ', 5, 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/c76dc2b4968a49c4a9c451f9e9870665_9366/Ao_Khoac_Giau_Lung_Chu_T_Power_Hong_IT9199_21_model.jpg', 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/c76dc2b4968a49c4a9c451f9e9870665_9366/Ao_Khoac_Giau_Lung_Chu_T_Power_Hong_IT9199_21_model.jpg', 750, 1, 730, 100, 1, 1, '2023-05-07', 1);

INSERT INTO dbo.Products
VALUES
(8, N'QUẦN SHORT LEGGING TECHFIT', 5, 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/3d29b9a0dfd14dc29ad5e97ad0aa5d1b_9366/Quan_Short_Legging_Techfit_mau_xanh_la_IU1854_21_model.jpg', 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/c5befae8645443a1be73c132f0ea958a_9366/Quan_Short_Legging_Techfit_mau_xanh_la_IU1854_23_hover_model.jpg', 470, 1, 442, 100, 1, 1, '2023-01-22', 1);

INSERT INTO dbo.Products 
VALUES
(4, N'ÁO SƠ MI KHOÁC NGOÀI NGẮN TAY NEUCLASSICS+', 5, 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/bcc24b21b3ee486aa19cb1ad5a0f39c7_9366/Ao_So_Mi_Khoac_Ngoai_Ngan_Tay_Neuclassics_DJen_IM4436_21_model.jpg', 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/39d618d391dd4dee9ab870706ec47198_9366/Ao_So_Mi_Khoac_Ngoai_Ngan_Tay_Neuclassics_DJen_IM4436_23_hover_model.jpg', 887, 1, 800, 100, 1, 1, '2023-06-30', 1);

INSERT INTO dbo.Products  
VALUES
(3, N'ÁO BABY TEE ', 5, 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/b4aa9c97b27f4ac0bb5db10a955224d2_9366/Ao_Baby_Tee_3_Soc_DJo_IP0665_21_model.jpg', 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/a1c73472b8d54741b1cc79777831a4fa_9366/Ao_Baby_Tee_3_Soc_DJo_IP0665_23_hover_model.jpg', 763, 1, 700, 100, 1, 1, '2023-12-27', 1);

INSERT INTO dbo.Products 
VALUES
(2, N'ÁO POLO SỌC COLORBLOCK BÓNG BẦU DỤC', 5, 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/f61da11c6f204993b4b4a79b7e89ca21_9366/Ao_Polo_Soc_Colorblock_Bong_Bau_Duc_trang_IU4353_21_model.jpg', 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/fc7ab824d1bc44bcb0948fb0163773a9_9366/Ao_Polo_Soc_Colorblock_Bong_Bau_Duc_trang_IU4353_23_hover_model.jpg', 75.0, 1, 0, 100, 0, 1, '2023-10-30', 1);

INSERT INTO dbo.Products 
VALUES
(9, N'KHẨUTRANG', 5, 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/697b4fe2e3fb480b9a6daf810151896d_9366/Khau_Trang_trang_HT5741_21_model.jpg', 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/e71bd6b9220740de8bb7af810152222b_9366/Khau_Trang_trang_HT5741_22_model.jpg', 250, 1, 220, 100, 1, 1, '2023-01-10', 1);

INSERT INTO dbo.Products 
VALUES
(8, N'W FI 3S OH PT', 5, 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/9c831942d0404519964a49dc4330a967_9366/W_FI_3S_OH_PT_Xam_IR5740_21_model.jpg', 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/6a6997520d054a3d95626a2004df8e4f_9366/W_FI_3S_OH_PT_Xam_IR5740_23_hover_model.jpg', 321, 1, 300, 100, 1, 1, '2023-06-02', 1);

INSERT INTO dbo.Products 
VALUES
(1, N'ÁO THUN WINNERS', 5, 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/b66c4e22093f475e9910ada501184009_9366/Ao_Thun_Winners_2.0_adidas_Sportswear_trang_GP9639_01_laydown.jpg', 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/4aa2b28a24f542608b16ac84014f893a_9366/Ao_Thun_Winners_2.0_adidas_Sportswear_DJen_GP9632_01_laydown.jpg', 492, 1, 450, 100, 0, 1, '2023-06-27', 1);

INSERT INTO dbo.Products 
VALUES
(9, N'TÚI RÚT THỂ THAO TREFOIL', 5, 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/0479246771c64c6caabaa7f500bb9573_9366/Tui_rut_the_thao_Trefoil_DJen_BK6726_01_standard.jpg', 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/070de06dae0345e0b6aea7f500bb9f15_9366/Tui_rut_the_thao_Trefoil_DJen_BK6726_02_standard_hover.jpg', 231, 1, 210, 100, 1, 1, '2023-01-20', 1);

INSERT INTO dbo.Products 
VALUES
(5, N'ÁO KHOÁC THỂ THAO', 5, 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/46e7a2dbd1f2453c8360ab8f00f60a78_9366/Ao_khoac_the_thao_Primeblue_SST_DJen_GD2374_21_model.jpg', 'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/3ca5cbe9a9d04a3aa83aab8f00f62d98_9366/Ao_khoac_the_thao_Primeblue_SST_DJen_GD2374_23_hover_model.jpg', 124, 1, 112, 100, 0, 1, '2023-06-09', 1);

INSERT INTO dbo.Products 
VALUES
(9,N'TÚI ĐEO HÔNG',5,'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/29025317ca63474aa527af9d011e555a_9366/Tui_DJeo_Hong_DJen_IM1137_01_standard.jpg','https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/8964791b0b4241aaa4b2af9d011e5b23_9366/Tui_DJeo_Hong_DJen_IM1137_02_standard.jpg',555,1,550,100,0,1,'2023-09-21',1);

INSERT INTO dbo.Products  
VALUES
(6,N'ÁO HOODIE GRAPHIC ADIDAS',5,'https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/feb76b359ede4bf3ada9a2617387316a_9366/Ao_Hoodie_Graphic_adidas_Adventure_mau_xanh_la_IL5179_21_model.jpg','https://assets.adidas.com/images/h_840,f_auto,q_auto,fl_lossy,c_fill,g_auto/60aeec4ca1ee414faa67c603aaa28bab_9366/Ao_Hoodie_Graphic_adidas_Adventure_mau_xanh_la_IL5179_23_hover_model.jpg',750,1,740,100,1,1,'2023-01-12',1);

--teelab
INSERT INTO dbo.Products 
VALUES
(9, N'TeeLab Tote Bag', 5, 'https://bizweb.dktcdn.net/100/415/697/products/ac070.png?v=1701860256097', 'https://bizweb.dktcdn.net/100/415/697/products/3-e0bf5575-641b-439c-920b-be9e1bc692db.jpg?v=1701860259243', 550, 3, 460, 100, 0, 1, '2023-06-10', 1);

INSERT INTO dbo.Products 
VALUES
(6, N'TeeLab Hoodie', 5, 'https://bizweb.dktcdn.net/100/415/697/products/hd062.png?v=1701406502053', 'https://bizweb.dktcdn.net/100/415/697/products/te8568-m47bf8gh-1-ll2o-hinh-mat-sau-0.jpg?v=1701406504390', 450, 3, 350, 100, 1, 1, '2023-06-20', 1);

INSERT INTO dbo.Products 
VALUES
(1, N'Áo Thun Teelab Unisex Sporty ', 5, 'https://bizweb.dktcdn.net/100/415/697/products/2.png?v=1703058668573', 'https://bizweb.dktcdn.net/100/415/697/products/2-3c0dd869-2595-4331-a7aa-3748d120e5e8.jpg?v=1703059043290', 700, 3, 655, 100, 1, 1, '2023-06-27', 1);

INSERT INTO dbo.Products 
VALUES
(2, N'Áo Polo Teelab', 5, 'https://bizweb.dktcdn.net/100/415/697/products/ap038.png?v=1701403713977', 'https://bizweb.dktcdn.net/100/415/697/products/nta126-otdh1mb0-1-e1r6-hinh-mat-sau-0.jpg?v=1701403716573', 800, 3, 700, 100, 1, 1, '2023-05-18', 1);

INSERT INTO dbo.Products 
VALUES
(4, N'Áo Sơ Mi Ngắn Tay Teelab ', 5, 'https://bizweb.dktcdn.net/100/415/697/products/384550405-645731404274519-6017578179223377354-n.jpg?v=1701331833920', 'https://bizweb.dktcdn.net/100/415/697/products/1-e70a9157-791a-4692-b2ba-2fd8cdc4e562.jpg?v=1701331833920', 750, 3, 0, 100, 0, 1, '2023-09-21', 1);

INSERT INTO dbo.Products 
VALUES
(3, N'Áo Babytee Local Brand Teelab', 5, 'https://bizweb.dktcdn.net/100/415/697/products/375024395-3129995117309854-9178531828995739374-n.jpg?v=1701400740073', 'https://bizweb.dktcdn.net/100/415/697/products/375022715-177701688679753-2399636707413358621-n.jpg?v=1701400740073', 550, 3, 450, 100, 0, 1, '2023-07-22', 1);

INSERT INTO dbo.Products 
VALUES
(7, N'Quần Short Teelab Local Brand Unisex', 5, 'https://bizweb.dktcdn.net/100/415/697/products/ps042.png?v=1701858929993', 'https://bizweb.dktcdn.net/100/415/697/products/9107e1ae-542c-4386-a162-25beb6062e79.jpg?v=1701858933517', 640, 3, 0, 100, 0, 1, '2022-06-30', 1);

INSERT INTO dbo.Products 
VALUES
(8, N'Quần Jean Teelab Local Brand Unisex', 5, 'https://bizweb.dktcdn.net/100/415/697/products/gp006.png?v=1701859469880', 'https://bizweb.dktcdn.net/100/415/697/products/go9260-guwvnp0b-1-7zj6-hinh-mat-sau-0.jpg?v=1701859472377', 800, 3, 0, 100, 0, 1, '2022-08-02', 1);

INSERT INTO dbo.Products 
VALUES
(8, N'Quần Nỉ Teelab Local Brand Unisex ', 5, 'https://bizweb.dktcdn.net/100/415/697/products/ps063.png?v=1701331530567', 'https://bizweb.dktcdn.net/100/415/697/products/nls-0312.jpg?v=1701331530567', 750, 3, 0, 100, 0, 1, '2022-06-27', 1);

INSERT INTO dbo.Products 
VALUES
(9, N'Ba lô Racer Teelab Logo Shadow', 5, 'https://bizweb.dktcdn.net/100/415/697/products/ac077.png?v=1701860275600', 'https://bizweb.dktcdn.net/100/415/697/products/2-0111559d-edbb-4f69-ae15-d69574212298.jpg?v=1701860275600', 700, 3, 0, 100, 0, 1, '2022-08-25', 1);

INSERT INTO dbo.Products 
VALUES
(5, N'Áo Khoác Teelab', 5, 'https://bizweb.dktcdn.net/100/415/697/products/6.png?v=1704451665220', 'https://bizweb.dktcdn.net/100/415/697/products/5.png?v=1704451665220', 870, 3, 0, 100, 0, 1, '2023-08-25', 1);

INSERT INTO dbo.Products 
VALUES
(4, N'Áo Sơ Mi Ngắn Tay Teelab', 5, 'https://bizweb.dktcdn.net/100/415/697/products/ss049.png?v=1701404400853', 'https://bizweb.dktcdn.net/100/415/697/products/2-56431c6a-8a22-4163-80e5-1cbd27dc19dd.jpg?v=1701404403387', 650, 3, 0, 100, 1, 1, '2023-08-25', 1);

INSERT INTO dbo.Products 
VALUES
(5, N'Áo Khoác Nhung Phối Da Teelab', 5, 'https://bizweb.dktcdn.net/100/415/697/products/1-cab32cf4-2e74-4903-a40c-75f93db18940.jpg?v=1701402843420', 'https://bizweb.dktcdn.net/100/415/697/products/2-a850b5da-cf96-4a03-bacf-7e9d9c6e009b.jpg?v=1701402845497', 810, 3, 0, 100, 0, 1, '2023-08-25', 1);

INSERT INTO dbo.Products 
VALUES
(6, N'Áo Hoodie World Rally', 5, 'https://bizweb.dktcdn.net/100/415/697/products/9-de75d565-a219-424a-b9d9-8f5cf2eaafaf.jpg?v=1704874592470', 'https://bizweb.dktcdn.net/100/415/697/products/11-fb9b37be-18a4-4a5c-bbdd-51ac15319683.jpg?v=1704874592470', 700, 3, 600, 100, 0, 1, '2023-08-25', 1);

INSERT INTO dbo.Products 
VALUES
(1, N'Áo Thun Teelab Blockcore', 5, 'https://bizweb.dktcdn.net/100/415/697/products/21-639678ff-fed2-499f-8c5b-21496539794d.jpg?v=1704706884117', 'https://bizweb.dktcdn.net/100/415/697/products/23-30fffd47-98ff-4b8e-83c1-160d8b9009cd.jpg?v=1704706885653', 550, 3, 500, 100, 0, 1, '2023-08-25', 1);

INSERT INTO dbo.Products 
VALUES
(7, N'Quần lót Boxer Nam', 5, 'https://bizweb.dktcdn.net/100/415/697/products/combo-boxer-2.jpg?v=1704869666713', 'https://bizweb.dktcdn.net/thumb/grande/100/415/697/products/combo-boxer-1-74b40889-141b-4704-93ae-cfeac154b384.jpg?v=1704705751970', 690, 3, 550, 100, 1, 1, '2023-08-25', 1);

INSERT INTO dbo.Products 
VALUES
(5, N'Áo Khoác Gió Teelab', 5, 'https://bizweb.dktcdn.net/100/415/697/products/2-c72361b4-eddf-4f0f-858f-0b5cc3136dbd.jpg?v=1703820511323', 'https://bizweb.dktcdn.net/100/415/697/products/4-9580e122-d38f-4ca6-943f-ed997fd977b1.jpg?v=1703820507480', 740, 3, 650, 100, 0, 1, '2023-08-25', 1);

INSERT INTO dbo.Products 
VALUES
(9, N'Dép Teelab Essentials', 5, 'https://bizweb.dktcdn.net/100/415/697/products/nt6314-l76xx5ar-1-28an-hinh-truc-dien-tren-xuong-0.jpg?v=1684768307833', 'https://bizweb.dktcdn.net/100/415/697/products/nt6314-l76xx5ar-1-28an-hinh-mat-truoc-0.jpg?v=1684824035640', 820, 3, 750, 100, 1, 1, '2023-08-25', 1);

INSERT INTO dbo.Products 
VALUES
(9, N'Mũ lưỡi trai Teelab', 5, 'https://bizweb.dktcdn.net/100/415/697/products/ac022.png?v=1701860080247', 'https://bizweb.dktcdn.net/100/415/697/products/den1.jpg?v=1701860086290', 300, 3, 250, 100, 0, 1, '2023-08-25', 1);

INSERT INTO dbo.Products 
VALUES
(3, N'Áo Thun Baby Tee butterfly', 5, 'https://bizweb.dktcdn.net/100/415/697/products/3-b93260cd-dffa-4213-8f54-1799760d8244.jpg?v=1701330714237', 'https://bizweb.dktcdn.net/100/415/697/files/2-42bfe9f3-ea59-4239-b384-121a418ab3a6.jpg?v=1692782830899', 520, 3, 480, 100, 1, 1, '2023-08-25', 1);

--ADD ACCOUNT STATUS
INSERT INTO AccountStatuses (status_name) VALUES ('Active');
INSERT INTO AccountStatuses (status_name) VALUES ('Inactive');

--ADD ACCOUNTS
INSERT INTO Accounts (email, password, username, role_id, created_at, status_id, phone_number, gender, address, avartar)
VALUES ('marketing@example.com', '123', 'haoquang', 4, '2024-01-15', 1, '0123123123', 'Male', 'Vinh', 'https://hoanghamobile.com/tin-tuc/wp-content/uploads/2023/07/avatar-dep-19.jpg');
INSERT INTO Accounts (email, password, username, role_id, created_at, status_id, phone_number, gender, address, avartar)
VALUES ('marketing1@example.com', '123', 'quangtuan', 4, '2024-01-15', 1, '0123123123', 'Male', 'ninhbinh', 'https://hoanghamobile.com/tin-tuc/wp-content/uploads/2023/07/avatar-dep-19.jpg');
INSERT INTO Accounts (email, password, username, role_id, created_at, status_id, phone_number, gender, address, avartar)
VALUES ('marketing2@example.com', '123', 'honghoangcutevai',4, '2024-01-15', 1, '987654321', 'female', 'vinh', 'https://scontent.fhan20-1.fna.fbcdn.net/v/t39.30808-6/401596121_248784718198428_3659951560417972903_n.jpg?_nc_cat=107&ccb=1-7&_nc_sid=efb6e6&_nc_ohc=eEgRNkqzV1cAX8zDcEO&_nc_ht=scontent.fhan20-1.fna&oh=00_AfAO3FXwMDg0gZ4uw91aEZ5JSV9WAz_JAfRO99z-xEN-CA&oe=65AD735C');
INSERT INTO Accounts (email, password, username, role_id, created_at, status_id, phone_number, gender, address, avartar)
VALUES ('marketing3@example.com', '123', 'tiepvala', 4, '2024-01-15', 1, '123456789', 'male', 'namha', 'https://vapa.vn/wp-content/uploads/2022/12/anh-avatar-cute-005.jpg');
INSERT INTO Accounts (email, password, username, role_id, created_at, status_id, phone_number, gender, address, avartar)
VALUES ('admin@example.com', '123', 'admin', 1, '2024-01-15', 1, '123456789', 'male', 'namha', 'https://vapa.vn/wp-content/uploads/2022/12/anh-avatar-cute-005.jpg');
INSERT INTO Accounts (email, password, username, role_id, created_at, status_id, phone_number, gender, address, avartar)
VALUES ('customer@example.com', '123', 'customer', 5, '2024-01-15', 1, '123456789', 'male', 'namha', 'https://vapa.vn/wp-content/uploads/2022/12/anh-avatar-cute-005.jpg');
INSERT INTO Accounts (email, password, username, role_id, created_at, status_id, phone_number, gender, address, avartar)
VALUES ('sales@example.com', '123', 'Sales', 3, '2024-01-15', 1, '123456789', 'male', 'namha', 'https://vapa.vn/wp-content/uploads/2022/12/anh-avatar-cute-005.jpg');
INSERT INTO Accounts (email, password, username, role_id, created_at, status_id, phone_number, gender, address, avartar)
VALUES ('sales1@example.com', '123', 'Sales 2', 3, '2024-01-16', 1, '123456789', 'male', 'namha', 'https://vapa.vn/wp-content/uploads/2022/12/anh-avatar-cute-005.jpg');
INSERT INTO Accounts (email, password, username, role_id, created_at, status_id, phone_number, gender, address, avartar)
VALUES ('sales2@example.com', '123', 'Sales 3', 3, '2024-01-17', 1, '123456789', 'male', 'namha', 'https://vapa.vn/wp-content/uploads/2022/12/anh-avatar-cute-005.jpg');
INSERT INTO Accounts (email, password, username, role_id, created_at, status_id, phone_number, gender, address, avartar)
VALUES ('inventory@example.com', '123', 'Inventory', 6, '2024-01-15', 1, '123456789', 'male', 'namha', 'https://vapa.vn/wp-content/uploads/2022/12/anh-avatar-cute-005.jpg');
INSERT INTO Accounts (email, password, username, role_id, created_at, status_id, phone_number, gender, address, avartar)
VALUES ('salemanager@example.com', '123', 'Sale Manager', 2, '2024-01-15', 1, '123456789', 'male', 'namha', 'https://vapa.vn/wp-content/uploads/2022/12/anh-avatar-cute-005.jpg');



--ADD BLOG_CATEGORIES
INSERT INTO BlogCategories (bc_name) VALUES (N'Seasons and Occasions');
INSERT INTO BlogCategories (bc_name) VALUES (N'Trends and Couture');
INSERT INTO BlogCategories (bc_name) VALUES (N'Fashion Styles');
INSERT INTO BlogCategories (bc_name) VALUES (N'Accessories and Essentials');

---ADD BLOG
INSERT INTO BlogPosts (title, updated_date, content, bc_id, thumbnail, brief, acid, status_id)
VALUES ('FASHION DESIGNING FOR DIFFERENT OCCASIONS: CASUAL WEAR, FORMAL WEAR, AND MORE', '2024-01-15', 'Types of clothes for different occasions,
Dressing appropriately for the event is essential, as it shows respect and consideration towards the host and other guests. For casual events like a weekend brunch or a picnic, comfortable yet stylish outfits work best. You can opt for flowy dresses, jumpsuits, or rompers in light fabrics such as cotton or linen.

You must dress elegantly for formal events such as weddings, gala dinners, or award ceremonies.

Women can choose from options like floor-length gowns and cocktail dresses with heels, while men should wear suits with ties or bow ties.

Business meetings call for business attire, including formal pantsuits for women, blouses, and pumps, while men should wear tailored trousers paired with button-up shirts and polished shoes.

Beach parties demand comfortable clothes that would let you move around quickly. One could try out shorts paired with crop tops; remember to carry your shades!

To sum up, understanding what clothing is appropriate for different occasions is crucial for fashion design. Doing so will make sure you look good while being respectful of others!

Fashion designing colleges in Delhi,
So, whether you’re passionate about designing casual or formal wear, there’s always an occasion calling for a specific type of outfit. By understanding the different clothes and businesses ideal for each event, you can create designs that cater to your client’s needs while showcasing your creativity.

There are various possibilities accessible in Delhi if you want to study fashion design and are trying to choose the best college to help you achieve your goals.

Indian School of Design and Innovation (ISDI), Pearl Academy, National Institute of Fashion Technology (NIFT), and JD Institute of Fashion Technology are a few of Delhi’s leading institutions for studying fashion design.

Attending one of these institutes will provide you with all the necessary skills and knowledge to excel as a fashion designer. With their expert faculty members, state-of-the-art infrastructure, industry exposure opportunities, and practical learning approach – these institutes ensure that their students become well-equipped professionals who can make their mark in the fashion world.', 1, 'https://www.lisaadelhi.com/wp-content/uploads/2023/05/outfits-for-different-occasions.png', 'Fashion is an ever-evolving industry that never ceases to amaze us with its new trends and styles every season. Fashion designing is creating beautiful garments that showcase one’s unique personality while keeping up with the latest fashion trends.', 1, 1);
INSERT INTO BlogPosts (title, updated_date, content, bc_id, thumbnail, brief, acid, status_id)
VALUES ('Elevate Your Fashion Game with Yes!poho: Style Tips for Every Occasion', '2022-11-25', 'Mix and Match: One of the best things about Yes!poho clothing collection is versatile. Mix and match different pieces to create unique outfits. Pair a flowy Yes!poho skirt with a casual tee for a chic everyday look, or dress it up with a stylish blouse for a night out. Experimenting with combinations can help you discover your personal style.
Layer Up: Layering is a fantastic way to add depth and interest to your outfits. Yes!poho offers various lightweight jackets, cardigans, and scarves that can be layered over your outfits. This not only keeps you cozy in cooler weather but also adds a stylish dimension to your look.
Accessorize Thoughtfully: Accessories can make or break an outfit, and Yes!poho has a selection of accessories that can complement your look perfectly. Consider adding statement earrings, a trendy handbag, or a stylish belt to complete your ensemble. Remember that less is often more when it comes to accessories, so choose wisely.
Prioritize Comfort: Yes!poho clothing is not only fashionable but also comfortable. When selecting your outfits, prioritize comfort so that you can feel confident and at ease throughout the day. Choose fabrics that breathe and allow for movement, especially if you have a busy day ahead.
', 1, 'https://media.licdn.com/dms/image/D4D12AQHOvC3TSwWr5w/article-cover_image-shrink_600_2000/0/1694583667144?e=2147483647&v=beta&t=PxwqDkYIDQrlMZoKmdfSle7sMuKOQzbw8eW9IMXDfXc', 'Yes!poho is committed to sustainability, and you can do your part by choosing their eco-friendly clothing options. Look for pieces made from organic materials like cotton or Tencel. Sustainable fashion not only reduces your environmental footprint but also ensures that your clothing lasts longer and feels comfortable.
', 1, 1);
INSERT INTO BlogPosts (title, updated_date, content, bc_id, thumbnail, brief, acid, status_id)
VALUES ('The Biggest Fashion Trends of the Summer Might Surprise You', '2023-01-11', 'THE SHOES
The most popular shoes offered on my site were a budget version of Valentino’s Tan-Go platform leather pumps: dramatically high-heels with an ankle strap clasp. These received 8% of total clicks.

The second most popular shoes offered on my site were a pair of white-and-grey, big, chunky running sneakers. These received 6% of total clicks. This is particularly interesting, considering less than one percent of looks from Summer 2022 runway collections I analyzed displayed sneakers. It seems like designers predicted this one incorrectly.

The least popular shoe? A pair of white leather boots, with only 3% of clicks. I guess these will have to wait until fall.

THE ACCESSORIES
Crossbody bags were the most popular accessory, receiving 6% of total clicks, compared to I-thought-these-were-trendy basket bags, which received solely 2%.

Gold, chunky jewelry also made the top 10 trends, with 5% of total clicks.

THE MOST POPULAR TREND OF THE SEASON
The most popular item on my site received 17% of total clicks—over twice as many clicks as the second-most popular. It was a simple, white, linen button-down shirt. I’d like to think this piece is very versatile—worn unbuttoned over a swimsuit in the summer, and tucked under a wool crewneck in the colder months.

So, what do you think —did the data get it right?', 2, 'https://images.squarespace-cdn.com/content/v1/5dfd72441e29143abd68e642/1661869991237-18C56FJLT2L1NQQPX6A5/Cover_Trends.jpg?format=500w', 'Earlier this summer, I launched a site allowing visitors to browse and shop the biggest fashion trends of the season. I decided to analyze their behavior — what did people click on? What did people not click on? And, by analyzing these clicks, can I draw a conclusion on what was truly trending this Summer of 2022? Here’s what the data has to say.', 3, 1);
INSERT INTO BlogPosts (title, updated_date, content, bc_id, thumbnail, brief, acid, status_id)
VALUES ('Here are the Biggest Up-and-Coming Trends of the Season', '2022-10-15', 'As a self-proclaimed minimalist, analyzing high-fashion collections offers me the opportunity to identify what staple trends are consistent across runways, regardless of the designer.

For instance, the midi length dress has been alive and well for a couple of seasons now, most recently seen in 71% of McQueen’s, 38% of Prada’s, and ~20% of Chanel’s and Jacquemus’ collections.

This data-backed way of determining fashion’s timeless and consistent trends can offer insight into what pieces you should be investing, and what will live on from this season to the next.

The opposite approach, of course, is to identify trends that make up a statistically less significant portion of a collection—say, no more than 10%. These micro-trends dance around the periphery of large, high-fashion collections, documented solely by data telling us they were, though imperceptibly, there.

So, let’s dive into the top micro-trends of the Spring / Summer 2021 season, as told by my latest software-identified-fashion-trends report, The Little Book of Big Fashion Data:

CASUAL BALENCIAGA
A sub-section of Balenciaga’s ready-to-wear looks this season contained casual-yet-high-fashion trends. Specifically, ~7% of the collection rocked sweatpants, ~9% wore graphic tees, and ~3% showed athletic suits: a matching pair of sweat-jackets and pants.

My favorite micro-trend at Balenciaga this season, however, was the phone holders; smartphone cases attached to lanyards hung around the neck, seen on ~7% of the runway.

BLUE AT BURBERRY
Straying from the brand’s signature plaid and earth-toned, camel-and-beige palette, creative director Riccardo Tisci found inspiration underwater for his latest Burberry collection. So, right on theme, some (~7%, to be exact) of the outfits that walked down the runway were entirely the color blue; monochromatic and “underwater” from head to toe.

COLD CHANEL
An interesting choice for the warmer, spring and summer months, ~6% of Chanel’s runway looks saw models wearing a layered set of not one, but two jackets.

FUTURISTIC FENDI
Created in collaboration with female-founded accessories label, Chaos, tech-charms—like smart watch faces, ear buds, and smart phones—latched onto and hanging from waist belts were seen in ~5% of Fendi’s latest collection.

STATEMENTS AT SAINT-LAURENT
Hidden within an otherwise predominantly classic, monochrome collection, polka-dotted tights made an appearance in ~9% of Saint Laurent’s looks this season.', 2, 'https://images.squarespace-cdn.com/content/v1/5dfd72441e29143abd68e642/1629819562322-6T38ZIZ4D00AB6EQ4XMG/MicroTrendsCover.png?format=750w', 'Software can be used to identify trends that make up a statistically in-significant portion of a collection—say, no more than 10%. These micro-trends dance around the periphery of large, high-fashion collections, documented solely by data telling us they were, though imperceptibly, there.', 3, 1);
INSERT INTO BlogPosts (title, updated_date, content, bc_id, thumbnail, brief, acid, status_id)
VALUES ('Are Luxury Goods Becoming Less...Luxurious?', '2023-08-15', 'Clearly, there are some things consumers will continue to consume proportionally to how economic wealth varies. General apparel is one of these things. Luxury apparel, however, is far more variable: a top-of-mind indulgence during times of prosperity, and the first thing to be dramatically forgotten during economic slumps.

WHAT DOES THIS MEAN?
Interestingly, this contradicts the idea that luxury goods are ‘recession-proof’, as the wealth of society’s highest earners — the 1% — has historically been unaffected during times of mass economic distress.

However, luxury has, over the past thirty years, turned from the 1% to the masses. Take Gucci, Dior, or Saint Laurent for example, who offer vast collections of small, logo-filled leather goods available under ~$1,000; attainable by a much wider income group.

As a result, the luxury industry is far more controlled by the economic variance of, say, the top ~40%. In this case, with close to a majority of Americans buying into the ultra-exclusive, status-bearing world of luxury, is the term ‘luxury’ still applicable?

THE PAST, PRESENT, & FUTURE OF LUXURY
‘Luxury’ fashion used to be driven by glamorous, custom, highly-expensive haute couture. Couture clientele, however, has seen a steep a 99% decline since 1950 — a stark shift in the industry I previously analyzed.

While handmade, personally-tailored couture was hard to scale, mass-produced leather goods, shoes, handbags, and t-shirts are quite easy to.

Luxury goods today, then, are far less exclusive, unattainable, and, as a result, luxurious. Or, the very definition of ‘luxury’ itself has evolved, now representing the status shown-off by affiliating with certain fashion items, instead of referring to their unachievable nature.

Is ‘luxury’ synonymous to logos? To showing off your income bracket via a screen-printed Givenchy t-shirt, or Balenciaga hoodie? To buying into the ‘luxury dream’ with a pair of mass-produced, high-top sneakers bathed in Dior’s name?

According to data, it seems like it.', 3, 'https://images.squarespace-cdn.com/content/v1/5dfd72441e29143abd68e642/1653579029362-FYPHJKRG41RA26ASZOVH/Cover_Luxury-Goods.jpg?format=750w', 'The elasticity of luxury goods is extreme. When the economy booms, luxury goods skyrocket, and vice versa: when the economy slumps, luxury goods plummet. Interestingly, this contradicts the idea that luxury goods are ‘recession-proof’, as the wealth of society’s highest earners — the 1% — has historically been unaffected during times of mass economic distress.', 2, 1);
INSERT INTO BlogPosts (title, updated_date, content, bc_id, thumbnail, brief, acid, status_id)
VALUES ('All the Data you Need to Know about Luxury Price Inflation', '2021-02-11', 'That’s right. Luxury brands including Ralph Lauren, Louis Vuitton, Michael Kors, and Chanel are suddenly adding a 17–20% markup on their products. For an industry that already famously (and non-transparently) inflates their goods by 4x or more, this additional markup can seem unwarranted.

Perhaps this is a way for high-fashion brands to re-establish themselves as luxurious, and fabulously out-of-reach? To demand more, simply because they can?

Well they, in fact, can charge more. Luxury goods have high income elasticity, meaning the more income a consumer has, the more they will purchase luxury goods. (Trust me on this one—I was an economics major my first year of college).

So, as 2021 continues to see household income and spending rise (by ~20% and 4%, respectively), luxury goods might just be in the perfect position to suddenly become more expensive.



WHY IS IT HAPPENING?
It is more probable, though, that brands aren’t raising prices because they can, rather because they have to. 

The detrimental effects of the COVID-19 pandemic have added more stress to an already-stressed international clothing supply chain, leading to labor shortages, high import restrictions, rising jet fuel prices for shipments, and heightened buyer-supplier inequalities.

Plus, considering 98% of Americans’ clothing is produced overseas, any minor changes in a global supply chain system’s costs or efficiencies will result in a monumental impact.

As a result of a tremendously complex (and, dare we call it, “broken”) supply chain, that Chanel boy bag, which was already going to sink you nearly $5,000, will suddenly cost you about one thousand more. 

WHAT NOW?
So, while brands are currently putting this price increase burden on consumers with the hopes that they won’t care, or possibly even notice, long-term solutions to luxury good cost increases must begin with supply chain practices. 

How, you ask? I’ve said it once and I’ll say it again: advancements in machine learning and artificial intelligence technologies are far too robust for luxury to still be suffering from poor demand predictions, inaccurate stock optimizations, and heaps of unsold inventory. 

Though raising prices might work as a temporary band-aid over a decades-old supply chain wound, there is little excuse for brands to continue burdening consumers with costs—or in seeing higher cost inefficiencies in the first place—due to poor technological investments and low process optimizations in the long term.', 3, 'https://images.squarespace-cdn.com/content/v1/5dfd72441e29143abd68e642/1628608997301-NJKJJPBKO8VWRDPEFD5A/PriceInflation_Cover.jpeg?format=750w', 'That’s right. Luxury brands including Ralph Lauren, Louis Vuitton, Michael Kors, and Chanel are suddenly adding a 17–20% markup on their products. For an industry that already famously (and non-transparently) inflates their goods by 4x or more, this additional markup can seem unwarranted.', 2, 1);
INSERT INTO BlogPosts (title, updated_date, content, bc_id, thumbnail, brief, acid, status_id)
VALUES ('7 Must Have Fashion Accessories for Women', '2020-01-09', '1. Classy Handbags
Some people prefer large-sized handbags while others like smaller ones. Different people have different tastes and preferences when it comes to this type of accessory. One thing that we can all agree on, however, is that it adds a lot of glamour to any mode of dressing.

A handbag also comes in handy when you have other accessories and items that you like carrying around. It is recommendable to choose the color wisely so that it isn’t too off as compared to the other shades that you’ll be wearing. You can also carry a bag that matches at least one of your clothes or the rest of your accessories.

The occasion, weather, and activities at hand play a major role in your decision on the type of handbag to carry at that particular time. You can opt for a large bag if you have many items to carry, a clutch bag for a night out or dinner date, a sling bag for a long occasion, to mention but a few.

2. Rings/ Eternity Ring
Every woman has a secret wish of turning heads and calling for attention when she gets into a room or some other space. If you fall in this category, then diamond eternity rings are the way to go. This type of ring from Adinas Jewels is specifically designed to complement your entire structure, with your whole outfit included.

Eternity rings are designed in a stylish way, whereby a band is surrounded by a strip of precious metals or stones such as gold, diamonds, or other gemstones. The ring can consist of one of these or a combination of both. It symbolizes infinite and eternal love from a spouse if it came from them as a gift, and the same deep affection for yourself if you purchased it on your own. 

3. Watches, Bracelets, and Wristbands
Wrist accessories speak boldly of your personality. A watch plays double roles: showing time and standing in as a stylish fashion piece. There are a variety of sizes, models, brands, colors, and materials to choose from. You can get a watch at any price depending on your budget.

Bracelets and wristbands are another set of hand accessories a woman should never lack. Bracelets are created in plain designs or with additional details to suit your fashion taste. The material used to make these items range from metal, and silver, to gold, with attachments such as diamonds and other precious stones.', 4, 'https://thisladyblogs.com/wp-content/uploads/2021/12/flat-lay-with-women-accessories-800x500.jpg', 'Stylish accessories come in handy in spicing up and punctuating the contents of your wardrobe. They can take a simple outfit from basic to gorgeous or elegant. Their magic touch also does the trick in elevating your glamour and polishing your fashion sense. To pull through the day with a feeling of contention, comfort, and confidence, one must know how to pair up and match each accessory to the immediate body and footwear.', 4, 1);
INSERT INTO BlogPosts (title, updated_date, content, bc_id, thumbnail, brief, acid, status_id)
VALUES ('ESSENTIAL FASHION ACCESSORIES EVERY WOMAN SHOULD HAVE IN THEIR WARDROBE', '2023-01-20', 'A Belt
Belts can be used to hold your jeans in place or give shape to an oversized blazer. You can customize your look with a classic black or brown belt. This season’s essential accessory is the leather belt bag. It is a sophisticated upgrade to the bum bag. Belt bags are practical, stylish, and a great way to elevate your street style.

Bangles and Bracelets
Bracelets and bangles are fashion accessories that can be worn around the wrist. A bangle is more rigid than a bracelet, so there is a difference between them. Bangles are only available in a circular form. On the other hand, the bracelet is made of flexible material and can be worn around the wrist just like bangles. Bracelets can be decorated with ornaments.

Bangles are often made from silver or gold. Bangles are a Southeast Asian tradition. They can be made from glass, plastic or good and worn at many events. A bracelet can also be made from gold, silver, or leather. You can use the bracelets as love or friendship bands. There are many ways to wear a bracelet that serves different purposes.

 
Jewellery
Jewellery is a way of finishing a look since it gives a hint of fashion that aims to distinguish women from one another. Women prefer earrings to necklaces. When in doubt, choose a piece of jewellery that is versatile, that is able to be mixed and matched with different outfits. Jewellery can be used in simple and minimalistic outfits to add some flair. For example, simple studs, sophisticated necklaces, and basic rings are perfect to wear for everyday occasions. You won’t be disappointed with a bit of stylish and classy jewellery everywhere you travel.

Shapewear
Shapewear is the newest fashion trend that has transformed the lingerie and underwear fashion industry. Shapewear helps women feel attractive and confident, not just in herself, however also with her attire. They are available in a vast variety of styles made from different fabrics to provide that required support to pull in the stomach and give an attractive silhouette.

Watches
A watch is a small, wrist-worn timepiece. It is designed to keep a constant movement regardless of the movements induced by user activities. A wristwatch is an electronic timepiece that is worn around the wrist. It is attached to the wrist via a watch strap, bracelet, metal band, or any other type of bracelet. On the other hand, a pocket watch is meant to be worn in your pocket and attached to a chain.

Footwear
Because your shoe size is almost constant as you age, a well-fitted, well-made pair of shoes can be considered an investment. Classic, elegant shoes are always in fashion.

Comfortable and trendy shoes, which were once only worn to protect the feet from injury and cold, are now essential parts of any well-dressed ensemble. Some may argue that shoes are the best accessory. You can own many types of shoes, such as boots, sneakers, formal shoes, and running shoes.

.', 4, 'https://weddingvibe.com/wp-content/uploads/2022/02/fashion-1-1024x683.webp', 'An accessory in fashion is an accessory that adds to an individual’s attire. Accessory items are often used to enhance and complement the outfit. Accessories can also express or communicate the individual’s personality. Accessories come in different shapes, sizes, colors, shades, materials, etc. The 20th century saw the first use of accessories in fashion. This was the era when fashion was changing decade by decade.', 4, 1);


insert into ProductDetail
  values('XL','Blue',0,N'Chiếc áo thun adidas này biến mỗi ngày trở nên thật phong cách. Tận hưởng cảm giác mềm mại thoải mái đến từ chất vải hoàn toàn bằng cotton và nâng tầm outfit casual của bạn ngay tắp lự. Với kiểu dáng vừa vặn, chiếc áo này giúp bạn dễ dàng chinh phục ngày mới. Thiết kế tối giản mà đầy tính biểu tượng đảm bảo bạn sẽ muốn mặc đi mặc lại chiếc áo này. Phong cách nguyên bản kết hợp cảm giác thoải mái — một item không thể thiếu mỗi ngày dành cho những hành trình phiêu lưu trong cuộc sống.',17),
  ('L','Black',1,N'Lấy cảm hứng từ các kiểu dáng oversize và chi tiết độc đáo, chiếc áo track top adidas này mang đến nét mới mẻ cho một phong cách đầy tính biểu tượng. Với các túi khóa kéo và cổ tay bo gân cho kiểu dáng casual, thoải mái, áo mang trên mình logo Ba Lá kết nối bạn với di sản adidas. Chất vải denim cotton cao cấp tạo hiệu ứng sần, cùng kiểu dáng suông rộng cho phong cách dễ chịu. Đây là một cách đơn giản để mang đến phong cách thể thao cho cuộc sống hàng ngày của bạn.',18),
  ('XL','Black',1,N'Chiếc quần track pant quen thuộc nay khoác lên mình chất liệu denim classic để tạo nên thiết kế adidas này. Chất liệu denim cao cấp biến hóa thiết kế casual ấy trở thành một biểu tượng hiện đại. Các túi khóa kéo mang hơi hướng phong cách utilitarian. Kiểu dáng suông rộng giúp bạn luôn thoải mái. Logo Ba Lá ở phía trên ống quần bên trái kết nối thiết kế này với cội nguồn thể thao của adidas Originals. Từ thư giãn tại nhà tới hẹn hò brunch, chiếc quần này sẽ mang đến cho bạn phong cách thường ngày.',19),
  ('L','White',0,N'Sau khi chinh phục kỷ lục dead lift mới, hãy thư giãn với chiếc áo khoác adidas này. Cấu trúc lưng chữ T ẩn bên trong giúp lưu thông khí khi bạn khởi động, và có thể tháo layer thành áo không tay. Công nghệ AEROREADY thoát ẩm giúp bạn luôn khô ráo khi nâng tạ, hạ nhiệt hay khoe thành tích tập luyện trên mạng xã hội.Sản phẩm này làm từ tối thiểu 70% chất liệu tái chế. Bằng cách tái sử dụng các chất liệu đã được tạo ra, chúng tôi góp phần giảm lãng phí và giảm phụ thuộc vào các nguồn tài nguyên hữu hạn, cũng như giảm phát thải từ các sản phẩm mà chúng tôi sản xuất.',20),
  ('L','Semi Green Spark',0,N'Squat, nhảy dây hay tập HIIT. Bất kể bạn vận động theo cách nào, chiếc quần short bó adidas này sẽ đồng hành cùng bạn. Kiểu dáng ôm sát như lớp da thứ hai, cho cảm giác nhẹ nhàng và nâng đỡ trong từng động tác gập, duỗi và uốn người. Với công nghệ AEROREADY thấm hút ẩm giúp bạn luôn khô ráo, bạn sẽ luôn tự tin để tập thêm thật nhiều rep.',21),
  ('L','Black',1,N'Mượn cảm hứng từ kho di sản adidas để làm mới một thiết kế kinh điển — chiếc áo sơ mi khoác ngoài ngắn tay adidas Neuclassics+. Dáng áo suông rộng là lựa chọn hoàn hảo cho phong cách casual thường ngày, cùng chất vải satin mang đến nét thanh lịch. Thiết kế cổ bẻ và khuy bấm kết hợp hoàn hảo với các chi tiết thời thượng như logo Ba Lá 3D nổi và 3 Sọc.Làm từ 100% chất liệu tái chế, sản phẩm này đại diện cho một trong số rất nhiều các giải pháp của chúng tôi hướng tới chấm dứt rác thải nhựa.',22),
  ('M','Red',0,N'Chiếc áo thun adidas ôm body này cùng bạn tôn vinh di sản Originals một cách mới mẻ và tràn đầy năng lượng. Áo kết hợp chất vải pha cotton thoải mái với 3 Sọc đặc trưng chạy dọc hai tay áo. Hãy diện chiếc áo này để thể hiện sự kết nối giữa chính bạn với hàng thập kỷ truyền thống thể thao.Chất vải cotton trong sản phẩm này được khai thác thông qua Better Cotton. Better Cotton được khai thác thông qua mô hình chuỗi hành trình sản phẩm có tên gọi là cân bằng khối lượng. Điều này có nghĩa là Better Cotton không thể truy xuất nguồn gốc từ thành phẩm',23),
  ('XL','Black-White',1,N'Phong cách bóng bầu dục mang đến nét mới mẻ cho chiếc áo polo golf này. Công nghệ AEROREADY kiểm soát mồ hôi giúp bạn luôn mát mẻ và khô ráo, đảm bảo cảm giác thoải mái suốt vòng golf.Bằng cách chọn tái chế, chúng tôi có thể tái sử dụng những chất liệu đã được tạo ra, từ đó góp phần giảm lãng phí. Những lựa chọn chất liệu tái tạo sẽ giúp chúng tôi giảm phụ thuộc vào các nguồn tài nguyên hữu hạn. Các sản phẩm sử dụng kết hợp các chất liệu tái chế và tái tạo của chúng tôi có chứa tổng cộng tối thiểu 70% các chất liệu này.',24),
  ('L','White',1,N'Những ngày nắng chính là lúc hoàn hảo để chơi golf. Chiếc khẩu trang adidas này che chắn cho bạn khỏi ánh nắng mặt trời để bạn chơi golf không chút phân tâm. Công nghệ AEROREADY thấm hút ẩm giúp bạn luôn khô ráo suốt vòng golf. Quai dán phía sau cho phép bạn điều chỉnh độ ôm vừa ý.Làm từ một loạt chất liệu tái chế và có chứa tối thiểu 40% thành phần tái chế, sản phẩm này đại diện cho một trong số rất nhiều các giải pháp của chúng tôi hướng tới chấm dứt rác thải nhựa.',25),
  ('L','Green',0,N'Mượn cảm hứng từ kho di sản adidas để làm mới một thiết kế kinh điển — chiếc áo sơ mi khoác ngoài ngắn tay adidas Neuclassics+. Dáng áo suông rộng là lựa chọn hoàn hảo cho phong cách casual thường ngày, cùng chất vải satin mang đến nét thanh lịch. Thiết kế cổ bẻ và khuy bấm kết hợp hoàn hảo với các chi tiết thời thượng như logo Ba Lá 3D nổi và 3 Sọc.Làm từ 100% chất liệu tái chế, sản phẩm này đại diện cho một trong số rất nhiều các giải pháp của chúng tôi hướng tới chấm dứt rác thải nhựa.',26),
  ('L','Grey',0,N'Khi làm điều tốt, bạn cảm thấy dễ chịu. Đó chính là một vòng lặp. Chiếc áo thun adidas này làm từ chất liệu tái chế, là một phần cam kết của chúng tôi hướng tới chấm dứt rác thải nhựa. Thiết kế casual cho cảm giác thoải mái và năng động mỗi ngày, bất kể bạn ở đâu hay làm gì. Dù lựa chọn của bạn là tích cực tập luyện phục hồi hay nghỉ ngơi thư giãn cả ngày, chiếc áo thun này sẽ luôn đồng hành cùng bạn.',27),
  ('L','Black',1,N'Với kiểu dáng mềm mại, không cố định, mẫu túi thể thao này cực kỳ linh hoạt. Ngăn khóa kéo bên cạnh để giữ các vật dụng nhỏ trong tầm tay của bạn. Túi khoe logo Trefoil lớn ở mặt trước.',28),
  ('L','Black',1,N'Chiếc áo khoác thể thao độc nhất vô nhị. Nhìn có vẻ giống trang phục adidas kinh điển trong quá khứ, nhưng chiếc áo khoác thể thao Primeblue SST này tượng trưng cho sự thay đổi. Kiểu dáng biểu tượng làm từ sợi Parley Ocean Plastic, chất liệu tái chế nâng cấp từ rác thải nhựa thu gom trên các khu vực ven biển. Phong cách khẳng định cá tính đồng thời cũng thân thiện với hành tinh. Hãy đứng lên, thể hiện bản thân và chung tay cùng adidas hướng tới chấm dứt rác thải nhựa.',29),
  ('L','Balck',1,N'Khi nói đến phong cách đẳng cấp, đôi khi các item classic chính là tất cả những gì bạn cần. Chiếc túi đeo hông adidas Adicolor này sẽ đưa bạn trở lại thập niên 80 và 90, khi mà chỉ cần diện chiếc túi đeo hông ưa thích là đủ để bạn có được phong cách cực cool. Quai túi tùy chỉnh đảm bảo độ ôm hoàn hảo, cùng ngăn trước trơn láng đảm bảo các vật dụng thiết yếu luôn trong tầm tay. Chất vải nylon bóng bẩy và logo Ba Lá táo bạo tạo điểm nhấn đầy ấn tượng. Hãy đeo chiếc túi này và ngay lập tức đắm mình trong hoài niệm.Làm từ một loạt chất liệu tái chế và có chứa tối thiểu 40% thành phần tái chế, sản phẩm này đại diện cho một trong số rất nhiều các giải pháp của chúng tôi hướng tới chấm dứt rác thải nhựa.',30),
  ('L','Black-Green',1,N'Đi theo tiếng gọi phiêu lưu. Chiếc áo hoodie adidas này phủ họa tiết mô phỏng địa hình gồ ghề, tôn vinh những hành trình phiêu lưu giữa thiên nhiên hùng vĩ. Chất vải thun da cá thoải mái cho cảm giác dễ chịu, cùng logo Ba Lá tinh tế mang đến nét thể thao đặc trưng. Mặc chiếc áo này bên trong áo gile hoặc áo khoác phao và sẵn sàng cho ngày dài khám phá bất chấp nhiệt độ.

Chúng tôi hợp tác với Better Cotton nhằm cải thiện ngành trồng bông trên toàn cầu. Better Cotton góp phần cải thiện quy trình sản xuất bông toàn cầu cho người sản xuất, cho môi trường trồng trọt và cho tương lai của ngành trồng bông.
Better Cotton được khai thác thông qua mô hình chuỗi hành trình sản phẩm có tên gọi là cân bằng khối lượng. Điều này có nghĩa là Better Cotton không thể truy xuất nguồn gốc từ thành phẩm, tuy nhiên người nông dân Better Cotton hưởng lợi từ nhu cầu sử dụng Better Cotton tương ứng với khối lượng chúng tôi "khai thác".',31),

	
   ('XL','Đen/Kem',1,N'Có một ngăn lớn và hai ngăn phụ. Ngăn lớn có lót được làm từ vải poly chống thấm Lấy cảm hứng từ giới trẻ, sáng tạo liên tục, bắt kịp xu hướng và phát triển đa dạng các dòng sản phẩm là cách mà chúng mình hoạt động để tạo nên phong cách sống hằng ngày của bạn. Mục tiêu của TEELAB là cung cấp các sản phẩm thời trang chất lượng cao với giá thành hợp lý',32),
   ('XL','Kem',0,N'Không chỉ là thời trang, TEELAB còn là “phòng thí nghiệm” của tuổi trẻ - nơi nghiên cứu và cho ra đời năng lượng mang tên “Youth”. Chúng mình luôn muốn tạo nên những trải nghiệm vui vẻ, năng động và trẻ trung.
Lấy cảm hứng từ giới trẻ, sáng tạo liên tục, bắt kịp xu hướng và phát triển đa dạng các dòng sản phẩm là cách mà chúng mình hoạt động để tạo nên phong cách sống hằng ngày của bạn. Mục tiêu của TEELAB là cung cấp các sản phẩm thời trang chất lượng cao với giá thành hợp lý.
Chẳng còn thời gian để loay hoay nữa đâu youngers ơi! Hãy nhanh chân bắt lấy những những khoảnh khắc tuyệt vời của tuổi trẻ. TEELAB đã sẵn sàng trải nghiệm cùng bạn!',33),
   ('XL','Blue',0,N'Không chỉ là thời trang, TEELAB còn là “phòng thí nghiệm” của tuổi trẻ - nơi nghiên cứu và cho ra đời năng lượng mang tên “Youth”. Chúng mình luôn muốn tạo nên những trải nghiệm vui vẻ, năng động và trẻ trung.
Lấy cảm hứng từ giới trẻ, sáng tạo liên tục, bắt kịp xu hướng và phát triển đa dạng các dòng sản phẩm là cách mà chúng mình hoạt động để tạo nên phong cách sống hằng ngày của bạn. Mục tiêu của TEELAB là cung cấp các sản phẩm thời trang chất lượng cao với giá thành hợp lý.
Chẳng còn thời gian để loay hoay nữa đâu youngers ơi! Hãy nhanh chân bắt lấy những những khoảnh khắc tuyệt vời của tuổi trẻ. TEELAB đã sẵn sàng trải nghiệm cùng bạn!',34),
   ('L','Black',1,N'Không chỉ là thời trang, TEELAB còn là “phòng thí nghiệm” của tuổi trẻ - nơi nghiên cứu và cho ra đời năng lượng mang tên “Youth”. Chúng mình luôn muốn tạo nên những trải nghiệm vui vẻ, năng động và trẻ trung.
Lấy cảm hứng từ giới trẻ, sáng tạo liên tục, bắt kịp xu hướng và phát triển đa dạng các dòng sản phẩm là cách mà chúng mình hoạt động để tạo nên phong cách sống hằng ngày của bạn. Mục tiêu của TEELAB là cung cấp các sản phẩm thời trang chất lượng cao với giá thành hợp lý.
Chẳng còn thời gian để loay hoay nữa đâu youngers ơi! Hãy nhanh chân bắt lấy những những khoảnh khắc tuyệt vời của tuổi trẻ. TEELAB đã sẵn sàng trải nghiệm cùng bạn!',35),
   ('XL','White',1,N'Không chỉ là thời trang, TEELAB còn là “phòng thí nghiệm” của tuổi trẻ - nơi nghiên cứu và cho ra đời năng lượng mang tên “Youth”. Chúng mình luôn muốn tạo nên những trải nghiệm vui vẻ, năng động và trẻ trung.
Lấy cảm hứng từ giới trẻ, sáng tạo liên tục, bắt kịp xu hướng và phát triển đa dạng các dòng sản phẩm là cách mà chúng mình hoạt động để tạo nên phong cách sống hằng ngày của bạn. Mục tiêu của TEELAB là cung cấp các sản phẩm thời trang chất lượng cao với giá thành hợp lý.
Chẳng còn thời gian để loay hoay nữa đâu youngers ơi! Hãy nhanh chân bắt lấy những những khoảnh khắc tuyệt vời của tuổi trẻ. TEELAB đã sẵn sàng trải nghiệm cùng bạn!',36),
   ('L','White-Black',1,N'Không chỉ là thời trang, TEELAB còn là “phòng thí nghiệm” của tuổi trẻ - nơi nghiên cứu và cho ra đời năng lượng mang tên “Youth”. Chúng mình luôn muốn tạo nên những trải nghiệm vui vẻ, năng động và trẻ trung.
Lấy cảm hứng từ giới trẻ, sáng tạo liên tục, bắt kịp xu hướng và phát triển đa dạng các dòng sản phẩm là cách mà chúng mình hoạt động để tạo nên phong cách sống hằng ngày của bạn. Mục tiêu của TEELAB là cung cấp các sản phẩm thời trang chất lượng cao với giá thành hợp lý.
Chẳng còn thời gian để loay hoay nữa đâu youngers ơi! Hãy nhanh chân bắt lấy những những khoảnh khắc tuyệt vời của tuổi trẻ. TEELAB đã sẵn sàng trải nghiệm cùng bạn!',37),
   ('XL','Black',1,N'Không chỉ là thời trang, TEELAB còn là “phòng thí nghiệm” của tuổi trẻ - nơi nghiên cứu và cho ra đời năng lượng mang tên “Youth”. Chúng mình luôn muốn tạo nên những trải nghiệm vui vẻ, năng động và trẻ trung.
Lấy cảm hứng từ giới trẻ, sáng tạo liên tục, bắt kịp xu hướng và phát triển đa dạng các dòng sản phẩm là cách mà chúng mình hoạt động để tạo nên phong cách sống hằng ngày của bạn. Mục tiêu của TEELAB là cung cấp các sản phẩm thời trang chất lượng cao với giá thành hợp lý.
Chẳng còn thời gian để loay hoay nữa đâu youngers ơi! Hãy nhanh chân bắt lấy những những khoảnh khắc tuyệt vời của tuổi trẻ. TEELAB đã sẵn sàng trải nghiệm cùng bạn!',38),
   ('M','Blue-White',0,N'Không chỉ là thời trang, TEELAB còn là “phòng thí nghiệm” của tuổi trẻ - nơi nghiên cứu và cho ra đời năng lượng mang tên “Youth”. Chúng mình luôn muốn tạo nên những trải nghiệm vui vẻ, năng động và trẻ trung.
Lấy cảm hứng từ giới trẻ, sáng tạo liên tục, bắt kịp xu hướng và phát triển đa dạng các dòng sản phẩm là cách mà chúng mình hoạt động để tạo nên phong cách sống hằng ngày của bạn. Mục tiêu của TEELAB là cung cấp các sản phẩm thời trang chất lượng cao với giá thành hợp lý.
Chẳng còn thời gian để loay hoay nữa đâu youngers ơi! Hãy nhanh chân bắt lấy những những khoảnh khắc tuyệt vời của tuổi trẻ. TEELAB đã sẵn sàng trải nghiệm cùng bạn!',39),
   ('XL','Black',0,N'Không chỉ là thời trang, TEELAB còn là “phòng thí nghiệm” của tuổi trẻ - nơi nghiên cứu và cho ra đời năng lượng mang tên “Youth”. Chúng mình luôn muốn tạo nên những trải nghiệm vui vẻ, năng động và trẻ trung.
Lấy cảm hứng từ giới trẻ, sáng tạo liên tục, bắt kịp xu hướng và phát triển đa dạng các dòng sản phẩm là cách mà chúng mình hoạt động để tạo nên phong cách sống hằng ngày của bạn. Mục tiêu của TEELAB là cung cấp các sản phẩm thời trang chất lượng cao với giá thành hợp lý.
Chẳng còn thời gian để loay hoay nữa đâu youngers ơi! Hãy nhanh chân bắt lấy những những khoảnh khắc tuyệt vời của tuổi trẻ. TEELAB đã sẵn sàng trải nghiệm cùng bạn!',40),
   ('M','Black',1,N'Không chỉ là thời trang, TEELAB còn là “phòng thí nghiệm” của tuổi trẻ - nơi nghiên cứu và cho ra đời năng lượng mang tên “Youth”. Chúng mình luôn muốn tạo nên những trải nghiệm vui vẻ, năng động và trẻ trung.
Lấy cảm hứng từ giới trẻ, sáng tạo liên tục, bắt kịp xu hướng và phát triển đa dạng các dòng sản phẩm là cách mà chúng mình hoạt động để tạo nên phong cách sống hằng ngày của bạn. Mục tiêu của TEELAB là cung cấp các sản phẩm thời trang chất lượng cao với giá thành hợp lý.
Chẳng còn thời gian để loay hoay nữa đâu youngers ơi! Hãy nhanh chân bắt lấy những những khoảnh khắc tuyệt vời của tuổi trẻ. TEELAB đã sẵn sàng trải nghiệm cùng bạn!',41),
   ('XL','Black',1,N'Không chỉ là thời trang, TEELAB còn là “phòng thí nghiệm” của tuổi trẻ - nơi nghiên cứu và cho ra đời năng lượng mang tên “Youth”. Chúng mình luôn muốn tạo nên những trải nghiệm vui vẻ, năng động và trẻ trung.
Lấy cảm hứng từ giới trẻ, sáng tạo liên tục, bắt kịp xu hướng và phát triển đa dạng các dòng sản phẩm là cách mà chúng mình hoạt động để tạo nên phong cách sống hằng ngày của bạn. Mục tiêu của TEELAB là cung cấp các sản phẩm thời trang chất lượng cao với giá thành hợp lý.
Chẳng còn thời gian để loay hoay nữa đâu youngers ơi! Hãy nhanh chân bắt lấy những những khoảnh khắc tuyệt vời của tuổi trẻ. TEELAB đã sẵn sàng trải nghiệm cùng bạn!',42),
   ('X','White-Blue',1,N'Không chỉ là thời trang, TEELAB còn là “phòng thí nghiệm” của tuổi trẻ - nơi nghiên cứu và cho ra đời năng lượng mang tên “Youth”. Chúng mình luôn muốn tạo nên những trải nghiệm vui vẻ, năng động và trẻ trung.
Lấy cảm hứng từ giới trẻ, sáng tạo liên tục, bắt kịp xu hướng và phát triển đa dạng các dòng sản phẩm là cách mà chúng mình hoạt động để tạo nên phong cách sống hằng ngày của bạn. Mục tiêu của TEELAB là cung cấp các sản phẩm thời trang chất lượng cao với giá thành hợp lý.
Chẳng còn thời gian để loay hoay nữa đâu youngers ơi! Hãy nhanh chân bắt lấy những những khoảnh khắc tuyệt vời của tuổi trẻ. TEELAB đã sẵn sàng trải nghiệm cùng bạn!',43),
   ('XL','Black-White',1,N'Không chỉ là thời trang, TEELAB còn là “phòng thí nghiệm” của tuổi trẻ - nơi nghiên cứu và cho ra đời năng lượng mang tên “Youth”. Chúng mình luôn muốn tạo nên những trải nghiệm vui vẻ, năng động và trẻ trung.
Lấy cảm hứng từ giới trẻ, sáng tạo liên tục, bắt kịp xu hướng và phát triển đa dạng các dòng sản phẩm là cách mà chúng mình hoạt động để tạo nên phong cách sống hằng ngày của bạn. Mục tiêu của TEELAB là cung cấp các sản phẩm thời trang chất lượng cao với giá thành hợp lý.
Chẳng còn thời gian để loay hoay nữa đâu youngers ơi! Hãy nhanh chân bắt lấy những những khoảnh khắc tuyệt vời của tuổi trẻ. TEELAB đã sẵn sàng trải nghiệm cùng bạn!',44),
   ('XL','Green-White',0,N'Không chỉ là thời trang, TEELAB còn là “phòng thí nghiệm” của tuổi trẻ - nơi nghiên cứu và cho ra đời năng lượng mang tên “Youth”. Chúng mình luôn muốn tạo nên những trải nghiệm vui vẻ, năng động và trẻ trung.
Lấy cảm hứng từ giới trẻ, sáng tạo liên tục, bắt kịp xu hướng và phát triển đa dạng các dòng sản phẩm là cách mà chúng mình hoạt động để tạo nên phong cách sống hằng ngày của bạn. Mục tiêu của TEELAB là cung cấp các sản phẩm thời trang chất lượng cao với giá thành hợp lý.
Chẳng còn thời gian để loay hoay nữa đâu youngers ơi! Hãy nhanh chân bắt lấy những những khoảnh khắc tuyệt vời của tuổi trẻ. TEELAB đã sẵn sàng trải nghiệm cùng bạn!',45),
   ('L','Black-White',0,N'Không chỉ là thời trang, TEELAB còn là “phòng thí nghiệm” của tuổi trẻ - nơi nghiên cứu và cho ra đời năng lượng mang tên “Youth”. Chúng mình luôn muốn tạo nên những trải nghiệm vui vẻ, năng động và trẻ trung.
Lấy cảm hứng từ giới trẻ, sáng tạo liên tục, bắt kịp xu hướng và phát triển đa dạng các dòng sản phẩm là cách mà chúng mình hoạt động để tạo nên phong cách sống hằng ngày của bạn. Mục tiêu của TEELAB là cung cấp các sản phẩm thời trang chất lượng cao với giá thành hợp lý.
Chẳng còn thời gian để loay hoay nữa đâu youngers ơi! Hãy nhanh chân bắt lấy những những khoảnh khắc tuyệt vời của tuổi trẻ. TEELAB đã sẵn sàng trải nghiệm cùng bạn!',46),
   ('2XL','White/Black/Blue',1,N'Không chỉ là thời trang, TEELAB còn là “phòng thí nghiệm” của tuổi trẻ - nơi nghiên cứu và cho ra đời năng lượng mang tên “Youth”. Chúng mình luôn muốn tạo nên những trải nghiệm vui vẻ, năng động và trẻ trung.
Lấy cảm hứng từ giới trẻ, sáng tạo liên tục, bắt kịp xu hướng và phát triển đa dạng các dòng sản phẩm là cách mà chúng mình hoạt động để tạo nên phong cách sống hằng ngày của bạn. Mục tiêu của TEELAB là cung cấp các sản phẩm thời trang chất lượng cao với giá thành hợp lý.
Chẳng còn thời gian để loay hoay nữa đâu youngers ơi! Hãy nhanh chân bắt lấy những những khoảnh khắc tuyệt vời của tuổi trẻ. TEELAB đã sẵn sàng trải nghiệm cùng bạn!',47),
   ('L','Black',1,N'Không chỉ là thời trang, TEELAB còn là “phòng thí nghiệm” của tuổi trẻ - nơi nghiên cứu và cho ra đời năng lượng mang tên “Youth”. Chúng mình luôn muốn tạo nên những trải nghiệm vui vẻ, năng động và trẻ trung.
Lấy cảm hứng từ giới trẻ, sáng tạo liên tục, bắt kịp xu hướng và phát triển đa dạng các dòng sản phẩm là cách mà chúng mình hoạt động để tạo nên phong cách sống hằng ngày của bạn. Mục tiêu của TEELAB là cung cấp các sản phẩm thời trang chất lượng cao với giá thành hợp lý.
Chẳng còn thời gian để loay hoay nữa đâu youngers ơi! Hãy nhanh chân bắt lấy những những khoảnh khắc tuyệt vời của tuổi trẻ. TEELAB đã sẵn sàng trải nghiệm cùng bạn!',48),
   ('XL','Black',1,N'Không chỉ là thời trang, TEELAB còn là “phòng thí nghiệm” của tuổi trẻ - nơi nghiên cứu và cho ra đời năng lượng mang tên “Youth”. Chúng mình luôn muốn tạo nên những trải nghiệm vui vẻ, năng động và trẻ trung.
Lấy cảm hứng từ giới trẻ, sáng tạo liên tục, bắt kịp xu hướng và phát triển đa dạng các dòng sản phẩm là cách mà chúng mình hoạt động để tạo nên phong cách sống hằng ngày của bạn. Mục tiêu của TEELAB là cung cấp các sản phẩm thời trang chất lượng cao với giá thành hợp lý.
Chẳng còn thời gian để loay hoay nữa đâu youngers ơi! Hãy nhanh chân bắt lấy những những khoảnh khắc tuyệt vời của tuổi trẻ. TEELAB đã sẵn sàng trải nghiệm cùng bạn!',49),
   ('M','Black',1,N'Không chỉ là thời trang, TEELAB còn là “phòng thí nghiệm” của tuổi trẻ - nơi nghiên cứu và cho ra đời năng lượng mang tên “Youth”. Chúng mình luôn muốn tạo nên những trải nghiệm vui vẻ, năng động và trẻ trung.
Lấy cảm hứng từ giới trẻ, sáng tạo liên tục, bắt kịp xu hướng và phát triển đa dạng các dòng sản phẩm là cách mà chúng mình hoạt động để tạo nên phong cách sống hằng ngày của bạn. Mục tiêu của TEELAB là cung cấp các sản phẩm thời trang chất lượng cao với giá thành hợp lý.
Chẳng còn thời gian để loay hoay nữa đâu youngers ơi! Hãy nhanh chân bắt lấy những những khoảnh khắc tuyệt vời của tuổi trẻ. TEELAB đã sẵn sàng trải nghiệm cùng bạn!',50),
   ('XL','White',0,N'Không chỉ là thời trang, TEELAB còn là “phòng thí nghiệm” của tuổi trẻ - nơi nghiên cứu và cho ra đời năng lượng mang tên “Youth”. Chúng mình luôn muốn tạo nên những trải nghiệm vui vẻ, năng động và trẻ trung.
Lấy cảm hứng từ giới trẻ, sáng tạo liên tục, bắt kịp xu hướng và phát triển đa dạng các dòng sản phẩm là cách mà chúng mình hoạt động để tạo nên phong cách sống hằng ngày của bạn. Mục tiêu của TEELAB là cung cấp các sản phẩm thời trang chất lượng cao với giá thành hợp lý.
Chẳng còn thời gian để loay hoay nữa đâu youngers ơi! Hãy nhanh chân bắt lấy những những khoảnh khắc tuyệt vời của tuổi trẻ. TEELAB đã sẵn sàng trải nghiệm cùng bạn!',51);

-- Thêm giá trị 'Active' vào cột 'status' trong bảng [SWP391_Group6].[dbo].[Status]
UPDATE [SWP391_Group6].[dbo].[Status]
SET [status] = 'Active';

-- Thêm giá trị 'Active' vào cột 'status' trong bảng [SWP391_Group6].[dbo].[AccountStatuses]
UPDATE [SWP391_Group6].[dbo].[AccountStatuses]
SET [status] = 'Active';

-- Thêm giá trị 'Active' vào cột 'status' trong bảng [SWP391_Group6].[dbo].[statusOrder]
UPDATE [SWP391_Group6].[dbo].[statusOrder]
SET [statuss] = 'Active';

-- Thêm giá trị 'Active' vào cột 'status' trong bảng [SWP391_Group6].[dbo].[Roles]
UPDATE [SWP391_Group6].[dbo].[Roles]
SET [status] = 'Active';

-- Thêm giá trị 'Active' vào cột 'status' trong bảng [SWP391_Group6].[dbo].[Categories]
UPDATE [SWP391_Group6].[dbo].[Categories]
SET [status] = 'Active';

-- Thêm giá trị 'Active' vào cột 'status' trong bảng [SWP391_Group6].[dbo].[Brand]
UPDATE [SWP391_Group6].[dbo].[Brand]
SET [status] = 'Active';

-- Thêm giá trị 'Active' vào cột 'status' trong bảng [SWP391_Group6].[dbo].[BlogCategories]
UPDATE [SWP391_Group6].[dbo].[BlogCategories]
SET [status] = 'Active';


INSERT INTO Settings ([type], [value], [order], [status])
SELECT '1', role_name, 1, 'Active'
FROM Roles;
INSERT INTO Settings ([type], [value], [order], [status])
SELECT '2', brandname, 2, 'Active'
FROM Brand;
INSERT INTO Settings ([type], [value], [order], [status])
SELECT '3', cname, 3, 'Active'
FROM Categories;
INSERT INTO Settings ([type], [value], [order], [status])
SELECT '4', bc_name, 4, 'Active'
FROM BlogCategories;
INSERT INTO Settings ([type], [value], [order], [status])
SELECT '5', status, 5, 'Active'
FROM statusOrder;
INSERT INTO Settings ([type], [value], [order], [status])
SELECT '6', status_name, 6, 'Active'
FROM AccountStatuses;

--UPDATE DE SAU NAY KHI AI DANG BLOG THI SE LAY DUOC AUTHOR
CREATE TRIGGER tr_UpdateBlogAuthor
ON BlogPosts
AFTER INSERT
AS
BEGIN
    UPDATE BlogPosts
    SET BlogPosts.author = (SELECT Accounts.username 
                            FROM inserted
                            JOIN Accounts ON inserted.acid = Accounts.acid 
                            WHERE Accounts.role_id = 4
                            AND BlogPosts.acid = inserted.acid)
    FROM BlogPosts
    INNER JOIN inserted ON BlogPosts.post_id = inserted.post_id;
END;
CREATE TRIGGER tr_BlogPosts_AfterInsert
ON BlogPosts
AFTER INSERT
AS
BEGIN
    UPDATE b
    SET b.author = a.username
    FROM BlogPosts b
    INNER JOIN inserted i ON b.post_id = i.post_id
    INNER JOIN Accounts a ON b.acid = a.acid;
END;

UPDATE b
SET b.author = a.username
FROM BlogPosts b
INNER JOIN Accounts a ON b.acid = a.acid;


CREATE TRIGGER Settings_UpdateRoles
ON [SWP391_Group6].[dbo].[Settings]
AFTER UPDATE
AS
BEGIN
    IF UPDATE([type]) OR UPDATE([status])
    BEGIN
        UPDATE r
        SET r.role_name = s.[value],
            r.status = s.[status]
        FROM [SWP391_Group6].[dbo].[Roles] r
        INNER JOIN inserted s ON r.role_id = s.id
    END
END

CREATE TRIGGER Settings_UpdateBrand
ON [SWP391_Group6].[dbo].[Settings]
AFTER UPDATE
AS
BEGIN
    IF UPDATE([value]) OR UPDATE([status])
    BEGIN
        UPDATE b
        SET b.brandname = s.[value],
            b.status = s.[status]
        FROM [SWP391_Group6].[dbo].[Brand] b
        INNER JOIN inserted s ON b.brandname = s.[value] AND s.[type] = 2
    END
END

CREATE TRIGGER UpdateProductRate
ON Feedback
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
    -- Cập nhật rate cho các sản phẩm bị ảnh hưởng bởi thay đổi trong bảng Feedback
    UPDATE Products
    SET rate = (
        SELECT AVG(rating)
        FROM Feedback
        WHERE Feedback.pid = Products.pid
    )
    WHERE Products.pid IN (
        SELECT DISTINCT pid
        FROM inserted
        UNION
        SELECT DISTINCT pid
        FROM deleted
    )
END;



